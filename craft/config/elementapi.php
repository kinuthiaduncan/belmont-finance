<?php
namespace Craft;

return [
    'endpoints' => [
        'customerContent.json' => [
            'elementType' => 'GlobalSet',
            'criteria' => ['handle' => 'customerArea'],
            'transformer' => function(GlobalSetModel $global) {
                return [
                    'supportInfo' => $global->customerSupportInfo->getRawContent(),
                    'paymentNotices' => $global->paymentNotices->getRawContent()
                ];
            },
        ],


        'news.json' => [
            'elementType' => 'Entry',
            'criteria' => [
                'section' => 'news',
                'limit'   => 3,
                'search' => 'contextOptions:dealers'
            ],
            'transformer' => function(EntryModel $entry) {
                $categories = [];
                foreach ($entry->dealerCategory as $k => $v){
                    $categories[$k] = $v->title;
                } 
                return [
                    'title' => $entry->title,
                    'slug' => $entry->slug,
                    'date' => $entry->dateCreated,
                    'products' => $categories
                ];
            },
        ],


        'news/<slug:{slug}>.json' => function($slug) {
            return [
                'elementType' => 'Entry',
                'criteria' => ['slug' => $slug],
                'first' => true,
                'transformer' => function(EntryModel $entry) {
                    return [
                        'title' => $entry->title,
                        'content' => $entry->standardContent->getParsedContent()
                    ];
                },
            ];
        },
    ]
];