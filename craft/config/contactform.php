<?php
namespace Craft;
    $toEmail = craft()->request->getPost('toEmail');
    $toEmail = craft()->security->validateData($toEmail);

    return array(
        'toEmail'             => ($toEmail ?: 'marketing@belmontfinancellc.com'),
        'prependSubject'      => 'New message from the website',
        'prependSender'       => 'From ',
        'allowAttachments'    => true,
    );