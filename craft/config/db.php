<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
	'*' => array(
        'tablePrefix' => 'cr4ft',
    ),
	'.test' => array(
		// The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
		'server' => 'localhost',

		// The database username to connect with.
		'user' => 'root',

		// The database password to connect with.
		'password' => '',

		// The name of the database to select.
		'database' => 'admuser_craft'
	),
    
    'belmont.backbonetek.com' => array(
        // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
        'server' => 'localhost',

        // The database username to connect with.
        'user' => 'admin_belmont',

        // The database password to connect with.
        'password' => 'ZQpXWtItTV',

        // The name of the database to select.
        'database' => 'admin_craft_belmont'
    ),

	'belmontfinancellc.com' => array(
		// The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
		'server' => 'localhost',

		// The database username to connect with.
		'user' => 'admuser_craft',

		// The database password to connect with.
		'password' => '0IEv^S1S#nfZ',

		// The name of the database to select.
		'database' => 'admuser_craft'
	)
);
