<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    '*' => array(
        'omitScriptNameInUrls' => true,
    ),

    '.dev' => array(
        'devMode' => true,
        'isSystemOn' => true,
        'environmentVariables' => array(
            'siteUrl' => 'http://belmontfinance.dev:8000/'
        )
    ),

    'belmont.backbonetek.com' => array(
        'cooldownDuration' => 0,
        'isSystemOn' => true,
        'environmentVariables' => array(
            'siteUrl' => 'http://belmont.backbonetek.com/'
        )
    ),

    'belmontfinancellc.com' => array(
        'cooldownDuration' => 0,
        'isSystemOn' => true,
        'environmentVariables' => array(
            'siteUrl' => 'https://belmontfinancellc.com/'
        )
    )
);
