<?php
namespace Craft;

class UpdateCategories_MainController extends BaseController
{
	protected $allowAnonymous = true;
	private $authKey; 
    public function actionAdd()
    {
        $this->requirePostRequest();

        $this->authKey = 'q3p498gADF4gj';

        $auth = craft()->request->getPost('auth');

        if($auth == $this->authKey){
        	$groupId = craft()->request->getPost('group');
        	$title = craft()->request->getPost('title');

        	$category = new CategoryModel();
			$category->groupId = $groupId; // Or whatever ID your new category's group has.
			$category->getContent()->title = $title; // Slug will be automagically created
			craft()->categories->saveCategory($category);
            $this->returnJson('success');
        }else{
            $this->returnJson('unauthorized');
        }
        
    }
}