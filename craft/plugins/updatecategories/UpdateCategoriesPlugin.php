<?php
namespace Craft;

class UpdateCategoriesPlugin extends BasePlugin
{
    function getName()
    {
         return Craft::t('Update Categories');
    }

    function getVersion()
    {
        return '1.0';
    }

    function getDeveloper()
    {
        return 'Rhine Design LLC';
    }

    function getDeveloperUrl()
    {
        return 'http://rhinedesign.biz';
    }
}