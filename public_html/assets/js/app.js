// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs


$(function(){

	$(document).foundation();

	$('.single-item').slick({
	accessibility: false,
	autoplay: true,
	arrows: false,
	fade: true,
	speed: 1600,
	slidesToShow: 1,
	});

	$('form input').floatlabel();
	$('#login-dropdowns').on('mouseenter', '.customer-button', function(){
		$('#login-dropdowns .customer-button').each(function(index){
			if( $(this).hasClass('current') )
			{
				$(this).removeClass('current');
			}
		});

		$(this).addClass('current');
	});

	$('#login-dropdowns').on('click', function(e) {
	    e.stopPropagation();
	});

	$(document).on('click', function (e) {
	 	$('#login-dropdowns .customer-button').removeClass('current');
	});

    $(".accordion").on("click", ".accordion-navigation", function (event) {
        if($(this).hasClass('active')){
            $(".accordion-navigation.active").removeClass('active').find(".content").slideUp("fast");    
        }
        else {
            $(".accordion-navigation.active").removeClass('active').find(".content").slideUp("fast");
            $(this).addClass('active').find(".content").slideToggle("fast");    
        }
    });


    /* sideways tabs script */
    $('.sectiontabs').find('.sectiontabs__links__content > div').hide();
    $('.sectiontabs').find('.sectiontabs__links__content > div:first-child').show();
    $('.sectiontabs').find('.sectiontabs__links li:first-child a').addClass('active');

    $('.sectiontabs').on('click', '.sectiontabs__links a', function(){
        $('.sectiontabs').find('.sectiontabs__links__content > div').hide();
        $('.sectiontabs').find('#' + $(this).attr('data-id')).show();
        $('.sectiontabs').find('.sectiontabs__links a').removeClass('active');
        $(this).addClass('active');
    });

});
