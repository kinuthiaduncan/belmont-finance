## Setting up server for Belmont Finance

server requirements (xampp 1.8.3+ should probably be okay for dev)
specific modules / extensions for when you're ready 
*relatively up to date versions of PHP & Mysql w InnoDB
*Mcrypt PHP Extension
*OpenSSL PHP Extension
*Mbstring PHP Extension
*Reflection Extension
*PCRE Extension
*SPL Extension
*PDO Extension
*PDO MySQL Extension
*Mcrypt Extension
*OpenSSL Extension
*Multibyte String Extension
*cURL
*crypt() with BLOWFISH_CRYPT enabled
*ImageMagick Extension (optional for dev environment, a pain to install)

## GIT
*You'll need git for windows http://git-scm.com/download/win
*Clone this project

## Virtual hosts
*Virtual host going to /mgt/public for the staff and /public_html for the front facing website
*http://www.delanomaloney.com/2013/07/10/how-to-set-up-virtual-hosts-using-xampp/

## Composer (staff admin)
*Installation for windows https://getcomposer.org/doc/00-intro.md#installation-windows
*The installation should add composer to your windows path
*Next you'll have to have composer download all the project requirements by navigating in your command prompt to mgt/ and running 'composer install'. This will download laravel and other project libraries like twig bridge

## Environment config and database
*Right now, we're utilizing a mostly empty database, and you can create the required db structure by running the migrations that we've created in laravel
*You'll have to create an empty database like 'belmont' for example (probably through phpmyadmin). 
*Each server has its own unique environment file that is not committed to the repository for security reasons
*There is an example file called .env.example that you can rename to '.env' and put your db config in there
*All db schema changes will be done through migrations http://laravel.com/docs/5.0/migrations, to get your database current, run 'php artisan migrate' to build all the schemas we've defined already

## Compiling sass with foundation
*install grunt
*bower install in the correct folder
*grunt watch in the correct folder (currently have grunt setup in each folder)

## Craft setup (currently not setup)


## Laravel tips / notes
*Remember to read the docs for Laravel 5.0, some things are different
*Most of our logic code is going in Controllers (in Http/Controllers/) and sometimes in Http/routes.php
	*Any new controllers should extend Base_Controller because it will be easier to create global variables
*Models are stored app/Models
*Templating is done with Twig instead of Laravel's blade - and you'll see the template files in resources/views
	*http://twig.sensiolabs.org/documentation


## core package / changes (should be forked instead? todo?)
- shouldn't have to change anything, able to extend them the right way now.

## Docker notes
*When docker fails to sync the local filepath, run this command in powershell: Set-NetConnectionProfile -InterfaceAlias "vEthernet (DockerNAT)" -NetworkCategory Private
*If docker container fails after a few seconds: log in and start the mysql service