<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvertPhonesString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractapp', function($table)
        {
            $table->string('homePhone', 14)->change();
            $table->string('cellPhone', 14)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('contractapp', function($table)
        {
            $table->integer('homePhone')->change();
            $table->integer('cellPhone')->change();
        });
    }
}
