<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalesslipSignedToContractapp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractapp', function ($table) {
          $table->boolean('sales_agreement_signed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractapp', function ($table) {
          $table->dropColumn('sales_agreement_signed');
        });
    }
}
