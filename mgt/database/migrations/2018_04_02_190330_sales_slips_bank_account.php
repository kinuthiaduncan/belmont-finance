<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesSlipsBankAccount extends Migration
{
    /**down_payment_method
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sales-slips', function ($table)
        {
            $table->boolean('ach_is_savings')->after('down_payment_method')->nullable();
            $table->integer('ach_day_of_month')->after('down_payment_method')->nullable();
            $table->text('ach_account_number')->after('down_payment_method')->nullable();
            $table->text('ach_routing_number', 9)->after('down_payment_method')->nullable();
            $table->text('installation_address')->after('install_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('sales-slips', function ($table)
        {
            $table->dropColumn('ach_is_savings');
            $table->dropColumn('ach_day_of_month');
            $table->dropColumn('ach_account_number');
            $table->dropColumn('ach_routing_number');
            $table->dropColumn('installation_address');
        });
    }
}
