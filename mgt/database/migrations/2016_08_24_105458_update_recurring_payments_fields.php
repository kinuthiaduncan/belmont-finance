<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRecurringPaymentsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->string('card_name', 64);
        $table->dropColumn('payment_source');
        $table->integer('withdrawal_day');
        $table->dropColumn('payment_date');
        $table->date('start_date');
        $table->bigInteger('cc');
        $table->integer('ccv');
        $table->string('belmont_account_number', 32);
        $table->string('card_type', 16);
        $table->string('frequency', 16);
        $table->integer('customer_id');
        $table->date('signature_date');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->dropColumn('card_name');
        $table->text('payment_source');
        $table->dropColumn('withdrawal_day');
        $table->dateTime('payment_date');
        $table->dropColumn('start_date');
        $table->dropColumn('cc');
        $table->dropColumn('ccv');
        $table->dropColumn('belmont_account_number', 32);
        $table->dropColumn('card_type', 16);
        $table->dropColumn('frequency', 16);
        $table->dropColumn('customer_id');
        $table->dropColumn('signature_date');
      });
    }
}
