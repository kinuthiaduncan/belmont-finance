<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalesAgreementLinkToContractapplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractapp', function ($table) {
          $table->string('sales_agreement')->nullable();
          $table->string('description')->nullable();
          $table->boolean('addon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractapp', function ($table) {
          $table->dropColumn('sales_agreement');
          $table->dropColumn('description');
          $table->dropColumn('addon');
        });
    }
}
