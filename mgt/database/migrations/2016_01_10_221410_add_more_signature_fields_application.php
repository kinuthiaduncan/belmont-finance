<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreSignatureFieldsApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('contractapp', function($table)
        {
            $table->date('licExpDate')->nullable();
            $table->date('licIssueDate')->nullable();
            $table->text('revolving_signature')->nullable();
            $table->text('revolving_signature_cobuyer')->nullable();
            $table->text('buyerAutoLoan', 1);

            $table->text('buyerCreditCards', 1)->change();
            $table->text('buyerCheckingAccount', 1)->change();
            $table->text('buyerSavingsAccount', 1)->change();
            $table->text('buyerCreditReferences', 1)->change();
            $table->text('declaredBankruptcy', 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('contractapp', function($table)
        {
            $table->dropColumn('licExpDate');
            $table->dropColumn('licIssueDate');
            $table->dropColumn('revolving_signature');
            $table->dropColumn('revolving_signature_cobuyer');
            $table->dropColumn('buyerAutoLoan');
        });
    }
}
