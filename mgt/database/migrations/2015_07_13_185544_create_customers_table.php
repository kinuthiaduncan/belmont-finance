<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('customers', function($table){
			$table->increments('id');

			$table->string('email');
			$table->string('password',60);

			$table->string('first_name');
			$table->string('last_name');

			$table->string('address');
			$table->string('address_two');

			$table->string('city');
			$table->string('state');
			$table->integer('zip');
			$table->string('country')->nullable();

			$table->string('phone')->nullable();
			$table->string('phone_two')->nullable();

			$table->integer('contract_id')->unsigned();
			
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('customers');
	}

}
