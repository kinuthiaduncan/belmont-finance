<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentsRecurringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payments_recurring', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('contract_id');
        $table->dateTime('payment_date');
        $table->text('payment_source');
        $table->decimal('amount', 6, 2);
        $table->timestamp('created_at');
        $table->timestamp('updated_at');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('payments_recurring');
    }
}
