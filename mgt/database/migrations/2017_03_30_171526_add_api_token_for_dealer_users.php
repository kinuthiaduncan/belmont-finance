<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiTokenForDealerUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dist_users', function ($table) {
            $table->string('api_token', 255)->after('remember_token')->nullable();
            $table->binary('signature')->after('api_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dist_users', function ($table) {
            $table->dropColumn('api_token');
            $table->dropColumn('signature');
        });
    }
}
