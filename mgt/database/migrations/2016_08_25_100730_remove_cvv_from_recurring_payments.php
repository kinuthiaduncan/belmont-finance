<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCvvFromRecurringPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->dropColumn('cvv');
        $table->dropColumn('electronic_signature');
        $table->decimal('final_amount', 6, 2);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->integer('cvv');
        $table->text('electronic_signature');
        $table->dropColumn('final_amount');
      });
    }
}
