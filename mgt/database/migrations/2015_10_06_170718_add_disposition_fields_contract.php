<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDispositionFieldsContract extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('contracts', function($table)
		{
		    $table->string('disposition_status')->nullable();
		    $table->date('disposition_date')->nullable();
		    $table->float('promo_rate')->nullable();
		    $table->float('discount_other')->nullable();
		    $table->float('discount_promo')->nullable();
		    $table->float('fl_doc_fee')->nullable();
		    $table->float('wire_charge')->nullable();
		    $table->float('loan_charge')->nullable();
		    $table->float('transfer_amount')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('contracts', function($table)
		{
			$table->dropColumn('disposition_status');
		    $table->dropColumn('disposition_date');
		    $table->dropColumn('promo_rate');
		    $table->dropColumn('discount_other');
		    $table->dropColumn('discount_promo');
		    $table->dropColumn('fl_doc_fee');
		    $table->dropColumn('wire_charge');
		    $table->dropColumn('loan_charge');
		    $table->dropColumn('transfer_amount');
		});
	}

}
