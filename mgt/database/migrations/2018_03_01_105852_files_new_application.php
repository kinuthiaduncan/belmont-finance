<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FilesNewApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('files', function($table)
        {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('local_path')->nullable();
            $table->integer('file_type')->unsigned();
            $table->timestamps();
        });

        //
        Schema::create('file_types', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        //
        Schema::table('contractapp', function ($table)
        {
            $table->string('identification_type', 32)->nullable()->after('distributor_account');
            $table->string('identification_state', 2)->nullable()->after('identification_type');
            $table->string('co_identification_type', 32)->nullable()->after('distributor_account');
            $table->string('co_identification_state', 2)->nullable()->after('identification_type');
            $table->date('co_licExpDate');

            $table->string('mailing_address', 40)->nullable()->after('zip');
            $table->string('mailing_address_two', 40)->nullable()->after('mailing_address');
            $table->string('mailing_city', 40)->nullable()->after('mailing_address_two');
            $table->string('mailing_zip', 10)->nullable()->after('mailing_city');

            $table->string('co_mailing_address', 40)->nullable()->after('mailing_zip');
            $table->string('co_mailing_address_two', 40)->nullable()->after('co_mailing_address');
            $table->string('co_mailing_city', 40)->nullable()->after('co_mailing_address_two');
            $table->string('co_mailing_zip', 10)->nullable()->after('co_mailing_city');

            $table->string('other_income_source', 32)->nullable()->after('monthlyGrossPay');
            $table->decimal('other_income_amount', 8, 2)->nullable()->after('other_income_source');

            $table->string('co_other_income_source', 32)->nullable()->after('coMonthlyGrossPay');
            $table->decimal('co_other_income_amount', 8, 2)->nullable()->after('co_other_income_source');

            $table->decimal('coRefLandlordMonthlyPayment', 8, 2)->nullable()->after('refLandlordMonthlyPayment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('contractapp', function ($table)
        {
            $table->dropColumn('identification_type');
            $table->dropColumn('identification_state');
            $table->dropColumn('co_identification_type');
            $table->dropColumn('co_identification_state');
            $table->dropColumn('co_licExpDate');
            $table->dropColumn('mailing_address');
            $table->dropColumn('mailing_address_two');
            $table->dropColumn('mailing_city');
            $table->dropColumn('mailing_zip');
            $table->dropColumn('co_mailing_address');
            $table->dropColumn('co_mailing_address_two');
            $table->dropColumn('co_mailing_city');
            $table->dropColumn('co_mailing_zip');
            $table->dropColumn('other_income_source');
            $table->dropColumn('other_income_amount');
            $table->dropColumn('co_other_income_source');
            $table->dropColumn('co_other_income_amount');
            $table->dropColumn('coRefLandlordMonthlyPayment');
        });

        Schema::drop('files');
        Schema::drop('file_types');
    }
}
