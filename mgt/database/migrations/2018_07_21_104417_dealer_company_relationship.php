<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DealerCompanyRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dist_dealer_relations', function($table)
        {
            $table->increments('id');
            $table->integer('dealer_id')->unsigned();
            $table->integer('distributor_id')->unsigned();
            $table->integer('parent_user_id')->unsigned()->nullable();
            $table->boolean('admin')->default(0);
            $table->text('user_status', 32)->nullable();
            $table->timestamps();
        });

                //
        Schema::table('contractapp', function($table)
        {
            $table->integer('dist_id')->unsigned()->nullable()->after('dealerId');
        });

        Schema::table('dist_user_denied_promos', function($table)
        {
            $table->integer('distributor_id')->unsigned()->nullable()->after('user_id');
        });

        $dealers = \App\Models\Dealer::all();

        foreach($dealers as $dealer) {
            DB::table('dist_dealer_relations')->insert(
                [
                    'dealer_id' => $dealer->id,
                    'distributor_id' => $dealer->company_id,
                    'parent_user_id' => $dealer->parent_user_id ? $dealer->parent_user_id : null,
                    'user_status' => $dealer->user_status,
                    'admin' => $dealer->admin
                ]
            );

            // then update each user's deal with a distributor id
        }

        DB::statement('UPDATE contractapp a
            left join dist_users b on a.dealerId = b.id
            left join dist_companies c on b.company_id = c.id
            set a.dist_id = c.id
            where (dealerId IS NOT NULL AND dealerId != 0 AND c.id IS NOT NULL);');

        DB::statement('UPDATE dist_user_denied_promos a
            left join dist_users b on a.user_id = b.id
            left join dist_companies c on b.company_id = c.id
            set a.distributor_id = c.id;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('dist_dealer_relations');

        Schema::table('contractapp', function($table)
        {
            $table->dropColumn('dist_id');
        });

        Schema::table('dist_user_denied_promos', function($table)
        {
            $table->dropColumn('distributor_id');
        });
    }
}
