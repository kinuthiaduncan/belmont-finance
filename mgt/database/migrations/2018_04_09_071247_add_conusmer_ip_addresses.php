<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConusmerIpAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sales-slips', function ($table)
        {
            $table->text('ip_address', 20)->after('ach_is_savings')->nullable();
            $table->text('completion_ip_address', 20)->after('ach_is_savings')->nullable();
        });

        Schema::table('contractapp', function ($table)
        {
            $table->text('ip_address', 20)->after('fca_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        //
        Schema::table('sales-slips', function ($table)
        {
            $table->dropColumn('ip_address');
            $table->dropColumn('completion_ip_address');
        });

        Schema::table('contractapp', function ($table)
        {
            $table->dropColumn('ip_address');
        });
    }
}
