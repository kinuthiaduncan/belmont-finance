<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('dist_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('username')->unique();
			$table->string('password', 60);
			$table->timestamp('created_at');
			$table->boolean('admin');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// drop distributor user table
		Schema::drop('dist_users');
	}

}
