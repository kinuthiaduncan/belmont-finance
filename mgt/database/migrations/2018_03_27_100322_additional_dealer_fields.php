<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalDealerFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dist_companies', function ($table)
        {
            $table->text('address', 64)->after('company_name')->nullable();
            $table->text('address_two', 64)->after('address')->nullable();
            $table->text('city', 64)->after('address_two')->nullable();
            $table->text('state', 2)->after('city')->nullable();
            $table->text('zip', 2)->after('state')->nullable();
            $table->text('phone', 20)->after('zip')->nullable();
            $table->text('contact_email', 64)->after('phone')->nullable();
            $table->boolean('enable_new_online')->nullable();
            $table->dropColumn('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dist_companies', function ($table)
        {
            $table->dropColumn('address');
            $table->dropColumn('address_two');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('zip');
            $table->dropColumn('phone');
            $table->dropColumn('contact_email');
            $table->dropColumn('enable_new_online');
            $table->text('states', 128)->nullable();
        });
    }
}
