<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NachaSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_dl_ach', function($table)
        {
            $table->increments('id');
            $table->date('download_date');
            $table->boolean('lockbox_downloaded')->default(false);
        });

        Schema::table('payments_ach', function($table)
        {
            $table->integer('contract_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments_dl_ach');
        //

         Schema::table('payments_ach', function($table)
        {
            $table->dropColumn('contract_id');
        });
    }
}
