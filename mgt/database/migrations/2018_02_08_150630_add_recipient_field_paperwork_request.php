<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecipientFieldPaperworkRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dealer_paperwork_request', function ($table) {
            $table->string('company_recipient_name', 64)->after('ship_to_given_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealer_paperwork_request', function ($table) {
            $table->dropColumn('company_recipient_name');
        });
    }
}
