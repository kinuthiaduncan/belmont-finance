<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Promos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('promos', function($table)
        {
            $table->increments('id');
            $table->text('program_id', 16)->nullable();
            $table->text('loan_type', 16)->nullable();
            $table->text('name', 32)->nullable();
            $table->text('description', 32)->nullable();
            $table->integer('days_deferred')->unsigned()->nullable();
            $table->decimal('rate_factor', 4, 2)->unsigned()->nullable();
            $table->decimal('interest_rate', 6, 2)->unsigned()->nullable();
            $table->decimal('min_buy_rate', 6, 2)->unsigned()->nullable();
            $table->boolean('is_sac')->nullable();
            $table->boolean('is_equal_payments')->nullable();
            $table->timestamps();
        });

        Schema::table('dist_companies', function ($table) {
            $table->string('program_id', 16)->after('id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promos');

        Schema::table('dist_companies', function ($table) {
            $table->string('program_id');
        });
    }
}
