<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountTypeBooleanToRecurringAch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments_ach_recurring', function (Blueprint $table) {
            $table->boolean('is_savings_account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments_ach_recurring', function (Blueprint $table) {
            $table->dropColumn('is_savings_account');
        });
    }
}
