<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoansTermsToContractapp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractapp', function ($table) {
            $table->decimal('apr', 5, 2)->nullable();
            $table->tinyInteger('loanLength')->nullable();
            $table->tinyInteger('defer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractapp', function ($table) {
            $table->dropColumn('apr');
            $table->dropColumn('loanLength');
            $table->dropColumn('defer');
        });
    }
}
