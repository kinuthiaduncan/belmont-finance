<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FewMoreAppFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('contractapp', function($table)
		{
		    $table->float('amount_financed');
		    $table->boolean('revolving')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('contractapp', function($table)
		{
		    $table->dropColumn('amount_financed');
		    $table->dropColumn('revolving');
		});
	}

}
