<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsComapny extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dist_companies', function($table)
		{
		    $table->timestamp('updated_at');
		    $table->timestamp('created_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dist_companies', function($table)
		{
		    $table->dropColumn('updated_at');
		    $table->dropColumn('created_at');
		});
	}

}
