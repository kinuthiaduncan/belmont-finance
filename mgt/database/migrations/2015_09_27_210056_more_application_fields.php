<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoreApplicationFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contractapp', function($table)
		{
			$table->string('coHomePhone', 14)->nullable();
			$table->string('coCellPhone', 14)->nullable();
			$table->string('coSsn', 9)->nullable();
			$table->string('coDriversOrId', 32)->nullable();
			$table->string('coEmail', 96)->nullable();
			$table->string('refLandlordAddress', 128)->nullable();


			// make sure they can be nullable if necessary
			$table->string('initial', 12)->nullable()->change();

			//address
			$table->string('addressTwo')->nullable()->change();


			// previous info
			$table->string('prevAddress')->nullable()->change();
			$table->string('prevCityStateZip')->nullable()->change();

			// other general info

			$table->integer('homePhone')->length(14)->nullable()->change();
			$table->integer('cellPhone')->length(14)->nullable()->change();
			//employer info
			$table->string('milRank')->nullable()->change();
			$table->string('employerAddress')->nullable()->change();
			$table->string('jobTitle')->nullable()->change();
			$table->float('monthlyGrossPay')->nullable()->change();
			$table->string('employerPhone',14)->nullable()->change();
			$table->string('employerExt',6)->nullable()->change();

			//previous employer info
			$table->string('prevEmployer')->nullable()->change();
			$table->integer('prevYrsEmployed')->nullable()->change();
			$table->string('prevEmployerAddress')->nullable()->change();
			$table->string('prevEmployerCityStateZip')->nullable()->change();
			$table->string('prevJobTitle')->nullable()->change();
			$table->string('prevEmployerPhone',14)->nullable()->change();
			$table->string('prevEmployerExt',6)->nullable()->change();

			//cobuyer information
			$table->string('coFirstName')->nullable()->change();
			$table->string('coLastName')->nullable()->change();
			$table->string('coInitial')->nullable()->change();
			$table->date('coBirthdate')->nullable()->change();
			$table->string('coAddress')->nullable()->change();
			$table->string('coAddressTwo')->nullable()->change();
			$table->string('coCity')->nullable()->change();
			$table->string('coState',2)->nullable()->change();
			$table->string('coZip',9)->nullable()->change();

			//cobuyer employer info
			$table->string('coEmployer')->nullable()->change();
			$table->integer('coYrsEmployed')->nullable()->change();
			$table->string('coMilRank')->nullable()->change();
			$table->string('coEmployerAddress')->nullable()->change();
			$table->string('coEmployerCityStateZip')->nullable()->change();
			$table->string('coJobTitle')->nullable()->change();
			$table->float('coMonthlyGrossPay')->nullable()->change();
			$table->string('coEmployerPhone',14)->nullable()->change();
			$table->string('coEmployerExt',6)->nullable()->change();

			//landlord mortgage holder info
			$table->string('refLandlordName')->nullable()->change();
			$table->float('refPropertyValue')->nullable()->change();
			$table->string('refAccountNumber',30)->nullable()->change();
			$table->string('refLandlordPhone',14)->nullable()->change();
			$table->string('refLandlordAdress')->nullable()->change();
			$table->string('refLandlordCity')->nullable()->change();
			$table->string('refLandlordState',2)->nullable()->change();
			$table->string('refLandlordZip',9)->nullable()->change();
			$table->float('refLandlordMonthlyPayment')->nullable()->change();
			$table->float('refLandlordPresBalance')->nullable()->change();

			//credit reference one
			$table->string('credrefOneName')->nullable()->change();
			$table->string('credrefOneAccount',30)->nullable()->change();
			$table->string('credrefOnePhone',14)->nullable()->change();
			$table->string('credrefOneStreetAdress')->nullable()->change();
			$table->string('credrefOneCity')->nullable()->change();
			$table->string('credrefOneState',2)->nullable()->change();
			$table->string('credrefOneZip',9)->nullable()->change();
			$table->float('credrefOneMonthlyPayment')->nullable()->change();
			$table->float('credrefOneMonthlyPresBalance')->nullable()->change();

			//credit reference Two
			$table->string('credrefTwoName')->nullable()->change();
			$table->string('credrefTwoAccount',30)->nullable()->change();
			$table->string('credrefTwoPhone',14)->nullable()->change();
			$table->string('credrefTwoStreetAdress')->nullable()->change();
			$table->string('credrefTwoCity')->nullable()->change();
			$table->string('credrefTwoState',2)->nullable()->change();
			$table->string('credrefTwoZip',9)->nullable()->change();
			$table->float('credrefTwoMonthlyPayment')->nullable()->change();
			$table->float('credrefTwoMonthlyPresBalance')->nullable()->change();

			//buyer checking account info
			$table->string('buyerCheckingAccountNum',30)->nullable()->change();
			$table->string('buyerCheckingBank')->nullable()->change();
			$table->string('buyerCheckingBankCity')->nullable()->change();

			//buyer saving account info
			$table->string('buyerSavingsAccountNum',30)->nullable()->change();
			$table->string('buyerSavingsBank')->nullable()->change();
			$table->string('buyerSavingsBankCity')->nullable()->change();

			//buyer bankruptcy info
			$table->string('yearofBankruptcy',4)->nullable()->change();
			$table->string('cityStateOfBankruptcy')->nullable()->change();

			//personal reference One
			$table->string('personalRefOneName')->nullable()->change();
			$table->string('personalRefOnePhone',14)->nullable()->change();
			$table->string('personalRefOneRelationship')->nullable()->change();

			//personal reference Two
			$table->string('personalRefTwoName')->nullable()->change();
			$table->string('personalRefTwoPhone',14)->nullable()->change();
			$table->string('personalRefTwoRelationship')->nullable()->change();
			$table->string('coSign')->nullable()->change();

			//legal agreement
			$table->string('initials',12)->change();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('contractapp', function($table)
		{
			$table->dropColumn('coHomePhone');
			$table->dropColumn('coCellPhone');
			$table->dropColumn('coSsn');
			$table->dropColumn('coDriversOrId');
			$table->dropColumn('coEmail');
			$table->dropColumn('refLandlordAddress');
		});
	}

}
