<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRulesPromo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('promos', function($table)
        {
            $table->decimal('ach_buy_rate', 6, 2)->after('min_buy_rate')->default(0);
            $table->boolean('disable_deferred')->after('ach_buy_rate')->default(0);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('promos', function($table)
        {
            $table->dropColumn('ach_buy_rate');
            $table->dropColumn('disable_deferred');
            $table->dropColumn('deleted_at');
        });
    }
}
