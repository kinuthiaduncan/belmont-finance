<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ratings', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->decimal('fee',5,2)->nullable();
			$table->decimal('percent',5,2)->nullable();
			$table->boolean('couponBook');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('ratings');
	}

}
