<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAchpamentsrecurring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payments_ach_recurring', function ($table) {
        $table->string('electronic_signature')->nullable(true)->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payments_ach_recurring', function ($table) {
        $table->string('electronic_signature')->nullable(false)->change();
      });
    }
}
