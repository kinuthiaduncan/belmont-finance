<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesSlipData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        Schema::create('sales-slips', function($table)
        {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->integer('promo_id')->nullable()->unsigned();
            $table->date('sale_date')->nullable();
            $table->date('install_date')->nullable();
            $table->boolean('is_addon')->nullable();
            $table->string('sale_notes')->nullable();
            $table->decimal('shipping_handling', 10, 2)->nullable();
            $table->string('sale_state', 2)->nullable();
            $table->decimal('sales_tax', 10, 2)->nullable();
            $table->decimal('down_payment', 10, 2)->nullable();
            $table->decimal('payment_factor', 10, 2)->nullable();
            $table->decimal('buy_rate', 10, 2)->nullable();
            $table->string('down_payment_method', 32)->nullable();
            $table->timestamps();
        });

        Schema::create('sales-slip-items', function($table)
        {
            $table->increments('id');
            $table->integer('sales_slip_id')->unsigned();
            $table->decimal('price', 10, 2)->nullable();
            $table->integer('qty')->unsigned()->nullable();
            $table->string('sku', 32)->nullable();
            $table->string('description', 128)->nullable();
            $table->boolean('tax_exempt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('sales-slips');
        Schema::drop('sales-slip-items');
    }
}
