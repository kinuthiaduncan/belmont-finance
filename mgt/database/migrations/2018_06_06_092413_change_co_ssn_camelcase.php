<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCoSsnCamelcase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('contractapp', function ($table)
        {
            $table->text('co_ssn', 255)->after('coSsn')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('contractapp', function ($table)
        {
            $table->dropColumn('co_ssn', 255);
        });
    }
}
