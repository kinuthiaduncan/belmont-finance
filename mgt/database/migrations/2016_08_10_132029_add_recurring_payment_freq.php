<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecurringPaymentFreq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments_ach_recurring', function($table)
        {
            $table->string('frequency', 12)->after('withdrawal_day')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payments_ach_recurring', function($table)
        {
            $table->dropColumn('frequency');
        });
    }
}
