<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperworkRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        Schema::create('dealer_paperwork_request', function($table)
        {
            $table->increments('id');
            $table->integer('dealer_id')->unsigned();
            $table->string('dealer_name', 32)->nullable();
            $table->string('dealer_number', 20)->nullable();
            $table->string('phone_number', 15)->nullable();
            $table->string('street_address', 64)->nullable();
            $table->string('city', 32)->nullable();
            $table->string('state', 20)->nullable();
            $table->string('zip_code', 12)->nullable();
            $table->string('language', 32)->nullable();
            $table->string('states_required')->nullable();
            $table->string('installment_paperwork', 64)->nullable();
            $table->string('revolving_paperwork', 64)->nullable();
            $table->boolean('ship_to_given_address')->nullable();
            $table->string('alternate_phone_number', 15)->nullable();
            $table->string('alternate_street_address', 64)->nullable();
            $table->string('alternate_city', 32)->nullable();
            $table->string('alternate_state', 20)->nullable();
            $table->string('alternate_zip_code', 12)->nullable();
            $table->dateTime('sent_at')->nullable();
            $table->integer('sent_by')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('dealer_paperwork_request');
    }
}
