<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalSalesSlipFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sales-slips', function ($table)
        {
            $table->integer('promo_id_4')->after('promo_id')->nullable();
            $table->integer('promo_id_3')->after('promo_id')->nullable();
            $table->integer('promo_id_2')->after('promo_id')->nullable();
            $table->boolean('is_notice_cancellation_completed')->after('is_completed')->nullable();
            $table->boolean('is_completed_signed')->after('is_notice_cancellation_completed')->nullable();
        });

        Schema::table('contractapp', function ($table)
        {
            $table->integer('funding_status')->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sales-slips', function ($table)
        {
            $table->dropColumn('promo_id_4');
            $table->dropColumn('promo_id_3');
            $table->dropColumn('promo_id_2');
            $table->dropColumn('is_notice_cancellation_completed');
            $table->dropColumn('is_completed_signed');
        });

        Schema::table('contractapp', function ($table)
        {
            $table->dropColumn('funding_status');
        });
    }
}
