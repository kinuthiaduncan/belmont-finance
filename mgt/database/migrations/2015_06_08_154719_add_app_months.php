<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAppMonths extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('contractapp', function($table)
		{
			//address
			$table->integer('monthsAtAddress');
			// previous info
			$table->string('prevAddressTwo');
			$table->integer('prevYrsAtAddress');
			$table->integer('prevMonthsAtAddress');
			//employment
			$table->string('selfEmployed');
			$table->integer('monthsEmployed');
			$table->integer('prevMonthsEmployed');
			//cobuyer
			$table->string('coRelation');			
			$table->string('coSelfEmployed');
			$table->integer('coMonthsEmployed');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('contractapp', function($table)
		{
			$table->dropColumn('addressTwo');
			$table->dropColumn('monthsAtAddress');
			// previous info
			$table->dropColumn('prevAddressTwo');
			$table->dropColumn('prevYrsAtAddress');
			$table->dropColumn('prevMonthsAtAddress');
			//employment
			$table->dropColumn('selfEmployed');
			$table->dropColumn('monthsEmployed');
			$table->dropColumn('prevMonthsEmployed');
			//cobuyer
			$table->dropColumn('coRelation');
			$table->dropColumn('coAddressTwo');
			$table->dropColumn('coSelfEmployed');
			$table->dropColumn('coMonthsEmployed');

		});
	}

}
