<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppIsOnline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('contractapp', function ($table)
        {
            $table->boolean('is_online')->after('id')->nullable();
            $table->decimal('buy_rate', 5, 2)->after('apr')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('contractapp', function ($table)
        {
            $table->dropColumn('is_online');
            $table->dropColumn('buy_rate');
        });
    }
}
