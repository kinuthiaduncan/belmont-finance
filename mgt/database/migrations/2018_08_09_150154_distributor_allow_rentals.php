<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DistributorAllowRentals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dist_companies', function($table)
        {
            $table->boolean('app_allow_renters');
        });

        // update current companies based on their product id
        DB::table('dist_companies')
            ->update(['app_allow_renters' => 1]);

        DB::table('dist_companies')
            ->where('product_id', '!=', 26)
            ->orWhere('dist_companies.product_id', '!=', 28)
            ->update(['app_allow_renters' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dist_companies', function($table)
        {
            $table->dropColumn('app_allow_renters');
        });
    }
}
