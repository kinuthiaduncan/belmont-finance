<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractappUserApproved extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('contractapp', function($table)
		{
			$table->integer('dealerId');
			$table->text('lastStatus');
			$table->integer('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('contractapp', function($table)
		{
			$table->dropColumn('dealerId');
			$table->dropColumn('lastStatus');
			$table->dropColumn('status');
		});
	}

}
