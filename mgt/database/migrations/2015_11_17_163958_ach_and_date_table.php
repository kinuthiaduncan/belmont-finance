<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AchAndDateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payments_dl', function($table)
        {
            $table->increments('id');
            $table->date('download_date');
        });

        //
        Schema::create('payments_ach', function($table)
        {
            $table->increments('id');
            $table->string('belmont_account_number');
            $table->boolean('downloaded');
            $table->boolean('recurring');
            $table->boolean('failed');
            $table->string('account_number');
            $table->string('routing_number');
            $table->float('amount');
            $table->string('bank_name');
            $table->string('bank_address');
            $table->integer('customer_id')->nullable();
            $table->string('electronic_signature');
            $table->string('signature_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('payments_dl');
         Schema::drop('payments_ach');
    }
}
