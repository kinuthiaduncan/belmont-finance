<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowRevolvingCredit extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('dist_companies', function($table)
		{
			$table->boolean('allow_revolving');
		});

		Schema::table('contractapp', function($table)
		{
			$table->string('credit_type');
			$table->boolean('buyerCreditCards');
			$table->boolean('buyerCreditReferences');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('dist_companies', function($table)
		{	
			$table->dropColumn('allow_revolving');
		});

		Schema::table('contractapp', function($table)
		{
			// $table->dropColumn('credit_type');
			$table->dropColumn('buyerCreditCards');
			$table->dropColumn('buyerCreditReferences');
		});
	}

}
