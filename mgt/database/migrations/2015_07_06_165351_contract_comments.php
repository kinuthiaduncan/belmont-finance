<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractComments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('comments', function($table)
		{
				$table->increments("id");
				$table->timestamps();
				$table->text("text");
				$table->integer("user_id")->unsigned();
				$table->integer("contract_id")->unsigned();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('comments', function($table)
		{
		});
	}
}
