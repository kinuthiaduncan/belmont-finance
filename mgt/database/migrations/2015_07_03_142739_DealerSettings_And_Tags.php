<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DealerSettingsAndTags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('dist_settings', function($table){
			$table->increments('id');
			$table->integer('option_id')->unsigned();
			$table->integer('company_id')->unsigned();
			$table->string('name');
			$table->string('value')->nullable();
		});

		//
		Schema::create('dist_tags', function($table){
			$table->increments('id');
			$table->integer('company_id')->unsigned();
			$table->string('name',32);
			$table->timestamps();
		});

		Schema::create('dist_tag_relations', function($table){
			$table->increments('id');
			$table->integer('contract_id')->unsigned();
			$table->integer('tag_id')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('dealer_settings');
		Schema::drop('dealer_tags');
		Schema::drop('tag_relations');
	}

}
