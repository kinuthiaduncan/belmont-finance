<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogAddTimestampsAndItemId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('logs', function($table)
		{
			$table->timestamps();
			$table->integer('table_id');
			$table->string('column_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('logs', function($table)
		{
			$table->dropColumn('created_at');
			$table->dropColumn('updated_at');
			$table->dropColumn('table_id');
			$table->dropColumn('column_name');
		});
	}

}
