<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewAccountTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('customer_tokens', function($table)
        {
            $table->increments('id');
            $table->text('token', 32);
            $table->integer('app_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('customers', function($table)
        {
            $table->integer('temp_app_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_tokens');

        Schema::table('customers', function($table)
        {
            $table->dropColumn('temp_app_id');
        });
    }
}
