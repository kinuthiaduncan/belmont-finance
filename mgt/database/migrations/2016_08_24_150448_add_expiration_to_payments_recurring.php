<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpirationToPaymentsRecurring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->tinyInteger('exp_month');
        $table->integer('exp_year');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->dropColumn('exp_month');
        $table->dropColumn('exp_year');
      });
    }
}
