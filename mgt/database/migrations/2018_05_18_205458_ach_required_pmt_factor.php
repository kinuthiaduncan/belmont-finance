<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AchRequiredPmtFactor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dist_products_pmt_factors', function ($table)
        {
            $table->boolean('ach_required')->default(0)->after('min_buy_rate');
        });

        Schema::table('dist_pmt_factors', function ($table)
        {
            $table->boolean('ach_required')->default(0)->after('min_buy_rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dist_products_pmt_factors', function ($table)
        {
            $table->dropColumn('ach_required');
        });

        Schema::table('dist_pmt_factors', function ($table)
        {
            $table->dropColumn('ach_required');
        });
    }
}
