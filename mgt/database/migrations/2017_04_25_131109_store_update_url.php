<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StoreUpdateUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('app_store_orders', function($table)
        {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->string('store_type', 32);
            $table->string('store_order_id','256')->nullable();
            $table->string('store_order_api_url','256')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('app_store_orders');
    }
}
