<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('dist_users', function($table)
		{
				$table->enum('user_status', ['Approved', 'Denied', 'Pending', 'Closed', 'Terminated', 'Hold']);
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::table('dist_users', function($table)
		{
				$table->dropColumn('user_status');
		});
	}
}
