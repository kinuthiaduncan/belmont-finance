<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logs', function($table)
		{
			$table->increments('id');
			$table->string('table_name');
			$table->boolean('staff_changed')->default(false);
			$table->boolean('dealer_changed')->default(false);
			$table->integer('staff_user')->nullable()->unsigned();
			$table->integer('dealer_user')->nullable()->unsigned();
			$table->string('old_value')->nullable();
			$table->string('new_value');
			$table->timestamp('change_time');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('logs');
	}

}
