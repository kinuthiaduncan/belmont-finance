<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FileTypeDefinitions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('file_types')->insert(
            ['name' => 'Credit Agreement'],
            ['name' => 'Sales Slip'],
            ['name' => 'Unsigned Credit Agreement']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('file_types')->whereIn('name', ['Credit Agreement', 'Sales Slip'])->delete();
    }
}
