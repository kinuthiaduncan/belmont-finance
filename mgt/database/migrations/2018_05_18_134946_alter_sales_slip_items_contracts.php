<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSalesSlipItemsContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sales-slips', function ($table)
        {
            $table->decimal('retail_value', 9, 2)->after('amount_financed')->nullable();
        });

        Schema::table('sales-slip-items', function ($table)
        {
            $table->text('attachments')->after('sku')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sales-slips', function ($table)
        {
            $table->dropColumn('retail_value');
        });

        Schema::table('sales-slip-items', function ($table)
        {
            $table->dropColumn('attahments');
        });
    }
}
