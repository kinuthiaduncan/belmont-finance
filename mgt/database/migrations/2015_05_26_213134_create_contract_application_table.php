
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractApplicationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contractapp', function(Blueprint $table)
		{
			// default columns used by laravel's eloquent orm
			$table->increments('id');
			$table->timestamps();

			//name
			$table->string('firstName');
			$table->string('lastName');

			$table->string('initial');

			//address
			$table->string('address');
			$table->string('addressTwo');
			$table->string('city');
			$table->string('state',2);
			$table->string('zip',9);
			$table->integer('yrsAtAddress');
			$table->text('residenceStatus');
			$table->text('residenceType');

			// previous info
			$table->string('prevAddress');
			$table->string('prevCity');
			$table->string('prevState',2);
			$table->string('prevZip',9);

			// other general info
			$table->date('birthdate');
			$table->integer('homePhone')->length(14);
			$table->integer('cellPhone')->length(14);
			$table->string('ssn');
			$table->string('email');
			$table->integer('numDependents');
			$table->string('driversOrId',32);

			//employer info
			$table->string('employer');
			$table->integer('yrsEmployed');
			$table->string('milRank');
			$table->string('employerAddress');
			$table->string('employerCity');
			$table->string('employerState');
			$table->string('employerZip',9);
			$table->string('jobTitle');
			$table->float('monthlyGrossPay');
			$table->string('employerPhone',14);
			$table->string('employerExt',6);

			//previous employer info
			$table->string('prevEmployer');
			$table->integer('prevYrsEmployed');
			$table->string('prevEmployerAddress');
			$table->string('prevEmployerCityStateZip');
			$table->string('prevJobTitle');
			$table->string('prevEmployerPhone',14);
			$table->string('prevEmployerExt',6);

			//cobuyer information
			$table->string('coFirstName');
			$table->string('coLastName');
			$table->string('coInitial');
			$table->date('coBirthdate');
			$table->string('coAddress');
			$table->string('coAddressTwo');
			$table->string('coCity');
			$table->string('coState',2);
			$table->string('coZip',9);
			$table->string('coHomePhone',14);
			$table->string('coCellPhone',6);
			$table->string('coSsn');
			$table->string('coDriversOrId',32);
			$table->string('coEmail');

			//cobuyer employer info
			$table->string('coEmployer');
			$table->integer('coYrsEmployed');
			$table->string('coMilRank');
			$table->string('coEmployerAddress');
			$table->string('coEmployerCity');
			$table->string('coEmployerState');
			$table->string('coEmployerZip',9);
			$table->string('coJobTitle');
			$table->float('coMonthlyGrossPay');
			$table->string('coEmployerPhone',14);
			$table->string('coEmployerExt',6);

			//landlord mortgage holder info
			$table->string('refLandlordName');
			$table->float('refPropertyValue');
			$table->string('refAccountNumber',30);
			$table->string('refLandlordPhone',14);
			$table->string('refLandlordAdress');
			$table->string('refLandlordCity');
			$table->string('refLandlordState',2);
			$table->string('refLandlordZip',9);
			$table->float('refLandlordMonthlyPayment');
			$table->float('refLandlordPresBalance');

			//credit reference one
			$table->string('credrefOneName');
			$table->string('credrefOneAccount',30);
			$table->string('credrefOnePhone',14);
			$table->string('credrefOneStreetAdress');
			$table->string('credrefOneCity');
			$table->string('credrefOneState',2);
			$table->string('credrefOneZip',9);
			$table->float('credrefOneMonthlyPayment');
			$table->float('credrefOneMonthlyPresBalance');

			//credit reference Two
			$table->string('credrefTwoName');
			$table->string('credrefTwoAccount',30);
			$table->string('credrefTwoPhone',14);
			$table->string('credrefTwoStreetAdress');
			$table->string('credrefTwoCity');
			$table->string('credrefTwoState',2);
			$table->string('credrefTwoZip',9);
			$table->float('credrefTwoMonthlyPayment');
			$table->float('credrefTwoMonthlyPresBalance');

			//buyer checking account info
			$table->boolean('buyerCheckingAccount');
			$table->string('buyerCheckingAccountNum',30);
			$table->string('buyerCheckingBank');
			$table->string('buyerCheckingBankCity');

			//buyer saving account info
			$table->boolean('buyerSavingsAccount');
			$table->string('buyerSavingsAccountNum',30);
			$table->string('buyerSavingsBank');
			$table->string('buyerSavingsBankCity');

			//buyer bankruptcy info
			$table->boolean('declaredBankruptcy');
			$table->string('yearofBankruptcy',4);
			$table->string('cityStateOfBankruptcy');

			//personal reference One
			$table->string('personalRefOneName');
			$table->string('personalRefOnePhone',14);
			$table->string('personalRefOneRelationship');

			//personal reference Two
			$table->string('personalRefTwoName');
			$table->string('personalRefTwoPhone',14);
			$table->string('personalRefTwoRelationship');

			//legal agreement
			$table->string('initials',3);
			$table->string('buyerSign');
			$table->date('buyerDate');
			$table->string('coSign');
			$table->date('coDate');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contractapp');
	}

}
