<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PromosOrdinal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('promos', function($table)
        {
            $table->integer('ordinal')->nullable()->after('is_equal_payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('promos', function($table)
        {
            $table->dropColumn('ordinal');
        });
    }
}
