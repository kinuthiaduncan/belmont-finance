<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemoteSignAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('signature_tokens', function($table)
        {
            $table->increments('id');
            $table->text('token', 32);
            $table->integer('dealer_id')->unsigned()->nullable();
            $table->text('file_id', 32)->nullable();
            $table->text('email', 32);
            $table->text('signer_name', 32)->nullable();
            $table->text('ip_address', 32)->nullable();
            $table->text('form_data')->nullable();
            $table->timestamp('signed_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('signature_tokens');
    }
}
