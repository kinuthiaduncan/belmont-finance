<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('contracts', function($table)
		{
		    $table->increments('id');
		    $table->integer('distributorId')->nullable();
		    $table->integer('customerId')->nullable();
		    $table->string('customerName')->nullable();
		    $table->integer('sameAsCashId')->nullable();
		    $table->decimal('amountFinanced',9,2)->nullable();
		    $table->decimal('discount',9,2)->nullable();
		    $table->decimal('acquisitionFee',9,2)->nullable();
		    $table->decimal('couponBookFee',9,2)->nullable();
		    $table->decimal('reserveFee',9,2)->nullable();
		    $table->decimal('sameAsCashPayable',9,2)->nullable();
		    $table->decimal('distributorPayable',9,2)->nullable();
		    $table->integer('accountNumber')->nullable();
		    $table->string('statusText',512)->nullable();
		    $table->tinyInteger('statusId')->nullable();
		    $table->timestamp('applicationDate')->nullable();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('contracts');
	}

}
