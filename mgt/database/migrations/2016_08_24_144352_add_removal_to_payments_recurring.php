<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemovalToPaymentsRecurring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->tinyInteger('requested_removal');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->dropColumn('requested_removal');
      });
    }
}
