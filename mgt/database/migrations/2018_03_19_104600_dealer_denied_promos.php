<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DealerDeniedPromos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dist_user_denied_promos', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('promo_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('dist_company_denied_promos', function($table)
        {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('promo_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('sales-slips', function ($table)
        {
            $table->boolean('is_completed')->after('id')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('dist_user_denied_promos');
        Schema::drop('dist_company_denied_promos');

        Schema::table('sales-slips', function ($table)
        {
            $table->dropColumns('is_completed');
        });
    }
}
