<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('contractapp', function ($table) {
            $table->decimal('tradein', 11, 2)->nullable();
            $table->string('product_model', 256)->nullable();
            $table->string('product_serial', 256)->nullable();
            $table->string('product_attachments', 256)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractapp', function ($table) {
            $table->dropColumn('tradein');
            $table->dropColumn('product_model');
            $table->dropColumn('product_serial');
            $table->dropColumn('product_attachments');
        });
    }
}
