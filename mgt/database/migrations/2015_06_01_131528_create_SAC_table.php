<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSACTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotionals', function($table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('SACMonths')->nullable();
			$table->integer('deferDays')->nullable();
			$table->decimal('percentage',5,2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('promotionals');
	}

}
