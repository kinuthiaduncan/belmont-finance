<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSigneeFlagsRemoteToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('signature_tokens', function($table)
        {
            $table->boolean('needs_primary_sign')->after('form_data')->nullable();
            $table->boolean('needs_co_sign')->after('form_data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('signature_tokens', function($table)
        {
            $table->dropColumn('needs_primary_sign');
            $table->dropColumn('needs_co_sign');
        });
    }
}
