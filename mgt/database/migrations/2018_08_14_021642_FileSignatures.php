<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FileSignatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_signatures', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('file_id')->unsigned();
            $table->timestamp('signed_at')->nullable();
            $table->char('ip_address',15)->nullable();
            $table->char('signee',2);
            $table->timestamps();

            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
            $table->index('file_id');
        });

        // clean up other table
        Schema::table('files', function(Blueprint $table)
        {
            $table->dropColumn('needs_signed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files_signatures');

        Schema::table('files', function(Blueprint $table)
        {
            $table->string('needs_signed')->nullable();
        });
    }

       
}
