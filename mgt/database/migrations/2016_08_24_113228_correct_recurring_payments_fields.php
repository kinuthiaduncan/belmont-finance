<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorrectRecurringPaymentsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments_recurring', function ($table) {
          $table->text('cc')->change();
          $table->renameColumn('ccv', 'cvv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payments_recurring', function ($table) {
        $table->bigInteger('cc')->change();
        $table->renameColumn('cvv', 'ccv');
      });
    }
}
