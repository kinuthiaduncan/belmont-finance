<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomDealerFactors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dist_pmt_factors', function($table)
        {
            $table->increments('id');
            $table->integer('dist_id')->unsigned();
            $table->decimal('pmt_factor', 6, 2)->nullable();
            $table->decimal('min_buy_rate', 6, 2)->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('dist_pmt_factors');
    }
}
