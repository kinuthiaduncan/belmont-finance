<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppFlags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('contractapp_flags', function($table)
        {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->char('flag_key', 10); // char is faster lookup for indexing than varchar, use descriptive text just for readability...
            $table->text('note');
            $table->timestamps();

            // add foreign keys and indexes
            $table->foreign('app_id')->references('id')->on('contractapp');
            $table->index('flag_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('contractapp_flags');
    }
}
