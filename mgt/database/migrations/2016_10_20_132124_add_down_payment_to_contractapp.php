<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDownPaymentToContractapp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractapp', function ($table) {
          $table->decimal('down_payment_amount', 5, 2)->nullable();
          $table->decimal('sales_tax_amount', 5, 2)->nullable();
          $table->decimal('shipping_amount', 5, 2)->nullable();
          $table->string('sale_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractapp', function ($table) {
          $table->dropColumn('down_payment_amount');
          $table->dropColumn('sales_tax_amount');
          $table->dropColumn('shipping_amount');
          $table->dropColumn('sale_description');
        });
    }
}
