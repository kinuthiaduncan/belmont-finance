<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class StateDistributorControls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('settings_state', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('state', 2);
            $table->decimal('max_apr', 5, 2)->nullable();
            $table->timestamps();
        });


        $us_state_abbrevs = array('AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY');
        foreach($us_state_abbrevs as $state)
        {
            DB::table('settings_state')->insert(
                ['state' => $state]
            );
        }

        Schema::table('dist_settings', function ($table) {
            $table->string('filter_one',32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('settings_state');
        Schema::table('dist_settings', function ($table) {
            $table->dropColumn('filter_one');
        });
    }
}
