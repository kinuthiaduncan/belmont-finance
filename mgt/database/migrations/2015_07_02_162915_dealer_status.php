<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DealerStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('dist_companies', function($table)
		{
			$table->enum('dealer_status', ['Approved', 'Denied', 'Pending', 'Closed', 'Terminated', 'Hold']);
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::table('dist_companies', function($table)
		{
			$table->dropColumn('dealer_status');
		});
	}
}
