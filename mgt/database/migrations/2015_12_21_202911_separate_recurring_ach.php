<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeparateRecurringAch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         //
        Schema::create('payments_ach_recurring', function($table)
        {
            $table->increments('id');
            $table->string('belmont_account_number', 32)->nullable();
            $table->string('account_number',512);
            $table->string('routing_number', 9);
            $table->float('amount', 6, 2);
            $table->string('bank_name', 64);
            $table->string('bank_address');
            $table->integer('customer_id')->unsigned();
            $table->integer('withdrawal_day')->unsigned();
            $table->string('electronic_signature');
            $table->date('signature_date');
            $table->integer('contract_id')->unsigned()->nullable();
            $table->integer('application_id')->unsigned()->nullable();
            $table->boolean('processed')->default(false);
            $table->boolean('requested_removal')->default(false);
            $table->timestamps();
        });

         Schema::table('payments_ach', function($table){
            $table->dropColumn('recurring');
            $table->dropColumn('failed');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('payments_ach_recurring');

         Schema::table('payments_ach', function($table){
            $table->boolean('recurring');
            $table->boolean('failed');
         });
    }
}
