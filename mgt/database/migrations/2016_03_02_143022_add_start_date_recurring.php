<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartDateRecurring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //
        Schema::table('payments_ach_recurring', function($table)
        {
            $table->date('start_date');
        });

        Schema::table('payments_ach', function($table)
        {
            $table->date('payment_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payments_ach_recurring', function($table)
        {
            $table->dropColumn('start_date');
        });

        Schema::table('payments_ach', function($table)
        {
            $table->dropColumn('payment_date');
        });
    }
}
