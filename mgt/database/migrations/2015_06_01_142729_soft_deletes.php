<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftDeletes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('promotionals', function($table)
		{
		    $table->softDeletes();
		});

		Schema::table('ratings', function($table)
		{
		    $table->softDeletes();
		});

		Schema::table('dist_companies', function($table)
		{
		    $table->softDeletes();
		});

		Schema::table('dist_users', function($table)
		{
		    $table->softDeletes();
		});

		Schema::table('contracts', function($table)
		{
		    $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('promotionals', function($table)
		{
		    $table->dropColumn('deleted_at');
		});

		Schema::table('ratings', function($table)
		{
		    $table->dropColumn('deleted_at');
		});

		Schema::table('dist_companies', function($table)
		{
		    $table->dropColumn('deleted_at');
		});

		Schema::table('dist_users', function($table)
		{
		    $table->dropColumn('deleted_at');
		});

		Schema::table('contracts', function($table)
		{
		    $table->dropColumn('deleted_at');
		});
	}

}
