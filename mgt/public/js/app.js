$(document).foundation();

// this is a function for the confirm popup - object.confirm(message, function to be run on success, and the object itself?)
jQuery.fn.extend({
  confirm: function(msg, successOrGet, _this){
  	var confirmModal = $('#confirmModal');
  	var $_this = $(_this);
  	var message = msg.length ? msg: 'Are you sure you want to do this?';
  	confirmModal.find('#modalTitle').text(message);
  	confirmModal.foundation('reveal', 'open');
  	window.confirmModal._this = _this;
  	if(successOrGet === true){
  		confirmModal.data('redirect', true);
  	}else{
  		window.confirmModal.success = successOrGet;
  	}
  }
});

window.spinner = '<div class="button-spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';



$(function(){
	var _success = $('#success');
	//xcsrf token
	$.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

    $('body').on('click', '.ajax-link', function(e){
    	e.preventDefault();
    	$(this).addClass('current active')
    	var href = this.getAttribute('href');
    	var container = $(this).data('ajc');
    	if(container){
    		$('#' + container).load(href + ' #' + container);
    	}else{
	    	$('#main-ajax').load(href + ' #main-ajax > *', function( response, status, xhr ){
	    		var $response = $(response);
	    		var topBar = $response.find('#top-bar').html();
	    		$('#top-bar').html(topBar);

	    		// update other stuff
	    		// document.title = response.match(/<title>(.*?)<\/title>/)[1];
	    		document.title = response.match(/<title[^>]*>([^<]+)<\/title>/)[1];
	    		window.history.pushState({"html":response.html,"pageTitle":response.pageTitle},"", href);
	    		$(document).foundation('dropdown', 'reflow');
	    	});
	    }

    });

    window.onpopstate = function() {
    	// location.reload();
    };


	//
	$('#yesconfirm').on('click', function(e){

		var confirmModal = $('#confirmModal');
		if( !confirmModal.data('redirect') ){
			e.preventDefault();
		}
		var storedSuccess = window[window.confirmModal.success];
		storedSuccess(window.confirmModal._this);
		confirmModal.foundation().foundation('reveal', 'close');
		updatePageAjax();
	});

	$('#noconfirm').on('click', function(){
		var confirmModal = $('#confirmModal');
		confirmModal.foundation().foundation('reveal', 'close');
	});


	$(document).ajaxError(function (e, xhr, settings) {
        if(xhr.status == 401) {
           location.reload();
        }
    });

	// ajax form submissions
	$('body').on('submit', '.ajax-form', function(e){
		e.preventDefault();
		var _this = $(this);
		var button = _this.find('button');
		var origText = button.text();
		_this.find('button').text('Please Wait...').prop('disabled', true);
		$('#loading').fadeIn();

		$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			dataType: 'json',
			data: $(this).serialize(),
			success: function(){
				_this.find('button').prop('disabled', false);

				//remove class marking fields as changed if necessary
				_this.find('.changed').each(function(){
					$(this).removeClass('changed');
				});

				// close modal if in modal
				_this.closest('.reveal-modal').foundation('reveal', 'close');
				button.text('Success!');
				setTimeout(function(){
					button.text(origText);
				}, 1000);

				$('#loading').hide();

                // allows a custom handler function after form submit
                if(window[window.onAjaxComplete]){
                    console.log( window['onAjaxComplete'] );
                    window[window.onAjaxComplete]();
                }

				updatePageAjax();


				_success.fadeIn();

				$('body').find('input:visible, textarea:visible, select:visible').first().focus();
				setTimeout(function(){
					_success.fadeOut();
				},1000);
			},
			error: function(data, textStatus, errorThrown){
				if(data.status == "498") {
					$('#loading').find('.cs-loader-inner').html('<h2>Your login or form timed out - redirecting you in a few seconds...</h2>')
					$('#loading').show();
					setTimeout(function(){
						window.location.href = "/login";
					 }, 3000);
				} else {
					var errors = data.responseJSON;
					_this.find('.error:not(input,select,textarea)').remove();
					var firstLoop = true;
					$('#loading').hide();

					for(var error in errors){
						// find input and put error message after it
						var input = _this.find('input[name="' + error + '"], input[name="' + error + '[]"], select[name="' + error + '"]');
	          			if(input.length > 1) {
							input.closest('.error-target').after('<small class="error">' + errors[error] + '</small>');
						}
						else{
							input.addClass('error').after('<small class="error">' + errors[error] + '</small>');
						}

						// reveal position of error
						if(firstLoop && $(input).offset()){
							$('.step').hide();
							$(input).closest('.step').show();
							console.log( $(input).offset().top);
							$('html, body').animate({
							    scrollTop: $(input).offset().top
							}, 600);
							firstLoop = false;
						}

					}
				}

				_this.find('button').prop('disabled', false).text('Try Again');
			}
		});
	});



	$('.ajax-form').on('change', '.error', function(e){
		$(this).removeClass('error');
		$(this).siblings('.error').remove();
	});

	//deletion across entire - need to specify delete url and id as data attributes, pass function
	$(document).on('click', '#delete, .delete', function(e){
		e.preventDefault();
		$(document).confirm('Do you really want to delete this?', 'deleteItem', this);
	});

	$('body').on('click', '#otherFiltersButton', function(){
		$('#otherFilters').toggle();
		if(!$(this).hasClass('active')){
			$(this).html("&#x25B2;").addClass('active');

		}else{
			$(this).html("&#x25BC;").removeClass('active');
		}
	});

	$('body').on('change', '#dateRange', function(){
		if( $(this).val() == "custom" ){
			$('#customDateFilters').show();
		}else{
			$('#customDateFilters').hide();
		}
	});


});

function deleteItem(_this){
	console.log('attempting to delete item');
	$.ajax({
		type: 'DELETE',
		url: $(_this).data("href") + '/' + $(_this).data("id"),
		success: function(){
			if( $(_this).data('redirect') ){
				console.log('redirect');
				window.location.href = $(_this).data('redirect');
			}else{
				updatePageAjax();
			}
		}
	})
}

//add a class .ajax-update to trigger a page refresh with this function. will need an id also
function updatePageAjax(url){
	if(!url){
		url = window.location.href
	}
	var ajaxTables = $('.ajax-update');
	var pathtopage = window.location.href;
	ajaxTables.each(function(){
		var _this = $(this);
		_this.load(window.location.href + ' #' + this.id, function(){
			$(this).children(':first').unwrap();
		});
	});
}

function msieversion() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    /* var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // Edge (IE 12+) => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    } */

    // other browser
    return false;
}
