
function replaceLink(match, p1, p2, offset, string) {
  // app link
  var parsed = '';
  if(p1 == 'app') {
  	parsed = '<a href="/contracts/by-app/' + p2 +  '" target="_blank"> #' + p2 + '</a>';
  }
  return parsed;
}

$(function(){
	// parse elements for interpolation
	$('.parse').each(function(e){
		var string = $(this).attr('title');
	
		var result = string.replace(/{\?([A-Za-z]+):([0-9]+)\?}/g, replaceLink);
		$(this).attr('title', result);
	});
});