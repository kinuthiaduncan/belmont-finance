// code for all modal controls goes in here
jQuery.fn.extend({
  showLoader: function() {
    return this.each(function() {
    	this.innerHTML = '<div class="loader"><h3>LOADING</h3><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div></div>';
    });
  }
});

$(function(){
	var genModal = $('#genModal');

	//edit distributor
	$('body').on('click', '#content .distrib_table .edit', function(e) {
		genModal.find('.content').showLoader();
		genModal.find('.content').load('/distributors/profile/' + $(this).attr('data-id') +  ' #content');
		e.preventDefault();
		genModal.find('.overTitle').text('DISTRIBUTOR EDIT / VIEW');
		genModal.find('.modalTitle').text($(this).attr('data-title'));
		genModal.foundation('reveal', 'open');
	});


	//edit staff
	$('body').on('click', '#content .staff_table .edit', function(e) {
		genModal.find('.content').showLoader();
		genModal.find('.content').load('/users/profile/' + $(this).attr('data-id') +  ' #content');
		e.preventDefault();
		genModal.find('.overTitle').text('USER EDIT / VIEW');
		genModal.find('.modalTitle').text($(this).attr('data-title'));
		genModal.foundation('reveal', 'open');
	});
});