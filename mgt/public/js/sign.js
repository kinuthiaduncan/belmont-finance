$(function(){
    


	// **********************************8**
	// add event handlers
	// *************************************

 	localStorage.clear();

	var isDrawMode = true;
	var isTypingMode = false;
	var currentSignatureCanvas;
	var currentSignaturePad;
	var isReplaceSigImg = true;
	var isDrwaSigBoxLines = true;

	$('.signature-draw-icon img').addClass('img-selected');
	$('.signature-typing-icon img').removeClass('img-selected');
	$('.signature-typing-input').hide();
	$('.signature-typing-font').hide();
	// $('#input_signature').hide();


	FastClick.attach(document.body);

	window.onresize = resizeCanvas;

	// close intro window
	$('#begin').on('click touchstart', function(){
		$('#introSignModal').foundation('reveal', 'close');
		$('#backOverlay').hide();
	});

	// read confirmation
	$('#readConfirmation').on('click touchstart', function(e){
		skipAgreement = true;
		$('#readConfirmation').hide();
        $('#signatures').removeClass('short');
        $("#start").click().show();

        // change wording to submit if just one signature
		if(signatureCount == totalSignatures){
			$("#start").text('Submit');
		}
	});

	// back to top button
	$('#backToTop').on('click touchstart', function(e){
		contract.panzoom('reset');
		$('#backToTop').hide();
	});
	$('#backToTop').hide();


	// Go back to application
	$('#confirmBack').on('click touchstart', function(e) {
		window.history.back();
	});

	// Stay on application
	$('#stayHere').on('click', function(e) {
		$('#goBackModal').foundation('reveal', 'close');
		$('#backOverlay').hide();
	});
	$(".signature-draw-icon").click(function(){
		//
		//alert();
		currentSignaturePad.clear();
		$('#input_signature').val("");
		isDrawMode = true;
		isTypingMode = false;
		$('.signature-draw-icon img').addClass('img-selected');
		$('.signature-typing-icon img').removeClass('img-selected');
		$('.signature-typing-input').hide();
		$('.signature-typing-font').hide();
		currentSignaturePad.on();
		drawSigBoxLines();
	});


	$(".signature-typing-icon").click(function(){
		//alert();
		currentSignaturePad.clear();
		isTypingMode = true;
		isDrawMode = false;
		$('#input_signature').val("");
		$('.signature-typing-icon img').addClass('img-selected');
		$('.signature-draw-icon img').removeClass('img-selected');
		$('.signature-typing-input').show();
		$('.signature-typing-font').show();
		$('#input_signature').focus();
		currentSignaturePad.off();
		drawSigBoxLines();
	});

	$('#input_signature').on('input', function(e){
		if(isDrawMode) return;
		if(isTypingMode){
			typeSignature($(this).val());
			if($(this).val() == ""){
				requireSignature = true;
			} else {
				requireSignature = false;
			}
		}
	});

	function typeSignature(text){
		currentSignaturePad.clear();
		var ctx = currentSignaturePad._canvas.getContext('2d');
        // ctx.font="54px arial";
        ctx.font = $('#select_signature_font').val();
        // ctx.font-style
        ctx.fillStyle = '#444';
        ctx.strokeStyle = '#444';
        ctx.lineWidth = 4;
     	var txtWidth = ctx.measureText(text).width;    
        ctx.fillText(text, ctx.canvas.clientWidth * 0.5 - txtWidth / 2, ctx.canvas.clientHeight * 0.58);	
        drawSigBoxLines();
	}

	$('#select_signature_font').change(function(){
		typeSignature($("#input_signature").val());
		$('#input_signature').focus();
	});

	$('#select_signature_font option').on('click', function(e){
		$('#input_signature').focus();
	});

	$("#btn_clear").click(function(){
		// console.log($('#sig-loc-' + (signatureCount - 1) + "> img"));
		$('#sig-loc-' + (signatureCount - 1) + "> img").attr("src","");
		isReplaceSigImg = true;
		requireSignature = true;
		currentSignaturePad.on();
		$("#signature-tools").show();
		$(".signature-draw-icon").trigger("click");
		$('#sig-loc-' + (signatureCount - 1)).addClass('current');
		$(".signature-clear").hide();
		drawSigBoxLines();
		isDrwaSigBoxLines = true;
	});		
	// panzoom on the contract view
	var contract = $('#contract');
	contract.panzoom();
	// fires whenever panned and/or zoomed
	contract.on('panzoomchange', function(e){
		if ((Math.abs(contract.position()['top']) / contract.height() * 100) >= 13) {
			$('#backToTop').show();
		} else $('#backToTop').hide();
	});

    // enable scrolling on contracts
    $('body').on('mousewheel', function(event) {
        event.preventDefault();
        var loc = contract.panzoom("getMatrix")[5];
        if(event.deltaY < 0) {
            contract.panzoom("pan", 0, -50, { relative: true });
        } else {
            if(loc < 250) {
                contract.panzoom("pan", 0, 50, { relative: true });
            }
        }
    });




    // output signature locations
	for (var i = 0, len = sigLocations.length; i < len; i++) {

		var top = sigLocations[i][1]/totalPages;
		var left = sigLocations[i][0];
		// height offset must be adjusted based on number of pages
		var height = 1.4267 - totalPages * 0.2134;

        // change size if it's an initial
        var init = sigLocations[i][4];
        var width = (init == 1) ? '6' : '22';
        var initClass = (init == 1) ? 'initial' : '';
        
		contract.append('<div class="sig-loc ' + initClass + '" id="sig-loc-' + i + '" style="height: ' + height + '%; width: ' + width + '%; top: ' + top + '%; left: ' + left + '%"></div>');
	}

    // ********************************************************************************
	// pretty much all the action piled in here....
	// ********************************************************************************
	// set up initial variables
	var signatureCount = 0;
	var totalSignatures = sigLocations.length;
	var signaturePads = [];
	var data = [];
	var requireSignature = true;

	$('#start, #skip').on('click touchstart', function(e){
		e.preventDefault();

        // resize if initial box
		if(signatureCount <= totalSignatures && signatureCount){
			var signatureIndex = signatureCount - 1;
			lastPad = signaturePads[signatureCount - 1];
			buttonId = $(this).attr('id');

			//console.log(lastPad); previous canvas object

			// if not the skip button, show an alert and do not move forward
			if(requireSignature && buttonId != 'skip') {
				if(language == 'Spanish') {
					alert('Esta firma es requerida. Por favor, firme para continuar');
				} else {
					alert('This signature is required - please sign in order to continue');
				}
			} else {
				// move to next signature
				if(buttonId == 'skip') {
					// clear canvas if skip button is pressed
					var context = lastPad._canvas.getContext('2d');
					context.clearRect(0, 0, lastPad._canvas.width, lastPad._canvas.height);
				}

				var docHeight = $('#contract').height() / totalPages;
				var sigY = $('#sig-loc-' + signatureIndex)[0].offsetTop;

				// add the last signature signed to our data to send
				data.push({x: sigLocations[signatureIndex][0]*16.5, y: (sigY / docHeight), sig: signaturePads[signatureIndex].toDataURL(), init: sigLocations[signatureIndex][4]});

				// last signature, so post to our handler
				if(signatureCount == totalSignatures) {
					if(language == 'Spanish') {
						$(this).text('Por favor espera...');
					} else {
						$(this).text('Please wait...');
					}

					$(this).attr('disabled', 'disabled');
					$('#loading').show();

					$.ajax({
						url: submitSignUrl,
						type: 'post',
						data: {
							signatures: data,
							sessionKey: sessionKey,
							step: "final",
							firstName: firstName,
							lastName: lastName,
							id: appId
						},
						success: function(data){
							$('#contract, #signatures, #backToTop, #navBar').hide();
							$('#successPane').show();
							$('#loading').hide();
							$('.signature-typing-input').hide();
							$('.signature-typing-font').hide();
							$('#signature-tools').hide();
							$('#btn_clear').hide();
							$('.sign-button-container').hide();


							// do any post submission checks
							// only on credit app right now
							if($('#checking')) {
								setTimeout(function(){
									$.ajax({
										url: "/sign/status",
										type: 'GET',
										success: function(){
											$('#checking').hide();
											$('#completed').show();
										},
										error: function(){
											$('#checking').hide();
											$('#failure').show();
										}
									}); 
								}, 1500);
							}

							// define signSuccess in view
							if (typeof signSuccess === "function") { 
							    signSuccess();
							}
						}
					});

					// add signature on document and move forward
				} else {
					if(signatureCount <= totalSignatures + 1){
						$('#contract').find('.current').removeClass('current');
						$('#signatureLabel').text(sigLocations[signatureCount][3]);

						if( sigLocations[signatureCount][2] == 'optional'){
							$('#skip').show();
							requireSignature = false;
						} else {
							$('#skip').hide();
							requireSignature = true;
						}

						signatureCount++;
						$(lastPad._canvas).hide();
						//$('#signature-tools').hide();
						// $('.signature-typing-input').hide();
						if( isReplaceSigImg ){
							$('#sig-loc-' + (signatureCount - 2) + "> img").remove();
							$('#sig-loc-' + (signatureCount - 2)).append('<img src="' + lastPad.toDataURL() + '">');
							localStorage.setItem('prevImg' + sigLocations[signatureCount - 2][5],lastPad.toDataURL());
							// console.log(lastPad._canvas.width);
							localStorage.setItem('prevImgWidth'  + sigLocations[signatureCount - 2][5],lastPad._canvas.width);
							localStorage.setItem('prevImgHeight' + sigLocations[signatureCount - 2][5],lastPad._canvas.height);	
						}
						
						$('#sig-loc-' + (signatureCount - 1)).addClass('current');

						var canvas = document.getElementById('sig' + signatureCount);

						var newSignaturePad = new SignaturePad(canvas, {
							minWidth: 2, maxWidth: 4, velocityFilterWeight: 0.55, penColor: "#444",
							onEnd: function() {
								// mark that user has signed
								requireSignature = false;
							}
						});


						if( localStorage.getItem("prevImg" + sigLocations[signatureCount-1][5])){

							$('#sig-loc-' + (signatureCount - 1)).append('<img src="' + localStorage.getItem("prevImg" + sigLocations[signatureCount -1][5]) + '">');
							isReplaceSigImg = false;
							requireSignature = false;
							newSignaturePad.clear();
							newSignaturePad.off();
							// var ratio =  Math.max(window.devicePixelRatio || 1, 1);
						    // newSignaturePad._canvas.getContext('2d').scale(ratio,ratio);
						    // elem.getContext("2d").scale(ratio, ratio);

							newSignaturePad.fromDataURL(localStorage.getItem("prevImg" + sigLocations[signatureCount -1][5]),{
								"width": localStorage.getItem("prevImgWidth" + sigLocations[signatureCount -1][5]),
								"height": localStorage.getItem("prevImgHeight" + sigLocations[signatureCount -1][5])
							});
							$("#signature-tools").hide();
							$('.signature-typing-input').hide();
							$('.signature-typing-font').hide();
							$('.signature-clear').show();
							isDrwaSigBoxLines = false;

						} else {
							isReplaceSigImg = true;
							requireSignature = true;
							newSignaturePad.on();
							$("#signature-tools").show();
							$('#sig-loc-' + (signatureCount - 1)).addClass('current');
							// $('.signature-typing-input').hide();
							// $('.signature-typing-font').hide();
							$(".signature-draw-icon").trigger('click');
							$('.signature-clear').hide();
							isDrwaSigBoxLines = true;

						}


						signaturePads.push(newSignaturePad);
						currentSignaturePad = newSignaturePad;
						$(canvas).show();

						console.log(canvas);// current canvas object;
						$("#input_signature").val("");

						currentSignatureCanvas = canvas;

						contract.panzoom("resetZoom", {
							animate: true
						});
						if(signatureCount == totalSignatures){
							$(this).text('Submit');
						}

                        if( $('#sig-loc-' + (signatureCount - 1)).hasClass('initial') ){
                            $(canvas).css('max-width', '300px');
                        }
                        resizeCanvas(canvas);
                        if(isDrwaSigBoxLines) drawSigBoxLines();
                        
                        $(window).resize(function() {
                            resizeCanvas(canvas);
                        });
                        contract.panzoom("pan", 0, -sigLocations[signatureCount -1][1]/(100*totalPages)*contract.height() + 90, {animate: true});
					}
				}
			}
		} else {
			if(!skipAgreement) {
				alert('You must read and agree before signing');
			} else {
				$('#beginMessage').hide();
				$('#signatureLabel').show();
				$('#signatureLabel').text(sigLocations[signatureCount][3]);
				sigLocations[signatureCount][2] == 'optional' ? $('#skip').show() : $('#skip').hide();
				signatureCount++;
				var canvas = document.getElementById('sig' + signatureCount);

				var newSignaturePad = new SignaturePad(canvas, {minWidth: 2, maxWidth: 4, velocityFilterWeight: 0.55,  penColor: "#444", onEnd: function(){
					requireSignature = false;
				}});


				signaturePads.push( newSignaturePad );
				currentSignaturePad = newSignaturePad;
				$(canvas).show();

				// console.log(canvas);//first canvas object
				$('#input_signature').val("");


				currentSignatureCanvas = canvas;

				$('#signature-tools').show();
				// $('.signature-clear').show();
				// $('.signature-typing-input').show();

				$('#sig-loc-0').addClass('current');
				$(this).html('Next Signature &nbsp;<i class="fi-arrow-right"></i>');

                if( $('#sig-loc-' + (signatureCount - 1)).hasClass('initial') ){
                    $(canvas).css('max-width', '300px');
                }
                resizeCanvas(canvas);
                drawSigBoxLines();
                $(window).resize(function() {
                    resizeCanvas(canvas);
                });
                contract.panzoom("pan", 0, -sigLocations[signatureCount -1][1]/(100*totalPages)*contract.height() + 30 );
			}
		}
	});

	// draw X_____ in signature box
	function drawSigBoxLines() {
		// Draw X__________ in signature box
		var c = document.getElementById('sig' + signatureCount);
		var ctx = c.getContext("2d");
        ctx.font="54px arial";
        ctx.fillStyle = '#aaa';
        ctx.strokeStyle = '#aaa';
        ctx.lineWidth = 2;
        ctx.fillText("X", ctx.canvas.clientWidth * 0.03, ctx.canvas.clientHeight * 0.85);

		// ________
		ctx.moveTo((ctx.canvas.clientWidth * 0.03 + 40),ctx.canvas.clientHeight * 0.85);
		ctx.lineTo(ctx.canvas.clientWidth * 0.95,ctx.canvas.clientHeight * 0.85);
		ctx.stroke();
	}


	var resizeCanvas = function(elem) {
		// When zoomed out to less than 100%, for some very strange reason,
	    // some browsers report devicePixelRatio as less than 1
	    // and only part of the canvas is cleared then.
	    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
	    elem.width = elem.offsetWidth * ratio;
	    elem.height = elem.offsetHeight * ratio;
	    elem.getContext("2d").scale(ratio, ratio);

		for(signatureIndex in signaturePads) {
			var signature = signaturePads[signatureIndex];
			var dataUrl = signature.toDataURL();
		    signature.clear();
		    signature.fromDataURL(dataUrl);
		}
	}
});
