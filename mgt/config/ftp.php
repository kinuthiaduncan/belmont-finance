<?php
return array(

    /*
	|--------------------------------------------------------------------------
	| Default FTP Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the FTP connections below you wish
	| to use as your default connection for all ftp work.
	|
	*/

    'default' => 'test',

    /*
    |--------------------------------------------------------------------------
    | FTP Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the FTP connections setup for your application.
    |
    */

    'connections' => array(

        'test' => array(
            'host'   => 'ftp.belmontfinancellc.com',
            'port'  => 21,
            'username' => 'system@belmontfinancellc.com',
            'password'   => 'adshf#$gas51',
            'passive'   => true
        ),
    ),
);