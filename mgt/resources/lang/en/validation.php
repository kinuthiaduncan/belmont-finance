<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "The :attribute must be accepted.",
	"active_url"           => "The :attribute is not a valid URL.",
	"after"                => "The :attribute must be a date after :date.",
	"alpha"                => "The :attribute may only contain letters.",
	"alpha_space"		   => "The :attribute must only contain letters and spaces",
	"alpha_dash"           => "The :attribute may only contain letters, numbers, and dashes.",
	"alpha_num"            => "The :attribute may only contain letters and numbers.",
	"array"                => "The :attribute must be an array.",
	"before"               => "The :attribute must be a date before :date.",
	"between"              => [
		"numeric" => "The :attribute must be between :min and :max.",
		"file"    => "The :attribute must be between :min and :max kilobytes.",
		"string"  => "The :attribute must be between :min and :max characters.",
		"array"   => "The :attribute must have between :min and :max items.",
	],
	"boolean"              => "The :attribute field must be true or false.",
	"confirmed"            => "The :attribute you entered doesn't match, please enter it again.",
	"date"                 => "The :attribute is not a valid date.",
	"date_format"          => "The :attribute does not match the format :format.",
	"different"            => "The :attribute and :other must be different.",
	"digits"               => "The :attribute must be :digits digits.",
	"digits_between"       => "The :attribute must be between :min and :max digits.",
	"email"                => "The :attribute must be a valid email address.",
	"filled"               => "The :attribute field is required.",
	"exists"               => "The selected :attribute is invalid.",
	"image"                => "The :attribute must be an image.",
	"in"                   => "The selected :attribute is invalid.",
	"integer"              => "The :attribute must be an integer.",
	"ip"                   => "The :attribute must be a valid IP address.",
	"max"                  => [
		"numeric" => "The :attribute may not be greater than :max.",
		"file"    => "The :attribute may not be greater than :max kilobytes.",
		"string"  => "The :attribute may not be greater than :max characters.",
		"array"   => "The :attribute may not have more than :max items.",
	],
	"mimes"                => "The :attribute must be a file of type: :values.",
	"min"                  => [
		"numeric" => "The :attribute must be at least :min.",
		"file"    => "The :attribute must be at least :min kilobytes.",
		"string"  => "The :attribute must be at least :min characters.",
		"array"   => "The :attribute must have at least :min items.",
	],
	"not_in"               => "The selected :attribute is invalid.",
	"numeric"              => "The :attribute must be a number.",
	"regex"                => "The :attribute format is invalid.",
	"required"             => "The :attribute field is required.",
	"required_if"          => "The :attribute field is required when :other is :value.",
	"required_with"        => "The :attribute field is required when :values is present.",
	"required_with_all"    => "The :attribute field is required when :values is present.",
	"required_without"     => "The :attribute field is required when :values is not present.",
	"required_without_all" => "The :attribute field is required when none of :values are present.",
	"same"                 => "The :attribute and :other must match.",
	"size"                 => [
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
		"array"   => "The :attribute must contain :size items.",
	],
	"unique"               => "The :attribute has already been taken.",
	"url"                  => "The :attribute format is invalid.",
	"timezone"             => "The :attribute must be a valid zone.",

	"match_signed" => "Your signature must match your name",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'amount_financed' => [
			'min' => 'Our minimum finance amount is $1,000. You may have to adjust the down payment in order to continue.'
		],
		'ach_account_number' => [
			'required_without' => 'Provide your bank account number'
		],
		'ach_routing_number' => [
			'required_without' => 'Provide your bank routing number'
		],
		'birthdate' => [
			'before' => 'You must be 18 or older in order to obtain financing.'
		],
		'coBirthdate' => [
			'before' => 'You must be 18 or older in order to obtain financing.'
		],
		'coHomePhone' => [
			'required_without' => 'We need either a home phone or cell number (Do not enter the same number for both)',
			'different' => 'We need either a home phone or cell number (Do not enter the same number for both)'
		],
		'coCellPhone' => [
			'required_without' => 'We need either a home phone or cell number (Do not enter the same number for both)',
			'different' => 'We need either a home phone or cell number (Do not enter the same number for both)'
		],
		'coEmployerPhone' => [
			'required_if' => 'Employer phone number is required'
		],
		'cc' => [
			'luhns' => 'Your debit card number is invalid',
		],
		'cellPhone' => [
			'required_without' => 'We need either a home phone or cell number (Do not enter the same number for both)',
			'different' => 'We need either a home phone or cell number (Do not enter the same number for both)'
		],
		'coCellPhone' => [
			'required_without' => 'We need either a home phone or cell number (Do not enter the same number for both)',
			'different' => 'We need either a home phone or cell number (Do not enter the same number for both)'
		],
		'coHomePhone' => [
			'required_without' => 'We need either a home phone or cell number (Do not enter the same number for both)',
			'different' => 'We need either a home phone or cell number (Do not enter the same number for both)'
		],
		'ssn' => [
			'regex' => 'Please follow the format 111-11-1111, including dashes',
			'ssn_nine' => 'SSN is incorrect, please enter your correct SSN'
		],
		'coRefLandlordMonthlyPayment' => [
			'required_without' => 'If your address is different than the primary applicant, let us know your monthly mortgage or rent cost'
		],
		'co_ssn' => [
			'regex' => 'Please follow the format 111-11-1111, including dashes'
		],
		'routing_number' => [
			'aba' => 'Incorrect routing / aba number'
		],
		'email' => [
			'confirmed' => 'Make sure that your email is entered correctly in both fields'
		],
		'final_amount' => [
			'min' => 'Our minimum financing amount is $1000. You may have to adjust your down payment to continue.',
			'max' => 'Your final amount financed must be under $:max, which was the amount requested on the credit application'
		],
		'homePhone' => [
			'required_without' => 'We need either a home phone or cell number (Do not enter the same number for both)',
			'different' => 'We need either a home phone or cell number (Do not enter the same number for both)'
		],
		'mailing_address' => [
			'required_without' => 'Check the box above if your mailing address is the same as your physical address, else fill in the mailing address.'
		],
		'personalRefOneRelationship' => [
			'required' => 'You must provide one reference'
		],
		'residenceType' => [
			'no_renters' => 'Sorry, we are not currently able to offer financing to renters for these products.'
		],
		'residenceStatus' => [
			'required_if' => 'Explain where you live and your payment arrangement since you selected "Other".'
		],
		'us_citizen' => [
			'in' => 'Sorry, we are not able to offer financing to non-US citizens at this time.'
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [
		'cc' => 'debit card number',
		'cvv' => 'CVV / CVC',
		'card_name' =>'Cardholder Name',
		'refLandlordMonthlyPayment' => 'Monthly Mortgage or Rent Payment',
	],

];
