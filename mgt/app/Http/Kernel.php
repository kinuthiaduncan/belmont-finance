<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'App\Http\Middleware\VerifyCsrfToken'
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'App\Http\Middleware\Authenticate',
		'signtoken' => 'App\Http\Middleware\SignTokenAuthenticate',
		// 'auth' => 'App\Http\Middleware\AfterAuthenticate',
		'auth.basic' => 'App\Http\Middleware\BasicAuthentication',
		'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',
		'admin' => 'App\Http\Middleware\AuthAdmin',
		'api.auth' => 'App\Http\Middleware\ApiAuthenticate',
		'lockout' => 'App\Http\Middleware\Lockout',
		'restrictconsumer' => 'App\Http\Middleware\RestrictedConsumer',
		'dealerchoose' => 'App\Http\Middleware\DealerChoose'
	];

}
