<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class DealerPaperworkRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => 'required|min:10|max:15',
            'street_address' => 'required|max:64',
            'city' => 'required|max:32',
            'state' => 'required|max:20',
            'zip_code' => 'required|min:5|max:12',
            'language' => 'required',
            'states_required' => 'required|max:256',
            'installment_paperwork' => 'required_without:revolving_paperwork',
            'revolving_paperwork' => 'required_without:installment_paperwork',
            'ship_to_given_address' => 'required',
            'company_recipient_name' => 'max:64',
            'alternate_street_address' => 'required_if:ship_to_given_address,No|max:64',
            'alternate_city' => 'required_if:ship_to_given_address,No|max:32',
            'alternate_state' => 'required_if:ship_to_given_address,No|max:20',
            'alternate_zip_code' => 'required_if:ship_to_given_address,No|max:12',
        ];
    }

    public function messages()
    {
        return [
            'language.required' => 'Select either English or Spanish for your paperwork',
            'installment_paperwork.required_without' => 'You must select at least one type of paperwork',
            'revolving_paperwork.required_without' => 'You must select at least one type of paperwork',
            'states_required.required' => 'Please list the states for which you need this paperwork',
            'alternate_phone_number.required_if' => 'Fill out all the fields for this address',
            'alternate_city.required_if' => 'Fill out all the fields for this address',
            'alternate_state.required_if' => 'Fill out all the fields for this address',
            'alternate_zip_code.required_if' => 'Fill out all the fields for this address'
        ];
    }

}
