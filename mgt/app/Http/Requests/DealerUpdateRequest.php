<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class DealerUpdateRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
        	'email' => 'required|email|min:2|unique:dist_users'
    	];
	}

	public function messages()
	{
		return [
        	'email.unique' => 'We already have record of your email as a user - please contact Belmont Finance if you need help'
    	];
	}

}
