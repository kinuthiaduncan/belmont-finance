<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Log;
use Session;

class SalesSlipPostRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$step = $this->input('step');

		switch ($step) {
			case "one":
				return [
					'email' => 'email',
					'coEmail' => 'email',
					'state' => 'required|min:2|max:2',
					'is_addon' => 'required_without:closed_required|boolean',
					'term' => 'required_with:closed_required|integer',
					'apr' => 'required_with:closed_required|numeric|between:0,29.99'
				];
			case "two":
				return [
					'qty_one' => 'required|integer|between:1,20',
					'qty_two' => 'integer|between:1,20',
					'qty_three' => 'integer|between:1,20',
					'price_one' => 'required|numeric|between:1,99999.99',
					'price_two' => 'numeric|between:1,99999.99',
					'price_three' => 'numeric|between:1,99999.99',
					'retail_price' => 'numeric|between:1,99999.99',
					'sku_one' => 'max:32',
					'sku_two' => 'max:32',
					'sku_three' => 'max:32',
					'description_one' => 'max:128',
					'description_two' => 'max:128',
					'description_three' => 'max:128',
					'attachments' => 'max:128'
				];

			case "three":
				$app = \App\Models\ContractApplication::findOrFail($this->session()->get('sales-slip-data.app_id'));
				$amountAllowed = $app->amount_financed - $app->down_payment_amount;

				return [
					'shipping' => 'numeric|between:0,999.99',
					'trade_in' => 'numeric|between:0,99999.99',
					'sales_tax' => 'numeric|between:0,999.99',
					'down_payment' => 'numeric|between:0,99999.99',
					'final_amount' => 'required|numeric|min:1000|max:' . $amountAllowed,
				];

			case "four":
				return [
					'ach_account_number' => 'required_without:is_ach_bypassed|digits_between:6,20',
					'ach_routing_number' => 'required_without:is_ach_bypassed|digits:9',
					'ach_is_savings' => 'boolean',
					'ach_day_of_month' => 'integer|between:1,30'
				];

			case "five":
				return [
				];

			case "final":
			return [];
		}

	}

	public function all()
    {
        $input = parent::all();

        return $input;
    }

    public function messages()
	{
		$app = \App\Models\ContractApplication::findOrFail($this->session()->get('sales-slip-data.app_id'));
				$amountAllowed = $app->amount_financed;
	    return [
	        'final_amount.between' => 'Total amount must be $' . $amountAllowed . ' or less.'
	    ];
	}
}
