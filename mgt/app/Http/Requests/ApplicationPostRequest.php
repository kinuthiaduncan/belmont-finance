<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Log;
use Session;
use Carbon\Carbon;
use Auth;

class ApplicationPostRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$step = $this->input('step');

		// this switches out the rules if a coapp is filling out by self remotely
		$coapplicant = $this->session()->get('appFields.' . $this->sessionKey .'.coapplicant') == 'Yes' ? true : false;
		$notRequiredCoAppRemote = Session::has('coAppRemote') ? '' : 'required|';
		$requiredCoAppRemote = Session::has('coAppRemote') || $coapplicant == 'Yes' ? 'required|' : '';

		switch ($step) {
			// Amount Requested
			case "one":
				return [
					'down_payment_amount' => 'numeric|min:0|max:99999.99',
					'amount_financed' => 'required|numeric|min:' . (1000 + $this->down_payment_amount) . '|max:99999.99',
				];
			// Coapplicant yes or no
			case "two":
				return [];

			// identification
			case "three":
				

				return [
					'us_citizen' => $notRequiredCoAppRemote . 'in:Yes,No',
					'identification_type' => $notRequiredCoAppRemote . 'in:driver license,photo id',
					'identification_state' => $notRequiredCoAppRemote . 'min:2,max:2',
					'licExpDate' => $notRequiredCoAppRemote . 'date',
					'driversOrId' => $notRequiredCoAppRemote . 'max:64',

					'co_identification_type' => $requiredCoAppRemote . 'in:driver license,photo id',
					'co_identification_state' => $requiredCoAppRemote . 'min:2,max:2',
					'co_licExpDate' => $requiredCoAppRemote . 'date',
					'coDriversOrId' => $requiredCoAppRemote . 'max:64'
				];

			// Phone and Other info
			case "four":	
				$minimumBirthdate = Carbon::now()->subYears(18)->format('Y-m-d');

				// check distributor id for remote first, then fallback to local
				if($this->session()->has('dist_id')) {
					$distId = $this->session()->get('dist_id');
				} else {
					$distId = $this->session()->get('selectedDistributorId');
				}
			
				return [
					'firstName' => 'required|min:1|max:40|alpha_space',
					'lastName' => 'required|min:1|max:40|alpha_space',
					'initial' => 'max:1',
					'birthdate' => 'required|date|before:' . $minimumBirthdate,
					'residenceType' => 'required|in:Own,Rent,Mobile,Other|no_renters:' . $distId,
					'residenceStatus' => 'required_if:residenceType,==,Other|max:128',
					'yrsAtAddress' => 'required|integer',
					'monthsAtAddress' => 'integer',
					'refLandlordMonthlyPayment' => 'numeric|required',
					'ssn' => 'required|min:1|max:12|ssn_nine|regex:/[0-9]{3}-[0-9]{2}-[0-9]{4}/'
				];

			// Address
			case "five":
				return [
					'address' => 'required|min:1|max:40',
					'addressTwo' => 'max:40',
					'city' => 'required|min:1|max:40',
					'state' => 'required|min:1|max:2',
					'zip' => 'required|min:5|max:10',
					'homePhone' => 'required_without:cellPhone|different:cellPhone|min:10|max:16',
					'cellPhone' => 'required_without:homePhone|different:homePhone|min:10|max:16',
					'mailing_address' => 'required_with:mailing_address_different|max:40',
					'mailing_address_two' => 'max:40',
					'mailing_city' => 'required_with:mailing_address|max:40',
					'mailing_state' => 'required_with:mailing_address|max:2',
					'mailing_zip' => 'required_with:mailing_address|min:5|max:10',
					'email' => 'e-mail|max:40'
				];

			// Employment
			case "six":
				return [
						'is_retired' => 'required|in:Yes,No',
					    'employer' => 'required|min:1|max:40',
					    'employerPhone' => 'required_if:is_retired,No|min:1|max:12',
					    'numDependents' => 'required',
						'yrsEmployed' => 'required|min:0|max:99|numeric',
						'monthsEmployed' => 'integer|min:0|max:12',
						'jobTitle' => 'required|min:1|max:40',
						'monthlyGrossPay' => 'required|min:1|max:8',
						'other_income_source' => 'max:32',
						'other_income_amount' => 'required_with:other_income_source|numeric|min:1|max:99999.99'
					];
			// Coapplicant Info
			case "seven":
				$minimumBirthdate = Carbon::now()->subYears(18)->format('Y-m-d');
				return [

						'coFirstName' => $requiredCoAppRemote . 'max:40|alpha_space',
						'coLastName' => $requiredCoAppRemote . 'max:40|alpha_space',
						'coInitial' => 'max:1',
						'coBirthdate' => $requiredCoAppRemote . 'max:10|date|before:' . $minimumBirthdate,
						'co_ssn' => $requiredCoAppRemote . 'ssn_nine|not_in:' . $this->session()->get('appFields.' . $this->sessionKey .'.ssn') . '|regex:/[0-9]{3}-[0-9]{2}-[0-9]{4}/'
					];
			// References
			case "eight":
				return [
					'coAddress' => $requiredCoAppRemote . 'max:40',
					'coAddressTwo' => 'max:40',
					'coCity' => $requiredCoAppRemote . 'max:40',
					'coState' => $requiredCoAppRemote . 'max:2',
					'coZip' => $requiredCoAppRemote . 'min:5|max:9',
					'coHomePhone' => ($requiredCoAppRemote ?  'required_without:coCellPhone|' : '') . 'different:coCellPhone|min:10|max:16',
					'coCellPhone' => ($requiredCoAppRemote ?  'required_without:coHomePhone|' : '') . 'different:coHomePhone|max:16',
					'coRefLandlordMonthlyPayment' => 'required_with:copy_address',
					'co_mailing_address' => 'required_with:co_mailing_address_different|max:40',
					'co_mailing_address' => 'required_with:co_mailing_address_different|max:40',
					'co_mailing_address_two' => 'max:40',
					'co_mailing_city' => 'required_with:co_mailing_address|max:40',
					'co_mailing_state' => 'required_with:co_mailing_address|max:2',
					'co_mailing_zip' => 'required_with:co_mailing_address|min:5|max:10',
					'coEmail' => 'e-mail|max:40'
				];

			// Coapplicant Employment
			case "nine":
				return [
					    'coEmployer' => $requiredCoAppRemote . 'min:1|max:40',
						'coYrsEmployed' => $requiredCoAppRemote . 'min:0|max:99|numeric',
						'coEmployerPhone' => ($requiredCoAppRemote ?  'required_if:co_is_retired,==,No|' : '') . 'min:1|max:12',
						'coMonthsEmployed' => 'min:0|max:12|integer',
						'coJobTitle' => $requiredCoAppRemote . 'min:1|max:40',
						'coMonthlyGrossPay' => $requiredCoAppRemote . 'min:1|max:8',
						'co_other_income_source' => 'max:32',
						'co_other_income_amount' => 'required_with:co_other_income-source|numeric',
					];


			case "ten":
				return [
					'personalRefOneRelationship' => 'required|max:32',
					'personalRefOneName' => 'required|max:32',
					'personalRefOnePhone' => 'required|max:16',
				];

			case "final":
				return [];
		}

	}

	public function all()
    {
        $input = parent::all();

        $phoneFields = [
        	'employerPhone',
			'homePhone',
			'coEmployerPhone',
			'cellPhone',
			'coCellPhone',
			'coHomePhone',
			'refLandlordPhone',
			'credrefOnePhone',
			'credrefTwoPhone',
			'personalRefOnePhone'
		];

		$dollarFields = [
			'amount_financed',
			'monthlyGrossPay',
			'coMonthlyGrossPay',
			'refLandlordMonthlyPayment',
			'refLandlordPresBalance'
		];

		foreach($phoneFields as $phone){
			if(isset($input[$phone])){
				$input[$phone] = preg_replace('/[^0-9]/', '', $input[$phone]);
			}
		}

		foreach($dollarFields as $field){
			if(isset($input[$field])){
				$input[$field] = str_replace([',','$'], '', $input[$field]);
			}
		}

        return $input;
    }
}
