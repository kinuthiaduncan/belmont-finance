<?php
use \App\Models\User as User;

URL::setRootControllerNamespace('App\Http\Controllers');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| If there are any issues on production with routes 404ing, try placing them before the route controller call
|
*/

// customer routing
Route::group(['domain' => 'accounts.' . getenv('DOMAIN')], function()
{
    Route::get('login', 'Auth\CustomerAuthController@showLoginForm');
    Route::post('login', 'Auth\CustomerAuthController@login');
    Route::post('/password/reset', 'Auth\CustomerPasswordController@reset');
    Route::get('/password/reset/{token?}', 'Auth\CustomerPasswordController@showResetForm');
    Route::post('/password/email', 'Auth\CustomerPasswordController@sendResetLinkEmail');

    Route::post('/verify-reg', 'Customer\DashboardController@verifyReg');
    Route::get('/verify-reg', 'Customer\DashboardController@verifyReg');
    Route::post('/verify-reg-two', 'Customer\DashboardController@verifyRegTwo');
    Route::get('/expired', 'Customer\DashboardController@expired');
    Route::get('/shared-login', 'Customer\DashboardController@sharedLogin');
    Route::get('/privacy-policy', 'Customer\DashboardController@getPrivacyPolicy');

    Route::get('/new-account', 'Customer\RegisterController@index');
    Route::post('/new-account', 'Customer\RegisterController@postIndex');
    Route::post('/new-account-success', 'Customer\RegisterController@postCreate');

    // only routes that unfinisehd accounts can access
    Route::group(['middleware' => ['auth:consumer', 'lockout']], function(){
        Route::get('welcome', 'Customer\DashboardController@appFiles');
        Route::get('file/view/{id}', 'Customer\FileController@getFile');
    });


    // protected routes go in here
    Route::group(['middleware' => ['auth:consumer', 'lockout', 'restrictconsumer']], function(){

            // reset a customer's account
        Route::get('/reset-account/{id}', 'Customer\DashboardController@getResetAccount');
        Route::post('/reset-account/', 'Customer\DashboardController@postResetAccount');
        Route::get('/', 'Customer\DashboardController@index');
        Route::controller('payments', 'Customer\PaymentController');
        Route::controller('profile', 'Customer\ProfileController');
    });
});

// distributor routing
Route::group(['domain' => 'dealer.' . getenv('DOMAIN')], function()
{
    Route::get('login', 'Auth\DealerAuthController@showLoginForm');
    Route::post('login', 'Auth\DealerAuthController@login');
    Route::post('/password/reset', 'Auth\DealerPasswordController@reset');
    Route::get('/password/reset/{token?}', 'Auth\DealerPasswordController@showResetForm');
    Route::post('/password/email', 'Auth\DealerPasswordController@sendResetLinkEmail');

    // manually included protection in controller
    Route::controller('support', 'Dealer\DealerContentController');
    Route::post('/api/remote-app', 'Api\ApiController@postRemoteApp');
    Route::get('/sign/status', 'Dealer\RemoteSignController@statusComplete');

    // route to have dealer choose their distributor when logging in
    Route::get('/choose-distributor/{id}', 'Auth\DealerAuthController@chooseDistributor');


    // protected signature token routes
    Route::group(['middleware' => ['signtoken']], function(){
        Route::get('/sign/welcome', 'Dealer\RemoteSignController@welcome');
        Route::get('/sign/start', 'Dealer\RemoteSignController@start');
        Route::post('/sign/submit', 'Dealer\RemoteSignController@submit');
        Route::get('/sign/complete', 'Dealer\RemoteSignController@complete');

        // path for newer sequential paperwork
        Route::get('/sign/go', 'Dealer\RemoteSignController@signAll');
        Route::post('/sign/next', 'Dealer\RemoteSignController@signAllNext');
        Route::post('/apps/paperwork/is-completed', 'Dealer\DealerSignController@isPaperworkComplete');
    });


    // protected routes go in here
    Route::group(['middleware' => ['auth:dealer', 'dealerchoose']], function(){

        // manual page routes = do this moving forward as laravel discontinued the auto controller routing
        Route::get('/request-paperwork', 'Dealer\DealerContentController@requestPaperwork');
        Route::post('/request-paperwork/submit', 'Dealer\DealerFormController@requestPaperwork');
        Route::get('/new-app', 'Dealer\DealerDashboardController@newApp'); // go to a view that shows different application types
        Route::get('/email-app', 'Dealer\DealerDashboardController@emailApp');
        Route::post('/email-app', 'Dealer\DealerDashboardController@postEmailApp');

        Route::get('/apps/paperwork/start/{id}', 'Dealer\DealerApplicationController@getSalesSlip');
        Route::get('/apps/paperwork/generate/{id}', 'Dealer\DealerSignController@viewGeneratePaperwork');
        Route::post('/apps/paperwork/generate/', 'Dealer\DealerSignController@generatePaperwork');
        Route::post('/apps/paperwork/lcc/', 'Dealer\DealerSignController@lcc');
        Route::get('/apps/paperwork/print/{id}', 'Dealer\DealerSignController@printPaperwork');
        Route::post('/apps/paperwork/send/{id}', 'Dealer\DealerSignController@sendPaperwork');
        Route::get('/apps/paperwork/sign/{id}', 'Dealer\DealerSignController@signPaperwork');
        Route::post('/apps/paperwork/submit/', 'Dealer\DealerSignController@postSubmitPaperwork');
        Route::get('/apps/paperwork/{id}', 'Dealer\DealerSignController@viewStartPaperwork');
        Route::post('/apps/paperwork/is-complete', 'Dealer\DealerSignController@isPaperworkComplete');

        Route::get('/apps/submit-app', 'Dealer\DealerApplicationController@submitNoSign');

        Route::get('/apps/request-funding/{id}', 'Dealer\DealerDashboardController@requestFunding');
        Route::post('/apps/request-funding', 'Dealer\DealerDashboardController@postRequestFunding');
        Route::get('/apps/sign-request-funding/{id}', 'Dealer\DealerDashboardController@signRequestFunding');

        Route::get('/apps/request-completion/{id}', 'Dealer\DealerDashboardController@requestCompletion');
        Route::post('/apps/request-completion/{id}', 'Dealer\DealerDashboardController@postRequestCompletion');
        Route::post('/apps/sign-completion/', 'Dealer\DealerDashboardController@postSignCompletion'); // dealer submits signature post
        Route::get('/apps/sign-completion/{id}', 'Dealer\DealerDashboardController@signCompletion');

        Route::get('/apps/sign-ach/{id}', 'Dealer\DealerSignController@getSignAch'); // dealer submits signature post
        Route::post('/apps/sign-ach', 'Dealer\DealerSignController@postSignAch');

        Route::get('/apps/cancel/{id}', 'Dealer\DealerApplicationController@confirmCancel');
        Route::post('/apps/cancel/{id}', 'Dealer\DealerApplicationController@cancel');

        Route::get('/apps/reset-sales-slip/{id}', 'Dealer\DealerApplicationController@confirmResetSalesSlip');
        Route::post('/apps/reset-sales-slip/{id}', 'Dealer\DealerApplicationController@resetSalesSlip');

        Route::get('/apps/files/{appId}/{fileId}', 'Dealer\DealerFileController@view');
        Route::get('/upload-file/{appId?}', 'Dealer\DealerFileController@viewUploadFile');
        Route::post('/upload-file', 'Dealer\DealerFileController@handleUpload');
        Route::post('/assign-file', 'Dealer\DealerFileController@assignUpload');


        Route::get('/', 'Dealer\DealerDashboardController@dashboard');
        Route::controller('register', 'Dealer\DealerRegisterController');
        Route::controller('users', 'Dealer\DealerUserController');
        Route::controller('contracts', 'Dealer\DealerContractController');
        Route::controller('settings', 'Dealer\DealerSettingsController');
        Route::controller('apps', 'Dealer\DealerApplicationController');
    });
});

// staff routing
// authentication is in each controller right now
Route::group(['domain' => 'staff.' . getenv('DOMAIN')], function()
{

	Route::get('/', ['middleware' => 'auth', 'uses' => 'DashboardController@index']);
	Route::get('/my-profile', ['middleware' => 'auth', 'uses' => 'DashboardController@profile']);
    
    // manual page routes do this moving forward
    Route::get('/settings/dealers/paperwork-requests', 'DealerPaperworkController@paperworkRequests');
    Route::get('/settings/dealers/paperwork-requests/{id}', 'DealerPaperworkController@paperworkRequest');
    Route::post('/settings/dealers/paperwork-sent/{id}', 'DealerPaperworkController@paperworkSent');

    // products and related
    Route::get('/settings/distributor/product/{id}', 'Settings\ProductController@view');
    Route::post('/settings/distributor/product/{id}/pmtFactor', 'Settings\ProductController@createPmtFactor');

    Route::get('/settings/apps', 'SettingsController@viewAppSettings');

    // new promos (not legacy promos)
    Route::get('/distributors/profile/{id}/promos', 'DistributorController@setPromos');
    Route::post('/distributors/profile/setPromos', 'DistributorController@sendSetPromos');

    // dealer payment factors
    Route::post('/distributors/profile/pmt-factor', 'DistributorController@setPmtFactor');

    // dealer update states
    Route::post('/distributors/profile/{id}/updateState', 'DistributorController@updateState');

    Route::delete('distributors/profile/{id}/delete-dealer/{dealerId}', 'DistributorController@deleteDealer');
    
    Route::get('distributors/profile/{id}/disable-dealer/{dealerId}', 'DistributorController@disableDealer');
    Route::get('distributors/profile/{id}/approve-dealer/{dealerId}', 'DistributorController@approveDealer');




    // app views
    Route::get('apps/', 'ApplicationController@index');
    Route::get('apps/approved', 'ApplicationController@approved');
    Route::get('apps/assigned', 'ApplicationController@sold');
    Route::get('apps/funding-requested', 'ApplicationController@fundingRequested');
    Route::get('apps/funded', 'ApplicationController@funded');
    Route::get('apps/search', 'ApplicationController@search');
    Route::post('apps/filter', 'ApplicationController@setFilter');

    Route::get('apps/{id}/credit-app', 'ApplicationController@viewCreditAgreement');
    Route::get('apps/{id}/sales-slip', 'ApplicationController@viewSalesSlip');
    Route::get('apps/{id}/notice-cancellation', 'ApplicationController@viewNoticeCancellation');
    Route::get('apps/{id}/completion', 'ApplicationController@viewCompletion');
    Route::get('apps/{id}/ach', 'ApplicationController@viewAch');
    Route::get('apps/{id}/view', 'ApplicationController@getAppData');
    Route::get('apps/{id}/files', 'ApplicationController@allFiles');
    Route::post('apps/{id}/buy-rate', 'ApplicationController@setBuyRate');

    Route::get('apps/file/{id}', 'ApplicationController@viewFile');
    Route::get('files/{id}/review', 'FileController@reviewFile');
    Route::post('files/{id}/review', 'FileController@postReviewFile');


    // clear consumers
    Route::post('apps/clear-consumer', 'ApplicationController@clearConsumer');
    // fca assign id manually
    Route::post('apps/assign-fca', 'ApplicationController@associateWithFCA');

    // these all manually protect auth in controller constructors
	Route::controller('distributors', 'DistributorController');
	Route::controller('contracts', 'ContractController');
	Route::controller('users', 'UserController');
	Route::controller('settings', 'SettingsController');
	Route::controller('comments', 'CommentController');
    Route::controller('payments', 'PaymentController');
    Route::controller('applications', 'ApplicationController');

    //logout & login route, works for both user types
    Route::get('/login', 'LoginController@loginView');
    Route::post('login', 'Auth\AuthController@login');
    Route::post('/password/reset', 'Auth\PasswordController@reset');
    Route::get('/password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    Route::post('/password/email', 'Auth\PasswordController@sendResetLinkEmail');

	// load profiles for distributors (note to also define the company model in the routerserviceprovider file)
	Route::get('distributors/profile/{company}', 'DistributorController@profile');
});


Route::get('/home', 'LoginController@homeRedirect');
Route::get('/cs', 'Auth\AuthController@checkSession');
Route::get('/logout', 'UserController@logout');

Route::controller('api', 'Api\ApiController');
