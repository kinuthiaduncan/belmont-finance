<?php namespace App\Http\Controllers\Api;
use App\Http\Controllers\Base_Controller as Controller;
use Illuminate\Http\Request;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\Dealer;
use Carbon\Carbon;
use Session;

class ApiController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        abort('API currently disabled', 403);
        // admin except for logout
        $this->middleware('api.auth', ['except' => ['postAuth', 'getCheckApps', 'postGruntTasks', 'postRemoteApp']]);
        $this->middleware('cors');
        $this->middleware('auth.basic', ['only' => ['postGruntTasks']]);

        $this->monitorPath = getenv('MONITOR_APP_DIR');
    }


	public function postAuth(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
                exit;
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
            exit;
        }

        $currentDealer = Dealer::where('email', $request->input('email') )->first();

        if($currentDealer){
          if($currentDealer->user_status != "Approved" || $currentDealer->company->dealer_status != "Approved"){
            return response()->json(['error' => 'NA'], 401);
            exit;
          }
        }else{
          return response()->json(['error' => 'NA'], 401);
          exit;
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    public function postGruntTasks(){
        shell_exec('grunt build');
        shell_exec('php artisan route:cache');
    }

    private function truncate($string, $length) {
        return trim((strlen($string) > $length) ? substr($string, 0, $length) : $string);
    }

    public function postRemoteApp(Request $request)
    {

        $user = $this->simpleAuth($request);


        Auth::guard('dealer')->login($user);

        $this->saveExternalApp($request);

        return redirect()->action('Dealer\DealerApplicationController@getSign');
    }



    /**
    * Really simple authentication method just for this open dealer api post to sign
    */
    private function simpleAuth(Request $request)
    {
        // require dealer email and token
        if(!$request->dealer_email || !$request->api_token) {
            abort('404');
        }

        $dealer = Dealer::where('email', '=', $request->dealer_email)
            ->where('api_token', '=', $request->api_token)
            ->whereNotNull('api_token')->first();

        if(!$dealer) {
            abort('503', 'auth failure');
        }

        return $dealer;
    }


    /**
    * Take an externally posted application, add it to records, and get ready for signing
    */
    private function saveExternalApp(Request $request)
    {   

        $fieldNameArray = [
            'birthdate','numDependents','firstName','initial','lastName','ssn','address','addressTwo','yrsAtAddress','employerCityAndState','monthsAtAddress','city','state','zip','homePhone','cellPhone','email','refLandlordMonthlyPayment','refPropertyValue',
            'employer','employerCity','employerState','employerPhone','employerZip','employerExt','jobTitle','monthlyGrossPay','yrsEmployed','milRank','monthsEmployed','refLandlordName','refLandlordPhone',
            'coBirthdate','coInitial','coAddress','coCity','coState', 'prevCity', 'prevState', 'prevZip','co_ssn','coFirstName',
            'coZip','coHomePhone','coCellPhone','coEmail','coEmployer','coEmployerPhone','coJobTitle','coMonthlyGrossPay','coYrsEmployed','coMonthsEmployed','coEmployerCity','coEmployerState','coEmployerZip',
            'ach_amount','ach_day','ach_bank_name','ach_bank_routing','ach_bank_account','bank_address','ach_branch_name','ach_bank_city','ach_bank_state','ach_bank_zip',
            'driversOrId','licExpDate','coDriversOrId','residenceType','accountType','ach_account_type','down_payment_amount','refLandlordAddress','signerName','cosignerName',
            'licIssueDate','licExpDate'
        ];

        $session = $request->session();
        $sessionKey = 'extapp' . microtime(true);

        // set session key to allow for reloading app
        $session->set('dealer.currentSessionKey', $sessionKey);
        $session->set('dealer.orderUrl', $request->order_url);
        $session->set('dealer.externalStore', $request->return_url);

        foreach($fieldNameArray as $fieldName) {
            $session->set($sessionKey . '.' . $fieldName, '');
        }



        // manually map posted variables
        $session->set($sessionKey . '.' . 'firstName', $request->first_name);
        $session->set($sessionKey . '.' . 'lastName', $request->last_name);
        $session->set($sessionKey . '.' . 'residenceType', $request->home_status);
        $session->set($sessionKey . '.' . 'ach_account_type', 'checking');
        $session->set($sessionKey . '.' . 'email', $request->email);
        $session->set($sessionKey . '.' . 'state', $request->state);
        $session->set($sessionKey . '.' . 'zip', $request->zip);
        $session->set($sessionKey . '.' . 'ssn', $request->ssn);
        $session->set($sessionKey . '.' . 'address', $request->address);
        $session->set($sessionKey . '.' . 'addressTwo', $request->address_unit);
        $session->set($sessionKey . '.' . 'city', $request->city);
        $session->set($sessionKey . '.' . 'birthdate', $request->date_of_birth);
        $session->set($sessionKey . '.' . 'homePhone', $request->phone);
        $session->set($sessionKey . '.' . 'monthlyGrossPay', $request->monthly_income);

        $session->set($sessionKey . '.' . 'coFirstName', $request->co_first_name);
        $session->set($sessionKey . '.' . 'coLastName', $request->co_last_name);
        $session->set($sessionKey . '.' . 'coAddress', $request->co_address);
        $session->set($sessionKey . '.' . 'coCity', $request->co_city);
        $session->set($sessionKey . '.' . 'coState', $request->co_state);
        $session->set($sessionKey . '.' . 'co_ssn', $request->co_ssn);
        $session->set($sessionKey . '.' . 'coBirthdate', $request->co_date_of_birth);

        $session->set($sessionKey . '.' . 'ach_amount', $request->ach_monthly_withdrawal_amount);
        $session->set($sessionKey . '.' . 'ach_day', $request->ach_monthly_withdrawal_date);
        $session->set($sessionKey . '.' . 'ach_bank_name', $request->ach_depisitoryName);
        $session->set($sessionKey . '.' . 'ach_bank_account', $request->ach_account_number);
        $session->set($sessionKey . '.' . 'ach_bank_routing', $request->ach_routing_number);
        $session->set($sessionKey . '.' . 'down_payment_amount', $request->down_payment);

        $session->set($sessionKey . '.' . 'amount_financed', $request->financing_purchase_amount);
        $session->set($sessionKey . '.' . 'loanLength', $request->financing_payment_term);

        $session->set($sessionKey . '.' . 'apr', $request->financing_annual_percentage_rate * 100);

        $session->set($sessionKey . '.' . 'credit_type', 'closed');
        $session->set($sessionKey . '.' . 'doc_fees', $request->financing_documentation_fee);

        // default fill all blank to avoid empty index error in other method
        /*foreach($fieldNameArray as $fieldName) {
            if(!$session->get($sessionKey . '.' . $fieldName)) {
                $session->set($sessionKey . '.' . $fieldName, '');
            }
        }*/

        return true;
    }

}
