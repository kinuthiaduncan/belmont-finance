<?php

namespace App\Http\Controllers\Auth;

use App\Dealer;
use Validator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Session;
use Auth;
use Illuminate\Http\Request;
use Cookie;
use Route;

class DealerAuthController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new dealers, as well as the
    | authentication of existing dealers.
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins, AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * Where to redirect dealers after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $guard = 'dealer';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([$this->guestMiddleware()], ['except' => ['logout', 'chooseDistributor']]);
        // dd(Route::getCurrentRoute()->getActionName());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:dealers',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Dealer::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

        /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        //$did = Request::cookie('name');
        //$distributor = \App\Models\Distributor::find();
        return view('auth.login', ['context' => 'dealer']);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::guard($this->getGuard())->logout();
        Session::forget('admin-pseudo');
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }


    /* verifies and sets the dealer's chosen distributor that they login as */
    public function chooseDistributor(Request $request, $id) {
        $request->session()->forget('selectedDistributorId');

        // make sure dealer is under distributor
        // staff login as dealer
        if($pseudoId = Session::get('admin-pseudo')){
            $dealer = \App\Models\Dealer::where('id', '=', $pseudoId)->first();
        } else {
            $dealer = Auth::guard('dealer')->user();
        }
        
        if( $dealer->companies()->where('distributor_id', $id)->exists() ) {
            $distributor = \App\Models\DealerCompany::findOrFail($id);
            $request->session()->set('selectedDistributorId', $id);
            return redirect('/');
        } else {
            abort('403', 'Authorization Error');
        }
    }


    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->action('Auth\DealerAuthController@showLoginForm')
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

}
