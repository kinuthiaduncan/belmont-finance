<?php namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;


class FileController extends \App\Http\Controllers\Base_Controller {

	/*
	|--------------------------------------------------------------------------
	| Dashboard Controller
	|--------------------------------------------------------------------------
	|
	|
	*/
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}

	public function getFile($id)
	{
		$file = \App\Models\File::findOrFail($id);
		$this->authFile($file);

		return response()->file(base_path($file->local_path));
	}

	private function authFile($file)
	{
		if($this->user->temp_app_id != $file->app_id || ($this->user->contract && $this->user->contract->app->id != $file->app_id)) {
			abort(404);
		}
		return true;
	}
}
