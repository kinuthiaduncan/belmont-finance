<?php namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use Validator;
use Session;
use \App\Models\Payment;
use \App\Models\PaymentRecurring;
use \App\Models\AchPayment;
use \App\Models\Contract;
use Crypt;
use Mail;
use Log;
use Carbon\Carbon;
use BrowserDetect;


class PaymentController extends \App\Http\Controllers\Base_Controller {

	/*
	|--------------------------------------------------------------------------
	| Dashboard Controller
	|--------------------------------------------------------------------------
	|
	|
	*/
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}

	public function getOneTime(){
		// get content from craft to display notices
		$httpClient = new \GuzzleHttp\Client();
		$https = getenv('APP_ENV') == "local" ? 'http://' : 'https://';
		try{
			$response = $httpClient->get($https . getenv('DOMAIN') . '/customerContent.json');
			$data = json_decode($response->getBody());
		}
		catch(\Exception $e) {
			$data = new \stdClass();
			$data->data[0] = 'Server connection issue';
		}

		return view('accounts/payments/one-time' ,['content' => []]);
	}




	public function getRecurring(){

		return view('accounts/payments/recurring', ['recurring' => $this->contract->hasRecurring() ]);

	}




	public function postCancelRecurring(Request $request){

			$staffEmail = \App\Models\Setting::where('name', '=', 'paymentEmails')->first()->value;
            $staffEmail = explode(",", str_replace(' ', '', $staffEmail ));
            
            $emails = [];
            foreach($staffEmail as $email)
            {
                $emails[$email] = $email;
            }

            if($request->has('tac')) {
                $payment = \App\Models\AchPaymentRecurring::where('contract_id', $this->contract->id)->first();
                $ach = true;
            } else {
		  		$payment = \App\Models\PaymentRecurring::where('contract_id', $this->contract->id)->first();
                $ach = false;
			}

            // ensure that customer owns recurring payment
            if( $this->contract->customer->id == $payment->customer_id ) {
    			$payment->requested_removal = 1;
    			$payment->save();

            	$contract = $this->contract;

            	$protocol = getenv('PRODUCTION') == "FALSE" ? 'http://' : 'https://';

                $url = $ach ? $protocol . 'staff.' . getenv('DOMAIN') . '/payments/recurring/' . $payment->id : $protocol . 'staff.' . getenv('DOMAIN') . '/payments/recurring-card/' . $payment->id;

            	Mail::send('emails.staff-recurring-payment-remove', ['url' => $url], function ($m) use($emails, $contract, $ach){
      				$name = $contract->customerName;
                    $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
                    $subject = $ach ? "ACH cancellation request - $name" : "CC cancellation request - $name";
                    $m->to($emails)->subject($subject);
                });
            }

		return view('accounts/payments/response-cancel-recurring');

	}

	public function postProcessCard(Request $request){

		$validator = Validator::make($request->all(), [
			'cc' =>'required|digits_between:12,19|luhns',
			'cvv' => 'required|digits_between:3,4',
			'card_name' => 'required|min:3|string',
			'amount' => 'numeric',
			'exp_month' => 'required|digits:2|min:1,max:12',
			'exp_year' => 'required|digits:4'
		]);

		if ( $validator->fails() ) {

			$failedString = '';
			foreach($validator->getMessageBag()->all() as $message) {
				$failedString .= $message . ', ';
			}

			$logMessage = 'CC Pay Validation #' . $this->contract->accountNumber . ': ' . $failedString;
			Log::info($logMessage);
            return redirect('payments/one-time')
                ->withErrors($validator)
                ->withInput();
        } else {

        	// check for existing payment, only allow one
        	$existingTransaction = Payment::whereContractId($this->contract->id)->where('payment_date', '=', Carbon::now()->format('Y-m-d'))->get();
        	if(!$existingTransaction->isEmpty()) {
        		// there is a payment already
        		$back = false;
        		$summary = "Payment already scheduled.";
        		$details = "You already have a credit / debit card payment scheduled. Please wait for a future date to make another payment.";
        	} else {
	    		// continue with transaction
	        	$result = $this->processPayment($request);

	        	if($result['approved']){

	        		$paymentLog = Payment::create();
	        		$paymentLog->contract_id = Session::get('contractId', $this->contract->id);
	        		$paymentLog->amount = floatval( $request->input('amount') );
	        		$paymentLog->payment_source = substr( $request->input('cc'), -4);
	        		$paymentLog->payment_date = date('Y-m-d');
	        		$paymentLog->save();

	        		$summary = 'Payment was accepted.';
	        		$details = '';
	        		$back = false;

	        		// if staff logs in as customer to make a payment there might not be a customer relation
	        		if($this->contract->customer) {
		                Mail::send('emails.customer-single-payment-success', ['name' => $this->contract->customer->first_name], function ($m){
	                        $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
		                    $m->to($this->contract->customer->email)->subject("Payment Submittal");
		                });
		            }

	        	}else{
	        		$summary = 'Payment failed.';
	        		$details = $result['message'];
	        		Log::warning('CC Payment Fail: ' . $details .', ' . Session::get('contractId', $this->contract->id));
	        		$request->flashExcept('cc', 'ccv');
	        		$back = true;
	        	}
	        }

			return view('accounts/payments/response', ['summary' => $summary, 'details' => $details, 'back' => $back]);

        }
	}



	public function postProcessAch(Request $request){
		$this->logUA('Single ACH');

		$validator = Validator::make($request->all(), [
			'routing_number' => 'required|digits:9|confirmed|aba',
			'account_number' => 'required|numeric|confirmed|digits_between:4,16',
			'account_type' => 'required',
			'amount_ach' => 'numeric|min:0|max:9999',
			'electronic_signature' => 'required|regex:/(^[A-Za-z ]+$)+/|max:32',
			'payment_date' => 'required|date|after:' . date('Y-m-d', strtotime("-1 day")) . '|before:' . date('Y-m-d', strtotime("+1 month"))
		]);
		// looks for most recently scheduled one time ACH payment
		if ($this->contract) {
			$paymentQuery = AchPayment::where('customer_id', '=', $this->contract->customerId)->orderBy('payment_date', 'desc')->first();
			$currentAchDate = ($paymentQuery ? $paymentQuery->payment_date : null);
		}

		if ($validator->fails()) {
				return redirect('payments/one-time')
                ->withErrors($validator)
                ->withInput();
        }
		// check if customer already has a payment scheduled on this date
		else if ($currentAchDate == $request->payment_date) {
				return view('accounts/payments/response', ['summary' => 'Duplicate Payment Error', 'details' => 'You already scheduled an ACH payment for this date. Please delete the existing payment if you\'d like to submit a new one.', 'back' => true]);
		}
		else{
        	$payment = AchPayment::create($request->only('account_type', 'routing_number', 'electronic_signature', 'payment_date'));
            $payment->amount = $request->amount_ach;
        	$payment->account_number = Crypt::encrypt($request->input('account_number'));
        	$payment->belmont_account_number = $this->contract->accountNumber;
			$payment->is_savings_account = $request->input('account_type');
        	$payment->customer_id = $this->contract->customer ? $this->contract->customer->id : null;
        	$payment->signature_date = date('Y-m-d');
        	$payment->save();

            // email to customer payment notification
			$name = ($this->contract->customer ? $this->contract->customer->first_name : "null");
            Mail::send('emails.customer-single-payment-success', ['name' => $name], function ($m){
                $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
                $m->to($this->contract->customer->email)->subject("Payment Submittal");
            });
        	return view('accounts/payments/response', ['summary' => 'ACH payment submitted', 'details' => 'Your ACH payment has been submitted. We will contact you if there are any issues processing this.', 'back' => true]);
        }
	}

	public function postProcessRecurringAch(Request $request){
		$validator = Validator::make($request->all(), [
			'routing_number' => 'required|aba',
			'account_number' => 'required|numeric|digits_between:4,16',
			'account_type' => 'required',
			'amount' => 'required|min:0.01|max:9999|numeric',
			'electronic_signature' => 'required',
			'withdrawal_day' => 'required|min:1|max:28|numeric',
			'start_date' => 'required|date|after:' . date('Y-m-d') . '|before:' . date('Y-m-d', strtotime("+1 year"))
		]);

		if ($validator->fails()) {
            return redirect('payments/recurring')
                ->withErrors($validator)
                ->withInput();
        }else{
        	$payment = \App\Models\AchPaymentRecurring::create($request->only('account_type', 'routing_number', 'amount', 'electronic_signature', 'withdrawal_day'));
        	$payment->account_number = Crypt::encrypt($request->input('account_number'));
        	$payment->belmont_account_number = $this->contract->accountNumber;
			$payment->is_savings_account = $request->input('account_type');
            $payment->frequency = strtoupper($request->input('frequency'));
            $payment->start_date = Carbon::parse($request->input('start_date'))->format('Y-m-d');
        	$payment->contract_id = $this->contract->id;
        	$payment->customer_id = $this->contract->customer ? $this->contract->customer->id : null;
        	$payment->signature_date = date('Y-m-d');
        	$payment->save();

        	$staffEmail = \App\Models\Setting::where('name', '=', 'paymentEmails')->first()->value;

            $staffEmail = explode(",", str_replace(' ', '', $staffEmail ));
            $emails = [];
            foreach($staffEmail as $email)
            {
                $emails[$email] = $email;
            }

        	$contract = $this->contract;

        	$protocol = getenv('PRODUCTION') == "FALSE" ? 'http://' : 'https://';

            $url =  $protocol . 'staff.' . getenv('DOMAIN') . '/payments/recurring/' . $payment->id;

            // send emails to staff and customer notifying of recurring payment request
        	Mail::send('emails.staff-recurring-payment-add', ['url' => $url], function ($m) use($emails, $contract){
  				$name = $contract->customerName;
                $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
                $m->to($emails)->subject("ACH recurring request - $name");
            });

        	if($contract->customer) {
	            Mail::send('emails.customer-recurring-payment-request', ['name' => $contract->customer->first_name], function ($m) use($contract){
	                $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
	                $m->to($contract->customer->email)->subject("ACH request sent");
	            });
	        }

        	return view('accounts/payments/response', ['summary' => 'ACH payment submitted', 'details' => 'Your ACH recurring payment has been submitted. We will contact you if there are any issues processing this.', 'back' => true]);
        }
	}


	protected function processPayment($request){
    	$amount = number_format( floatval( $request->input('amount') ), 2, '.', '');

    	// testing, disable ssl requirements for localhost
    	if( getenv('ELAVON_TESTING') == 'true' ){
    		$verify = false;
    	}else{
    		$verify = true;
    	}

    	$httpClient = new \GuzzleHttp\Client();

        $formParams = [
            'ssl_merchant_id' => getenv('ELAVON_ID'),
            'ssl_user_id' => getenv('ELAVON_ACCOUNT'),
            'ssl_pin' => getenv('ELAVON_SECRET'),
            'ssl_test_mode' => getenv('ELAVON_TESTING'),
            'ssl_amount' => $amount,
            'account_credit' => $request->input('amount'),
            'acct_first' => explode(' ',trim($this->contract->customerName))[0],
            'acct_last' => isset(explode(' ',trim($this->contract->customerName))[1]) ? explode(' ',trim($this->contract->customerName))[1] : '',
            'belmont_no' => $this->contract->accountNumber,
            'is_recurring' => 0,
            'ssl_show_form' => 'false',
            'ssl_transaction_type' => 'ccsale',
            'ssl_cvv2cvc2_indicator' => '1',
            '4_ssn' => '0000',
            'ssl_card_number' => $request->input('cc'),
            'ssl_exp_date' => $request->input('exp_month') . substr($request->input('exp_year'), -2),
            'ssl_cvv2cvc2' => $request->input('cvv'),
            'ssl_avs_address' => false,
            'ssl_avs_zip' => false,
            'ssl_result_format' => 'ASCII',
            'ssl_email' => 'test@belmontfinancellc.com',
        ];

		$response = $httpClient->post( getenv('ELAVON_GATEWAY') , [
			'form_params' => $formParams,
			'verify' => $verify
		]);

        //retrieve document response back from elavon
        $doc = new \DOMDocument();
        libxml_use_internal_errors(true);
        $responseBody = $response->getBody();
        $responseContents = $responseBody->getContents();
        $responseContents = str_replace("\n", ",", $responseContents);
        preg_match_all("/([^,=]+)=([^,=]+)/", $responseContents, $r);
        $result = array_combine($r[1], $r[2]);
        $doc->loadHTML($responseContents);

        // verify approved or unnapproved
        if ( $result && isset($result['ssl_result_message']) ) {
            if ($result['ssl_result'] === "0") {
                return ['approved' => true, 'message' => 'Your payment was accepted.'];
            } else {
                return ['approved' => false, 'message' => 'ERROR: ' . $result['ssl_result_message']];
            }
        } else {
              $error = $result['errorMessage'];
              Log::warning($result['errorMessage']);

              // log transaction details
              $formParams['ssl_card_number'] = '';
              $formParams['ssl_cvv2cvc2'] = '';
              Log::warning(print_r($formParams, true));
              return ['approved' => false, 'message' => 'Error with transaction: ' . $error . ' Please try again, correct your credit card information if possible, try again later, or contact Belmont Finance if you keep seeing this'];
        }

	}

	public function postProcessRecurringCard(Request $request) {
		$validator = Validator::make($request->all(), [
			'cc' => 'required|digits_between:12,19|luhns',
			'card_name' => 'required|min:3|max:64|string',
			'card_type' => 'required',
			'exp_month' => 'required|min:1|max:12|numeric',
			'exp_year' => 'required|numeric',
			'exp_date' => 'required|date|after:' . date('Y-m-d'),
			'withdrawal_day' => 'required|min:1|max:28|numeric',
			'payment_start_date' => 'required|date|after:' . date('Y-m-d') . '|before:' . date('Y-m-d', strtotime("+1 year")),
			'card_amount' => 'required|min:0.01|max:9999|numeric',
			'final_amount' => 'required|numeric|between:0.01,9999'
		]);

		if ($validator->fails()) {
			return redirect('payments/recurring')->withErrors($validator)->withInput();
    } else {
			$payment = PaymentRecurring::create($request->only('card_name', 'withdrawal_day'));
			$payment->start_date = $request->input('payment_start_date');
			$payment->amount = $request->input('card_amount');
			$payment->cc = Crypt::encrypt($request->input('cc'));
			$payment->exp_month = $request->input('exp_month');
			$payment->exp_year = $request->input('exp_year');
			$payment->belmont_account_number = $this->contract->accountNumber;
			$payment->card_type = $request->input('card_type');
			$payment->frequency = strtoupper($request->input('card_frequency'));
			$payment->contract_id = $this->contract->id;
			$payment->customer_id = $this->contract->customer ? $this->contract->customer->id : null;
			$payment->signature_date = date('Y-m-d');

    		$final_amount = number_format(floatval( $request->input('card_amount') ), 2, '.', '');

			$payment->final_amount = $final_amount;
			$payment->save();
		}

		$contract = $this->contract;
		$staffEmail = \App\Models\Setting::where('name', '=', 'paymentEmails')->first()->value;
			$staffEmail = explode ( ",", $staffEmail );
			$emails = [];
			foreach($staffEmail as $email) {
					$emails[$email] = $email;
			}

		$protocol = getenv('PRODUCTION') == "FALSE" ? 'http://' : 'https://';

		$url =  $protocol . 'staff.' . getenv('DOMAIN') . '/payments/recurring-card/' . $payment->id;

		// send emails to staff and customer notifying of recurring payment request
		Mail::send('emails.staff-recurring-payment-add', ['url' => $url], function ($m) use($emails, $contract){
            $name = $contract->customerName;
            $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
			$m->to($emails)->subject("Debit card recurring request - $name");
		});

		Mail::send('emails.customer-recurring-payment-request', ['name' => $contract->customer->first_name, 'card' => true], function ($m) use($contract){
            $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
			$m->to($contract->customer->email)->subject("Recurring Credit / Debit Card Request");
		});

		return view('accounts/payments/response', ['summary' => 'Card payment submitted', 'details' => 'Your recurring card payment has been submitted. We will contact you if there are any issues processing this.', 'back' => true]);

	}

    // displays a view to confirm cancelling payment to consumer
    public function getConfirmCancelPayment(Request $request, $paymentId){
        $payment = AchPayment::find($paymentId);

        // protect  - eg make sure it's the right account number and payment exists
        if($payment && $this->contract->accountNumber == $payment->belmont_account_number){
            return view('accounts/payments/cancel-payment', ['payment' => $payment]);
        } else {
            abort('404');
        }
    }

    // displays a view to confirm cancelling payment to consumer
    public function postConfirmCancelPayment(Request $request){
        $payment = AchPayment::find($request->input('pid'));

        // protect  - eg make sure it's the right account number and payment exists
        if($payment && $this->contract->accountNumber == $payment->belmont_account_number){
            if( (date('Y-m-d') == $payment->payment_date && date('h') < 17) || date('Y-m-d') < $payment->payment_date){
                $payment->delete();

                Mail::send('emails.customer-single-payment-cancellation', ['name' => $this->contract->customer->first_name], function ($m){
                    $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
                    $m->to($this->contract->customer->email)->subject("ACH Payment Cancellation");
                });
                return redirect('/');
            } else {
                abort('403', 'This cancellation request is after the cutoff time.');
            }
        } else {
            abort('404');
        }
    }

    // returns sample schedules to consumer that shows them how recurring ach will look and work.
    public function getRecurringExample(Request $request){
        $amount = $request->input('amount');
        $frequency = $request->input('frequency');
        $day = $request->input('day');

        $today = Carbon::now();
        $firstDate = Carbon::now();
        $firstDate->day = $day;

        $firstDate = $today->diffInDays( $firstDate, false ) > 3 ? $firstDate : $firstDate->addMonth();

        if($frequency == "Monthly"){
            $amount = number_format($amount, 2);
            $output = "For example, your first payment of \$$amount would be on the " . $firstDate->format('jS \\of F') .
            " and your next payment of the same amount would be next month on the " . $firstDate->addMonth(1)->format('jS \\of F');
        } elseif ($frequency == "Biweekly") {
            $secondDate = clone $firstDate;
            $secondDate->addDays(14);
            $output = "With a biweekly payment schedule, your first payment of \$$amount would be on the " . $firstDate->format('jS \\of F') .
                ", another payment of \$$amount would go through on the " . $secondDate->format('jS \\of F') .
                ", and \$$amount would again be deducted on the " .  $secondDate->addDays(14)->format('jS \\of F');

        } elseif ($frequency == "Weekly") {
            $secondDate = clone $firstDate;
            $secondDate->addDays(7);
            $output = "Example payment schedule: <br>";
            $output .= "<span class='small'>" . $firstDate->format('F j') . ': $' . $amount . '</span><br>';
            $output .= "<span class='small'>" . $secondDate->format('F j') . ': $' . $amount . '</span><br>';
            $output .= "<span class='small'>" . $secondDate->addDays(7)->format('F j') . ': $' . $amount . '</span><br>';
            $output .= "<span class='small'>" . $secondDate->addDays(7)->format('F j') . ': $' . $amount . '</span><br>';
        }
        return $output;
    }

    protected function logUA($type){
    	/*$browser = BrowserDetect::detect()->toString();
    	Log::info($this->contract->accountNumber . ' just submitted ' . $type . ', ' . $browser);*/
    }

}
