<?php namespace App\Http\Controllers\Customer;

use \App\Models\Contract;
use \App\Models\Payment;
use \App\Models\AchPayment;
use \App\Models\Customer as Customer;
use Illuminate\Http\Request;
use Session;
use Validator;
use Mail;
use Hash;
use Crypt;
use Log;

class DashboardController extends \App\Http\Controllers\Base_Controller {

	/*
	|--------------------------------------------------------------------------
	| Dashboard Controller
	|--------------------------------------------------------------------------
	|
	|
	*/
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		// get content from craft to display on dashboard
		$httpClient = new \GuzzleHttp\Client();
		$https = getenv('APP_ENV') == "local" ? 'http://' : 'https://';
		try {
			$response = $httpClient->get($https . getenv('DOMAIN') . '/customerContent.json');
			$data = json_decode($response->getBody());
		} catch(\Exception $e) {
			$data = new \stdClass();
			$data->data[0] = 'Server connection issue';
		}

		// get both ach and cc payments and merge them
		$recentPayments = Payment::whereContractId($this->contract->id)->limit(25)->orderBy('payment_date', 'DESC')->get()->toArray();
		$recentAchPayments = AchPayment::where('belmont_account_number', '=', $this->contract->accountNumber)->limit(25)->where('payment_date', '<', date('Y-m-d'))->orderBy('payment_date', 'DESC')->get()->toArray();
		foreach($recentAchPayments as &$apay){
			$apay['payment_source'] = substr(Crypt::decrypt($apay['account_number']), -4);
		}
		$pmts = array_merge($recentPayments, $recentAchPayments);

		// sort array by date
		usort($pmts, array($this, 'date_compare'));

		// now retrieve payments in the future (ach only)
		$futureAchPayments = AchPayment::where('belmont_account_number', '=', $this->contract->accountNumber)->where('payment_date', '>=', date('Y-m-d'))->orderBy('payment_date', 'DESC')->get();
		foreach($futureAchPayments as &$apay){
			$apay['payment_source'] = substr(Crypt::decrypt($apay['account_number']), -4);
			$apay['deletable'] = ( date('Y-m-d') == $apay->payment_date && date('h') >= 17 ) ? '0' : '1';
		}

		return view('accounts/dashboard', ['payments' => $pmts, 'content' => [], 'contract' => $this->contract, 'futurePayments' => $futureAchPayments] );
	}


	/* first step of registration */
	public function verifyReg(Request $request){

		if ($request->isMethod('post')) {
			$this->validate($request, [
				'fullName' => 'required|min:3|regex:/[a-zA-Z]{2,}\s[a-zA-Z]{2,}/',
				'accountNumber' => 'required|min:4'
			]);

			$accountNumber = $request->input('accountNumber');

			if(!$accountNumber){
				return redirect('login')->with('regerror', 'No account with that account number / name found');
			}

			$contract = Contract::where('accountNumber', '=', trim($accountNumber))
                ->orWhere('accountNumber', '=', ltrim($accountNumber, '0'))
                ->orWhereRaw("accountNumber REGEXP '[S,X]" . trim($accountNumber) . "$'")
                ->first();

			// verify account number
			if(!$contract){
				return redirect('login')->with('regerror', 'No account with that account number / name found');
			}
			else{

				// verify last name - same m
				if( strtolower(trim($contract->customerName)) != strtolower(trim($request->input('fullName'))) ){
					return redirect('login')->with('regerror', 'No account with that account number / name found');
				}


				if($contract->statusId != 1){
					return redirect('login')->with('regerror', 'Your account is not eligible for online access at this time.');
				}

				// make sure contract account has not been set up yet.
				if($contract->customer){
					if($contract->customer->locked_at) {
						return response()->view('errors.accounts-locked');
					} else {
						return redirect('login')->with('regerror', 'Account already exists. If you don\'t remember your login credentials, try resetting your password.');
					}
				}



				Session::set('customer.accountNumber', $contract->accountNumber);

				return view('accounts/new-account');
			}
		}else{
			if(!Session::get('customer.accountNumber')){
				abort('403', 'Invalid registration attempt');
			}
			return view('accounts/new-account');
		}
	}

	public function expired(){
		return view('accounts/expired');
	}


		/* second step of registration */
	public function verifyRegTwo(Request $request){

		$validator = Validator::make($request->all(), [
			'password' => 'required|confirmed|min:8|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/',
			'email' => 'required|email|confirmed|unique:customers'
		]);

		 if ($validator->fails()) {
            return redirect('verify-reg')
                ->withErrors($validator)
                ->withInput();
        }else{
            Log::info('Registration attempt ' . Session::get('customer.accountNumber'));

        	$contract = Contract::where('accountNumber', '=', Session::get('customer.accountNumber'))->first();
            if($contract) {

    			$contractApp = ($contract ? $contract->app : null);

            	$customer = new Customer();
            	$customer->email = $request->input('email');
            	$customer->password = Hash::make($request->input('password'));
            	$customer->first_name = explode(" ", $contract->customerName)[0];
    					$last_name = explode(" ", $contract->customerName);
    					$customer->last_name = end($last_name);
            	$customer->contract_id = $contract->id;

            	if($contractApp)
            	{
            		$customer->address = $contractApp->address;
            		$customer->address_two = $contractApp->addressTwo;
            		$customer->city = $contractApp->city;
            		$customer->zip = $contractApp->zip;
            		$customer->phone = $contractApp->homePhone;
            		$customer->phone_two = $contractApp->cellPhone;
            	}

            	$customer->save();

            	$contract->customerId = $customer->id;
            	$contract->save();

            	Mail::send('emails.new-customer', ['user' => $customer], function ($m) use ($customer) {
    	            $m->to($customer->email, $customer->name)->subject('Belmont Finance Account Created');
    	        });

    	        return view('accounts/account-success');
            }
            else {
                abort('403', 'There was an error locating your contract in our systems. This could be because of page timeout or that you are trying to register for an invalid contract. Please either start the signup process again or contact Belmont Staff for help');
            }
        }
	}

	// allows staff to delete customer account so they can create a new account for certain issues.
	public function getResetAccount($contractId){
		if(Session::has('admin-pseudo-cust')) {
			return view('accounts/reset/approve-reset', ['contract' => $this->contract, 'id' => $contractId]);
		}
		else{
			abort('404');
		}
	}

	public function postResetAccount(Request $request){
		if(Session::has('admin-pseudo-cust') ) {
			$contract = Contract::findOrFail($request->input('id'));
			$customer = $contract->customer;
			if($customer) {
				$customer->forceDelete();
			}
			$contract->customerId = null;
			$contract->save();
			return redirect('/');
		}
		else{
			abort('404');
		}
	}


	/**
	 * Show a limited dashboard to user
	 *
	 * @return Response
	 */
	public function appFiles()
	{
		$files = \App\Models\File::where('app_id', '=', $this->user->temp_app_id)
			->whereIn('file_type', [1,2,5,9,11])->get();
		return view('accounts/app-files', ['files' => $files] );
	}



	public function sharedLogin(){
		return view('accounts/shared-login');
	}

    public function getPrivacyPolicy(){
        $headers = ['Content-Type: application/pdf'];
        return response()->download(base_path() . '/resources/assets/documents/privacy-policy/jan-2017-privacy-belmont-finance.pdf', 'jan-2017-privacy-belmont-finance.pdf', $headers);
    }

	// to sort an array by payment date
	private static function date_compare($a, $b)
	{
	    $d1 = strtotime($a['payment_date']);
	    $d2 = strtotime($b['payment_date']);
	    return $d2 - $d1;
	}
}
