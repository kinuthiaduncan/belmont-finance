<?php namespace App\Http\Controllers\Customer;

use \App\Models\Contract;
use \App\Models\Payment;
use \App\Models\AchPayment;
use \App\Models\Customer as Customer;
use Illuminate\Http\Request;
use Session;
use Validator;
use Mail;
use Hash;
use Crypt;

class RegisterController extends \App\Http\Controllers\Base_Controller {

	/*
	|--------------------------------------------------------------------------
	| Dashboard Controller
	|--------------------------------------------------------------------------
	|
	|
	*/
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * A new customer online registration screen
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		// ensure this is valid request
		$this->authToken($request);

		return view('accounts.register.index', ['attempts' => $request->session()->get('ssnAttempts', 0)]);
	}

	/**
	 * Registration form with password
	 *
	 * @return Response
	 */
	public function postIndex(Request $request)
	{
		$this->validate($request, [
	        'ssn' => 'required|digits:4',
	    ]);

		// ensure this is valid request
		$token = $this->authToken($request);

		// check SSN
		if($request->ssn != $token->app->ssn) {
			if($request->session()->has('ssnAttempts')) {
				if( $request->session()->get('ssnAttempts') > 3 ) {
					$token->delete();
				} else {
					$request->session()->put('ssnAttempts', $request->session()->get('ssnAttempts') + 1);
				}
			} else {
				$request->session()->put('ssnAttempts', 1);
			}

			return view('accounts.register.index', ['attempts' => $request->session()->get('ssnAttempts', 0)]);
		}

		$request->session()->put('registerApp', $token->app->id);

		return view('accounts.register.new-account', []);
	}

	/**
	 * Registration form with password
	 *
	 * @return Response
	*/
	public function postCreate(Request $request)
	{
		$this->validate($request, [
			'password' => 'required|confirmed|min:8|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/',
		]);

		if(!$request->session()->has('registerApp')) {
			abort(404);
		} else {
			$app = \App\Models\ContractApplication::findOrFail($request->session()->get('registerApp'));
		}

    	$customer = new Customer();
    	$customer->email = $app->email;
    	$customer->password = Hash::make($request->input('password'));
    	$customer->first_name = $app->firstName;
		$customer->last_name =$app->lastName;
    	$customer->contract_id = $app->contract ? $app->contract->id : '';
		$customer->address = $app->address;
		$customer->address_two = $app->addressTwo;
		$customer->city = $app->city;
		$customer->zip = $app->zip;
		$customer->phone = $app->homePhone;
		$customer->phone_two = $app->cellPhone;
		$customer->temp_app_id = $app->id;
    	$customer->save();

    	if($app->contract) {
	    	$app->contract->customerId = $customer->id;
	    	$app->contract->save();
    	}

    	// remove token
    	$token = \App\Models\Auth\CustomerToken::where('app_id', '=', $request->session()->get('registerApp'))->delete();
    	$request->session()->forget('registerApp');

    	return view('accounts.register.new-account-success', ['app' => $app]);
	}



	protected function authToken(Request $request)
	{
		$token = \App\Models\Auth\CustomerToken::where('token', '=', $request->token)
			->firstOrFail();

		return $token;
	}
}