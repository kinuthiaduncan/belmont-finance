<?php namespace App\Http\Controllers\Customer;

use \App\Models\Contract;
use \App\Models\Payment;
use \App\Models\Customer as Customer;
use Illuminate\Http\Request;
use Session;
use Validator;
use Mail;
use Hash;

class ProfileController extends \App\Http\Controllers\Base_Controller {

	/*
	|--------------------------------------------------------------------------
	| Dashboard Controller
	|--------------------------------------------------------------------------
	|
	|
	*/
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		$this->user = $this->contract->customer;
		view()->share('user', $this->user);
	}

	public function getIndex()
	{
		return view('accounts/profile');
	}

	public function postUpdateEmail(Request $request)
	{
		$this->validate($request, [
	        'email' => 'required|email|unique:customers',
	    ]);

	    $this->user->email = $request->input('email');
	    $this->user->save();

		if($request->ajax()){
			return response()->json('success');
		}
	}

	public function postUpdateProfile(Request $request)
	{
		$this->validate($request, [
	        'address' => 'min:4',
	        'address_two' => 'min:4',
	        'city' => 'min:3',
	        'state' => 'min:2|max:2',
					'zip' => 'min:5|max:10',
	    ]);

	    $this->user->address = $request->input('address') ?: $this->user->address;
	    $this->user->address_two = $request->input('address_two') ?: $this->user->address_two;
	    $this->user->city = $request->input('city') ?: $this->user->city;
	    $this->user->state = $request->input('state') ?: $this->user->state;
			$this->user->zip = $request->input('zip') ?: $this->user->zip;
	    $this->user->save();

			// alert staff that address has changed
			$staffEmail = \App\Models\Setting::where('name', '=', 'paymentEmails')->first()->value;
			$staffEmail = explode(",", str_replace(' ', '', $staffEmail ));
			$emails = [];
			foreach($staffEmail as $email) {
					$emails[$email] = $email;
			}

			Mail::send('emails/staff-address-notification', ['user' => $this->user, 'contract' => $this->contract], function($m) use($emails) {
				$m->to($emails)->subject("Customer Address Change Notice");
			});

		if($request->ajax()){
			return response()->json('success');
		}
	}
}
