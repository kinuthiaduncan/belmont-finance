<?php namespace App\Http\Controllers;
use App\Models\Contract;
use App\Models\ContractApplication;

class DashboardController extends Base_Controller {

	/*
	|--------------------------------------------------------------------------
	| Dashboard Controller
	|--------------------------------------------------------------------------
	|
	|
	*/
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		//authenticate dashboard requests
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if($this->context == "staff"){
			$approved = Contract::ofStatus('approved')->whereBetween('applicationDate', [date('Y-m-d',strtotime('last sunday')), date('Y-m-d')])->sum('amountFinanced');
			$pending = Contract::ofStatus('pending')->whereBetween('applicationDate', [date('Y-m-d',strtotime('last sunday')), date('Y-m-d')])->sum('amountFinanced');
			$cancelled = Contract::ofStatus('cancelled')->whereBetween('applicationDate', [date('Y-m-d',strtotime('last sunday')), date('Y-m-d')])->sum('amountFinanced');

			$denied = ContractApplication::whereBetween('created_at', [date('Y-m-d',strtotime('last sunday')), date('Y-m-d', strtotime('tomorrow'))])->where('status', '3')->orderBy('created_at', 'desc')->get();

			$thisWeek = [
				'approved' => $approved,
				'pending' => $pending,
				'cancelled' => $cancelled,
				'denied' => $denied
			];


			$unapproved = Contract::whereApproved(false)->count();
			return view('staff/index', ['thisWeek' => $thisWeek, 'review' => $unapproved]);
		}
		
	}

	public function profile()
	{
		if($this->context == "staff"){
			return view('staff/my-profile');
		}
	}

}
