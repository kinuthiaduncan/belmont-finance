<?php namespace App\Http\Controllers;
use \App\Models\Contract;
use \App\Models\ContractApplication;
use \App\Models\File;
use Illuminate\Http\Request;
use Event;
use \App\Events\ApplicationFilterSelected;
use Artisan;

class ApplicationController extends Base_Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
		$this->middleware('auth');
	}

	/* the main view - applications that need a buy rate entered */
	public function index() {
		
		$apps = ContractApplication::where('is_online', '=', '1')
			->where(function($query) {
				$query->where('status', '=', 0)
				->orWhere('status', '=', '');
			})
			->orderBy('created_at', 'ASC')
			->with('files')
			->with('flags')
			->paginate(10);

		$remoteApps = \App\Models\RemoteSignature::whereIn('file_id', 
			[
				'280614', '281734', '281735', '281736', '282499'
			])->get();

		return view('staff.apps.index', [
			'pagetitle' => 'Applications',
			'filters' => $this->getAppFilters(),
			'apps' => $apps,
			'remoteApps' => $remoteApps,
			'counts' => $this->getAppCounts(),
			'tab' => 'apps'
		]);
	}

	/* the second view - apps that have been entered a buy rate and not sales slip yet */
	public function approved() {
		
		$apps = ContractApplication::doesntHave('salesSlipFile')
			->where('is_online', '=', '1')
			->where('status', '=', 1)
			->orderBy('updated_at', 'ASC')
			->whereNull('funding_status')
			->with('files')
			->paginate(10);

		return view('staff.apps.approved', [
			'pagetitle' => 'Applications',
			'filters' => $this->getAppFilters(),
			'apps' => $apps,
			'counts' => $this->getAppCounts(),
			'tab' => 'apps'
		]);
	}

	/* the third view - apps that have the sales slip completed */
	public function sold() {
		
		$apps = ContractApplication::has('salesSlipFile')
			->where('is_online', '=', '1')
			->where('status', '=', 1)
			->whereNotNull('buy_rate')
			->whereNull('funding_status')
			->orderBy('updated_at', 'ASC')
			->with('files')
			->paginate(15);

		return view('staff.apps.sold', [
			'pagetitle' => 'Applications',
			'filters' => $this->getAppFilters(),
			'apps' => $apps,
			'counts' => $this->getAppCounts(),
			'tab' => 'apps'
		]);
	}

	/* fourth view - apps waiting for funding */
	public function fundingRequested() {
	$apps = ContractApplication::where('is_online', '=', '1')
		->whereNotNull('buy_rate')
		->where('status', '=', 1)
		->whereIn('funding_status', [1,2])
		->orderBy('updated_at', 'ASC')
		->with('files')
		->paginate(15);

		return view('staff.apps.funding-requested', [
			'pagetitle' => 'Applications',
			'filters' => $this->getAppFilters(),
			'apps' => $apps,
			'counts' => $this->getAppCounts(),
			'tab' => 'apps'
		]);
	}


	// remove all consumer applications
	public function clearConsumer(Request $request) {
		$this->validate($request, [
			'firstName' => 'required_with:lastName|alpha_space',
			'lastName' => 'required_with:firstName|alpha_space',
	        'identifier' => 'required_with:firstName|integer'
	    ]);

		Artisan::call('apps:clearconsumer', [
			'firstName' => $request->firstName,
			'lastName' => $request->lastName,
			'identifier' => $request->identifier
		]);
		
		return response()->json('success');
	}


	public function viewCreditAgreement($appId){
		$app = ContractApplication::findOrFail($appId);

		$file = File::where('app_id', '=', $appId)
			->where('file_type', '=', 1)
			->first();

		if(!$file) {
			$file = File::where('app_id', '=', $appId)
				->where('file_type', '=', 3)
				->firstOrFail();
		}

		return response()->file(base_path($file->local_path));
	}


	public function viewFile($id){
		$file = File::where('id', '=', $id)
			->firstOrFail();

		return response()->file(base_path($file->local_path));
	}


	// controller method for online app search
	public function search(Request $request)
	{
		$term = $request->search;
		$filterOne = $request->filter_1;
		$filterTwo = $request->filter_2;

		$appSelectAttributes = ['contractapp.id', 'firstName', 'lastName', 'amount_financed',
			'fca_id', 'is_online', 'buy_rate', 'credit_type', 'contractapp.created_at', 'dist_companies.account_number', 'dist_companies.company_name'];

		$searchResults = ContractApplication::where('is_online', '=', 1)
			->leftJoin('dist_users', 'contractapp.dealerId', '=', 'dist_users.id')
			->leftJoin('dist_companies', 'dist_users.company_id', '=', 'dist_companies.id')
			->select($appSelectAttributes)
			->ofFilter('1', $filterOne)
			->ofFilter('2', $filterTwo)
			->where( function($query) use($term) {
				if( is_numeric(substr($term, 0, 1))) {
					$query->where('dist_companies.account_number', 'like', '%' . $term . '%')
					->orWhere('contractapp.id', '=', $term)
					->orWhere('fca_id', 'like', '%' . $term . '%');
				} else {
					$query->where('lastName', 'like', '%' . $term . '%')
					->orWhere('firstName', 'like', '%' . $term . '%');
				}
			})->paginate(15);

		return view('staff.apps.search', [
			'pagetitle' => 'Applications',
			'apps' => $searchResults,
			'filters' => $this->getAppFilters(),
			'counts' => $this->getAppCounts(),
			'term' => '"' . $term . '"' . ( $filterOne ? ', ' . $filterOne : '' ) . ( $filterTwo ? ', ' . $filterTwo : '' ),
			'tab' => 'apps'
		]);
	}

	// controller method to set an app's filter
	public function setFilter(Request $request)
	{
		$this->validate($request, [
			'app_id' => 'integer',
			'filter_group' => 'integer',
	        'filter_name' => 'max:64'
	    ]);

	    // remove any existing filter in this group for this app
	    \App\Models\AppExtras\AppFilter::where('filter_group', '=', $request->filter_group)
	    	->where('app_id', '=', $request->app_id)->delete();

	    // add the new filter
	    $appFilter = \App\Models\AppExtras\AppFilter::create([
	    	'app_id' => $request->app_id,
	    	'filter_group' => $request->filter_group,
	    	'filter_name' => $request->filter_name
	    ]);

	    $app = ContractApplication::findOrFail($request->app_id);

	    // call event on filter change
	    Event::fire(new \App\Events\ApplicationFilterSelected($appFilter, $app));

	    return response()->json('success');
	}


	public function getAppData($appId) {
		$app = ContractApplication::findOrFail($appId);

		$appArray = $app->toArray();
		return view('staff.apps.single.appdata', ['app' => $appArray]);
	}

	public function getViewAgreement($appId) {
		$app = ContractApplication::findOrFail($appId);

		if($app->sales_agreement){
			return response()->download('../' . $app->sales_agreement . '.pdf');
		} else {
			abort(404);
		}
	}

	private function getAppCounts() {
		$appCount = ContractApplication::where('is_online', '=', '1')
			->where(function($query) {
				$query->where('status', '=', 0)
				->orWhere('status', '=', '');
			})
			->count();

		$approvedCount = ContractApplication::doesntHave('salesSlipFile')
			->where('is_online', '=', '1')
			->whereNull('funding_status')
			->where('status', '=', 1)
			->count();

		$soldCount = ContractApplication::has('salesSlipFile')
			->where('is_online', '=', '1')
			->whereNull('funding_status')
			->where('status', '=', 1)
			->whereNotNull('buy_rate')
			->count();

		$fundingRequestedCount = ContractApplication::where('is_online', '=', '1')
			->whereNotNull('buy_rate')
			->where('status', '=', 1)
			->whereIn('funding_status', [1,2])
			->count();

		return ['apps' => $appCount, 'approved' => $approvedCount, 'sold' => $soldCount, 'fundingRequested' => $fundingRequestedCount];
	}


	// takes an online application and a given FCA id
	// and associates that online application with the FCA app
	public function associateWithFCA(Request $request)
	{
		$this->validate($request, [
			'fca_id' => 'required|integer',
			'app_id' => 'required|integer'
	    ]);

	    $newApp = \App\Models\ContractApplication::findOrFail($request->app_id);

	    // remove any existing apps that might be associated with this fca id already
	    $existingApps = \App\Models\ContractApplication::where('fca_id','=', $request->fca_id)->get();
	    foreach($existingApps as $app) {
	    	if($request->app_id != $app->id) {
	    		$app->delete();
	    	}
	    }

	   	// add the new fca id to this app
	    $newApp->fca_id = $request->fca_id;
	    $newApp->save();

	    return response()->json('success');
	}

	// return a view showing all files for this application
	public function allFiles($id)
	{
		$app = ContractApplication::findOrFail($id);

		$archivedFiles = \App\Models\File::where('app_id', '=', $app->id)
			->whereNotNull('archived_at')->get();

		return view('staff.apps.single.files', ['app' => $app, 'archived' => $archivedFiles]);
	}
	
	// get all filter options for a given group id
	private function getAppFilters()
	{
		return \App\Models\AppExtras\AppFilter::getOptions();
	}



}
