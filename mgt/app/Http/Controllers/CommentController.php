<?php namespace App\Http\Controllers;
use App\Models\Contract;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Base_Controller{

  public function __construct()
  {
    // construct parent in order to create shared variables
    parent::__construct();

    }

  public function postCreate(Request $request){
    $comments = new Comment;
    $comments->text = $request->input('comments');
    $comments->contract_id = $request->input('commentContractId');
    $comments->show_dist = $request->input('distributor');
    $comments->user_id = $this->user->id;
    $comments->save();

    if($request->ajax()){
      return response()->json('success');
    }
  }

  public function delete(Request $request, $id){
    $comment = Comment::findOrFail($id);
    $comment->delete();
    if($request->ajax()){
      return response()->json('success');
    }
  }
}
