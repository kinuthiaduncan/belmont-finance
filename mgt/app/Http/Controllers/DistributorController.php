<?php namespace App\Http\Controllers;

//this one is for requests made by belmont staff

use Illuminate\Http\Request;
use Auth;
use App\Models\DealerCompany;
use App\Models\Dealer;
use App\Models\Contract;
use App\Models\ContractApplication;
use Session;
use Mail;
use JWTAuth;
use Password;
use App\Events\DealerCreated;
use DB;

class DistributorController extends Base_Controller {

	/*
	|User controller for managing all user stuff
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		//authenticate dashboard requests
		$this->middleware('auth');

		$this->sortFilter = Session::get('staff.distributors.filters.Sort+By', '');
	}

	/* Filterig functions */
	//allows to set session for filters other than pending etc
	// make a post request, store the post filter in session, redirect back to original url
	public function postFilter(){
		//
	}
	public function getFilter(Request $request){
		if($request->has('sortby')){
			$request->session()->set('staff.distributors.filters.Sort+By', $request->input('sortby'));
		}

		$history = $request->session()->get('staff.history.2', 'distributors/list');

		if(strpos($history,'distributors') === "0"){
			return redirect($history);
		}else{
			return redirect('distributors');
		}
	}


	public function getReturnJson(){
		return DealerCompany::all();
	}

    /**
     * Show the profile for a distributor in order to view or edit
     * @param int $id the dealer ID
     * @return Response
     */
	public function getProfile($id)
	{
		$company = DealerCompany::ofDeleted($this->user)->findOrFail($id);

		// retrieve unfinished applications
		$outstandingApps = ContractApplication::where('distributor_account', $company->account_number)->where('status', '=', 0)->get();

		$products = \App\Models\DealerProduct::get();

        $statesApr = \App\Libraries\Settings\Settings::getDistributorApr($company);

		return view('staff/distributor/profile', [
                        'title' => 'Distributor Profile',
                        'tab' => 'distributors',
                        'item' => $company,
                        'showBack' => '/distributors\/',
                        'apps' => $outstandingApps,
                        'products' => $products,
                        'states' => $statesApr
        ]);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex($filter = "all")
	{
		return redirect()->action('DistributorController@getList');
	}

		/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getList($filter = "all")
	{
		$distributorCompanies = DealerCompany::ofStatus($filter)
			->ofSortBy($this->sortFilter)
			->ofDeleted($this->user)
			->withCount('recentContracts')
			->paginate(10);

		$products = \App\Models\DealerProduct::get();
    	return view('staff/distributor/index', ['title' => 'Distributors', 'distributors' => $distributorCompanies, 'tab' => 'distributors', 'filter' => $filter, 'products' => $products]);

	}



	public function deleteCompany($id)
	{
		// must be an admin to delete users
		if(\Auth::guard('web')->user()->hasRole('admin')){
			$company = DealerCompany::find($id);
			if($company){
				foreach ($company->users as &$distributor){
					$distributor->delete();
				}
				$company->delete();
				return 'true';
			}
			else{
				abort(404);
			}
		}
		else{
			abort(401);
		}
	}

	public function postUpdate(Request $request, $id)
	{
		$this->validate($request, [
			'dealer_status' => 'in:Approved,Denied,Pending,Closed,Hold',
	        'account_number' => 'unique:dist_companies,account_number,'.$id,
	        'credit_type' => 'in:0,1,2,3'
	    ]);

		$company = DealerCompany::ofDeleted($this->user)->find($id);

		if($this->user->hasRole('admin') || $this->user->hasRole('distributor')){

			if($this->user->hasRole('admin')){
				$company->company_name = $request->input('name');
			}
			$company->dealer_status = $request->input('dealer_status');
			$company->account_number = $request->input('account_number');
			$company->allow_revolving = $request->input('credit_type');
			$company->owner = $request->input('owner');
			$company->enable_new_online = $request->input('enable_new_online');
			$company->address = $request->input('address');
			$company->phone = $request->input('phone');
			$company->city = $request->input('city');
			$company->state = $request->input('state');
			$company->zip = $request->input('zip');
			$company->license = $request->input('license');
			$company->control_paperwork = $request->input('control_paperwork');
			$company->app_allow_renters = $request->input('app_allow_renters');

			if($company->product_id != $request->input('product_id'))
			{
				$company->product_id = $request->input('product_id');
				// update mailchimp users
				$mailchimp = new \App\Libraries\Mailchimp();
				if( $request->input('product_id') )
				{
					$newProduct = \App\Models\DealerProduct::findOrFail( $request->input('product_id') );
				}
				else
				{
					$newProduct = '';
				}
				try{
					$company->users->each(function($item, $key) use($mailchimp, $newProduct){
						$mailchimp->updateSubscriber($item->email, getenv('MAILCHIMP_DEALER_LIST'), ['INDUSTRY' => $newProduct->name]);
					});
				} catch(\Exception $e) {
				}
			}

			$company->save();

			if($request->ajax()){
				return response()->json('success');
			}
		}
		else{
			abort('403');
		}
	}

	public function sendEmail(Request $request, $id)
	{
		$company = DealerCompany::find($id);
		$company->user->name = $name();
		$company->user->email = $email();
		Mail::send('emails.status-changed', [ ], function ($message) {
			$message->from('test@rhinedesign.biz', 'Test');
			$message->to($email, $name);
		});
	}

	public function postSearch(Request $request)
	{
		$searchTerm = $request->input('search');
		$dealerCos = DealerCompany::ofDeleted($this->user)->where(function($query) use($searchTerm){
			$query->where('account_number', '=', $searchTerm)
			->orWhere('account_number', 'LIKE', '%' . $searchTerm . '%')
			->orWhere('company_name', 'LIKE', '%' . $searchTerm . '%')
			->orWhere('owner', 'LIKE', '%' . $searchTerm . '%');
		})->get();

		$products = \App\Models\DealerProduct::get();

		return view('staff/distributor/index', [
			'tab' => 'distributors',
			'distributors' => $dealerCos,
			'showBack' => '/distributors\/',
			'products' => $products
		]);
	}

	public function approveDealer($id, $dealerId){
		$distributor = \App\Models\DealerCompany::findOrFail($id);

		$distributor->users()->updateExistingPivot($dealerId, [
			'user_status' => 'Approved'
		]);

		return back();
	}

	public function disableDealer($id, $dealerId){
		$distributor = \App\Models\DealerCompany::findOrFail($id);

		$distributor->users()->updateExistingPivot($dealerId, [
			'user_status' => 'Hold'
		]);
		
		return back();
	}

	public function deleteDealer($id, $dealerId){
		if( $this->user->hasRole('admin') ){
			$dealer = Dealer::findOrFail($dealerId);
			$company = \App\Models\DealerCompany::findOrFail($id);

			$dealerService = new \App\Services\DealerService();
			$dealerService->delete($dealer, $company);

			return response()->json('success');
		}
		else{
			abort('403');
		}
	}

	public function getPseudo($id){
		if($this->user->hasRole('admin') || $this->user->hasRole('distributor')){
			// get distributor user
			Session::forget('selectedDistributorId');
			Session::put('admin-pseudo', $id);
			$http = getenv('PRODUCTION') == "false" ? 'http://' : 'https://';
			return redirect($http . 'dealer.' . getenv('DOMAIN') );
		}else{
			abort('403');
		}
	}

	public function getSendReset($id){
		$dealer = Dealer::findOrFail($id);
		// auto set some variables for 
        view()->composer('auth.emails.password', function($view){
            $protocol = 'https://';
            $view->with([
                'url' => $protocol . 'dealer.' . getenv('DOMAIN'),
            ]);
        });
        
		Password::broker('dealers')->sendResetLink(['email' => $dealer->email], function($message) {
 			$message->subject('Belmont Finance Password Reset');
   		});
   		return back()->withInput();
	}

	// one off for creating new distributor admin user
	public function postCreateAdmin(Request $request) {
		$this->validate($request, [
			'name' => 'required',
	        'email' => 'email|max:255|unique:dist_users'
	    ]);

		$distributor = \App\Models\DealerCompany::where('id', '=', $request->distributor_id)->firstOrFail();
		$dealer = Dealer::create([
			'email' => $request->email,
			'name' => $request->name
		]);

		$distributor->users()->save($dealer, [
			'admin' => 1,
			'user_status' => 'Approved'
		]);

		// send email for password resets
		// auto set some variables for 
        view()->composer('auth.emails.password', function($view){
            $protocol = 'https://';
            $view->with([
                'url' => $protocol . 'dealer.' . getenv('DOMAIN'),
            ]);
        });

		Password::broker('dealers')->sendResetLink(['email' => $dealer->email], function($message) {
		    $message->subject('Belmont Finance New Account');
		});

		return response()->json('success');
	}

	// creatimng a new distributor - copeid from old registar, not a typical store
	public function postStore(Request $request){
		$data = $request->all();

		//creating new distributor account
		if(isset($data['_distrib']) && $data['_distrib'])
		{
			// a distributor reset whole company account
			if(isset($data['_reset']) && $data['_reset']){
				$this->validate($request, [
					'email' => 'required|email|max:255|unique:dist_users',
				]);
			}
			else{
				// create a new distributor
				$this->validate($request, [
					'company_name' => 'required|max:255',
					'owner_name' => 'max:255',
					'admin' => 'boolean',
					'account_number' => 'required|max:100|unique:dist_companies',
					'email' => 'min:5|max:255|email',
				]);
			}
		}

		//creating new distributor admin account
		if($request->input('_distrib'))
		{
			// if this is a new company or existing company
			if(!isset($data['_company_id'])){
				$companyData = [
					'company_name' => $data['company_name'],
					'account_number' => $data['account_number'],
					'owner' => isset($data['owner_name']) ? $data['owner_name'] : null,
					'product_id' => isset($data['product_id']) ? $data['product_id'] : null,
					'dealer_status' => 'Approved'
				];

				$company = new DealerCompany($companyData);
				$company->save();
			}else{
				$company = DealerCompany::findOrFail($data['_company_id']);
			}

			// if there's an email provided, create the dealer admin user
			if($request->email) {

				$userData = [
					'email' => $request->email,
					'username' => null,
				];

				// look for existing dealer
				$dealer = Dealer::firstOrCreate(['email' => $request->email]);

				// create dealer company relationship
				DB::table('dist_dealer_relations')->insert([
					'dealer_id' => $dealer->id,
					'distributor_id' => $company->id,
					'admin' => 1,
					'user_status' => 'Approved'
				]);

				// auto set some variables for 
	            view()->composer('auth.emails.password', function($view){
	                $protocol = 'https://';
	                $view->with([
	                    'url' => $protocol . 'dealer.' . getenv('DOMAIN'),
	                ]);
	            });

				// send email for password resets
				Password::broker('dealers')->sendResetLink(['email' => $dealer->email], function($message) {
				    $message->subject('Password reminder');
				});

				\Event::fire(new DealerCreated($dealer, $company));

			}

			return $company;
		}
	}


	// add or remove state from distributor's allowed states to work in
	public function updateState(Request $request, $id) {
		$dealerState = \App\Models\Dealers\DealerState::where('dist_id', '=', $id)
			->where('state', '=', $request->state)->first();

		// toggle if exists or not
		if(!$dealerState) {
			\App\Models\Dealers\DealerState::create([
				'state' => $request->state,
				'licensed' => 1,
				'dist_id' => $id
			]);
		} else {
			$dealerState->delete();
		}

		return response()->json('success');
	}


	// return profile view of distributors but specific to setting promos
	public function setPromos($id) {
		$distributor = DealerCompany::findOrFail($id);

		if($distributor->allow_revolving == 1) {
			$simpleOrRevolving = 'REVOLVE';
		} else if ($distributor->allow_revolving == 0) {
			$simpleOrRevolving = 'SIMPLE';
		} else {
			$simpleOrRevolving = 'BOTH';
		}

		$promos = \App\Models\Promo::orderBy('ordinal')
			->orderBy('name')->get();
		$distributorPromos = \App\Models\Relations\CompanyPromoRelation::where('dist_id', '=', $id)
			->get()->pluck('promo_id');

		$pmtFactors = \App\Models\Dealers\DealerPmtFactor::where('dist_id', '=', $id)->get();

		return view('staff.distributor.profile-promos', [
			'item' => $distributor,
			'promos' => $promos,
			'rateFactors' => $pmtFactors,
			'distributorPromos' => $distributorPromos,
			'tab' => 'distributors',
			'showBack' => '/distributors/profile/' . $id
		]);
	}

	// create and destroy the dealer-promo model
	public function sendSetPromos(Request $request) {
		$promo = \App\Models\Relations\CompanyPromoRelation::where('dist_id', '=', $request->dist_id)
			->where('promo_id', '=', $request->promo_id)
			->first();

		if($promo) {
			$promo->delete();
		} else {
			\App\Models\Relations\CompanyPromoRelation::create([
				'dist_id' => $request->dist_id,
				'promo_id' => $request->promo_id
			]);
		}
	}

	// create the dealer-payment factor model
	public function setPmtFactor(Request $request) {
		$this->validate($request, [
			'payment_factor' => 'numeric|between:0,10.00',
	        'buy_rate' => 'numeric|between:0,100.00',
	    ]);

		\App\Models\Dealers\DealerPmtFactor::create([
			'dist_id' => $request->dist_id,
			'pmt_factor' => $request->payment_factor,
			'min_buy_rate' => $request->buy_rate
		]);
	}

}
