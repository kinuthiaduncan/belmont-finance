<?php namespace App\Http\Controllers\Dealer;

use Auth;
use App\Models\Contract;
use App\Models\Tag;
use Illuminate\Http\Request;
use Session;
use DB;
use Hash;


class DealerSettingsController extends \App\Http\Controllers\Base_Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		$this->middleware('admin', ['except' => ['getAccount']]);

		$this->validSettings =
		[
			'allowDts'
		];
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function getIndex()
	{
		$settings = $this->user->company->getSettings();
		return view('dealer/settings');
	}

	public function getAccount()
	{
		$settings = $this->user->company->getSettings();
		return view('dealer/account');
	}

	public function postCreateTag(Request $request){
		$companyId = $this->user->company->id;

		$this->validate($request, [
	        'name' => 'required|unique:dist_tags,name,NULL,id,company_id,' . $companyId . '|max:32'
	    ]);

		$tag = new Tag;
		$tag->name = $request->input('name');
		$tag->company_id = $companyId;
		$tag->save();

		return response()->json('success');
	}

	public function deleteTag(Request $request, $id){
		$tag = Tag::find($id);

		// reset session tag filter
		Session::forget('dealer.tags');


		//verify ownership of tag
		if($this->user->company->id == $tag->company_id){
			// first delete tagged contract relations
			$tag->taggedContracts()->delete();

			$tag->delete();
			return response()->json('success');
		}else{
			abort('403');
		}

		return response()->json('success');
	}

	public function getToggleSetting($setting)
	{
		if( in_array($setting, $this->validSettings) ){

			if( $this->user->company->getSetting($setting) )
			{	
				$this->disableSettings($setting);
				$this->user->company->setSetting($setting, '0');
			}
			else
			{
				$this->user->company->setSetting($setting, '1');
			}
		}
		else{
			abort('402');
		}
	}

	public function disableSettings($setting){
		if($setting == "allowDts")
		{
			// set all users to no parents
			$this->user->company->users()->update(["parent_user_id" => null]);
		}
	}

	public function postUpdatePassword(Request $request)
	{
		$this->validate($request, [
	        'old_password' => 'required',
	        'password' => 'required|confirmed|min:8',
	    ]);

		if (Auth::guard('dealer')->attempt(['email' => $this->user->email, 'password' => $request->input('old_password')]) || Session::get('admin-pseudo')) {
		    $this->user->password = Hash::make($request->input('password'));

		    $this->user->save();

		    if($request->ajax()){
				return response()->json('success');
			}
		}
		else{
			return response()->json([
                "old_password" => "Your password doesn't match what we have on file",
            ], 403);
		}

	}
}
