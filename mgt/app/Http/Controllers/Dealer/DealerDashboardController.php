<?php namespace App\Http\Controllers\Dealer;

use App\Models\ContractApplication;
use App\Models\Contract;
use Carbon\Carbon;
use App\Libraries\Contracts\ContractLibrary;
use App\Libraries\Contracts\RemoteSign;
use Illuminate\Http\Request;
use App\Http\Controllers\Dealer\Traits\AuthenticatesDealerTrait;


class DealerDashboardController extends \App\Http\Controllers\Base_Controller {
	use AuthenticatesDealerTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}

	// new dashboard
	public function dashboard(Request $request)
	{
		$appSelectAttributes = ['contractapp.id', 'firstName', 'lastName', 'fca_id', 'is_online', 'credit_type', 'contractapp.created_at', 'contractapp.status'];

		$waitingApps = ContractApplication::ofUser($this->user, $this->user->company)
			->whereStatus('0')
			->where('created_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())
			->orderBy('created_at', 'DESC')
			->paginate(5);

		$approvedApps = ContractApplication::ofUser($this->user, $this->user->company)
			->where('status', '=', 1)
			->where('created_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())
			->orderBy('created_at', 'DESC')
			->with('files')
			->paginate(5);

		$deniedApps = ContractApplication::ofUser($this->user, $this->user->company)
			->whereIn('status', [2,3])
			->where('created_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())
			->orderBy('created_at', 'DESC')
			->paginate(5);

		// $subusers = $this->user->company->contracts()->sum('current_reserve_amount');
		if( $this->user->hasRole('admin') )
		{
			$users = $this->user->company->users;
		}
		else
		{
			$users = $this->user->subUsers()->get();
		}

		return view('dealer/dashboard', [
			'waiting' => $waitingApps,
			'approved' => $approvedApps,
			'denied' => $deniedApps,
			'users' => $users,
			'news' => []
		])->withHeaders([
               'Cache-Control' => 'no-cache, no-store, must-revalidate'
        ]);
	}


	public function newApp(){

		$userOfCompany = $this->user->companies()->where('dealer_id', '=', $this->user->id)->first();

		if ($userOfCompany->pivot->user_status != "Approved" || $this->user->company->dealer_status == "Pending") {
			return view('errors.dealer-not-authorized');
		}

		return view('dealer.new-app');
	}

	public function emailApp(Request $request){
		$request->session()->forget('appId');
		return view('dealer.applications.email-app');
	}

	public function postEmailApp(Request $request) {
		$this->validate($request, [
	        'email' => 'required|email|max:48',
	        'coEmail' => 'email|max:48',
	        'amount_financed' => 'required|numeric|between:1000,99999.99',
	        'down_payment' => 'numeric|between:0,99999.99',
	        'credit_type' => 'in:Revolving,Closed-End'
	    ]);

		// determine which credit type to use
		$creditType = ( $this->user->company->allow_revolving == 1 ||
            ( $this->user->company->allow_revolving == 2
                && $request->credit_type == 'Revolving') ) ? 'Revolving' : 'Closed-end';


		if($creditType == 'Revolving') {
			$request->credit_type = "Revolving";
		} else {
			$request->credit_type = "Closed-End";
		}


		// get the file to use
		$fileId = ContractLibrary::retrieveCreditAgreement($this->user, $creditType);

		// initiate the remote sign request
		$remoteSigner = new RemoteSign($fileId);
		$remoteSigner->sendSignatureRequest($request->email, $this->user, [
			'coEmail' => $request->coEmail,
			'amount_financed' => $request->amount_financed,
			'credit_type' => $request->credit_type,
			'down_payment_amount' => $request->down_payment,
			'dist_id' => $this->user->company->id
		], $request->coEmail ? 1 : null);

		return response()->json('success');
	}

	public function requestFunding($id){
		$app = \App\Models\ContractApplication::findOrFail($id);
		return view('dealer.applications.request-funding', ['app' => $app]);
	}

	public function signRequestFunding($id){
		// authenticate
		$app = \App\Models\ContractApplication::findOrFail($id);
		$this->authApp($app);

		$dealerLibrary = new \App\Libraries\Contracts\DealerFundingLibrary($app, $this->user);
		return view('dealer/applications/sign', $dealerLibrary->signContractOrCompletion());
	}

	public function postRequestFunding(Request $request){
		$app = \App\Models\ContractApplication::findOrFail($request->id);
		$dealerLibrary = new \App\Libraries\Contracts\DealerFundingLibrary($app, $this->user);
		$dealerLibrary->submitDealerRequest($request);
		return response()->json('success');
	}

	// load index page for notice of completion form
	public function requestCompletion($id){
		$app = \App\Models\ContractApplication::findOrFail($id);
		return view('dealer.forms.collect-lcc', ['app' => $app]);
	}

	// load signature view for dealer to sign completion form
	public function signCompletion($id){
		$app = \App\Models\ContractApplication::findOrFail($id);
		$completionLibrary = new \App\Libraries\Contracts\CompletionLibrary($app, $this->user);
		return view('dealer/applications/sign', $completionLibrary->dealerSignCompletionForm());
	}

	// post signatures back
	public function postSignCompletion(Request $request){
		$app = \App\Models\ContractApplication::findOrFail($request->id);

		$completionLibrary = new \App\Libraries\Contracts\CompletionLibrary($app, $this->user);
		$completionLibrary->submitSign($request, true);
		return response()->json('success');
	}

	// send completion form email to consumer
	public function postRequestCompletion(Request $request, $id){
		$app = \App\Models\ContractApplication::findOrFail($id);

		$fileId = '254859';

		$completionLibrary = new \App\Libraries\Contracts\CompletionLibrary($app, $this->user);
		$existingFile = $completionLibrary->addTerms($this->user, $this->user->company);

		// initiate the remote sign request
		$remoteSigner = new RemoteSign($fileId);

		$remoteSigner->sendSignatureRequest($app->email, $this->user, ['app_id' => $app->id], null, $existingFile->id);

		return response()->json('success');
	}

}
