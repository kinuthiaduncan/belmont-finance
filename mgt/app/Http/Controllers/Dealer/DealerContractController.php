<?php namespace App\Http\Controllers\Dealer;

use Auth;
use Request;
use App\Models\Contract;
use App\Models\ContractApplication;
use App\Models\Tag;
use App\Libraries\Contracts\ContractLibrary;
use App\Http\Requests\ApplicationPostRequest;
use Session;
use JWTAuth;


class DealerContractController extends \App\Http\Controllers\Base_Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
		$this->tagfilters = Session::get('dealer.tags', []);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function getIndex($filter = null)
	{
		return redirect()->action('Dealer\DealerContractController@getAll');
	}

	public function getAll($filter = null)
	{
		$contracts = $this->user->company->contracts()->ofUser($this->user)->ofStatus($filter)->ofTag($this->tagfilters)->orderBy('applicationDate', 'desc')->paginate(10);
		return view('dealer/contracts', ['contracts' => $contracts, 'tags' => $this->tagfilters, 'routePath' => '/contracts/all/'] );
	}

	public function getUnassignedContracts($filter = null)
	{
		$contracts = $this->user->company->contracts()->ofStatus($filter)->whereNull('dist_user_id')->orderBy('applicationDate', 'desc')->paginate(15);
		return view('dealer/unassigned-contracts', ['contracts' => $contracts, 'routePath' => '/contracts/unassigned-contracts'] );
	}

	public function getEditContract($id) {
		$contract = Contract::find($id);
		if ($contract) {
			// check if contract belongs to user
			if((!$this->user->hasRole('admin') || $this->user->company->dist_id !== $contract->distributorId) &&
				$this->user->id !== $contract->dist_user_id
			){
				abort(401);
			}
		} else {
			$contract = null;
		}
		return view('dealer/edit-contract', ['item' => $contract]);
	}

	// if someone posts a search for contracts
	public function postSearch()
	{
		$user = Auth::guard('dealer')->user();
		$searchTerm = Request::input('search');
		$companyId = $this->user->company->dist_id;
		if( is_numeric(substr($searchTerm, 0, 1)) ){
			// searching by account number
			$contracts = Contract::where('distributorId','=', $companyId)->where('accountNumber', '=', $searchTerm)->paginate(15);
		}else{
			// searching by name
			$contracts = Contract::where('distributorId','=', $companyId)->where('customerName', 'LIKE', $searchTerm . '%')
				->orWhere('customerName', 'LIKE', '%' . $searchTerm)->where('distributorId','=', $companyId)->paginate(15);

		}
		// store search term in session
		/* Request::session()->put('staff.search', $searchTerm);

		$this->viewArray['pagetitle'] = 'Contract Search: "' . $searchTerm . '"';
		$this->viewArray['contracts'] = $contracts; %

		*/return view('dealer/contracts', ['contracts' => $contracts]);
	}
	public function postUnassignedContracts()
	{
		$id = Request::input('contractId');
		$contract = Contract::find($id);
		$contract->dist_user_id = Request::input('dist_user_id');
		$contract->save();
	}

	public function postTagContract(Request $request, $contractId)
	{
		if( $tagId = Request::input('tag') ){
			$tag = Tag::findOrFail($tagId);
			$contract = Contract::findOrFail($contractId);
			if($this->user->company->id == $tag->company_id && $this->user->company->dist_id == $contract->distributorId){
				//search for existing tag
				$isTagged = \App\Models\Relations\TagRelation::whereContractId($contractId)->whereTagId($tagId)->first();

				if( $isTagged ){
					$isTagged->delete();
				}else{
					$tagRelation = new \App\Models\Relations\TagRelation();
					$tagRelation->contract_id = $contractId;
					$tagRelation->tag_id = $tagId;
					$tagRelation->save();
				}

			}
		}
	}

	public function postTagFilter($tagId)
	{
		$tagFilters = Session::get('dealer.tags', []);
		if($tagFilters && in_array($tagId, $tagFilters))
		{
			$tagFilters = array_diff($tagFilters, [$tagId]);
			Session::put('dealer.tags', $tagFilters);

		}
		else
		{
			Session::push('dealer.tags', $tagId);
		}
	}

	public function postSubmitSalesAgreement(ApplicationPostRequest $request) {
        $cl = new ContractLibrary();
		return $cl->postSubmitSalesAgreement($request, 1);
	}

	public function postSubmitCancellation(Request $request) {
        $cl = new ContractLibrary();
		return $cl->submitCancellation($request);
	}
}
