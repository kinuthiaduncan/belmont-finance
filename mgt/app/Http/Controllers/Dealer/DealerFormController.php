<?php namespace App\Http\Controllers\Dealer;

use Carbon\Carbon;
use App\Http\Requests\DealerPaperworkRequest;
use App\Models\Dealers\PaperworkRequest;
use Mail;

class DealerFormController extends \App\Http\Controllers\Base_Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // construct parent in order to create shared variables
        parent::__construct();
        $this->middleware('auth:dealer');
    }

    public function requestPaperwork(DealerPaperworkRequest $request) {

        // change multiple input checkboxes to string
        $request->merge([
            'installment_paperwork' => is_array($request->installment_paperwork) ?
                implode(',', $request->installment_paperwork) : $request->installment_paperwork,
            'revolving_paperwork' => is_array($request->revolving_paperwork) ?
                implode(',', $request->revolving_paperwork) : $request->revolving_paperwork,
            'language' => is_array($request->language) ?
                implode(',', $request->language) : $request->language,
            'ship_to_given_address' => is_array($request->ship_to_given_address) ?
                implode(',', $request->ship_to_given_address) : $request->ship_to_given_address,
        ]);

        // add dealer id from current user
        $request->merge([
            'dealer_id' => $this->user->id
        ]);

        $paperworkRequest = PaperworkRequest::create($request->only([
            'id',
            'dealer_id',
            'dealer_name',
            'dealer_number',
            'phone_number',
            'street_address',
            'city',
            'state',
            'zip_code',
            'language',
            'states_required',
            'installment_paperwork',
            'revolving_paperwork',
            'ship_to_given_address',
            'company_recipient_name',
            'alternate_phone_number',
            'alternate_street_address',
            'alternate_city',
            'alternate_state',
            'alternate_zip_code'
        ]));

        // mail staff
        $url = 'https://staff.belmontfinancellc.com/settings/dealers/paperwork-requests/' . $paperworkRequest->id;
        Mail::send('emails.staff-paperwork-notification', ['url' => $url], function ($m){
            $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
            $m->to('marketing@belmontfinancellc.com')->subject("Paperwork Requested");
        });

    }
}