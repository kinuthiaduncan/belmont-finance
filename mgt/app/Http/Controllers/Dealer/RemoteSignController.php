<?php namespace App\Http\Controllers\Dealer;

use Auth;
use Illuminate\Http\Request;
use App\Models\Auth\SignToken;
use Log;
use Mail;
use \App\Libraries\Contracts\Traits\PaperworkTrait;

// this class handles requests relating to someone signing remotely via token authentication - not user / password auth

class RemoteSignController extends \App\Http\Controllers\Base_Controller {
	use PaperworkTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}


	/**
	 * Shows a welcome screen to the user before they sign the document
	 *
	 * @return Response
	 */

	public function welcome(Request $request)
	{
		$signToken = SignToken::where('token', '=', $request->token)->first();
		$data = json_decode($signToken->form_data);

		// make sure consumer starts fresh
		$request->session()->forget('appId');


		if( $this->getDocumentType($signToken->file_id) == 'completion' ) {
			return view('dealer.remote-sign.welcome-alt', [
				'includeView' => 'dealer.remote-sign._credit-application',
				'token' => $request->token
			]);

		} if( $this->getDocumentType($signToken->file_id) == 'sales-slip' || $this->getDocumentType($signToken->file_id) == 'contract' ) {

			// insert saved form data into the session
			$requestFields = json_decode($signToken->form_data, true);
			$request->session()->set('sales-slip-data', $requestFields);

			return view('dealer.remote-sign.welcome-contract', [
				'token' => $request->token
			]);

		} else if( $this->getDocumentType($signToken->file_id) == 'ach')  {
			return view('dealer.remote-sign.welcome-ach', [
				'token' => $request->token
			]);

		} else if( $this->getDocumentType($signToken->file_id) == 'cancellation')  {
			return view('dealer.remote-sign.welcome-cancellation', [
				'token' => $request->token
			]);

		} else if( $this->getDocumentType($signToken->file_id) == 'app')  {
			// ensure we set the session app id
			if(isset($data->app_id)) {
	    		$request->session()->put('appId', $data->app_id);
	    	}

			if($signToken->exist_file_id) {
				return view('dealer.remote-sign.finish-signing', [
					'includeView' => 'dealer.remote-sign._credit-application',
					'token' => $request->token
				]);
			} else {

				return view('dealer.remote-sign.welcome', [
					'includeView' => 'dealer.remote-sign._credit-application',
					'token' => $request->token
				]);
			}
		} else {
			// no file id given - assume sign any unsigned documents remaining
			
			if(!isset($data->app_id)) {
	    		abort('503', 'No documents found');
	    	}

	    	$isCoApp = isset($data->coApp) && $data->coApp ? 1 : 0;

	    	// look for documents
	    	 $existFile = \App\Models\File::where('app_id', $data->app_id)
        		->whereIn('file_type', [1, 2, 5, 9, 13])
        		->has($isCoApp ? 'unsignedSignaturesCo' : 'unsignedSignaturesPri')
        		->first();

        	if(!$existFile) {
        		$signToken->delete();
        		return view('dealer.remote-sign.complete-sequential', [
					'token' => $request->token
				]);
        	}

        	return view('dealer.remote-sign.welcome-sequential', [
				'token' => $request->token,
				'continue' => $request->session()->has('fileId') ? 1 : 0
			]);
	    }
	}


	// load the form to fill out before actually signing
	// not all of the paperwork options have a form to load
	// right now this is only for a application
	public function start(Request $request)
	{
		$protocol = (getenv('APP_ENV') == "production") ? 'https://' : 'http://';

		$signToken = SignToken::where('token', '=', $request->token)->first();
		$data = json_decode($signToken->form_data);
		$data->email = $signToken->email;

		if( !$request->session()->has('appId') ) {
	    	if(isset($data->app_id)) {
	    		$request->session()->put('appId', $data->app_id);
	    	} else {
	            $app = \App\Models\ContractApplication::create([
	            	'amount_financed' => $data->amount_financed,
	            	'down_payment_amount' => $data->down_payment_amount,
					'credit_type' => $data->credit_type,
					'dealerId' => $signToken->dealer_id,
					'dist_id' => $data->dist_id,
					'status' => -1
	            ]);
	            $request->session()->put('appId', $app->id);
	        }
	    }

        $request->session()->put('dealerId', $signToken->dealer_id);

        if(isset($data->dist_id)) {
        	$request->session()->put('dist_id', $data->dist_id);
        }


        if(isset($data->credit_type)) {
        	$request->session()->put('appFields.' . $request->token . '.credit_type', $data->credit_type);
        }

        $request->session()->forget('coAppRemote');

        if(isset($data->coAppRemote)) {
        	$request->session()->put('coAppRemote', 1);
        }

		return view('dealer/applications/application', [
			'data' => 'null', // this is to load a saved application
			'remoteData' => $data,
			'remoteApp' => $request->token,
			//'sessionKey' => $sessionKey,
			'submitFormUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/sign/submit?token=' . $request->token
		]);
	}


	// load the paperwork for signing
	// or, pass the form to the next signee
	public function complete(Request $request)
	{
		$signToken = SignToken::where('token', '=', $request->token)->first();
		$remoteSigner = new \App\Libraries\Contracts\RemoteSign($signToken->file_id);
		$data = json_decode($signToken->form_data);


		if( $this->getDocumentType($signToken->file_id) == 'app' && !isset($data->toSign)){

			$remoteSigner->saveApp($signToken);

			// send to coapplicant to complete form and sign
			if(isset($data->coEmail) && $signToken->needs_co_sign && !isset($data->coAppRemote)) {

				$dealer = \App\Models\Dealer::find($signToken->dealer_id);

				$remoteSigner->sendSignatureRequest($data->coEmail, $dealer, [
					'coAppRemote' => 1,
					'app_id' => $request->session()->get('appId')
				]);
				// $signToken->delete();

				return view('dealer.remote-sign.sent-application-coapp');
			}
		}

		// send the user to sign
        return view('dealer.applications.sign', $remoteSigner->prepareDocument($signToken));
	}


	// the post of the form or signatures back
	public function submit(\App\Http\Requests\ApplicationPostRequest $request)
	{
		// have to get reference to an application id
		$signToken = SignToken::where('token', '=', $request->token)->first();
		$data = json_decode($signToken->form_data);

		// set signature status to pass (not a count, just a status integer)
		if($signToken->needs_primary_sign && $signToken->needs_co_sign) {
			$signaturesNeeded = 1;
		} else if ($signToken->needs_co_sign) {
			$signaturesNeeded = 2;
		} else {
			$signaturesNeeded = 0;
		}

		if(isset($data->app_id)) {
			$app = \App\Models\ContractApplication::find($data->app_id);
		}
		
		// consumer has signed the certifcate of completion form
		if( $this->getDocumentType($signToken->file_id) == 'completion' ) {
			
			$completionLibrary = new \App\Libraries\Contracts\CompletionLibrary($app, $signToken->dealer);
			$file = $completionLibrary->submitSign($request);
			$this->emailStaffPaperwork($app);
			
		} else if( $this->getDocumentType($signToken->file_id) == 'sales-slip' || $this->getDocumentType($signToken->file_id) == 'contract' ) {
			$isAch = $request->session()->get('sales-slip-data.ach_account_number');

			// submit the form
			$contractLibrary = new \App\Libraries\Contracts\ContractLibrary();
			$contractLibrary->postSubmitSalesAgreement($request, $signaturesNeeded);

			// create ach to sign or cancellation to sign


			if( $isAch ) {
				$newToken = $this->createRemoteAch($app, $signaturesNeeded);
			} else {
				$newToken = $this->createRemoteCancellation($app, $signaturesNeeded);
			}

		} else if ( $this->getDocumentType($signToken->file_id) == 'ach' ) {
			// process signing
			$achLibrary = new \App\Libraries\Contracts\AchLibrary($app);
			$achLibrary->submitSign($request, ($signToken->needs_primary_sign == 0 || $signToken->needs_co_sign == null) ? 1 : 0);

			// create the next token and paperwork
			$newToken = $this->createRemoteCancellation($app, $signaturesNeeded);

		} else if ( $this->getDocumentType($signToken->file_id) == 'cancellation' ) {
			$cl = new \App\Libraries\Contracts\ContractLibrary();

			// tell the submitter whether or not the document will be complete after this signature
			$cl->submitCancellation(($signToken->needs_primary_sign == 0 || $signToken->needs_co_sign == null) ? 1 : 0);

			// send new token out for coapplicant, starting with sales slip
			if($signToken->needs_co_sign && $signToken->needs_primary_sign) {
				$this->createRemoteContract($app);
			}

		} else if ( $this->getDocumentType($signToken->file_id) == 'app' ) {
			// submission of application
			// this submits for validation and saving steps
			$contractLibrary = new \App\Libraries\Contracts\ContractLibrary();

			$contractLibrary->postSubmitApplication($request, $request->session()->has('coRemoteApp') || isset($data->coAppRemote) );

			if($request->step == "final") {

				$app = \App\Models\ContractApplication::findOrFail($request->session()->get('appId'));

				// if primary applicant still needs to sign, send to primary
				if(isset($data->coAppRemote)) {
					$remoteSigner = new \App\Libraries\Contracts\RemoteSign($signToken->file_id);

					// get signed file to send to coapp
					$file = \App\Models\File::where('app_id', '=', $app->id)
						->where('file_type', '=', 1)
						->orderBy('created_at', 'desc')
						->first();

					// reset file to unsigned
					$file->file_type = 3;
					$file->name = 'CoApp Signed Credit Agreement';
					$file->save();

					$remoteSigner->sendSignatureRequest($app->email, $app->dealerUser, [
						'app_id' => $app->id,
						'toSign' => 'Primary'
					], 0, $file->id);

				} else {
					$this->emailDealerApp($app);
				}
			}
		}
		
		// remove token and email dealer
		if($request->step == "final") {
			$signToken->delete();
		}
		
		// return a new token if there is another document to sign
		return response()->json('?token=' . (isset($newToken) ? $newToken->token : ''));
	}





	// these are the newer methods
	// this will just load the next unsigned document and display for consumer until none are remaining
	public function signAll(Request $request)
	{
		$signToken = SignToken::where('token', '=', $request->token)->first();
		$data = json_decode($signToken->form_data);

		// whether or not the coapplicant is the one signing
		$isCoApp = isset($data->coApp) && $data->coApp ? 1 : 0;

		$app = \App\Models\ContractApplication::findOrFail($data->app_id);

		$existFile = \App\Models\File::where('app_id', $data->app_id)
    		->whereIn('file_type', [1, 2, 5, 9, 13])
    		->has($isCoApp ? 'unsignedSignaturesCo' : 'unsignedSignaturesPri')
    		->first();

        if(!$existFile) {
        	$signToken->delete();
    		return view('dealer.remote-sign.complete-sequential', [
				'token' => $request->token
			]);
			$signToken->delete();
        }

        $request->session()->put('fileId', $existFile->id);

        // get the images
       	$includeSuccess = 'dealer.contract._remote-continue';
        $pdfImage = \App\Libraries\Contracts\ContractLibrary::generatePdfImage($existFile->local_path);

        // get signature locations
        $fileIds = $this->getFileIdByUnsignedFileType($existFile->file_type, $app);
        $signatureLocations = [];

        // in case there are multiple pages, loop and append
        // in remote signing right now, only do primary OR coapplicant signatures, not both at the same time.
        foreach($fileIds as $fileId) {
        	$signatureLocations = $signatureLocations + \App\Libraries\Contracts\ContractLibrary::retrieveSignatureLocations($fileId, $app->state, $isCoApp ? 2 : 3, $app->coRelation == 'Spouse' ? 1 : 0);
        }


        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );

        // send the user to sign
        return view('dealer.applications.sign', [
            'app' => $app,
            'buyerKey' => $app->coLastName ? 2 : 1, // we were using the view to filter these, so we also have to pass in a key here...
            'includeSuccess' => $includeSuccess,
            'pdfImage' => $pdfImage,
            'sessionKey' => $signToken->token,
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/sign/next?token=' . $signToken->token
        ]);
	}

	// post submit signature for given document (sequesntial type)
	public function signAllNext(Request $request)
	{
		$signToken = SignToken::where('token', '=', $request->token)->first();
		$data = json_decode($signToken->form_data);
		$app = \App\Models\ContractApplication::findOrFail($data->app_id);	
		$isCoApp = isset($data->coApp) && $data->coApp ? 1 : 0;

		$file = \App\Models\File::findOrFail($request->session()->get('fileId'));

		$this->submitPaperwork($app, $file, $request->signatures, $isCoApp);

        if( $this->isSigningComplete($data->app_id, $isCoApp) ) {
        	$this->emailStaffPaperwork($app);
        }

        // check to see if appl
        $existFile = \App\Models\File::where('app_id', $data->app_id)
        		->whereIn('file_type', [1, 2, 5, 9, 13])
        		->has($isCoApp ? 'unsignedSignaturesCo' : 'unsignedSignaturesPri')
        		->orderBy('file_type', 'ASC')
        		->first();

        return $signToken->token;
	}


	// check if an application's files exist
	public function statusComplete(Request $request)
	{
		if($request->session()->has('appId')) {

			$app = \App\Models\ContractApplication::where('id', '=', $request->session()->get('appId'))
				->first();

			$request->session()->forget('appId');

			if(!$app) {
				Log::warning('No app found by session id');
				abort(404);
			}

			$file = \App\Models\File::where('app_id', '=', $app->id)
				->where('file_type', '=', '1')
				->first();

			if(!$file) {
				Log::warning('No signed credit file found');
				abort(404);
			}

			// check if file actually exists
			if( file_exists ( base_path($file->local_path) ) ) {
				return response()->json('success');
			} else {
				Log::warning('signed credit file doesnt exist on file system');
				abort(404);
			}

		} else {
			abort(404);
		}
	}



	// signaturesNeeded: 0 = just primary applicant, 1 = both primary and coapplicant needed, 2 = just coapplicant needed
	private function createRemoteCancellation($app, $signaturesNeeded)
	{
		// create notice of cancellation to sign, method from trait returns file objects
		$files = $this->retrieveNoticeCancellation($app->salesSlip->sale_state, $app->distributor);
		if($signaturesNeeded != 2) {
			$cancelLibrary = new \App\Libraries\Contracts\NoticeCancellationLibrary($app, $files);
			$file = $cancelLibrary->writeTerms($app->salesSlip->sale_state);
		} else {
			// retrieve file already signed
			$file = \App\Models\File::where('app_id', '=', $data->app_id)
			->where('file_type', '=', 6)
			->orderBy('created_at', 'desc')
			->firstOrFail();
		}

		// create token for signing
		$remoteHelper = new \App\Libraries\Contracts\RemoteSign($files[0]);
		return $remoteHelper->createNewToken($signaturesNeeded == 2 ? $app->coEmail : $app->email, $app->dealerUser, ['app_id' => $app->id], $signaturesNeeded, $file->id);
	}


	// signaturesNeeded: 0 = just primary applicant, 1 = both primary and coapplicant needed, 2 = just coapplicant needed
	private function createRemoteAch($app, $signaturesNeeded)
	{
		$achLibrary = new \App\Libraries\Contracts\AchLibrary($app);
		$file = $achLibrary->writeTerms();

		// create token for ACH signing
		$remoteHelper = new \App\Libraries\Contracts\RemoteSign('ach');
		return $remoteHelper->createNewToken($signaturesNeeded == 2 ? $app->coEmail : $app->email, $app->dealerUser, ['app_id' => $app->id], $signaturesNeeded, $file->id);
	}


	// send remote contract to coapplicant, would already signed by primary
	private function createRemoteContract($app)
	{
		// retrieve file already signed
		$file = \App\Models\File::where('app_id', '=', $app->id)
			->where('file_type', '=', 4)
			->orderBy('created_at', 'desc')
			->firstOrFail();

		$fileId = $this->retrieveSalesSlip($app->salesSlip->sale_state, $app);

		// create token for ACH signing
		$remoteHelper = new \App\Libraries\Contracts\RemoteSign($fileId);
		return $remoteHelper->sendSignatureRequest($app->coEmail, $app->dealerUser, ['app_id' => $app->id], 2, $file->id);
	}


	protected function emailDealerApp($app)
    {
        $dealerEmails = $this->getDealerEmails($app);

        Mail::send('emails.dealer.remote-app-submitted', ['app' => $app], function($m) use($dealerEmails){
            $m->to($dealerEmails)->subject('Application Submitted');
        });
    }


    // Sends an email to the dealer that the certificate of completion has been signed by the consumer
    protected function emailDealerCompletion($app)
    {
    	$dealerEmails = $this->getDealerEmails($app);

        Mail::send('emails.dealer.completion-submitted', ['app' => $app], function($m) use($dealerEmails, $app){
            $m->to($dealerEmails)->subject('Certificate of Completion signed by ' . $app->firstName . ' ' . $app->lastName);
        });
    }

    protected function getDealerEmails($app) {
    	// locate users and admin dealer email addresses
        $primaryDealerEmail = [\App\Models\Dealer::where('id', '=', $app->dealerId)->first()->email];
        $secondaryDealerEmails = [];

        if($app->dealer) {
        	$secondaryDealers = \App\Models\Dealer::where('company_id', '=', $app->dealer->id)->where('admin', '=', 1)->get();

	        foreach ($secondaryDealers as $dealer) {
	            array_push($secondaryDealerEmails, $dealer->email);
	        }
        }

        return array_merge($primaryDealerEmail, $secondaryDealerEmails);
    }
}
