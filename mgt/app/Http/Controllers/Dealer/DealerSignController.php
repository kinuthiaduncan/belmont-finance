<?php namespace App\Http\Controllers\Dealer;

use App\Models\ContractApplication;
use App\Models\Contract;
use App\Models\File;
use Carbon\Carbon;
use App\Libraries\Contracts\ContractLibrary;
use App\Libraries\Contracts\RemoteSign;
use Illuminate\Http\Request;
use App\Http\Controllers\Dealer\Traits\AuthenticatesDealerTrait;
use App\Libraries\Contracts\Traits\PaperworkTrait;

class DealerSignController extends \App\Http\Controllers\Base_Controller {
	use AuthenticatesDealerTrait, PaperworkTrait;

    /**
     * Create a new controller instance.
     */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}

	// returns a view to the dealer explaining how to start the paperwork process
	// basically a segway into the form for contract or sales slip
	public function viewStartPaperwork(Request $request, $id) {
		$request->session()->forget('paperwork_options.docs');

		// check to make sure that there is a dealer user assigned to this application.
		// if no dealer exists, let's assign it to the current dealer user after we make sure they own the app
		$app = ContractApplication::findOrFail($id);
		if(!$app->dealerUser) {
			// authenticate that this app belongs to the user's company
			$this->authApp($app);
			$app->dealerId = $this->user->id;
			$app->save();
		}

		return view('dealer.paperwork.start', ['app' => $app]);
	}

	// returns a view after completing the paperwork information that generates the paperwork and collects any final info
	public function viewGeneratePaperwork(Request $request, $id) {
		if(!$request->session()->get('paperwork_options.docs')) {
					// save sales slip
			$cl = new \App\Libraries\Contracts\ContractLibrary();
			$cl->postSubmitSalesAgreement($request);
		}
		return view('dealer.paperwork.generate', ['app' => ContractApplication::findOrFail($id), 'method' => $request->method]);
	}


	public function lcc(Request $request) {

		$this->validate($request, [
	        'installation' => 'required'
	    ]);

		// just set a session variable for generate
		$request->session()->set('paperwork_options.docs', ['lcc']);
		$request->session()->set('paperwork_options.completion', 1);
		$request->session()->set('sales-slip-data', $request->all());

		return response()->json('success');
	}


	// ajax trigger to actually generate all remaining paperwork required for consumer
	public function generatePaperwork(Request $request) {
		$app = ContractApplication::findOrFail($request->id);

		$paperworkService = new \App\Services\PaperworkService($app, $this->user);
		$paperworkService->generateAll($request);

		return response()->json($paperworkService->combinePdfs());
	}

	// view to show files for printing
	public function printPaperwork($id) {

		// make sure dealer is allowed to view this application
		$app = ContractApplication::findOrFail($id);
		$this->authApp($app);

		$paperworkService = new \App\Services\PaperworkService($app, $this->user);
		$paperworkService->combinePdfs();

		$path = sys_get_temp_dir() . '/combined-' . $id . '.pdf';
		if( !file_exists($path)) {
			abort(404);
		}
		return response()->file($path);
	}


	// send paperwork via email and success
	public function sendPaperwork(Request $request, $id) {
		// save sales slip data and create file
		// make sure dealer is allowed to view app
        $app = ContractApplication::findOrFail($id);
        $this->authApp($app);

		$this->validate($request, [
	        'email' => 'required|email|max:48',
	        'coEmail' => $app->coLastName ? 'required|' : '' . 'email|max:48'
	    ]);

		// update email final time
	    $app->email = $request->email;
	    $app->coEmail = $request->coEmail ? $request->coEmail : null;
	    $app->save();

        $data = [
        	'app_id' => $app->id
        ];

        \App\Libraries\Contracts\RemoteSign::sendSequentialRequest($app->email, $app->coEmail ? $app->coEmail : null, $this->user, $data);

        return response()->json('success');
	}


	// load the view to sign the next sequential paperwork
	public function signPaperwork(Request $request, $id) {

		// be sure dealer is allowed to view app
		$app = \App\Models\ContractApplication::findOrFail($id);
		$this->authApp($app);

		// get all existing files left to sign
		$existFile = File::where('app_id', $app->id)
				->whereNull('archived_at')
				->where(function($q){
					$q->has('unsignedSignaturesPri')
						->orHas('unsignedSignaturesCo');
				})
        		->orderBy('file_type', 'ASC')
        		->first();


        if(!$existFile) {
        	return view('dealer.remote-sign.complete-sequential');
        }

        $request->session()->put('fileId', $existFile->id);

        // get the images
       	$includeSuccess = 'dealer.contract._local-continue';
        $pdfImage = \App\Libraries\Contracts\ContractLibrary::generatePdfImage($existFile->local_path);

        // get signature locations
        $fileIds = $this->getFileIdByUnsignedFileType($existFile->file_type, $app);
        $signatureLocations = [];

        $i = 0;
        foreach($fileIds as $fileId) {

            // get signature locations
            $newSignatureLocations = ContractLibrary::retrieveSignatureLocations($fileId, $app->salesSlip->sale_state, $app->coLastName ? true : false);
            foreach($newSignatureLocations as &$sig) {
                // add a page worth of y value to each signature
                if(isset($sig[1])) {
                    $sig[1] = $sig[1] + 100 * $i;
                }
            }
            $signatureLocations = array_merge($signatureLocations, $newSignatureLocations);
            $i++;
        }

        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );

        // send the user to sign
        return view('dealer.applications.sign', [
            'app' => $app,
            'buyerKey' => $app->coLastName && $app->coLastName ? 2 : 1,
            'includeSuccess' => $includeSuccess,
            'pdfImage' => $pdfImage,
            'sessionKey' => 'na',
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/apps/paperwork/submit'
        ]);
	}


	// post the next sequential paperwork

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postSubmitPaperwork(Request $request) {

		$app = \App\Models\ContractApplication::findOrFail($request->id);
		$this->authApp($app);	

		$file = File::findOrFail($request->session()->get('fileId'));

		// send 2 if both of them signed ,else just mark primary
		$this->submitPaperwork($app, $file, $request->signatures, $app->coLastName ? 2 : 0);

        if($this->isSigningComplete($app->id)) {
        	$this->emailStaffPaperwork($app);
        }

        return response()->json($app->id);
	}


	// post an ajax simple check if the paperwork is complete
	// Seems right now just for local, prmary applicant, doesn't check for coapplicant
	public function isPaperworkComplete(Request $request) {

		$app = \App\Models\ContractApplication::findOrFail($request->id);

		// bypass dealer authentication if it's a remote request.... not most secure but should work
		if(!$request->token) {
			$this->authApp($app);
		}

        // check to see if any unsigned files remain
        $fileCount = File::where('app_id', $app->id)
        		->whereNull('archived_at')
        		->whereIn('file_type', [1, 2, 5, 9, 13])
        		->where(function($q){
					$q->has('unsignedSignaturesPri')
						->orHas('unsignedSignaturesCo');
				})
        		->orderBy('file_type', 'ASC')
        		->count();

        return response()->json($fileCount ? $request->token : 'complete');
	}
}
