<?php namespace App\Http\Controllers\Dealer;

use Auth;
use Request;
use App\Models\Contract;
use JWTAuth;


class DealerContentController extends \App\Http\Controllers\Base_Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		$this->middleware('auth:dealer', ['except' => ['getSharedLogin']]);
	}

	public function getIndex()
	{
		return redirect('support/pages');
	}

	public function getPages($url = null)
	{
		$url = $url ? $url : '';

		$protocol = 'https://';
		if(getenv('PRODUCTION') == "false"){
			$protocol = 'http://';
		}

		return view('dealer/support', ['url' => $protocol . '' . getenv('DOMAIN') . '/dealer-support/' . $url]);
	}

	public function getSharedLogin()
	{
		return view('dealer/shared-login');
	}

	public function getNews($slug = null)
	{
		$httpClient = new \GuzzleHttp\Client();
		$https = getenv('APP_ENV') == "local" ? 'http://' : 'https://';

		$slug = $slug ? '/' . $slug : '';

		try {
			$response = $httpClient->get($https . getenv('DOMAIN') . '/news' . $slug . '.json');
			$data = json_decode($response->getBody());
		} catch(\Exception $e) {
			$data = new \stdClass();
			$data->data[0] = 'Server connection issue';
		}

		if($slug){
			// get content from craft to display on dashboard
			return view('dealer/news/single', ['item' => $data]);
		}else{
			$newsItems = [];
			$product = $this->user->company->product;
			foreach($data->data as $item){
				if(empty($item->products)){
					$newsItems[] = $item;
				}
				elseif($product && in_array($product->name, $item->products)){
					$newsItems[] = $item;
				}
			}

			return view('dealer/news/index', ['news' => $newsItems]);
		}
	}

	// return a view for a dealer to request paperwork
	public function requestPaperwork() {
		return view('dealer/forms/request-paperwork');
	}

}
