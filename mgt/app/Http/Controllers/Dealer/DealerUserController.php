<?php namespace App\Http\Controllers\Dealer;

use Auth;
use App\Models\Dealer;
use App\Models\Contract;
use Illuminate\Http\Request;
use Hash;
use Password;
use DB;

class DealerUserController extends \App\Http\Controllers\Base_Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
		$this->middleware('admin',  ['except' => ['getIndex', 'postUpdateAccount']]);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$user = $this->user;

		//todo - add check for settings to abort if not allowed by company
		if($user->hasRole('admin')){
			$dealers = $user->company->primaryUsers;
		}else{
			$dealers = $user->subUsers;
		}

		return view('dealer/users', ['dealers' => $dealers] );
	}

	public function getEditProfile($id){
		$item = Dealer::find($id);
		$promos = $item->company->promos();
		$userPromoIds = $item->promos($item->company);

		return view('dealer/_edit-modal', ['item' => $item, 'promos' => $promos, 'userPromos' => $userPromoIds] );
	}

	public function postUpdate(Request $request){
		$id = $request->input('id');

		$this->validate($request,[
			'name' => 'max:255',
			'email' => 'email|max:255',
		]);

		$user = Dealer::find($id);

		if(!in_array($this->user->company->id, $user->companies()->pluck('dist_companies.id')->toArray())) {
			abort(403, 'Authentication error');
		}

		// if promo rights have changed
		if($request->promo) {
			$promoDenied = \App\Models\Relations\DealerDenyPromoRelation::where('promo_id', '=', $request->promo)
				->where('distributor_id', '=', $this->user->company->id)
				->where('user_id', '=', $id)->first();
			if($promoDenied) {
				$promoDenied->delete();
			} else {
				\App\Models\Relations\DealerDenyPromoRelation::create(['user_id' => $id, 'distributor_id' => $this->user->company->id, 'promo_id' => $request->promo]);
			}
		} else {
			// edit user information
			
			$user->name = $request->input('name');

			// update mailchimp if email changed
			if($user->email != $request->email) {


				// check and abort if a user already exists in the company with that email
				$newDealerEmail = Dealer::where('email', '=', $request->email)->first();

				if($newDealerEmail) {
					$isInCompanyAlready = DB::table('dist_dealer_relations')
						->where('distributor_id', '=', $this->user->company->id)
						->where('dealer_id', '=', $newDealerEmail->id)
						->count();

					if( $isInCompanyAlready ) {
						abort(403, 'This user already exists in your company');
					}
				}

				// if dealer belongs to another company also, we must not change his email - we will create a new user account instead
				if( $user->companies()->count() > 1) {
					if($newDealerEmail) {
						$newDealer = $newDealerEmail;
						$newDealer->companies()->attach($this->user->company->id);
					} else {
						$newDealer = $user->replicate();
						$newDealer->email = $request->email;
						$newDealer->save();
					}

					// remove association of old dealer with this distributor
					$user->companies()->detach($this->user->company->id);

					// set user variable back to whoever it should be just for future
					$user = $newDealer;
				} else {
					$user->email = $request->email;
					$user->save();
				}

				// add to mailchimp
				\Event::fire(new \App\Events\DealerCreated($user, $this->user->company));
			}

			$this->user->company->users()->updateExistingPivot($user->id, [
				'admin' => $request->admin
			]);
		}
	
		if($request->ajax()){
			return response()->json('success');
		}
	}

	public function postDelete(Request $request){
		$this->middleware('admin');
		$id = $request->input('id');
		$user = Dealer::find($id);

		// verify this user belongs to dealer
		if(!in_array($this->user->company->id, $user->companies()->pluck('dist_companies.id')->toArray())) {
			abort(403, 'Authentication error');
		}

		$dealerService = new \App\Services\DealerService();
		$dealerService->delete($user, $this->user->company, $this->user);

		return response()->json('success');	    
	}

	public function postUpdateAccount(Request $request){
		$this->user->name = $request->input('name');
		$this->user->save();
	    return response()->json('success');
	}

	public function postStore(Request $request){
		$this->validate($request,[
				'password' => 'confirmed|min:6',
				'email' => 'required|email|max:255',
		]);
		$data = $request->all();

		$dealer = Dealer::whereEmail($request->email)->first();

		// if not admin or if checked for subuser
		$isSubuser = ( isset($data['subuser']) && $data['subuser'] ) || !$this->user->hasRole('admin') ? true : false;

		if( $dealer ) {
			if( $dealer->companies->contains($this->user->company) ) {
				abort(403, 'This user already exists in your company');
			} else {
				$dealer->companies()->attach($this->user->company->id, [
					'admin' => isset($data['admin']) ? $data['admin'] : 0,
					'parent_user_id' => $isSubuser ? $user->id : null,
					'user_status' => 'Approved'
				]);
			}

		} else {
			// creating new dealer

			$dealer = Dealer::create([
				'email' => $data['email'],
				'name' => $data['name'],
				'password' => isset($data['password']) ? Hash::make($data['password']) : null
			]);

			// update pivot entry
			$this->user->company->users()->save($dealer, [
				'admin' => isset($data['admin']) ? $data['admin'] : 0,
				'parent_user_id' => $isSubuser ? $this->user->id : null,
				'user_status' => 'Approved',
			]);

			// auto set some variables for the email view
	        view()->composer('auth.emails.password', function($view){
	            $protocol = 'https://';
	            $view->with([
	                'url' => $protocol . 'dealer.' . getenv('DOMAIN'),
	            ]);
	        });

			// send email for password resets
			if(!isset($data['password']) || $data['password'] == ""){
				Password::broker('dealers')->sendResetLink(['email' => $dealer->email], function($message) {
				    $message->subject('Belmont Finance - Set password');
				});
			}

			$dealer->save();
		}

		// add to mailchimp
		\Event::fire(new \App\Events\DealerCreated($dealer, $this->user->company));
		return $dealer;
	}
}
