<?php namespace App\Http\Controllers\Dealer;

use Auth;
use Request;
use Illuminate\Http\Request as OrigRequest;
use App\Libraries\Contracts\ContractLibrary;
use App\Libraries\Contracts\InstallmentContractLibrary;
use App\Libraries\Contracts\NoticeCancellationLibrary;
use App\Http\Requests\ApplicationPostRequest;
use App\Http\Requests\SalesSlipPostRequest;
use JWTAuth;
use Session;
use Carbon\Carbon;
use \App\Libraries\Contracts\Traits\PaperworkTrait;


class DealerApplicationController extends \App\Http\Controllers\Base_Controller {
    use PaperworkTrait; // gives us access to shared methods

    const NO_COAPP = 1;
    const COAPP_SPOUSE = 2;
    const COAPP_NON_SPOUSE = 3;

    /**
     * Create a new controller instance.
     *
     */

	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

	}

	/**
	 * Shows either an application form or a error page that they haven't been set up properly
	 *
	 * @return Response
	 */

	public function getIndex(OrigRequest $request)
	{  
        // keep in for legacy now, don't use this anymore....
        $token = JWTAuth::fromUser($this->user);

		if ($this->user->company->account_number) {
			$protocol = (getenv('APP_ENV') == "production") ? 'https://' : 'http://';

            $userOfCompany = $this->user->companies()->where('dealer_id', '=', $this->user->id)->first();

			if ($userOfCompany->pivot->user_status != "Approved" || $this->user->company->dealer_status == "Pending") {
				return view('errors.dealer-not-authorized');
			} else {

                // clear our saved sessions for now, having an issue with these
                Session::forget('appFields');
                Session::forget('appId');
                Session::forget('app-sign-required');

                // mark old signature process
                if($request->sign == 1) {
                    Session::set('app-sign-required', true);
                }

				// ask to load previous app if available
				if(false) {
					$storedApps = [];

					// recreate array of sessions and names to pass to view
					foreach(Session::get('appFields') as $key => $value)
					{
						if(isset($value['firstName']) && isset($value['lastName'])) {
							$storedApps[] = ['saveId' => $key, 'name' => $value['firstName'] . ' ' . $value['lastName']]; 
						}
					}

					return view('dealer/applications/load-previous-app', ['apps' => $storedApps]);
				} else {
						// set a new session key to ensure uniqueness of this application
					$data = 'null';
					do {
						$randomKey = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);
						$sessionKey = $randomKey;
					} while (Session::has('appFields.' . $sessionKey)); // ensure session key is unique

                    $view = $request->sign ? 'dealer.applications.application' : 'dealer.applications.application-no-sign';
					
					return view($view, [
                        'data' => $data,
                        'sessionKey' => $sessionKey,
                        'submitFormUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/apps/submit-application?token=' . $token
                    ]);
				}
			}
		} else {
			return view('dealer/applications/application-error');
		}

	}

	public function getTempError() {
		return view('dealer/temp-error');
	}


    /**
    * Loads a formatted pdf for signing
    * Sign the credit application (locally, not remotely)
    */
	public function getSign(){
		$sessionKey = Request::get('ses') ? Request::get('ses') : Session::get('dealer.currentSessionKey');

        // redirect if session doesn't exist
        /*if(!isset(Session::get($sessionKey)['lastName'])) {
            return redirect('/apps');
        }*/

        $user = $this->user;
        Session::set('appFields.' . $sessionKey . '.dealerId', $user->id);
        Session::set('appFields.' . $sessionKey . '.ip_address', Request::ip());

        // save the application before signing
        if(Session::has('appId')) {
            $app = \App\Models\ContractApplication::where('id', '=', Session::get('appId'))->first();
            $app = $this->saveApp(Session::get('appFields.' . $sessionKey), $user, $app);
        } else {
            $app = $this->saveApp(Session::get('appFields.' . $sessionKey), $user);
        }

        Session::put('appId', $app->id);



        // get the identification number of the file we're using
        $creditType = ( $user->company->allow_revolving == 1 ||
            ( $user->company->allow_revolving == 2
                && Session::get('appFields.' . $sessionKey . '.credit_type') == 'Revolving' )) ? 'revolving' : 'closed-end';

        $fileId = ContractLibrary::retrieveCreditAgreement($user, $creditType, Session::get('appFields.' . $sessionKey . '.language'));

		// generate a pdf prior to signing with the data all filled in
		$unsignedPdf = ContractLibrary::generatePdf($fileId, Session::get('appFields.' . $sessionKey), $app->id);

		// get an image of the pdf to display for the signee
		$pdfImage = ContractLibrary::generatePdfImage($unsignedPdf);

        // determine corelationship value, defaults to single buyer = 1

        $coAppRelationship = DealerApplicationController::NO_COAPP;

        if(Session::get('appFields.' . $sessionKey . '.coLastName')) {
            $coAppRelationship = (Session::get('appFields.' . $sessionKey . '.coRelation') == "spouse") ? DealerApplicationController::COAPP_SPOUSE : DealerApplicationController::COAPP_NON_SPOUSE;
        }

        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );
        $token = JWTAuth::fromUser($this->user);

        if(Session::has('dealer.orderUrl')) {
            $includeSuccess = 'dealer.contract._sales-slip-continue';
        } else {
            $includeSuccess = null;
        }
        
        // get signature locations
        $signatureLocations = ContractLibrary::retrieveSignatureLocations($fileId, Session::get('appFields.' . $sessionKey . '.state'), Session::get('appFields.' . $sessionKey . '.coLastName') ? true : false);

        // send the user to sign
        return view('dealer/applications/sign', [
            'app' => $app,
            'buyerKey' => $coAppRelationship, 
            'includeSuccess' => $includeSuccess,
            'pdfImage' => $pdfImage,
            'sessionKey' => $sessionKey, 
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/apps/submit-application?token=' . $token,
            'language' => Session::get('appFields.' . $sessionKey . '.language')
        ]);
    }


    // submits the application without signing
    public function submitNoSign() {
        $sessionKey = Request::get('ses') ? Request::get('ses') : Session::get('dealer.currentSessionKey');

        $user = $this->user;
        Session::set('appFields.' . $sessionKey . '.dealerId', $user->id);
        Session::set('appFields.' . $sessionKey . '.ip_address', Request::ip());

        // save the application before signing
        if(Session::has('appId')) {
            $app = \App\Models\ContractApplication::where('id', '=', Session::get('appId'))->first();
            $app = $this->saveApp(Session::get('appFields.' . $sessionKey), $user, $app);
        } else {
            $app = $this->saveApp(Session::get('appFields.' . $sessionKey), $user);
        }

        Session::put('appId', $app->id);

        // get the identification number of the file we're using
        $creditType = ( $user->company->allow_revolving == 1 ||
            ( $user->company->allow_revolving == 2
                && Session::get('appFields.' . $sessionKey . '.credit_type') == 'Revolving' )) ? 'revolving' : 'closed-end';

        $fileId = ContractLibrary::retrieveCreditAgreement($user, $creditType, Session::get('appFields.' . $sessionKey . '.language'));

        // generate a pdf prior to signing with the data all filled in
        $creditLibrary = new \App\Libraries\Contracts\CreditLibrary($app, $fileId);
        $unsignedPdf = $creditLibrary->addTerms(Session::get('appFields.' . $sessionKey));

        // get an image of the pdf to display for the signee
        $pdfImage = ContractLibrary::generatePdfImage($unsignedPdf);

        $app->status = 0;
        $app->save();

        $contractLibrary = new ContractLibrary();
        $contractLibrary->emailOnApplication($app);

        return view('dealer.contract.app-submit-success');
    }


    // view the sales slip form
    public function getSalesSlip(OrigRequest $request, $appId) {
        $app = \App\Models\ContractApplication::findOrFail($appId);

        Session::forget('sales-slip-data');
        Session::put('sales-slip-data.app_id', $appId);
        Session::put('credit_type', $request->credit_type);
                // just save a couple session variables from previous selections...
        $request->session()->set('paperwork_options', [
            'completion' => $request->completion == 'Yes' ? 1 : 0
        ]);

        // make sure correct access
        $this->authSalesSlip($app);

        // update application with given credit type if it changes
        if(($request->credit_type == 'Revolving' || $request->credit_type == 'Closed-End') && $app->credit_type != $request->credit_type) {
            $app->credit_type = $request->credit_type;
            $app->save();
        }

        $paperworkService = new \App\Services\PaperworkService($app, $this->user);
        if($error = $paperworkService->failedSetup()) {
            return view('dealer.paperwork.error-setup', ['error' => $error, 'app' => $app]);
        }


        return view('dealer.sales-slips.sales-slip-data', [
            'app' => $app,
            'states' => $this->user->company->dealerStates->lists('state'),
            'promos' => $app->eligiblePromos($this->user, $this->user->company),
            'completion' => $request->input('completion')
        ]);
    }
    

    // view and sign sales slip / contract
    public function getSignSalesSlip() {
        $app = \App\Models\ContractApplication::findOrFail(Session::get('sales-slip-data.app_id'));

        // make sure correct access
        $this->authSalesSlip($app);

        // Check which type of agreement is being signed
        $state = Session::get('sales-slip-data.state');


        $fileId = $this->retrieveSalesSlip($state, $app);     

        // get our installment contract library
        $salesSlip = new InstallmentContractLibrary($app, $fileId);


        // generate a pdf prior to signing with the data all filled in
		$unsignedPdf = $salesSlip->addTerms(Session::get('sales-slip-data'), $this->user);

        $pdfImage = ContractLibrary::generatePdfImage($unsignedPdf);

        // get signature locations
        $signatureLocations = ContractLibrary::retrieveSignatureLocations($fileId, $state);

        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );
        $token = JWTAuth::fromUser($this->user);
        $successView = 'dealer.contract._agreement';
        return view('dealer/applications/sign', [
            'app' => $app,
            'buyerKey' => $app->coLastName ? 3 : 1,
            'includeSuccess' => $successView,
            'pdfImage' => $pdfImage,
            'sessionKey' => 'test',
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/contracts/submit-sales-agreement?token=' . $token
        ]);
    }


    /* send the contract / sales slip to the consumer to sign remotely */
    public function sendSalesSlip(OrigRequest $request) {
        
        // save sales slip data and create file
        $app = \App\Models\ContractApplication::findOrFail($request->session()->get('sales-slip-data.app_id'));

        // make sure correct access
        $this->authSalesSlip($app);

        // Check which type of agreement is being signed
        $state = $request->session()->get('sales-slip-data.state');
        $fileId = $this->retrieveSalesSlip($state, $app); 

        // get our installment contract library
        $salesSlip = new InstallmentContractLibrary($app, $fileId);

        // generate a pdf prior to signing with the data all filled in
        $unsignedPdf = $salesSlip->addTerms(Session::get('sales-slip-data'), $this->user);

        // get file id through the local path
        $existFileId = \App\Models\File::where('local_path', '=', $unsignedPdf)->first();

        // get reference to remote signing library
        $remoteHelper = new \App\Libraries\Contracts\RemoteSign($fileId);
        $data = $request->session()->get('sales-slip-data');

        // mark coapplicant if sending separately
        if($request->separate) {
            $data['coEmail'] = $app->coEmail;
            $coApp = 1;
        } else {
            $coApp = 0;
        }

        $remoteHelper->sendSignatureRequest($app->email, $app->dealerUser, $data, $coApp, $existFileId->id);

        return view('dealer.sales-slips.sent-remote');
    }


    // view and sign the notice of cancellation for this project
    public function getSignCancellation(Request $request, $appId) {
        $app = \App\Models\ContractApplication::findOrFail($appId);

        // make sure correct access
        $this->authSalesSlip($app);

        $state = $app->salesSlip->sale_state;

        $files = $this->retrieveNoticeCancellation($state, $app->dealerUser->company);

        // notice of cancellations can have multiple pdfs
        // string them together and combine them
        $cancellation = new NoticeCancellationLibrary($app, $files);

        $signatureLocations = [];
        $i = 0;

        // generate a pdf prior to signing with the data all filled in
        $unsignedPdf = $cancellation->addTerms($state, $this->user);
        $pdfImages = ContractLibrary::generatePdfImage($unsignedPdf->local_path);

        // same as in RemoteSign Library...
        foreach($files as $fileId) {

            // get signature locations
            $newSignatureLocations = ContractLibrary::retrieveSignatureLocations($fileId, $state);
            foreach($newSignatureLocations as &$sig) {
                // add a page worth of y value to each signature
                if(isset($sig[1])) {
                    $sig[1] = $sig[1] + 100 * $i;
                }
            }
            $signatureLocations = array_merge($signatureLocations, $newSignatureLocations);
            $i++;
        }

        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );
        $token = JWTAuth::fromUser($this->user);
        $successView = 'dealer.contract._cancellation-sign-success';
        return view('dealer/applications/sign', [
            'app' => $app,
            'buyerKey' => $app->coLastName ? 3 : 1,
            'includeSuccess' => $successView,
            'pdfImage' => $pdfImages,
            'sessionKey' => 'test',
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/contracts/submit-cancellation?token=' . $token
        ]);
    }


    /**
    * Validate each step of the application
    */
	public function postSubmitApplication(ApplicationPostRequest $request)
	{
        // validate first
        $contractLibrary = new ContractLibrary();
		$contractLibrary->postSubmitApplication($request);
        return response()->json('success');
	}

	/**
    * Validate each step of the sales slip form
    */
	public function postSubmitSalesSlip(SalesSlipPostRequest $request)
	{
        // validate first
        $contractLibrary = new ContractLibrary();
		return $contractLibrary->postSubmitSalesSlip($request);
	}


	/**
	 * Shows view to set signature for dealer
	 *
	 * @return Response
	 */
	public function getSetSignature()
	{
		return view('dealer/applications/set-signature');
	}

    /**
     * Records signature for 
     *
     * @return Response
     */
    public function postSetSignature(OrigRequest $request)
    {
        if($request->signature) {
            $this->user->signature = $request->signature;
            $this->user->save();
        } else {

        }
    }

    // route to show confirmation to cancel application
    public function confirmCancel($id)
    {
        // this will authenticate the dealer's app for us if we pass dealer as second parameter
        $appHandler = new \App\Services\AppService($id, $this->user);
        return view('dealer.applications.confirm-cancel', ['app' => $appHandler->app]);
    }


    // route to cancel an application
    public function cancel($id)
    {
        // passing the dealer to the appservice authenticates the dealer to the app
        $appHandler = new \App\Services\AppService($id, $this->user);
        $appHandler->cancel();

        return response()->json('success');
    }


    // route to show confirmation to reset salesslip
    public function confirmResetSalesSlip($id)
    {
        // this will authenticate the dealer's app for us if we pass dealer as second parameter
        $appHandler = new \App\Services\AppService($id, $this->user);
        return view('dealer.applications.confirm-reset-sales-slip', ['app' => $appHandler->app]);
    }
    // route to actually perform the reset of the sales slip
    public function resetSalesSlip($id)
    {
        $appHandler = new \App\Services\AppService($id, $this->user);
        $appHandler->resetSalesSlip();

        return response()->json('success');
    }
    

    // make sure user is authenticated to view this sales slip
    private function authSalesSlip($app) {
        if( $this->user->id === $app->dealerId){
           // is authenticated
        }else{
            // check for admin user
            if($this->user->hasRole('admin', $app->distributor) && $this->user->companies()->where('dist_companies.id', '=', $app->distributor->id)->exists()) {
                // authenticated
            } else {
                abort(403, 'Authorization error');
            }
        }
    }

    private function saveApp($data, $dealer, $app = null) {
                    // we have a lot of extra miscellaneous data, so only pass defined fields to application
        $fieldsToSave = [
            'firstName','lastName','initial','address','addressTwo','city','state','zip','yrsAtAddress','prevAddress','prevCityStateZip','birthdate','homePhone','cellPhone','ssn','email','numDependents','employer','yrsEmployed','milRank','employerAddress','employerCityStateZip','jobTitle','monthlyGrossPay',
            'employerPhone','employerExt','prevEmployer','prevYrsEmployed','prevEmployerAddress','prevEmployerCityStateZip','prevJobTitle','prevEmployerPhone','prevEmployerExt','coFirstName','coLastName','coInitial','coBirthdate','coAddress','coAddressTwo','coCity','coState','coZip','coEmployer','coYrsEmployed','coMilRank','coEmployerAddress','coEmployerCityStateZip','coJobTitle','coMonthlyGrossPay','coEmployerPhone','coEmployerExt','refLandlordName','refPropertyValue','refAccountNumber','refLandlordPhone','refLandlordAdress',
            'refLandlordCity','refLandlordState','refLandlordZip','refLandlordMonthlyPayment','refLandlordPresBalance','credrefOneName','credrefOneAccount','credrefOnePhone','credrefOneStreetAdress','credrefOneCity','credrefOneState','credrefOneZip','credrefOneMonthlyPayment','credrefOneMonthlyPresBalance','credrefTwoName','credrefTwoAccount','credrefTwoPhone','credrefTwoStreetAdress','credrefTwoCity','credrefTwoState','credrefTwoZip','credrefTwoMonthlyPayment','credrefTwoMonthlyPresBalance','buyerCheckingAccountNum','buyerCheckingBank','buyerCheckingBankCity','buyerSavingsAccountNum','buyerSavingsBank','buyerSavingsBankCity','yearofBankruptcy','cityStateOfBankruptcy','personalRefOneName','personalRefOnePhone','personalRefOneRelationship','personalRefTwoName','personalRefTwoPhone',
            'personalRefTwoRelationship','initials','buyerSign','buyerDate','coSign','coDate','monthsAtAddress','prevAddressTwo','prevYrsAtAddress','prevMonthsAtAddress','selfEmployed','monthsEmployed','prevMonthsEmployed','coRelation','coSelfEmployed','coMonthsEmployed','residenceStatus','driversOrId','residenceType','buyerCheckingAccount','buyerSavingsAccount','declaredBankruptcy','coHomePhone','coCellPhone','co_ssn','coDriversOrId','coEmail','refLandlordAddress','dealerId','dealer_req_by','lastStatus','amount_financed','revolving','credit_type','buyerCreditCards','buyerCreditReferences','fca_id','distributor_account',
            'licExpDate','licIssueDate','revolving_signature','revolving_signature_cobuyer','buyerAutoLoan','pdf_path','sales_agreement','description','addon','sales_agreement_signed','down_payment_amount','sales_tax_amount','shipping_amount','sale_description','agreementType','apr','doc_fees','loanLength','defer','tradein','product_model','product_serial','product_attachments', 'dist_id',
            'identification_type', 'identification_state', 'co_identification_type', 'co_identification_state', 'co_licExpDate', 'mailing_address', 'mailing_address_two', 'mailing_city', 'mailing_zip', 'co_mailing_address', 'co_mailing_address_two', 'co_mailing_city', 'co_mailing_zip', 'other_income_source', 'other_income_amount', 'co_other_income_source', 'co_other_income_amount', 'coRefLandlordMonthlyPayment', 'ip_address'
        ];

        // store status as negative one to filter apps that did not sign, didn't continue on
        $finalAppData = [
            'is_online' => 1, //$dealer->company->enable_new_online ? 1 : null, // only if dealer is  marked for new paperwork setip
            'status' => -1
        ];

        foreach($fieldsToSave as $field) {
            $finalAppData[$field] = (isset($data[$field]) && $data[$field]) ? $data[$field] : '';
        }


        if($app) {
            $app->update($finalAppData);
        } else {
            $app = \App\Models\ContractApplication::create($finalAppData);
        }
        return $app;
    }
}
