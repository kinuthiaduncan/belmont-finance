<?php namespace App\Http\Controllers\Dealer\Traits;
/**
 * Helper Methods for Dealer controllers
 */
 
trait AuthenticatesDealerTrait
{
    // make sure user is authenticated to this app
    protected function authApp($app) {
        if( $app && $this->user->id === $app->dealerId){
           // is authenticated
        } else {
            // check for admin user
            $appUser = \App\Models\Dealer::where('id', '=',  $app->dealerId)->first();
            if($appUser && ($this->user->company->id === $appUser->company->id) && $this->user->hasRole('admin', $this->user->company)) {
                // authenticated
            } else {

            	// final auth check
            	if(($this->user->company->account_number == $app->distributor_account) && $this->user->hasRole('admin', $this->user->company)) {

            	} else {
            		abort(403, 'Authorization error');
            	}
            }
        }
    }
}
