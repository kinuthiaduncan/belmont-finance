<?php namespace App\Http\Controllers\Dealer;

use App\Models\File;
use Illuminate\Http\Request;
use App\Services\FileService;
use App\Http\Controllers\Dealer\Traits\AuthenticatesDealerTrait;


class DealerFileController extends \App\Http\Controllers\Base_Controller {
	use AuthenticatesDealerTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}


	// allows dealer to view a given file
	// accepts the id of a file record
	public function view($appId, $fileId)
	{
		$file = File::where('app_id', '=', $appId)
			->where('id', '=', $fileId)->firstOrFail();

		if(!in_array($file->file_type, [1,2,5,7,9,11])) {
			abort('403', 'You are not allowed to access this file type. Please contact Belmont Finance if you need help.');
		}

		$this->authApp($file->app);

		return response()->file(base_path($file->local_path));
	}


	// loads the view to allow the dealer to upload a file
	// if application id is passed through request, will load different set of options, 
	public function viewUploadFile($appId = null)
	{
		$app = \App\Models\ContractApplication::find($appId);
		if($app) {
			$this->authApp($app);
		}
		return view('dealer.file-upload', ['app' => $app]);
	}

	// handle the uploaded file
	public function handleUpload(Request $request)
	{
		$this->validate($request, [
	        'file' => 'required|mimes:jpeg,jpg,png,pdf,bmp'
	    ]);

		// create temporary file
	    $tempFile = FileService::storeTemporary($request, $this->user);

		return response()->json( $tempFile->id );
	}


	// assign the file to an app 
	public function assignUpload(Request $request)
	{
		// upload other document type
		if($request->description) {

			$this->validate($request, [
		        'description' => 'required|max:255',
		        'tempUploads' => 'required|regex:/^[\d\s,]*$/',
		        'appId' => 'required|integer'
		    ]);

		    $app = \App\Models\ContractApplication::findOrFail((int)$request->appId);
		    $this->authApp($app);


			
			// upload for new application
		} else {
		
		    $this->validate($request, [
		        'lastName' => 'required|max:32',
		        'firstName' => 'required|max:32',
		        'ssn' => 'required|digits:4',
		        'tempUploads' => 'regex:/^[\d\s,]*$/'
		    ]);

			// create mostly empty new application
			$app = \App\Models\ContractApplication::create([
				'firstName' => $request->firstName,
				'lastName' => $request->lastName,
				'ssn' => $request->ssn,
				'dealerId' => $this->user->id,
				'dist_id' => $this->user->company->id,
				'is_online' => 1,
				'status' => -1
			]);

			$docId = 1;
		}

		$ids = array_filter( explode(',', $request->tempUploads) );
		$fileHelper = new FileService();
		$fileHelper->assignTemporary($ids, $app, $request->description);

		return response()->json('success');
	}
}
