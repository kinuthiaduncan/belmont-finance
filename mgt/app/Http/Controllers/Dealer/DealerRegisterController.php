<?php namespace App\Http\Controllers\Dealer;

// existing dealers create their account

use App\Http\Requests\DealerLoginRequest;
use App\Http\Requests\DealerUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Models\Dealer;
use App\Models\DealerCompany;
use Auth;
use DB;
use Session;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;

class DealerRegisterController extends \App\Http\Controllers\Base_Controller {

	/*
	|User controller for managing all user stuff
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return view('auth/register');

	}

	public function postIndex(DealerLoginRequest $request)
	{
		if($request->input('username') && $request->input('password')){
			$old_record = DB::connection('mysql_old')
				->table('distributors')
				->where('USERNAME', '=', $request->input('username'))
				->where('PASSWORD', '=', $request->input('password'))
				->first();

			if($old_record && $old_record->ID){
				// use a date in 2016 to mark distributors who have already gone through the activation process
				if($old_record->SIGNUP_DATE < "2016-01-01 00:00:00"){
					// set session value to confirm step two process
					Session::put('dealer_register', $old_record->ID);
					return view('auth/register-two', ['company' => $old_record]);
				}else{
					return view('auth/register', ['other_errors' => ['Your account was already activated - contact Belmont Finance if you need help.']]);
				}
			}else{
				return view('auth/register', ['other_errors' => ['No matching user / password combination found - contact Belmont Finance if you need help.']]);
			}
		}else{

		}

		
	}

	public function postFinalize(DealerUpdateRequest $request){
		if(Session::has('dealer_register')){
			$companyID = Session::pull('dealer_register');
			$dealer = new Dealer;
			$dealer->email = $request->input('email');
			$dealer->name = $request->input('name');
			$dealer->admin = 1;
			$newCoID = DealerCompany::where('dist_id','=',$companyID)->first()->id;
			$dealer->company_id = $newCoID;
			$dealer->save();

			// send email for password resets
			Password::dealer()->sendResetLink(['email' => $dealer->email], function($message) {
			    $message->subject('Belmont Finance Account Creation');
			});

			// mark distributor as activated
			DB::connection('mysql_old')
				->table('distributors')
				->where('ID', '=', $companyID)
				->update(['SIGNUP_DATE' => '2016-01-01 00:00:00']);

			return view('auth/register-done');
		}else{
			return view('auth/register');
		}
	}

}
