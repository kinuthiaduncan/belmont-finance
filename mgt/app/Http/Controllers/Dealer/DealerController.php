<?php namespace App\Http\Controllers;

//this one is for handling logins from a logged in dealer / distributor (the other one is for belmont staff requests)

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Auth;

class DealerController extends Base_Controller {

	/*
	|User controller for managing all user stuff
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		//authenticate dashboard requests
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index($action)
	{
		if($action == "logout")
		{
			Auth::logout();
    		return redirect('/');
		}
	}

}
