<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Models\User;
use Auth;
use Session;
use Hash;
use Password;

class UserController extends Base_Controller {

	/*
	|User controller for managing all user stuff
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		//authenticate dashboard requests
		$this->middleware('auth', ['except' => ['logout']]);

		// admin except for logout
		$this->middleware('admin', ['except' => ['logout']]);
	}

	/**
	 * Logs out a user
	 *
	 * @return Response
	 */
	public function logout()
	{
		Session::forget('admin-pseudo');
		Session::forget('admin-pseudo-cust');
		Session::forget('contractId');

		if($this->context == "staff"){
			\Auth::guard('web')->logout();
		}elseif($this->context == "dealer"){
			Session::forget('selectedDistributorId');
			\Auth::guard('dealer')->logout();
		}elseif($this->context == "accounts"){
			\Auth::guard('consumer')->logout();
		}
		return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
	}

	/**
	 * Shows the basic profile for a user
	 *
	 * @return Response
	 */
	public function getProfile($id)
	{
		$user = User::find($id);
		if($user){
			return view('staff/user/profile', ['title' => 'User Profile', 'tab' => 'staff', 'item' => $user]);
		}
		else{
			abort(404);
		}
	}

	/**
	 * Update role for a user
	 *
	 * @return Response
	 */
	public function postUpdateRole(Request $request, $userId)
	{
		$this->validate($request, [
	        'role' => 'required|in:admin,csr,loan,distributor',
	        'value' => 'boolean|required',
	    ]);

		$user = User::find($userId);
		if($user->id != $this->user->id){
			$user->{$request->role} = $request->value;
			$user->save();
			if($request->ajax()){
				return response()->json('success');
			}
		}
		else
		{
			abort('403', 'You can\'t edit your own user permissions');
		}
	}

	public function postUpdatePassword(Request $request)
	{
		$this->validate($request, [
	        'old_password' => 'required',
	        'password' => 'required|confirmed|min:8',
	    ]);

		if (Auth::guard('web')->attempt(['email' => $this->user->email, 'password' => $request->input('old_password')])) {
		    $this->user->password = Hash::make($request->input('password'));

		    $this->user->save();

		    if($request->ajax()){
				return response()->json('success');
			}
		}
		else{
			abort(403, 'Incorrect password');
		}

	}


	/**
	 * deletes a user
	 *
	 * @return Response
	 */
	public function delete($id)
	{
		// must be an admin to delete users
		if(\Auth::guard('web')->user()->hasRole('admin')){
			$user = User::find($id);
			if($user){
				$user->delete();
				return 'true';
			}
			else{
				abort(404);
			}
		}
		else{
			abort(401);
		}
	}

	/**
	 * bypass login for admin
	 *
	 * @return Response
	 */
	public function getSimulate($id)
	{
		$user = User::find($id);
		if($user){
			\ Auth::guard('web')->login($user);
			return redirect('/');
		}
		else{
			abort(404);
		}
	}



	public function postSearch(Request $request)
	{
		$searchTerm = $request->input('search');
		$users = User::where('name', 'LIKE', '%' . $searchTerm . '%')->get();

		return view('staff/user/index', ['tab' => 'staff', 'users' => $users, 'showBack' => '/users\/', 'term' => $searchTerm]);
	}


	/**
	 * Shows the user dashboard
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		//get list of all users for index
		$users = User::orderBy('name','desc')->paginate(20);
		return view('staff/user/index', ['title' => 'Users', 'tab' => 'staff', 'users' => $users]);
	}


	public function postStore(Request $request){
		$messages = [
			'role.required' => 'You must specify at least one user role'
		];

		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'confirmed|min:6',
			'role' => 'required|min:1',
		], $messages);

		$data = $request->all();

		$userRoles = [
			'admin' =>  false,
			'loan' =>  false,
			'loan_manager' => false,
			'csr' => false,
			'csr_manager' => false,
			'distributor' => false,
			'distributor_manager' =>  false
		];

		//fill in user roles
		if(isset($data['role'])){
			if(is_array($data['role']))
			{
				foreach($data['role'] as $value)
				{
					if(array_key_exists($value, $userRoles))
					{
						$userRoles[$value] = true;
					}
				}
			}
			else{
				if(array_key_exists($data['role'], $userRoles))
				{
					$userRoles[$data['role']] = true;
				}
			}
		}

		$userData = array_merge($userRoles,
			[
				'name' => $data['name'],
				'email' => $data['email'],
				'password' => bcrypt($data['password']),
			]
		);

		$user = User::create($userData);

		if(!$data['password'] || $data['password'] == ""){
			// send email for password resets
			Password::broker('users')->sendResetLink(['email' => $user->email], function($message) {
			    $message->subject('Account created');
			});
		}

		return $user;
	}

}
