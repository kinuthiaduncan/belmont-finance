<?php namespace App\Http\Controllers;
// logout controller withouot auth middleware, primarily using it to share variables off the base controller

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Cookie;
use App\Models\User;

class LoginController extends Base_Controller {

	/*
	User controller for managing all user stuff
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
	}

	/**
	 * Login view
	 *
	 * @return Response
	 */
	public function loginView(Request $request)
	{
		$cookieUser = NULL;
		if($request->input('clear') !== "1"){
			if($staffCookie = $request->cookie('user_id'))
			{
				$user = User::find($staffCookie);
				$cookieUser = [
					"name" => $user->name,
					"email" => $user->email
				];

			}
		}
		
		return view('auth/login', ['cookieUser' => $cookieUser]);
	}

	public function homeRedirect(){
		return redirect('/');
	}

}
