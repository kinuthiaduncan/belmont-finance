<?php namespace App\Http\Controllers;

use \App\Models\ContractApplication;
use \App\Models\File;
use Illuminate\Http\Request;

// staff side file controller


class FileController extends Base_Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();
		$this->middleware('auth');
	}

	/* load  view for staff to look at pages of a file and review */
	public function reviewFile($id) {
		$file = File::findOrFail($id);
		$thumbnails = \App\Services\FileService::getThumbnails($file);

		return view('staff.file.review-file', ['pages' => $thumbnails, 'file' => $file]);
	}

	public function postReviewFile(Request $request, $id) {
		$this->validate($request, [
	        'pages.*' => 'required|integer',
	    ]);

		$file = File::findOrFail($id);

		$fs = new \App\Services\FileService();
		$fs->arrangeFiles($request, $file);

		return redirect($request->session()->get('staff.history.2', '/apps'));
	}

}
