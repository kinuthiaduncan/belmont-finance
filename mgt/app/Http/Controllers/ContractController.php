<?php namespace App\Http\Controllers;
use \App\Models\Contract;
use \App\Models\ContractApplication;
use \App\Models\DealerCompany;
use \App\Models\Promotional;
use \App\Models\Rating;
use \App\Http\Requests\ContractUpdateRequest;
use Request;
use Auth;
use Session;
use Log;
use Mail;
use Carbon\Carbon;

class ContractController extends Base_Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		//authenticate dashboard requests
		$this->middleware('auth');
		$this->request = new Request;

		// pass these variables to contract index template - can pass filter, routePath, and contracts variable to view (and pagetitle)
		$this->viewArray = [
			'tab' => "contracts",
			'filter' => "all",
			'otherFilters' => $this->translateFilters(Session::get('staff.contracts.filters', [])),
			'contracts' => NULL,
			'routePath' => Request::path(),
			'showBack' => false,
			'reviewCount' => Contract::whereApproved(false)->count()
		];

		// different types of filters for contracts
		$this->filterArray = [
			"pending" => 0,
			"cancelled" => 2,
			"approved" => 1
		];

		$this->dateFilter = Session::get('staff.contracts.filters.Date+Range', '');
		$this->distributorFilter = Session::get('staff.contracts.filters.Distributor', '');
		$this->productFilter = Session::get('staff.contracts.filters.Product', '');
	}

	/* Filterig functions */
	//allows to set session for filters other than pending etc
	// make a post request, store the post filter in session, redirect back to original url
	public function postFilter(){
		if(Request::has('dateRange')){
			if(Request::input('dateRange') == "custom"){
				// Issue is here, passing in array instead of an argument
				Session::set('staff.contracts.filters.Date+Range', ['start' => Request::input('start'), 'end' => Request::input('end')]);
			}else{
				Session::set('staff.contracts.filters.Date+Range', Request::input('dateRange'));
			}
		}

		if(Request::has('product')){
			Session::set('staff.contracts.filters.Product', Request::input('product'));
		}

		return redirect(Request::input('_redirect'));
	}


	public function getFilter(){
		if(Request::has('distributor')){
			$dealer = DealerCompany::withTrashed()->where('account_number', '=', Request::input('distributor'))->first();
			if($dealer) {
				Session::set('staff.contracts.filters.Distributor', Request::input('distributor'));
			} else {
				// no dealer, show non matching page, don't set filter
				return redirect('contracts/all');
			}
		}

		$history = Request::session()->get('staff.history.2', 'contracts/all');
		if(strpos($history,'contracts') === "0"){
			return redirect($history);
		}else{
			return redirect('contracts/all');
		}

	}

	// clear a filter by using the url contracts/clear-filter/thefilterinquestion, note that we're using a staff 'namespace' for the session
	public function getClearFilter($filter = null){
		Request::session()->forget('staff.contracts.filters.' . $filter);
		return redirect(Request::session()->get('staff.history.2', 'contracts/all'));
	}



	// show all contracts, also handles filters
	public function getIndex($filter = null)
	{
		return redirect()->action('ContractController@getAll');
	}

	// show all contracts, also handles filters
	// handles filters via parameters
	//
	public function getAll($filter = "all")
	{

		$contracts = Contract::ofDist($this->distributorFilter)->ofDateRange($this->dateFilter)->ofStatus($filter)->ofProduct($this->productFilter)->orderBy('applicationDate','desc')->paginate(15);
		$products = \App\Models\DealerProduct::get();

		$this->viewArray['routePath'] = 'contracts/all';
		$this->viewArray['contracts'] = $contracts;
		$this->viewArray['filter'] = $filter;
		$this->viewArray['products'] = $products;
		return view('staff/contract/index', $this->viewArray);
	}

	// if someone posts a search for contracts
	public function postSearch()
	{
		$searchTerm = Request::input('search');

		if( is_numeric(substr($searchTerm, 1, 1)) ){
			// searching by account number
			$contracts = Contract::ofDateRange($this->dateFilter)->where('accountNumber', 'LIKE', '%' . $searchTerm . '%')->orWhere('applicationId', 'LIKE', '%' . $searchTerm . '%')->paginate(15);
		}else{
			// searching by name
			$contracts = Contract::ofDateRange($this->dateFilter)->where('customerName', 'LIKE', $searchTerm . '%')
				->orWhere('customerName', 'LIKE', '%' . $searchTerm)->paginate(15);
		}

		// store search term in session
		Request::session()->set('staff.search', $searchTerm);

		$this->viewArray['pagetitle'] = 'Contract Search: "' . $searchTerm . '"';
		$this->viewArray['contracts'] = $contracts;
		$this->viewArray['showBack'] = '/contracts\/';

		return view('staff/contract/index',  $this->viewArray);
	}


	// handle filtering a contract search
	public function getSearch($filter = null)
	{
		if (Request::session()->has('staff.search')) {

			$searchTerm = Request::session()->get('staff.search');

			if($filter && array_key_exists($filter, $this->filterArray)){
				if( is_numeric(substr($searchTerm, 1, 1)) ){
					// searching by account number
					$contracts = Contract::ofDateRange($this->dateFilter)->where('accountNumber', '=', $searchTerm)->where('statusId', '=', $this->filterArray[$filter])->paginate(15);
				}else{
					// searching by name
					$contracts = Contract::ofDateRange($this->dateFilter)->where('customerName', 'LIKE', $searchTerm . '%')->where('statusId', '=', $this->filterArray[$filter])
						->orWhere('customerName', 'LIKE', '%' . $searchTerm)->ofDateRange($this->dateFilter)->where('statusId', '=', $this->filterArray[$filter])->paginate(15);
				}

				$this->viewArray['filter'] = $filter;
			}
			else{
				if( is_numeric(substr($searchTerm, 1, 1)) ){
					// searching by account number
					$contracts = Contract::ofDateRange($this->dateFilter)->where('accountNumber', '=', $searchTerm)->paginate(15);
				}else{
					// searching by name
					$contracts = Contract::ofDateRange($this->dateFilter)->where('customerName', 'LIKE', $searchTerm . '%')
						->orWhere('customerName', 'LIKE', '%' . $searchTerm)->ofDateRange($this->dateFilter)->paginate(15);
				}
			}

			$this->viewArray['pagetitle'] = 'Contract Search: "' . $searchTerm . '"';
			$this->viewArray['contracts'] = $contracts;
			$this->viewArray['routePath'] = 'contracts/search';
			$this->viewArray['showBack'] = '/contracts\/';

			return view('staff/contract/index',  $this->viewArray);

		}else{
			return redirect()->action('ContractController@getAll');
		}

	}


	public function getViewContract($id)
	{
		$contract = Contract::find($id);
		return view('staff/contract/view-contract', ['item' => $contract]);
	}

	/**
	* load edit contract view
	*/
	public function getEdit($id)
	{
		$contract = Contract::with(['app', 'dealer'])->find($id);
		if($contract){
			$promos = Promotional::get();
			$ratings = Rating::get();
			$companies = DealerCompany::where('dealer_status', 'Approved')->get();
			return view('staff/contract/edit-contract', ['item' => $contract, 'promos' => $promos, 'ratings' => $ratings, 'companies' => $companies, 'showBack' => '/contracts\/', 'tab' => 'contracts']);
		}
		else{
			return redirect('contracts/');
		}
	}

	// same as getEdit just by app id in url
	public function getByApp($id)
	{
		$app = \App\Models\ContractApplication::findOrFail($id);
		if(!$app->contract) {
			// redirect to app view
			return redirect('/contracts/app-data/' . $id);
		}

		return redirect()->action(
		    'ContractController@getEdit', ['id' => $app->contract->id]
		);
	}

	/**
	* load contract store order info view
	*/
	public function getOrders($id)
	{
		$contract = Contract::with(['app'])->find($id);
		if($contract){
			return view('staff/contract/orders-contract', ['item' => $contract, 'showBack' => '/contracts/edit/' . $contract->id, 'tab' => 'contracts']);
		}
		else{
			return redirect('contracts/');
		}
	}

	/**
	* Post store update
	*/
	public function postOrders($id)
	{
		$contract = Contract::with(['app'])->findOrFail($id);
		if($contract){
			$store = new \App\Libraries\Ecommerce\StoreLibrary($contract->app->appOrders);
			$store->updateOrder();
		}
		return redirect('/contracts/orders/' . $id);
	}


	public function postUpdate(Request $request, $id)
	{
		$contract = Contract::find($id);
		$unadminVars = array_filter(Request::only('amountFinanced', 'discount', 'acquisitionFee', 'reserveFee', 'sameAsCashPayable','statusId'), 'strlen');
		$contract->update($unadminVars);

		// admin only
		if(Auth::guard('web')->user()->admin == 1){
			$adminVars = array_filter(Request::only('customerName','applicationDate','distributorId','distributorPayable', 'ratingId', 'sameAsCashId', 'buy_rate', 'accountNumber'), 'strlen');
			// only admin can update these fields
			$contract->update($adminVars);
		}

		$contract->save();

		if(Request::ajax()){
			return response()->json('success');
		}
	}

	public function postApprove($id)
	{
		if (Request::ajax()){
			$contract = Contract::find($id);
			$contract->approved = true;
			$contract->save();

			return response()->json('success');
		}else{
			abort('403', 'Invalid approval attempt');
		}
	}

	public function delete($id){
		if( $this->user->hasRole('admin') ){
			$contract = Contract::findOrFail($id);
			$contract->delete();
			if(Request::ajax()){
				return response()->json('success');
			}
		}else{
			abort('403', 'Not authorized');
		}
	}

	public function getReview(){
		$contracts = Contract::whereApproved(false)->orderBy('applicationDate','desc')->paginate(15);
		$this->viewArray['contracts'] = $contracts;
		return view('staff/contract/review-contract',  $this->viewArray);
	}

	// performs a fresh start on a contract
	public function getFreshStart($id){

		//if($this->user->hasRole('admin')){
			$contract = Contract::findOrFail($id);


			$freshStart = $contract->replicate();
			$freshStart->distributorId = getenv('BELMONT_DEALER_ID');
			$freshStart->dist_user_id = NULL;

			$freshStart->accountNumber = Request::get('new_account');
			$freshStart->save();

			$contract->fresh_start = $freshStart->id;
			$contract->save();
			return redirect('contracts/edit/' . $freshStart->id);
		//}else{
			abort('403');
		//}
	}

	public function getDenied(){
		$apps = ContractApplication::whereStatus(3)->orderBy('created_at', 'DESC')->paginate(15);
		$this->viewArray['apps'] = $apps;
		return view('staff/contract/denied-apps',  $this->viewArray);
	}

	public function getAppData($id){
		$app = ContractApplication::findOrFail($id);
		$appArray = $app->toArray();
		$newApp = [];
		foreach ($appArray as $key => $value) {
			$pieces = preg_split('/(?=[A-Z]|_)/',$key);
			$newApp[ucwords(str_replace('_', '', implode(' ', $pieces)))] = $value;
		}
		$this->viewArray['app'] = $newApp;
		$this->viewArray['logs'] = $app->getFileLogs();
		return view('staff/contract/view-app',  $this->viewArray);
	}

	public function getAppLog($file){
		return view('staff/contract/view-log', ['data' =>$this->getFileData(  str_replace('|', '/', urldecode($file)))]);
	}

	/**
	 * Create a new controller instance.
	 *
	 * @return RedirectResponse
	 */
	public function getAccess($contractId)
	{
		if (Request::ajax()){
			abort('403', 'Invalid access via ajax');
			die();
		}

		if( $this->user->hasRole('admin') || $this->user->hasRole('csr') )
		{
			Session::put('admin-pseudo-cust', $contractId);

			$http = getenv('PRODUCTION') == "false" ? 'http://' : 'https://';
			return redirect($http . 'accounts.' . getenv('DOMAIN') );
		}
		else
		{
			abort('403', 'You don\'t have proper privileges to access customer accounts.');
		}

	}

	private function translateFilters($filters)
	{
		if(isset($filters['Product']) && $filters['Product'])
		{
			$filters['Product'] = \App\Models\DealerProduct::find($filters['Product'])->name;
		}
		return $filters;
	}

	// pass relative path to storage directory for reading
	private function getFileData($filePath){
		$logPath = storage_path() . '/' . getenv('APP_LOG_DIR');
		return nl2br((file_get_contents($logPath . $filePath)));
	}

	// lock out customer from their account
	public function getLockCustomer($id){
		$contract = \App\Models\Contract::findOrFail($id);
		if(!$contract->customer) {
			$customer = \App\Models\Customer::create(['email' => 'noconsumer@belmontfinancellc.com',
				'first_name' => '',
				'last_name' => '',
				'address' => '',
				'address_two' => '',
				'city' => '',
				'state' => '',
				'zip' => '',
				'phone' => '',
				'phone_two' => '',
				'contract_id' => $contract->id
			]);
			$contract->save();
		} else {
			$customer = $contract->customer;
		}

		$customer->locked_at = Carbon::now()->format('Y-m-d');
		$customer->save();
		return back();
	}

	// re access customer from their account
	public function getUnlockCustomer($id){
		$contract = \App\Models\Contract::findOrFail($id);

		if($contract->customer->email == "noconsumer@belmontfinancellc.com") {
			\App\Models\Customer::destroy($contract->customer->id);
		}

		if($contract->customer) {
			$contract->customer->locked_at = null;
			$contract->customer->save();
		}
		return back();
	}
}
