<?php namespace App\Http\Controllers;
use App\Http\Requests\PromoRequest;
use App\Models\Promotional;
use App\Models\Rating;
use App\Models\Contract;
use App\Models\DealerCompany;
use App\Models\Setting;
use Illuminate\Http\Request as OrigRequest;
use Illuminate\Support\Facades\App;
use App\Models\Dealers\PaperworkRequest;
use Request;
use DB;
use Crypt;
use Mail;
use DateTime;

class SettingsController extends Base_Controller {


	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		//authenticate dashboard requests
		$this->middleware('auth');

		$this->middleware('admin');

		view()->share('current',  Request::segment(2));
		$settings = Setting::all();
		$this->settings = $settings->keyBy('name')->toArray();
		view()->share('settings', $this->settings);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return redirect()->action('SettingsController@getGeneral');
	}

	public function postSave(OrigRequest $request)
	{
		$this->validate($request, [
   			'company_id' => 'min:10|max:10'
        ]);

		$settings = $request->only(['applicationEmails', 'paymentEmails',
			'destination_name',
			'routing_number',
			'company_id',
			'settlement_account',
			'file_id',
			'originating_bank',
			'company_name',
			'account_type',
			'batch_name',
			'statement_description',
			'originating_routing'
		]);

		foreach($settings as $settingName => $settingVal){
			$setting = new Setting;
			if($settingVal)
			{
				$setting->upsert($settingName, $settingVal);
			}
		}

		if($request->ajax()){
			return response()->json('success');
		}
	}

	public function getDistributors()
	{
		$products = \App\Models\DealerProduct::get();

		$pendingPaperworkRequests = PaperworkRequest::whereNull('sent_at')->count();
		return response()->view('staff/settings/distributors', ['products' => $products, 'pendingPaperworkRequests' => $pendingPaperworkRequests]);
	}

	public function getTest(){
		trigger_error("Fatal error", E_USER_ERROR);
	}

	public function postAddProduct(OrigRequest $request)
	{
		$this->validate($request, [
   			'name' => 'required|min:3'
        ]);

		$product = new \App\Models\DealerProduct();
		$product->name = $request->input('name');
		$product->save();

		// post new product to craft
		$httpClient = new \GuzzleHttp\Client();
		$https = getenv('APP_ENV') == "local" ? 'http://' : 'https://';

		try {
			$response = $httpClient->post($https . getenv('DOMAIN') . '/actions/updateCategories/main/add', [
				'form_params' => [
					'auth' => 'q3p498gADF4gj',
					'group' => 2,
					'title' => $product->name
				]
			]);
			$data = $response->getBody()->getContents();

		} catch(\Exception $e) {
			$data = new \stdClass();
			$data->data[0] = 'Server connection issue';
		}

		if($request->ajax()){
			return response()->json('success');
		}
	}

	public function deleteProduct(OrigRequest $request, $id)
	{
		$product = \App\Models\DealerProduct::find($id);

		$product->delete();

		// uncategorize all dealers who had product
		\App\Models\DealerCompany::where('product_id', '=', $id)->update(['product_id' => null]);

		if($request->ajax()){
			return response()->json('success');
		}
	}


	public function getPromos()
	{
		$promos = Promotional::paginate(20);
		return response()->view('staff/settings/promos', ['promos' => $promos]);
	}

	public function getEditPromo($id)
	{
		$promo = Promotional::find($id);
		return response()->view('staff/settings/edit-promo', ['item' => $promo]);
	}

	public function getGeneral()
	{
		return response()->view('staff/settings/index');
	}

	public function getPayments($date = null)
	{
		if(!$date){
			$ccDays = DB::table('payments_dl')->whereBetween('download_date', [date('Y-m-d', strtotime('-9 days')), date('Y-m-d', strtotime('yesterday'))] )->lists('download_date');
			$achDays = DB::table('payments_dl_ach')->whereBetween('download_date', [date('Y-m-d', strtotime('-9 days')), date('Y-m-d', strtotime('yesterday'))] )->get();

			$period = new \DatePeriod(
			     new \DateTime(date('Y-m-d', strtotime('-9 days'))),
			     new \DateInterval('P1D'),
			     new \DateTime(date('Y-m-d', strtotime('today')))
			);

			$downloadDays = [];
			$achDownloadedDays = [];

			foreach($period as $date){
				$date = $date->format("Y-m-d");
				$downloaded = in_array($date, $ccDays);

				// also check ach if downloaded
				$achDownloaded = false;
				$lockbox = null;
				foreach($achDays as $key => $item)
				{
					if($date == $item->download_date)
					{
						$achDownloaded = true;
						$lockbox = $item->lockbox_downloaded;
						break;
					}
				}
				$achDownloadDays[] = ['date' => $date, 'downloaded' => $achDownloaded, 'lockbox' => $lockbox];
				// $downloadedAch = in_array($date, $achDays['download_date']);
				$downloadDays[] = ['date' => $date, 'downloaded' => $downloaded];
			}

			// check bank settings
			$verifySettings = true;
			$verifySettings = $this->settings['company_id']['value'] ? $verifySettings : false;
			$verifySettings = $this->settings['file_id']['value'] ? $verifySettings : false;
			$verifySettings = $this->settings['originating_bank']['value'] ? $verifySettings : false;
			$verifySettings = strlen( $this->settings['routing_number']['value'] ) == "9" ? $verifySettings : false;
			$verifySettings = $this->settings['settlement_account']['value'] ? $verifySettings : false;
			$verifySettings = $this->settings['statement_description']['value'] ? $verifySettings : false;

			return response()->view('staff/settings/payments', ['downloadDays' => array_reverse($downloadDays), 'achDownloadDays' => array_reverse($achDownloadDays), 'verifyAchSettings' => $verifySettings]);
		}
		else
		{

			$payments = \App\Models\Payment::leftJoin('contracts', 'payments.contract_id', '=', 'contracts.id')->select('payment_date', 'accountNumber', 'amount', 'payment_source', 'customerName')->wherePaymentDate($date)->get()->toArray();

			$downloadString = '';

			foreach($payments as &$payment)
			{
				// if account number starts with s x or r, use that letter, else, use s
				$payment['account_type'] = in_array( strtolower(substr($payment['accountNumber'], 0, 1)), ['s', 'x' , 'r'] ) ? strtoupper( substr($payment['accountNumber'], 0, 1) ) : 'S';
				// $payment['accountNumber'] = $payment['account_type']
				$payment['payment_type'] = '01';

				// can't think of a valid batchnumber use case?
				$payment['batch_number'] = '00';

				// get firstname, lastname and truncate to 20 chars
				$nameParts = explode(" ", $payment['customerName']);
				$lastName = array_pop($nameParts);
				$firstName = implode(" ", $nameParts);
				$payment['customerName'] = $lastName . ',' . $firstName;
				$payment['customerName'] = str_pad( substr($payment['customerName'], 0, 20) , 20, ' ', STR_PAD_RIGHT);

				$paymentAmount = $payment['amount'];
				$totalAmount = $paymentAmount;
				$paymentFee = 0.00;

				$payment['account_number'] = in_array( strtolower(substr($payment['accountNumber'], 0, 1)), ['s', 'x' , 'r'] ) ? substr( $payment['accountNumber'], 1) : str_pad( $payment['accountNumber'], 9, 0, STR_PAD_LEFT );

				$payment['amount'] = str_pad( number_format( $totalAmount, 2, '.', ''), 12, ' ', STR_PAD_LEFT);

				$payment['payment_source'] = str_pad( $payment['payment_source'], 12, ' ', STR_PAD_RIGHT );

				$payment['additional_fee'] = str_pad( number_format( $paymentFee, 2, '.', '' ), 6, ' ', STR_PAD_LEFT );

				$downloadString .= $payment['account_type'] . $payment['account_number'] . $payment['customerName'] . $payment['amount'] . '01' . '00' . $payment['payment_source'] . ' ' . $payment['additional_fee'] . "\r\n";

			}

			//mark as downloaded in database
			DB::table('payments_dl')->insert(['download_date' => $date]);

			// go ahead and clear 30 days ago so table stays small
			DB::table('payments_dl')->where('download_date', '<', date('Y-m-d', strtotime('-1 month')))->delete();

			return response()->make($downloadString, '200')
				->header('Content-Type', 'text/plain')
				->header('Content-Disposition', 'attachment; filename="CCWEB-'. $date . '.sdf' .'"')
				->header('Content-Length', strlen($downloadString))
				->header('Connection', 'close');
		}

	}

	public function getPaymentsReport() {
		return view('staff/settings/payments-report');
	}

	public function postPaymentsReport(OrigRequest $request)
	{
		$searchMonth = ($request->month ? : 'null');
		$searchMonth = (strlen($searchMonth) < 2 ? '0' . $searchMonth : $searchMonth);
		$monthString = DateTime::createFromFormat('!m', $searchMonth)->format('F');
		$searchYear = ($request->year ? : 'null');
		$searchString = $searchYear . '-' . $searchMonth;
		$payments = \App\Models\AchPayment::where('payment_date', 'like', $searchString.'%')->orderBy('payment_date', 'desc')->get();

		$pdfString = '<style>
										table { width:100%; margin-left:auto; margin-right:auto; }
										th { border-bottom:1px solid black }
										td { border-bottom:1px dashed black }
										.footer { position: fixed; left: 0px; bottom: -170px; right: 0px; height: 150px; }
										.pagenum:before { content: counter(page); }
										.last { border-bottom:0px }
									</style>';
		$pdfString .= '<div class="footer"><span class="pagenum"></span></div>';
		$pdfString .= '<center><h4>Belmont Finance LLC<br>';
		$pdfString .= 'ACH Payment Report<br>';
		$pdfString .= 'For the month of ' . $monthString . ' ' . $searchYear . '</h4></center><br>';
		$pdfString .= '<table style="width:85%">';
		$pdfString .= '<tr><th>Account Number</th><th>Account Name</th><th>Payment Date</th><th>Amount</th></tr>';
		$totalAmount = 0;
		foreach ($payments as $payment) {
			$totalAmount += $payment->amount;
			$customerFirstName = (($payment->customer and $payment->customer->first_name) ? $payment->customer->first_name : '');
			$customerLastName = (($payment->customer and $payment->customer->last_name) ? $payment->customer->last_name : '');
			$pdfString .= '<tr><td>' . $payment->belmont_account_number . '</td><td>' . $customerFirstName . ' ' . $customerLastName . '</td><td>' . $payment->payment_date . '</td><td>$' . number_format($payment->amount,2) . '</td></tr>';
		}
		$pdfString .= '<tr><td class="last"></td><td class="last"></td><td class="last"></td><td class="last">Total: $' . number_format($totalAmount,2) . '</td></tr>';
		$pdfString .= '</table>';


		$pdf = App::make('dompdf.wrapper');
		$pdf->loadHTML($pdfString);

		return $pdf->download($monthString . $searchYear . 'payments.pdf');
	}


	public function getRecurringPayments(){
		$payments = \App\Models\AchPaymentRecurring::whereProcessed(0)->get();
        $deletePayments = \App\Models\AchPaymentRecurring::whereRequestedRemoval(1)->get();
		return response()->view('staff/settings/recurring-payments', ['payments' => $payments, 'remove' => $deletePayments, 'current' => 'rec-payments']);
	}

	public function getRecurringCard(){
		$payments = \App\Models\PaymentRecurring::whereProcessed(0)->get();
        $deletePayments = \App\Models\PaymentRecurring::whereRequestedRemoval(1)->get();
		return response()->view('staff/settings/recurring-card', ['payments' => $payments, 'remove' => $deletePayments, 'current' => 'rec-card']);
	}

	public function getAchPaymentNacha($date)
	{
		$payments = \App\Models\AchPayment::where(DB::raw('DATE(payment_date)'), $date)->get();

		$nacha = new \App\Libraries\NachaFile();


		$nacha->setBankRT($this->settings['routing_number']['value'])
			->setCompanyId($this->settings['company_id']['value'])
			->setSettlementAccount($this->settings['settlement_account']['value'])
			->setOriginRouting($this->settings['originating_routing']['value'])
			->setFileID($this->settings['file_id']['value'])
			->setOriginatingBank($this->settings['originating_bank']['value'])
			->setFileModifier('A')
			->setImmediateDestinationName($this->settings['destination_name']['value'])
			->setCompanyName($this->settings['company_name']['value'])
			->setBatchInfo($this->settings['batch_name']['value'])
			->setDescription($this->settings['statement_description']['value'], $date)
			->setEntryDate(date('m/d/Y'));

		$payments->each(function($item, $key) use($nacha){
			if(!$nacha->addDebit([
				'AccountNumber' => $item->belmont_account_number,
				'TotalAmount' => $item->amount,
				'BankAccountNumber' => Crypt::decrypt($item->account_number),
				'AccountType' => ($item->is_savings_account ? "SAVINGS" : "CHECKING" ),
				'RoutingNumber' => $item->routing_number,
				'FormattedName' => $item->electronic_signature
			])){
				// log failure
			}
		});
		$totalDebits = $payments->sum('amount');
		$nacha->addCredit([
			'AccountNumber' => '1273719361',
			'TotalAmount' => $totalDebits,
			'BankAccountNumber' => $this->settings['settlement_account']['value'],
			'RoutingNumber' => $this->settings['routing_number']['value'],
			'FormattedName' => $this->settings['company_name']['value'],
			'AccountType' => $this->settings['account_type']['value'],
		]);
		$nacha->generateFile();

		//mark as downloaded in database
		DB::table('payments_dl_ach')->insert(['download_date' => $date]);

		return response()->make($nacha->fileContents, '200')
				->header('Content-Type', 'text/plain')
				->header('Content-Disposition', 'attachment; filename="NACHA-'. $date . '.ach' .'"')
				->header('Content-Length', strlen($nacha->fileContents))
				->header('Connection', 'close');



	}

	public function getAchPaymentLockbox($date)
	{

		$payments = \App\Models\AchPayment::leftJoin('contracts', 'payments_ach.contract_id', '=', 'contracts.id')->select('payments_ach.created_at', 'electronic_signature', 'belmont_account_number', 'amount', 'account_number', 'customerName')->where(DB::raw('DATE(payments_ach.payment_date)'), $date)->get()->toArray();

		$downloadString = '';

			foreach($payments as &$payment)
			{
				// if account number starts with s x or r, use that letter, else, use s
				$payment['account_type'] = in_array( strtolower(substr($payment['belmont_account_number'], 0, 1)), ['s', 'x' , 'r'] ) ? strtoupper( substr($payment['belmont_account_number'], 0, 1) ) : 'S';
				// $payment['accountNumber'] = $payment['account_type']
				$payment['payment_type'] = '01';

				// can't think of a valid batchnumber use case?
				$payment['batch_number'] = '01';

				// get firstname, lastname and truncate to 20 chars
				$nameParts = explode(" ", $payment['electronic_signature']);
				$lastName = array_pop($nameParts);
				$firstName = implode(" ", $nameParts);
				$payment['customerName'] = $lastName . ',' . $firstName;
				$payment['customerName'] = str_pad( substr($payment['customerName'], 0, 20) , 20, ' ', STR_PAD_RIGHT);

				$payment['belmont_account_number'] = in_array( strtolower(substr($payment['belmont_account_number'], 0, 1)), ['s', 'x' , 'r'] ) ? substr( $payment['belmont_account_number'], 1) : str_pad( $payment['belmont_account_number'], 9, 0, STR_PAD_LEFT );

				$payment['amount'] = str_pad( number_format( $payment['amount'], 2, '.', ''), 12, ' ', STR_PAD_LEFT);

				$payment['account_number'] = substr( Crypt::decrypt($payment['account_number']), -4);
				$payment['payment_source'] = str_pad( $payment['account_number'], 12, ' ', STR_PAD_RIGHT );

				$payment['additional_fee'] = str_pad( number_format( 0.00, 2, '.', '' ), 6, ' ', STR_PAD_LEFT );

				$downloadString .= $payment['account_type'] . $payment['belmont_account_number'] . $payment['customerName'] . $payment['amount'] . '01' . '00' . $payment['payment_source'] . ' ' . $payment['additional_fee'] . "\r\n";

			}

			//mark as downloaded in database
			DB::table('payments_dl_ach')->where('download_date', '=', $date)->update(['lockbox_downloaded' => 1]);

			// go ahead and clear 30 days ago so table stays small
			DB::table('payments_dl')->where('download_date', '<', date('Y-m-d', strtotime('-1 month')))->delete();

			$beginningDate = new \Carbon\Carbon($date);
			$beginningDate->subDays(7);
			$endDate = new \Carbon\Carbon($date);
			$endDate->subDays(16);

			// remove account number if older than seven days
			$payments = \App\Models\AchPayment::whereBetween('payment_date', [$endDate->format('Y-m-d h:i:s'), $beginningDate->format('Y-m-d h:i:s')])->get();

			$payments->each(function($item, $key){
				$account_number = Crypt::decrypt($item->account_number);
				$item->account_number = Crypt::encrypt( substr( $account_number, -4 ) );
				$item->save();
			});


			return response()->make($downloadString, '200')
				->header('Content-Type', 'text/plain')
				->header('Content-Disposition', 'attachment; filename="ACHWEB-'. $date . '.sdf' .'"')
				->header('Content-Length', strlen($downloadString))
				->header('Connection', 'close');
	}

    public function getStates(){
        $states = DB::table('settings_state')->select('state', 'max_apr')->orderBy('state', 'asc')->get();
        return response()->view('staff/settings/states', ['states' => $states]);
    }

    public function postUpdateState(OrigRequest $request){
        $this->validate($request, [
            'state'     => 'min:2|max:2|alpha',
            'max_apr'   => 'numeric|min:0.00|max:99.99'
        ]);

        $states = DB::table('settings_state')->where('state', $request->state)->update(['max_apr' => $request->max_apr]);
        return response()->json('success');
    }


    public function viewAppSettings(){
        return response()->view('staff.settings.apps');
    }

}
