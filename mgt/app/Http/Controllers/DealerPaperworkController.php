<?php namespace App\Http\Controllers;

use App\Models\DealerCompany;
use Illuminate\Http\Request as OrigRequest;
use Illuminate\Support\Facades\App;
use Request;
use Mail;
use App\Models\Dealers\PaperworkRequest;
use Carbon\Carbon;

class DealerPaperworkController extends Base_Controller {

    public function paperworkRequests() {
        $pending = PaperworkRequest::orderBy('created_at', 'ASC')->whereNull('sent_at')->get();

        $sent = PaperworkRequest::orderBy('sent_at', 'DESC')->whereNotNull('sent_at')->limit(20)->get();

        return response()->view('staff/settings/dealer-paperwork', ['pending' => $pending, 'sent' => $sent]);
    }

    // load an individual request to view
    public function paperworkRequest($id) {
        $paperworkRequest = PaperworkRequest::findOrFail($id);

        return response()->view('staff/settings/dealer-paperwork-single', ['pRequest' => $paperworkRequest]);
    }

    // mark a paperwork request sent
    public function paperworkSent($id) {
        $paperworkRequest = PaperworkRequest::findOrFail($id);

        $paperworkRequest->sent_at = Carbon::now()->format('Y-m-d h:m:i');
        $paperworkRequest->save();
        Mail::send('emails.dealer-paperwork-sent', [], function ($m) use ($paperworkRequest){
            $m->from('noreply@belmontfinancellc.com', 'Belmont Finance LLC');
            $m->to($paperworkRequest->dealer->email)->subject("Paperwork Sent");
        });
        return redirect('/settings/dealers/paperwork-requests');
    }
}