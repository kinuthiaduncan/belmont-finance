<?php namespace App\Http\Controllers\Settings;

use App\Models\DealerCompany;
use Illuminate\Http\Request as OrigRequest;
use App\Models\Dealers\DealerState;
use App\Http\Controllers\Base_Controller as Base_Controller;
use Request;
use Artisan;


class ImportController extends Base_Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		//authenticate dashboard requests
		$this->middleware('auth');
		$this->middleware('admin');

		view()->share('current',  Request::segment(2));
	}

	/**
	 * Show the main page to user
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('staff/settings/import');
	}

	public function importStates(OrigRequest $request)
	{
		$file = $request->file('dealer_states');
		$prevDealer = false;
		$isFirstLine = true;

		// store temporarily to process next
		if (!file_exists(storage_path('imports'))) {
		    mkdir(storage_path('imports'), 0755, true);
		}
		move_uploaded_file ( $file->getPathName() , storage_path('imports/import_dealer_states.csv') );
		\Artisan::call('import:dealerstates', [
			'filepath' => storage_path('imports/import_dealer_states.csv')
		]);

		return view('staff/settings/import');
	}

	public function importDealerInfo(OrigRequest $request)
	{
		$file = $request->file('dealer_info');
		$prevDealer = false;
		$isFirstLine = true;

		// store temporarily to process next
		if (!file_exists(storage_path('imports'))) {
		    mkdir(storage_path('imports'), 0755, true);
		}
		move_uploaded_file ( $file->getPathName() , storage_path('imports/dealer_website_info.csv') );
		\Artisan::call('import:dealerinfo', [
			'filepath' => storage_path('imports/dealer_website_info.csv')
		]);

		return view('staff/settings/import');
	}
}
