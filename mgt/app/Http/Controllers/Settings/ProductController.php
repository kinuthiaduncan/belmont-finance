<?php namespace App\Http\Controllers\Settings;

use App\Models\DealerCompany;
use Illuminate\Http\Request as OrigRequest;
use App\Models\Dealers\DealerState;
use App\Http\Controllers\Base_Controller as Base_Controller;
use Request;
use Artisan;


class ProductController extends Base_Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// construct parent in order to create shared variables
		parent::__construct();

		//authenticate dashboard requests
		$this->middleware('auth');

		$this->middleware('admin');

		view()->share('current',  Request::segment(2));
	}

	public function view($id)
	{
		$product = \App\Models\DealerProduct::findOrFail($id);

		return view('staff.settings._distributors-product', ['product' => $product]);
	}

	public function createPmtFactor(OrigRequest $request, $id)
	{
		$this->validate($request, [
	        'payment_factor' => 'required|numeric|between:0,100.00',
	        'buy_rate' => 'numeric|between:0,100.00'
	    ]);

	    $pmtFactor = \App\Models\Dealers\DealerProductPmtFactor::create([
	    	'product_id' => $id,
	    	'pmt_factor' => $request->payment_factor,
	    	'min_buy_rate' => $request->buy_rate
	    ]);

		return response()->json('success');
	}
}
