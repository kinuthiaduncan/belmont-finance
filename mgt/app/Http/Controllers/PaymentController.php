<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\PaymentRecurring;
use App\Models\AchPaymentRecurring;
use Crypt;
use Mail;

class PaymentController extends Base_Controller{

  public function __construct()
  {
    // construct parent in order to create shared variables
    parent::__construct();

    $this->middleware('auth');
  }

  public function getRecurring($id){


    $payment = AchPaymentRecurring::find($id);


    if($payment){
      $accountNumber = Crypt::decrypt($payment->account_number);
      return view('staff/payments/view', ['payment' => $payment, 'account' => $accountNumber]);
    }
    else{
      abort('404');
    }
  }

  public function getRecurringCard($id){


    $payment = PaymentRecurring::find($id);


    if($payment){
      $cc = Crypt::decrypt($payment->cc);
      $cvv = $payment->cvv;
      return view('staff/payments/view-recurring-card', ['payment' => $payment, 'cc' => $cc, 'cvv' => $cvv]);
    }
    else{
      abort('404');
    }
  }

    public function getSingle($id){

    $payment = \App\Models\AchPayment::find($id);

    if($payment){
      $accountNumber = Crypt::decrypt($payment->account_number);
      return view('staff/payments/single', ['payment' => $payment, 'account' => $accountNumber]);
    }
    else{
      abort('404');
    }
  }

  public function postMarkRecurring(Request $request, $id){
    $payment = AchPaymentRecurring::find($id);

    $accountNumber = Crypt::decrypt($payment->account_number);

    $payment->account_number = Crypt::encrypt( substr($accountNumber, -4) );

    $payment->processed = 1;

    $payment->save();

    if($payment->customer_id){
      // send email to customer informing them
      $email = $payment->customer->email;
      $name = $payment->customer->first_name;
      Mail::send('emails.customer-recurring-payment-setup', ['name' => $name], function ($m) use($email){
          $m->to($email)->subject("Belmont Finance Recurring Payment set up");
      });
    }

    if($request->ajax()){
      return response()->json('success');
    }
  }

  public function postMarkRecurringCard(Request $request, $id){
    $payment = PaymentRecurring::find($id);

    $cc = Crypt::decrypt($payment->cc);

    $payment->cc = Crypt::encrypt( substr($cc, -4) );

    $payment->processed = 1;

    $payment->save();

    if($payment->customer_id){
      // send email to customer informing them
      $email = $payment->customer->email;
      $name = $payment->customer->first_name;
      Mail::send('emails.customer-recurring-payment-setup', ['name' => $name, 'card' => true], function ($m) use($email){
          $m->to($email)->subject("Belmont Finance Recurring Payment set up");
      });
    }

    if($request->ajax()){
      return response()->json('success');
    }
  }


  public function postRemove(Request $request, $id){
    $payment = AchPaymentRecurring::find($id);

    $payment->delete();

    if($request->ajax()){
      return response()->json('success');
    }
  }

  public function postRemoveCard(Request $request, $id){
    $payment = PaymentRecurring::find($id);

    $payment->delete();

    if($request->ajax()){
      return response()->json('success');
    }
  }
}
