<?php namespace App\Http\Controllers;

use Auth;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Request;
use Session;
use App\Models\Setting;


/*
* Using this controller to share template data between views
*/

abstract class Base_Controller extends BaseController {
	use DispatchesJobs, ValidatesRequests;

	public function __construct()
	{
		//share context (eg staff or dealer subdomain) with all views
		$url = Request::url();
		$subdomain = explode(".", $url );
		$subdomain = explode("/", array_shift($subdomain));
		$subdomain = array_pop($subdomain);
		$this->context = $subdomain;

		$this->protocol = getenv('PRODUCTION') == "FALSE" ? 'http://' : 'https://';

		if($subdomain === "dealer"){
			if($pseudoId = Session::get('admin-pseudo')){
				$this->user = \App\Models\Dealer::findOrFail($pseudoId);
			}else{
				$this->user = Auth::guard('dealer')->user();
			}
		}
		elseif($subdomain === "accounts"){
			if($pseudoId = Session::get('admin-pseudo-cust')){
				$this->user = Auth::guard('web')->user();
				$this->contract = \App\Models\Contract::findOrFail($pseudoId);
				Session::put('contractId', Session::get('admin-pseudo-cust'));
			}else{
				$this->user = Auth::guard('consumer')->user();
				if($this->user)
				{
					if(!Session::get('contractId') && !$this->user->temp_app_id){
						$this->contract = \App\Models\Contract::where('customerId', '=', $this->user->id)->first();
						if(!$this->contract){
							abort('403', 'Your account has not been set up properly.');
						}

						if($this->contract->fresh_start){
							$this->contract = \App\Models\Contract::findOrFail($this->contract->fresh_start);
						}

						Session::put('contractId', $this->contract->id);

					}
					else
					{
						$this->contract = \App\Models\Contract::find(Session::get('contractId'));
					}
				}
			}



		}
		elseif($subdomain === "staff"){
			$this->user = Auth::guard('web')->user();
		}

		// store last page url so we can redirect to it for certain actions like when filtering
		if(Request::isMethod('get')){
			Request::session()->set('staff.history.2', Request::session()->get('staff.history.1'));
			Request::session()->set('staff.history.1', Request::url());
		}

		view()->share('user', $this->user);
		view()->share('context', $subdomain);
		view()->share('url', $url);
	}
}
