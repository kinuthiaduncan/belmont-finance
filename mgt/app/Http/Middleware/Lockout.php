<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Auth;

// middleware to lock out consumers

class Lockout {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		if( Auth::guard('consumer')->user() && Auth::guard('consumer')->user()->locked_at ){
			return response()->view('errors.accounts-locked');
		}

		return $next($request);
	}

}
