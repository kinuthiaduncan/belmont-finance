<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use DB;

class DealerChoose {

	/**
	 * Allows dealer to select which distributor if they have multiple
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{	
		// check if selected distributor is set by session
		if(!$request->session()->has('selectedDistributorId')) {

			// staff login as dealer
			if($pseudoId = Session::get('admin-pseudo')){
				$dealer = \App\Models\Dealer::where('id', '=', $pseudoId)->first();
			} else {
				$dealer = Auth::guard('dealer')->user();
			}

			// if multiple companies exist for the dealer, redirect them to choose one
			if( $dealer->companies()->count() > 1) {
				return response()->view('dealer.choose-distributor', ['user' => $dealer]);
			} else if ( $dealer->companies()->count() == 1 ) {
				// select distributor automatically
				$request->session()->set('selectedDistributorId', $dealer->companies()->first()->id);
			} else {
				abort(403, 'You do not have any accounts with Belmont Finance. Please contact support if you need help.');
			}
		}
		return $next($request);
	}

}
