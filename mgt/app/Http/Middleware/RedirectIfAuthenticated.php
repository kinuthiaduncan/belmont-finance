<?php namespace App\Http\Middleware;

//this middleware protects staff from accessing dealer and vice versa

use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;

class RedirectIfAuthenticated {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 */
	public function __construct()
	{
		//$this->auth = Auth::admin();
	}

	 /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/');
        }

        return $next($request);
    }

	/* add routes to disable user type check here */
	protected function excludedRoutes($request)  
	{
	    $routes = [
	        'auth/*'
	    ];

	    foreach($routes as $route)
	        if ($request->is($route))
	            return true;

        return false;
	}

}