<?php namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Session;

class ApiAuthenticate {

	/**
	 * Makes sure an api request has correct credentials
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// bypass for staff submittal
		if( !Session::has('staff.applicationCompanyId') )
		{
		    try {
		        if (! $user = JWTAuth::parseToken()->authenticate()) {
		            return response()->json(['user_not_found'], 404);
		        }
		    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
		        return response()->json(['token_expired'], $e->getStatusCode());
		    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
		        return response()->json(['token_invalid'], $e->getStatusCode());
		    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
		        return response()->json(['token_absent'], $e->getStatusCode());
		    }
		}
		return $next($request);
	}

}
