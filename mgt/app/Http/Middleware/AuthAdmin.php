<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class AuthAdmin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$url = $request->url();
		$subdomain = explode(".", $url );
		$subdomain = explode("/", array_shift($subdomain));
		$subdomain = array_pop($subdomain);

		if($subdomain == "staff"){
			if(!Auth::guard('web')->user()->hasRole('admin')){
				abort(401);
			}
		}else{

			if($pseudoId = Session::get('admin-pseudo')){
				$this->user = \App\Models\Dealer::findOrFail($pseudoId);
			}else{
				$this->user = Auth::guard('dealer')->user();
			}

			if(!$this->user || !$this->user->hasRole('admin') ){
				abort(401);
			}
		}

		return $next($request);
	}

}
