<?php namespace App\Http\Middleware;

use Closure;
use \App\Models\Auth\SignToken;
use Illuminate\Http\Response;
use Carbon\Carbon;

class SignTokenAuthenticate {
	private $expirationTimeHours = 168;

	/**
	 * Middleware to make sure signature tokens are valid
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if( $request->input('token') ){
			$signToken = SignToken::where('token', '=', $request->input('token'))->first();

			// check if signature token exists and if not expired
			$now = Carbon::now();
			if($signToken && Carbon::parse($signToken->created_at)->diffInHours($now) <= $this->expirationTimeHours) {
				// successful auth
				return $next($request);
			} else {
				return new Response('Invalid token - please check your email and try again, or copy and paste the link provided. If the link provided still does not work, your access to this document may have expired - ask your dealer representative to resend.', 401);
			}
		}else{
			return new Response('Invalid token - please check your email and try again, or copy and paste the link provided. If the link provided still does not work, your access to this document may have expired - ask your dealer representative to resend.', 401);
		}
	}

}
