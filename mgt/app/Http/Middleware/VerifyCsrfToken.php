<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \Illuminate\Session\TokenMismatchException;
use Log;

class VerifyCsrfToken extends \Illuminate\Foundation\Http\Middleware\VerifyCsrfToken {

		/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 *
	 * @throws \Illuminate\Session\TokenMismatchException
	 */
	public function handle($request, Closure $next)
	{
		if ($this->isReading($request) || $this->excludedRoutes($request) || $this->tokensMatch($request))
		{
			return $this->addCookieToResponse($request, $next($request));
		}

		throw new TokenMismatchException;
	}

	/* add routes to disable csrf check here */
	protected function excludedRoutes($request)  
	{
	    $routes = [
	        'api/*'
	    ];

	    foreach($routes as $route)
	        if ($request->is($route))
	            return true;

        return false;
	}

}