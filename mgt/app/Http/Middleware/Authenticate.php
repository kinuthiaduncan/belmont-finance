<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
    	// get subdomain
        $subdomain = explode(".", $request->url());
        $subdomain = explode("/", array_shift($subdomain));
		$subdomain = array_pop($subdomain);

        if (Auth::guard($guard)->guest()) {
                if($subdomain === "dealer" && $request->session()->has('admin-pseudo')){
                    // do nothing
                } elseif($subdomain === "accounts" && $request->session()->has('admin-pseudo-cust')){
                    // do nothing
                } else {
                    if ($request->ajax() || $request->wantsJson()) {
                        return response('Unauthorized.', 401);
                    } else {
                        return redirect()->guest('login');
                    }
                }
        }
        return $next($request);
    }
}
