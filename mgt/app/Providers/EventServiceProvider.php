<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use App\Models\Contract;
use App\Models\Log;
use Auth;
use Log as ErrorLog;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		\App\Events\DealerCreated::class => [
			\App\Handlers\Events\AddDealerToMailchimp::class
		],
		\App\Events\ConsumerCreated::class => [
			\App\Handlers\Events\AddConsumerToMailchimp::class
		],
		\App\Events\ApplicationFilterSelected::class => [
        	\App\Handlers\Events\ApplyFilterAction::class,
    	],
        'file.created' => [
            'App\Events\FileSignatureEvent@fileCreated',
        ],
        'file.deleting' => [
            'App\Events\FileSignatureEvent@fileDeleting',
        ],
    	\App\Events\AppWasCreated::class => [
	        \App\Handlers\Events\FlagApplication::class,
	    ],
	    \App\Events\ApplicationApproved::class => [
			\App\Handlers\Events\EmailStaffRecurringPayment::class
		],

	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		Contract::updated(function($item){
			try{
				$ignoreFields = [
					'updated_at',
					'created_at',
					'deleted_at'
				];

				$staff = 0;
				$dealer = 0;

				if(Auth::guard('web')->user() && $userId = Auth::guard('web')->user()->getAttributes()['id']){
					$staff = 1;
				}elseif(Auth::guard('dealer')->user() && $userId = Auth::guard('dealer')->user()->getAttributes()['id']){
					$dealer = 1;
				}
				else{
					$userId = null;
				}

				if($staff || $dealer){
					foreach($item->getAttributes() as $attName => $attVal){
						if( !in_array( $attName, $ignoreFields ) ){
							//compare value to old value, if updated, log it.
							if( $attVal != $item->getOriginal($attName) ){
								$log = new Log;
								$log->table_name = "contracts";
								$log->table_id = $item->id;
								$log->column_name = $attName;
								$log->old_value = $item->getOriginal($attName);
								$log->new_value = $attVal;
								$log->staff_changed = $staff;
								$log->dealer_changed = $dealer;
								if($staff){
									$log->staff_user = $userId;
								}else{
									$log->dealer_user = $userId;
								}
								$log->save();
							}
						}
					}
				}
			}
			catch (\Exception $e) {
				ErrorLog::warning('Error logging contract update: ' . $e->getMessage() );
			    echo 'Exception: ',  $e->getMessage(), "\n";
			}
		});
	}

}
