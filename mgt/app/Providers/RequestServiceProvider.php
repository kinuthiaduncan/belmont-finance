<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Extensions\Request;

class RequestServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['request'] = $this->app->share(function($app)
		{
			return new Request();
		});
	}

}
