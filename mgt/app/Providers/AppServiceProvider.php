<?php namespace App\Providers;

use App\FileSignature;
use Illuminate\Support\ServiceProvider;
use App\Models\DealerCompany;
use App\Models\File;
use App\Models\ContractApplication;
use App\Models\SalesSlip;
use App\Models\Auth\SignToken;

use Event;
use Validator;
use Session;
use DB;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
    public $state;
    public $maxApr;

	public function boot()
	{
		//

		Validator::extendImplicit('lessthanone', function($attribute, $number, $parameters, $validator)
		{
	        if( isset($validator->getData()[$parameters[0]]) && $validator->getData()[$parameters[0]] < 1 ) {
	        	return false;
	        } else {
	        	return true;
	        }
        }, 'Required based on employment history');

        // custom luhns validation
		Validator::extend('luhns', function($attribute, $number, $parameters)
		{
            // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
	        $number=preg_replace('/\D/', '', $number);

	        // Set the string length and parity
	        $number_length=strlen($number);
	        $parity=$number_length % 2;

	        // Loop through each digit and do the maths
	        $total=0;
	        for ($i=0; $i<$number_length; $i++) {
	            $digit=$number[$i];
	            // Multiply alternate digits by two
	            if ($i % 2 == $parity) {
	                $digit*=2;
	                // If the sum is two digits, add them together (in effect)
	                if ($digit > 9) {
	                    $digit-=9;
	                }
	            }
	            // Total up the digits
	            $total+=$digit;
	        }

	      	// If the total mod 10 equals 0, the number is valid
	      	return ($total % 10 == 0) ? TRUE : FALSE;
        });

		// custom rule validating bank routing numbers
        Validator::extend('aba', function($attribute, $number, $parameters) {
        	$number = preg_replace('[\D]', '', $number); //only digits
		    if(strlen($number) != 9) {
		        return false;
		    }

		    $checkSum = 0;
		    for ($i = 0, $j = strlen($number); $i < $j; $i+= 3 ) {
		        //loop through routingNumber character by character
		        $checkSum += ($number[$i] * 3);
		        $checkSum += ($number[$i+1] * 7);
		        $checkSum += ($number[$i+2]);
		    }

		    if($checkSum != 0 and ($checkSum % 10) == 0) {
		        return true;
		    } else {
		        return false;
		    }
		});

		// custom rule make sure ssns don't begin with 9 unless jonathan consumer's number
        Validator::extend('ssn_nine', function($attribute, $number, $parameters)
        {
        	if($number == "999-99-9990") {
        		return true;
        	} else {
        		if($number[0] == "9") {
        			return false;
        		} else {
        			return true;
        		}
        	}
		});


		// check for renters
        Validator::extend('no_renters', function($attribute, $value, $parameters, $validator)
        {
        	$company = \App\Models\DealerCompany::where('id', '=', $parameters[0])->first();

        	if($company) {
        		if($company->app_allow_renters) {
        			$allowRenters = true;
        		} else {
        			$allowRenters = false;
        		}
        	} else {
        		$allowRenters = true;
        	}

        	return ($value == 'Rent' && !$allowRenters) ? false : true;
		});


		Validator::extend('alpha_space', function($attribute, $value)
		{
		    return preg_match('/^[\pL\s]+$/u', $value);
		});


        // custom rule for ensuring the apr does not exceed maximum allowed for the given state
        Validator::extend('state_apr', function($attribute, $value, $parameters, $validator)
        {

            // get state
            $data = $validator->getData();
            $app = \App\Models\ContractApplication::findOrFail($data['appid']);

            // return maximum apr
            $row = DB::table('settings_state')->select('max_apr')->where('state', $app->state)->lists('max_apr');
            $this->maxApr = $row[0];
            $this->state = $app->state;
            return $data['apr'] <= $row[0];
        }, 'Max :state APR :max_apr%');

        Validator::replacer('state_apr', function($message, $attribute, $rule, $parameters)
        {
            return str_replace(array(':state', ':max_apr'), [$this->state, $this->maxApr], $message);
        });


        DealerCompany::created(function ($company)
        {
        	// keep creating dist_id for new entries for legacy reasons, not even sure why anymore
            $max = (int) DealerCompany::withTrashed()->max('dist_id');
            $company->dist_id = $max + 1;
            $company->app_allow_renters = 1;

            $company->save();
        });

        
        // sign token creation - if already exists for given email, delete the old one
        // event for file creation to archive existing file that matches
        SignToken::creating(function ($token)
        {
        	$tokens = SignToken::where('email', '=', $token->email)->delete();
        });

        ContractApplication::deleting(function (ContractApplication $app) {
        	// remove all related
        	$app->flags()->delete();
        	$app->allFiles()->delete();
        	$app->salesSlip()->delete();
        	$app->appFilter()->delete();
        });

        ContractApplication::created(function (ContractApplication $app) {
        	Event::fire(new \App\Events\AppWasCreated($app));
        });

        ContractApplication::updating(function (ContractApplication $app) {

        	// if either email or coemail change
        	$old = $app->getOriginal();
        	$email = isset($old['email']) ? $old['email'] : '';
        	$coEmail = isset($old['coEmail']) ? $old['coEmail'] : '';

		    if ($app->email != $email || $app->coEmail != $app->coEmail) {
		        Event::fire(new \App\Events\AppWasCreated($app));
		    }
        });

        SalesSlip::deleting(function (SalesSlip $salesSlip) {
        	// remove all related
        	$salesSlip->items()->delete();
        });

	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
