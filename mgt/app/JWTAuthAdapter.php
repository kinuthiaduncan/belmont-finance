<?php

namespace App;

use Exception;
use Illuminate\Auth\AuthManager as AuthManager;
use Log;

class JWTAuthAdapter implements \Tymon\JWTAuth\Providers\Auth\AuthInterface
{
    /**
     * @var \Illuminate\Auth\AuthManager
     */
    protected $auth;

    /**
     * @param \Illuminate\Auth\AuthManager  $auth
     */
    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Check a user's credentials
     *
     * @param  array  $credentials
     * @return bool
     */
    public function byCredentials(array $credentials = [])
    {
        return $this->auth->guard('dealers')->attempt($credentials);
    }

    /**
     * Authenticate a user via the id
     *
     * @param  mixed  $id
     * @return bool
     */
    public function byId($id)
    {
        // dd($this->auth->guard('dealer')->onceUsingId($id));

        try {
            return $this->auth->guard('dealer')->onceUsingId($id);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get the currently authenticated user
     *
     * @return mixed
     */
    public function user()
    {
        return $this->auth->guard('dealer')->user();
    }
}
