<?php namespace App\Libraries\Contracts;

use App\Models\ContractApplication;
use FPDI;
use Carbon\Carbon;
use JWTAuth;
use Illuminate\Http\Request;
use App\Libraries\Contracts\ContractLibrary;
use Mail;

class AchLibrary {
    private $app;
    private $fileId;

    public function __construct(ContractApplication $app) {
        $this->app = $app;
        $this->fileId = 'ach';
    }

    // consumer view and sign completion form
    public function signAchForm($user) {

        // generate a pdf prior to signing with the data all filled in
        $unsignedPdf = $this->addTerms($user);

        $pdfImage = ContractLibrary::generatePdfImage($unsignedPdf->local_path);

        // get signature locations
        $signatureLocations = $this->retrieveSignatureLocations($this->fileId . '-' . 'dealer', $this->app->salesSlip->sale_state);

        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );
        $successView = 'dealer.contract._ach';
        
        return [
            'app' => $this->app,
            'buyerKey' => 1,
            'includeSuccess' => $successView,
            'pdfImage' => $pdfImage,
            'sessionKey' => 'test',
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/apps/sign-ach'
        ];
    }

    // after customer signs ach form
    public function submitSign($request, $isCompleted) {

        $file = \App\Models\File::where('app_id', '=', $this->app->id)
            ->where('file_type', '=', 14)
            ->first();

        $signedFile = ContractLibrary::signPdf(basename($file->local_path, '.pdf'), $request->signatures, $this->app->lastName, 6, $this->app, $isCompleted);
        return $signedFile;
    }
    

    public function addTerms($user){
        // authenticate user by id
        $this->authenticateSigning($user);
        return $this->writeTerms();
    }

    // custm write and mark on document
    // returns a File Object
    public function writeTerms(){
        // retrieve signed Sales Slip file reference

        // create new writer object
        $contractWriter = new \App\Libraries\Contracts\ContractWriter(base_path('./resources/assets/misc/' . $this->fileId . '.pdf' ));

        $contractWriter->write('APP ID: ' . $this->app->fca_id, 82, 54);
        $contractWriter->write($this->app->firstName . ' ' . $this->app->lastName, 64, 60);
        $contractWriter->write($this->app->address, 67, 66);
        $contractWriter->write($this->app->city, 60, 72);
        $contractWriter->write($this->app->state, 102, 72);
        $contractWriter->write($this->app->zip, 138, 72);


        $contractWriter->write(implode('     ', str_split($this->app->salesSlip->ach_routing_number)), 20, 207, 7.8);
        $contractWriter->write(implode('  ', str_split($this->app->salesSlip->ach_account_number)), 20, 225);

        $contractWriter->write($this->app->salesSlip->ach_day_of_month, 130, 163.7);
        $contractWriter->write($this->getMinPayment(), 130, 152.6);
        $contractWriter->write($this->getPaymentDueDate(), 130, 158);

        if($this->app->salesSlip->ach_is_savings) {
            $contractWriter->write('X', 135, 209);
        } else {
            $contractWriter->write('X', 135, 200);
        }

        $contractWriter->write(date('m/d/Y'), 100, 248);
        $contractWriter->write(date('m/d/Y'), 100, 260);

        // generate remaining pages
        for ($i = 2; $i <= $contractWriter->numPages; $i++) {
            $contractWriter->moveToPage($i);
        }

        // save pdf
        $path = '../documents/contracts/temp/' . $this->app->firstName . '-' . $this->app->lastName . '-ach-' . time() . '.pdf';
        return $contractWriter->finalizePdf($path, $this->app->id, 13, 'Unsigned ACH');
    }

    private function authenticateSigning($user)
    {
        if($this->app->dealerId != $user->id) {
            $appUser = \App\Models\Dealer::where('id', '=',  $this->app->dealerId)->first();
            
            if($user->admin && ($user->company->id == $appUser->company->id)) {
               // authenticated
            } else {
                 abort('403', 'Unauthorized Document Access');
            }
        }
    }

    public function retrieveSignatureLocations($pdfFileId)
    {   
        $signatureLoc = json_decode(file_get_contents('../resources/assets/signature/signatureLocations.json'));

        $fileMap = 'ach';

        // some files don't have any general signatures, only state specific
        if(isset($signatureLoc->{$fileMap})) {
            $fieldPositions = $signatureLoc->{$fileMap};
        } else {
            $fieldPositions = [];
        }

        return $fieldPositions;
    }

    private function getMinPayment()
    {
        return number_format($this->app->salesSlip->amount_financed * ($this->app->salesSlip->payment_factor / 100), 2, '.', '');
    }

    private function getPaymentDueDate()
    {
        $promos = [
            $this->app->salesSlip->promo_id,
            $this->app->salesSlip->promo_id_2,
            $this->app->salesSlip->promo_id_3,
            $this->app->salesSlip->promo_id_4
        ];

        $deferredPromo = \App\Models\Promo::whereIn('id', array_filter($promos))
                ->whereNotNull('days_deferred')
                ->orderBy('days_deferred', 'DESC')
                ->first();

        $daysDeferred = $deferredPromo ?  $deferredPromo->days_deferred : 30;

        return Carbon::now()->addDays($daysDeferred)->format('m/d/Y');
    }
}
