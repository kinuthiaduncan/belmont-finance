<?php namespace App\Libraries\Contracts;

use App\Models\ContractApplication;
use FPDI;
use Carbon\Carbon;
use App\Models\File;

class ContractWriter{
    public $installment;
    public $offset;
    public $numPages;

    public function __construct($path){
        $this->installment = new FPDI();
        $this->installment->AddPage('P', 'Legal');
        $this->numPages = $this->installment->setSourceFile($path);
        $tplIdx = $this->installment->importPage(1);
        $this->installment->useTemplate($tplIdx);
        $this->installment->SetFont('Courier', 'b', 8.5);
        $this->installment->SetTextColor(0, 0, 0);

        $this->offset = 0;
    }

    // add an existing pdf to the end of the current pdf
    public function appendFile($path){
        $this->installment->AddPage('P', 'Legal');
        // set the source file
        $this->installment->setSourceFile($path);
        // import page 1
        $tplIdx = $this->installment->importPage(1);
        $this->installment->useTemplate($tplIdx);
    }

    // write out the whole address block for a company
    public function writeCompanyAddress($company, $x, $y){
        $this->write($company->company_name, $x, $y);
        $this->write($company->address, $x, $y + 3.5);
        $this->write($company->city . ', ' . $company->state . ' ' . $company->zip, $x, $y + 7);
    }

    public function write($value, $x, $y, $adjustFontSize = false) {
        if($adjustFontSize) {
            $this->installment->SetFont('Courier', 'b', $adjustFontSize);
        }
        $this->installment->SetXY($x, $y + $this->offset);
        $this->installment->Write(0, $value);
        $this->installment->SetFont('Courier', 'b', 8.5);
    }

    public function cell($value, $x, $y, $width, $height, $align, $adjustFontSize = false) {
        if($adjustFontSize) {
            $this->installment->SetFont('Courier', 'b', $adjustFontSize);
        }

        $this->installment->SetXY($x, $y + $this->offset);
        $this->installment->Cell($width, $height, $value, 0, 0, 'R');

        $this->installment->SetFont('Courier', 'b', 8.5);
    }

    public function finalizePdf($savePath, $appId, $fileType, $fileName)
    {
        $fileId = File::create([
            'file_type' => $fileType,
            'app_id' => $appId,
            'name' => $fileName, 
            'local_path' => $savePath
        ]);

        $this->installment->Output(base_path($savePath), 'F');
        return $fileId;
    }

    public function moveToPage($pageNum) {
        $this->installment->AddPage('P', 'Legal');
        $tplIdx = $this->installment->importPage($pageNum);
        $this->installment->useTemplate($tplIdx);
    }
}