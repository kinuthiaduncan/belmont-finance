<?php namespace App\Libraries\Contracts;

use App\Models\ContractApplication;
use FPDI;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class InstallmentContractLibrary {
    private $app;
    public $fileId;

    public function __construct(ContractApplication $app, $fileId) {
        $this->app = $app;
        $this->fileId = $fileId;
    }

    public function addTerms($data, $user){
        // authenticate user by id
        $this->authenticateSigning($user);
        return $this->app->credit_type == 'Revolving' ? 
            $this->writeTermsRevolving($data) :
            $this->writeTermsClosedEnd($data);
    }


    private function writeTermsClosedEnd($data){
        // create new writer object
        $contractWriter = new \App\Libraries\Contracts\ContractWriter(base_path('resources/assets/sales-slips/' . $this->fileId . '.pdf'));

        if ($this->fileId == "271186") {
            $contractWriter->offset = -4;
        } else if ($this->fileId == "271183") {
            $contractWriter->offset = 3.3;
        }

        // write seller information
        $company = $this->app->dealerUser->company;
        $contractWriter->write($company->company_name . ($company->license ? ' (' . $company->license . ')' : ''), 46, 15.7);
        $contractWriter->write($company->address, 46, 19.7);
        $contractWriter->write($company->city . ', ' . $company->state . ' ' . $company->zip, 46, 23.4);
        $contractWriter->write($company->phone, 46, 27.7);

        // write buyer information
        $contractWriter->write($this->app->firstName . ' ' . $this->app->lastName, 44, 32);
        $contractWriter->write($this->app->coFirstName . ' ' . $this->app->coLastName, 44, 35);
        $contractWriter->write($this->app->address, 22, 40);
        $contractWriter->write($this->app->city, 16, 48);
        $contractWriter->write($this->app->state, 57, 48);
        $contractWriter->write($this->app->zip, 77, 48);


        $contractWriter->write( $data['description_one'], 125, 27);
        $contractWriter->write( $data['sku_one'], 125, 31);
        $contractWriter->write( $data['attachments'], 130, 35);
        $contractWriter->write( $data['retail_price'], 180, 31);

        $salePrice = number_format((float)$data['price_one'] * $data['qty_one'], 2, '.', '');
        $contractWriter->cell($salePrice, 191, 45.8, 16.5, 4, 'R', 7.5);

        if(isset($data['trade_in'])) {
            $contractWriter->cell(number_format((float)$data['trade_in'], 2, '.', ''), 191, 49.8, 16.5, 4, 'R', 7.5);
            //tradein
            // tradein description
            $contractWriter->write($data['trade_in_description'], 136, 59.5);
        } else {
            $data['trade_in'] = 0;
        }
        

        // subtotal
        $subtotal = $salePrice - $data['trade_in'];
        $contractWriter->cell(number_format((float)($subtotal), 2, '.', ''), 191, 57.7, 16.5, 4, 'R', 7.5);

        // sum right column
        $contractWriter->cell(number_format((float)$data['sales_tax'], 2, '.', ''), 191, 61.7, 16.5, 4, 'R', 7.5);

        //total after tradein and sales tax
        $contractWriter->cell(number_format((float)($subtotal + $data['sales_tax']), 2, '.', ''), 191, 65.7, 16.5, 4, 'R', 7.5);
        //down payment
        $contractWriter->cell(number_format((float)($data['down_payment']), 2, '.', ''), 191, 69, 16.5, 4, 'R', 7.5);
        // final
        $amountFinanced = $subtotal + $data['sales_tax'] - $data['down_payment'];
        $contractWriter->cell('$' . number_format((float)$amountFinanced, 2, '.', ''), 191, 74, 16.5, 4, 'R', 7.5);

    
        // lower section adjust offset
        if ($this->fileId == "271186") {
            // add FL DOC stamp tax if applicable
            if($data['state'] == 'FL') {
                $docTax = ceil($amountFinanced / 100) * 100 * 0.0035;
                $contractWriter->cell('$' . number_format((float)$docTax, 2, '.', ''), 191, 77.5, 16.5, 4, 'R', 7.5);
                $amountFinanced += $docTax;
            }

            // total after the doc stampt tax
            $contractWriter->cell('$' . number_format((float)$amountFinanced, 2, '.', ''), 191, 82, 16.5, 4, 'R', 7.5);

            $contractWriter->offset = 3.6;
        }

        // write promos and set some values that come from promos
        $promoIndex = 0;
        $daysDeferred = 30;
        
        if(isset($data['promos'])) {
            foreach($data['promos'] as $promoId) {
                $promo = \App\Models\Promo::where('id', '=', $promoId)->first();
                if($promo) {
                    $contractWriter->write($promo->description, 142, 126 + $promoIndex, 6);
                    $promoIndex = $promoIndex + 2.2;
                    if($promo->days_deferred) {
                        $daysDeferred = $promo->days_deferred;
                    }
                }
            }
        }

        Session::put('sales-slip-data.deferredDays', $daysDeferred);

        $paymentFactor = \App\Libraries\Contracts\ContractLibrary::getMinimumPaymentFactor($this->app, [
            'apr' => $data['apr'],
            'term' => $data['term'],
            'deferredDays' => $daysDeferred
        ]);

        $monthlyPayments = number_format($amountFinanced * $paymentFactor, 2);
        $totalPayments = $monthlyPayments * $data['term']; // number_format required to round monthlyPayments up

        // first payment due
        $contractWriter->write($this->getPaymentDueDate($daysDeferred), 81, 131);

        // Amount Financed
        $contractWriter->write(number_format($amountFinanced, 2), 80, 122);

        // FINANCE CHARGE
        $contractWriter->write(number_format($totalPayments - $amountFinanced, 2), 52, 122);


        // ANNUAL PERCENTAGE
        $contractWriter->write(number_format($data['apr'], 2), 15, 122);

        // Total of payments
        $contractWriter->write(number_format($totalPayments, 2), 120, 122);

        // total sale price (add tradein to down payment on dop line)
        $contractWriter->write(number_format((float)($data['down_payment'] + $data['trade_in']), 2, '.', ''), 165, 115);

        $contractWriter->write(number_format((float)($data['down_payment'] + $totalPayments), 2, '.', ''), 165, 122);

        // No. of Monthly payments
        $contractWriter->write($data['term'], 15, 134);

        // Amount of (monthly) payments
        $contractWriter->write('$' . number_format($monthlyPayments, 2), 55, 134);

        // cancellation deadlines
        /*$cancellationDate = $this->cancellationDate(strtoupper($this->app->state));
        $contractWriter->write($cancellationDate, 69, 334.7);
        $contractWriter->write($cancellationDate, 172, 334.7);*/

        // different dates for different states
        $contractWriter->offset = 0;
        if( $this->fileId == '271184_2' ) {
            $contractWriter->write(date('m/d/Y'), 12, 205);

            if($data['state'] == 'WI') {
                $contractWriter->write(date('m/d/Y'), 12, 227);
            } elseif ($data['state'] == 'IL') {
                $contractWriter->write(date('m/d/Y'), 12, 246);
            } elseif ($data['state'] == 'HI') {
                $contractWriter->write(date('m/d/Y'), 12, 264);
            } elseif ($data['state'] == 'WA') {
                $contractWriter->write(date('m/d/Y'), 12, 300);
            }
        } else if ($this->fileId == '271186') {
            if($data['state'] == 'WI') {
                $contractWriter->write(date('m/d/Y'), 12.5, 244);
            }
            $contractWriter->write(date('m/d/Y'), 13, 322);
        } else if ($this->fileId == '271185') {
            $data['state'] = 'MD';
            if($data['state'] == 'NJ') {
                $contractWriter->write(date('m/d/Y'), 12.5, 188);
            }

            if(in_array($data['state'], ['NJ', 'MI', 'PA'])) {
                $contractWriter->write(date('m/d/Y'), 12.5, 262.3);
            } else if ($data['state'] == 'MD') {
                $contractWriter->write($this->app->firstName . ' ' . $this->app->lastName, 40, 326);
                $contractWriter->write(date('m/d/Y'), 12.5, 291);
                $contractWriter->write(date('m/d/Y'), 12.5, 316);
                $contractWriter->write(date('m/d/Y'), 12.5, 341);
            }


            $contractWriter->write(date('m/d/Y'), 13, 322);
        } else if ($this->fileID = '271183') {
            if($data['state'] == 'MD') {
                $contractWriter->write(date('m/d/Y'), 12.5, 226);
            } else if ($data['state'] == 'RI') {
                $contractWriter->write(date('m/d/Y'), 12.5, 269.6);
            } else if ($data['state'] == 'MA') {
                $contractWriter->write(date('m/d/Y'), 145, 319);
            }

            if(in_array($data['state'], ['DE', 'DC', 'RI', 'MD'])) {
                $contractWriter->write(date('m/d/Y'), 12.5, 290);
            } 
        }

        $contractWriter->moveToPage(2);

        if($this->fileId == "271184_2") {
            $contractWriter->write($company->company_name, 136, 317);
        }

        // save pdf
        $path = '../documents/contracts/temp/' . $this->app->firstName . '-' . $this->app->lastName . '-contract-' . time() . '.pdf';
        $contractWriter->finalizePdf($path, $this->app->id, 2, 'Unsigned ' . $this->app->credit_type == 'Revolving' ? 'Sales Slip' : 'Contract');
        return $path;
    }

    private function writeTermsRevolving($data){
        // create new writer object
        $contractWriter = new \App\Libraries\Contracts\ContractWriter(base_path('resources/assets/sales-slips/' . $this->fileId . '.pdf'));
        // retrieve dealer company
        $dealer = \App\Models\Dealer::find($this->app->dealerId);
        $company = $dealer->company;

        // we have to adjust the offset as some of these have fields in different places
        if($this->fileId == "271482") {
            $contractWriter->offset = -12;
        } else if ($this->fileId == "271484") {
            $contractWriter->offset = -3;
        }

        // dealer information
        $contractWriter->write($company->company_name, 36, 37);
        $contractWriter->write($company->address . ' ' . $company->address_two, 36, 39.5);
        $contractWriter->write($company->city . ', ' . $company->state . ' ' . $company->zip , 36, 42);
        $contractWriter->write($company->phone , 36, 44.5);
        $contractWriter->write($company->license, 36, 48);

        // date of sale
        $contractWriter->write(date('m/d/Y'), 7.8, 29);

        // mark yes or no addon
        $addonPos = $data['is_addon'] ? [73.2,  29.8] : [82.2, 29.8];
        $contractWriter->write('X', $addonPos[0], $addonPos[1]);

        if($this->fileId == "271482") {
            $contractWriter->offset = -10.2;
        }
        

        // write name, address, phone
        $contractWriter->write($this->app->firstName .' ' . $this->app->lastName, 137, 35.4);
        $contractWriter->write($this->app->coFirstName .' ' . $this->app->coLastName, 137, 40.4);
        $contractWriter->write($this->app->address , 137, 46.4);
        $contractWriter->write($this->app->addressTwo , 137, 49.5);
        $contractWriter->write($this->app->city . ', ' . $this->app->state . ' ' . $this->app->zip , 137, 52);


        if($this->fileId == "271482") {
            // RI date
            if($data['state'] == 'MA') {
                $contractWriter->write(date('m/d/Y'), 140, 141);
            } else if ($data['state'] == 'RI') {
                $contractWriter->write(date('m/d/Y'), 11, 198);
            }
            $contractWriter->offset = -8.2;
        } else if($this->fileId == "271484") {
            $contractWriter->offset = 3;

            if($data['state'] == 'NJ') {
                $contractWriter->write(date('m/d/Y'), 11, 149);
            }
            if ( in_array($data['state'], ['NJ', 'PA', 'MI'])) {
                $contractWriter->write(date('m/d/Y'), 11, 182);
            }
            if($data['state'] == 'MD') {
                $contractWriter->write(date('m/d/Y'), 11, 223);
                $contractWriter->write(date('m/d/Y'), 11, 248);
            }
          
        }  else { // 271483
            $contractWriter->offset = 0;
                    // dates for different signature fields
            $contractWriter->write(date('m/d/Y'), 9, 155);

            if($data['state'] == 'HI') {
                $contractWriter->write(date('m/d/Y'), 9, 174);
            } else if ($data['state'] == 'VT') {
                $contractWriter->write(date('m/d/Y'), 9, 188.3);
            } else if ($data['state'] == 'WA') {
                $contractWriter->write(date('m/d/Y'), 9, 226.7);
            }

        }

        // sale prices
        $contractWriter->write($data['qty_one'], 9, 70.6);
        $contractWriter->write($data['qty_two'], 9, 74.3);
        $contractWriter->write($data['qty_three'], 9, 78);

        $contractWriter->write($data['description_one'], 20, 70.6);
        $contractWriter->write($data['description_two'], 20, 74.3);
        $contractWriter->write($data['description_three'], 20, 78);

        $contractWriter->write($data['sku_one'], 120, 70.6);
        $contractWriter->write($data['sku_two'], 120, 74.3);
        $contractWriter->write($data['sku_three'], 120, 78);

        $contractWriter->write((float)$data['price_one'], 173, 70.6);
        $contractWriter->write((float)$data['price_two'], 173, 74.3);
        $contractWriter->write((float)$data['price_three'], 173, 78);

        $salePrice = number_format((float)$data['price_one'] * $data['qty_one'] + (float)$data['price_two'] * $data['qty_two'] + (float)$data['price_three'] * $data['qty_three'], 2, '.', '');
        $contractWriter->write($salePrice, 173, 82.2);
        $contractWriter->write(number_format((float)$data['shipping'], 2, '.', ''), 173, 86.5);
        $contractWriter->write(number_format((float)$data['sales_tax'], 2, '.', ''), 173, 90.8);

        $totalSalesPrice = (float)$salePrice + (float)(isset($data['shipping_handling']) ? $data['shipping_handling'] : $data['shipping']) + (float)$data['sales_tax'];
        $contractWriter->write(number_format((float)$totalSalesPrice, 2, '.', ''), 173, 95.2);

        // down payment method
        if(isset($data['down_payment_method'])) {
            if($data['down_payment_method'] == "cash") {
                $contractWriter->write('X', 42.5, 98.6);
            } else if ($data['down_payment_method'] == "check") {
                $contractWriter->write('X', 48.7, 98.6);
            } else if ($data['down_payment_method'] == "credit") {
                $contractWriter->write('X', 54.9, 98.6);
            }
        }

        // minimum payment factor
        $pmtFactor = \App\Libraries\Contracts\ContractLibrary::getMinimumPaymentFactor($this->app, $data) * 100;

        if (false !== strpos($pmtFactor, '.'))
            $pmtFactor = rtrim(rtrim($pmtFactor, '0'), '.');

        $contractWriter->write($pmtFactor . '%', 58.7, 113);

        $contractWriter->write(number_format((float)$data['down_payment'], 2, '.', ''), 173, 99);

        $contractWriter->write(number_format($totalSalesPrice - (float)$data['down_payment'], 2, '.', ''), 173, 107);


        // write promos
        $promoIndex = 0;
        if(isset($data['promos'])) {
            foreach($data['promos'] as $promoId) {
                $promo = \App\Models\Promo::where('id', '=', $promoId)->first();
                if($promo) {
                    $contractWriter->write($promo->description, 53, 103 + $promoIndex, 6);
                    $promoIndex = $promoIndex + 2.2;
                }
            }
        }
        
        $contractWriter->moveToPage(2);

        // dealer information
        if($this->fileId == "271482") {
            $contractWriter->write($company->company_name, 135, 91.8);
        } else if($this->fileId == "271484") {
            $contractWriter->write($company->company_name, 135, 91.8);
        }  else {
            $contractWriter->write($company->company_name, 135, 225.8);
        }



        // save pdf
        $path = '../documents/contracts/temp/' . $this->app->firstName . '-' . $this->app->lastName . '-sales-slip-' . time() . '.pdf';
        $contractWriter->finalizePdf($path, $this->app->id, 2, 'Unsigned ' . $this->app->credit_type == 'Revolving' ? 'Sales Slip' : 'Contract');
        return $path;
    }

    /**
    * Calculate the start date for a loan and return formatted date
    * @return string formatted date
    */
    private function getPaymentDueDate($deferred){
        $today = Carbon::now();
        return $today->addDays($deferred)->format('m/d/Y');
    }

    private function roundUp($number, $precision = 2){
        $fig = pow(10, $precision);
        return (ceil($number * $fig) / $fig);
    }

    private function authenticateSigning($user)
    {
        if( $user->id === $this->app->dealerId){
           // is authenticated
        }else{
            // check for admin user
            $appUser = \App\Models\Dealer::where('id', '=', $this->app->dealerId)->first();
            if($user->company->id === $appUser->company->id && $user->admin) {
                // authenticated
            } else {
                abort(403, 'Authorization error');
            }
        }
    }

     // calculate cancellation date based on state
    private function cancellationDate($state) {
        $excludeSaturday = [ 'AL', 'HI', 'MI', 'MN', 'MS', 'MO', 'NJ', 'WI'];
        $federalHolidays = [
            'New Years Day' => Carbon::parse('January 1'),
            'Martin Luther King Jr Day' => Carbon::parse('third Monday of January'),
            'George Washingtons Birthday' => Carbon::parse('third Monday of February'),
            'Memorial Day' => Carbon::parse('last Monday of May'),
            'Independence Day' => Carbon::parse('July 4'),
            'Labor Day' => Carbon::parse('first Monday of September'),
            'Columbus Day' => Carbon::parse('second Monday of October'),
            'Veterans Day' => Carbon::parse('November 11'),
            'Thanksgiving Day' => Carbon::parse('fourth Thursday of November'),
            'Christmas Day' => Carbon::parse('December 25'),
        ];
        $cancelDate = Carbon::parse('today');
        $daysAdded = 0;
        while ($daysAdded < 3) {
            $cancelDate->addDay();
            if ( ($cancelDate->isWeekday() || ($cancelDate->isSaturday() && !in_array($state, $excludeSaturday))) && !in_array($cancelDate, $federalHolidays) ) {
                $daysAdded++;
            }
        }
        return $cancelDate->format('m/d/Y');
    }
}
