<?php namespace App\Libraries\Contracts;

use App\Models\ContractApplication;
use App\Services\FileSignatureService;
use FPDI;
use Carbon\Carbon;
use App\Models\File;
use App\Models\Dealer;


class CreditLibrary {

    private $app;
    public $fileId;

    public function __construct(ContractApplication $app, $fileId) {
        $this->app = $app;
        $this->fileId = $fileId;
    }

    public function addTerms($data)
    {
        return $this->generatePdf($data);
    }


     /**
    * Credit application only -
    * Retrieve the correct pdf and prepare it for signing (write all the field data to the pdf prior to signatures)
    */
    public function generatePdf($data) {
        $appId = $this->app->id;
        $pdfFileId = $this->fileId;

        // check if we already have a temporary pdf (if the page was refreshed we would)
        $existingFile = File::where('app_id', '=', $appId)
            ->where('file_type', '=', 3)
            ->first();

        if($existingFile) {
            // we had better clear out the old file every time in case dealer goes back and changes information
            $existingFile->delete();
        }

        $dealer = $this->app->dealerUser;
        $company = $this->app->distributor;

        // initialize pdf writer and set some defaults
        $applicationPDF = new FPDI();
        $applicationPDF->SetFont('Courier', 'b', 8.5);
        $applicationPDF->SetTextColor(0, 0, 0);

        $basePath = 'resources/assets/application/';

        $totalPages = $applicationPDF->setSourceFile(base_path($basePath . $pdfFileId . '.pdf'));

        // separate the first page to fill data on it
        $applicationPDF->AddPage('P', 'Letter');
        $firstPage = $applicationPDF->importPage(1);
        $applicationPDF->useTemplate($firstPage);
        $applicationPDF->SetAutoPageBreak(false);
        $applicationPDF->SetFont('Courier', 'b', 8.5);

        // get the locations of fields to set on pdf
        $fieldPositions = ContractLibrary::retrieveFieldLocations($pdfFileId);

        $date = Carbon::parse($this->app->created_at)->format('m/d/Y');

        // format a couple dollar fields
        $data['amount_financed'] = number_format((float)$data['amount_financed'], 2, '.', '');

        if(isset($data['down_payment_amount'])) {
            $data['down_payment_amount'] = number_format((float)$data['down_payment_amount'], 2, '.', '');
        }

        // revolving credit file
        if ($pdfFileId == '281734' || $pdfFileId == '281734-spanish' || $pdfFileId == '281736' || $pdfFileId == '281735' || $pdfFileId == '281735-spanish' || $pdfFileId == '282499') { 

            // mark joint spouse
            if( isset($data['coLastName']) && $data['coLastName']
            && isset($data['coRelation']) && $data['coRelation'] == "spouse") {

                $applicationPDF->SetXY('1.45', '14.7');
                $applicationPDF->Write(0, 'x');

            } else if( isset($data['coLastName']) && $data['coLastName'] ) {
                // non spousal agreement
                $applicationPDF->SetXY('1.45', '20');
                $applicationPDF->Write(0, 'x');

            } else {
                // individual agreement
                $applicationPDF->SetXY('1.45', '12');
                $applicationPDF->Write(0, 'x');
            }

            // mark other identification types
            if(isset($data['identification_type']) && $data['identification_type'] == 'photo id') {
                if(substr($pdfFileId, -7) == 'spanish') {
                    $applicationPDF->SetXY('75', '40');
                } else {
                    $applicationPDF->SetXY('25.5', '40');
                }
                $applicationPDF->Write(0, 'x');
            }

            if(isset($data['co_identification_type']) && $data['co_identification_type'] == 'photo id') {
                if(substr($pdfFileId, -7) == 'spanish') {
                    $applicationPDF->SetXY('73', '46');
                } else {
                    $applicationPDF->SetXY('33', '46');
                }
                
                $applicationPDF->Write(0, 'x');
            }

            // mark if not us citizen
            if(isset($data['us_citizen']) && $data['us_citizen'] == 'No') {
                $applicationPDF->SetXY('37.6', '73.6');
                $applicationPDF->Write(0, 'x');
            }

            // mark own or rent
            if($data['residenceType'] == "Own") {
                $applicationPDF->SetXY('1', '120');
                $applicationPDF->Write(0, 'x');
            } else if ($data['residenceType'] == "Rent") {
                $applicationPDF->SetXY('8', '120');
                $applicationPDF->Write(0, 'x');
            }

            // write distributor number
            if(substr($pdfFileId, -7) == 'spanish') {
                $applicationPDF->SetXY('60', '50.3');
            } else {
                $applicationPDF->SetXY('14', '50');
            }
            $applicationPDF->Write(0, $dealer->name);

            $applicationPDF->SetXY('13', '55');
            $applicationPDF->Write(0,  $company->account_number);

            // write date of signatures
            $applicationPDF->SetXY('185.5', '95.6');
            $applicationPDF->Write(0, date('m/d/Y'));

            if( isset($data['coLastName']) && $data['coLastName'] ){
                // write date of signatures
                $applicationPDF->SetXY('185.5', '105.4');
                $applicationPDF->Write(0, date('m/d/Y'));
            }   

        // retail  closed-end type
        } else if ($pdfFileId == '280614' || $pdfFileId == '280614-spanish') {
            // mark own or rent
            if($data['residenceType'] == "Own") {
                $applicationPDF->SetXY('14', '74');
                $applicationPDF->Write(0, 'x');
            } else if ($data['residenceType'] == "Rent") {
                $applicationPDF->SetXY('36', '74');
                $applicationPDF->Write(0, 'x');
            } else {
                $applicationPDF->SetXY('47', '74');
                $applicationPDF->Write(0, $data['residenceType']);
            }

            if($pdfFileId == '280614-spanish') { $applicationPDF->SetXY('159', '10'); } else { $applicationPDF->SetXY('151', '10'); };
            $applicationPDF->Write(0,  $company->account_number);

            // add dates manually since smaller size required
            $applicationPDF->SetFont('Courier', 'b', 6.5);
            $applicationPDF->SetXY('89', '184.7');
            $applicationPDF->Write(0, date('m/d/Y'));

            if( isset($data['coLastName']) && $data['coLastName']) {
                 // add dates manually since smaller size required
                $applicationPDF->SetXY('190', '184.7');
                $applicationPDF->Write(0, date('m/d/Y'), '9');
            }
            $applicationPDF->SetFont('Courier', 'b');

            // mark joint spouse
            if( isset($data['coLastName']) && $data['coLastName']
            && isset($data['coRelation']) && $data['coRelation'] == "spouse") {

                $applicationPDF->SetXY('15', '25');
                $applicationPDF->Write(0, 'x');

            } else if( isset($data['coLastName']) && $data['coLastName'] ) {
                // non spousal agreement
                $applicationPDF->SetXY('15', '28');
                $applicationPDF->Write(0, 'x');

            } else {
                // individual agreement
                $applicationPDF->SetXY('15', '22');
                $applicationPDF->Write(0, 'x');
            }
        }

        $phoneFields = ['homePhone', 'cellPhone', 'personalRefOnePhone', 'employerPhone', 'coHomePhone', 'coCellPhone', 'coEmployerPhone'];
        if( isset($data['copy_address']) && $data['copy_address'] ) {
            $data['coYrsAtAddress'] = $data['yrsAtAddress'];
            $data['coMonthsAtAddress'] = $data['monthsAtAddress'];
        }

        // add the rest of the fields as marked by location in assets file
        foreach ($fieldPositions as $fieldName => $coordinates) {
            $fieldValue = '';
            $location = $coordinates;

            // take care of special fields that may need formatting or other discretion
            if ($fieldName == 'ssn') {
                $fieldValue = "xxx-xx-" . substr(isset($data['ssn']) ? $data['ssn'] : '', -4);
            } else if ($fieldName == 'co_ssn') {
                $fieldValue = isset($data['co_ssn']) ? "xxx-xx-" . substr($data['co_ssn'], -4) : '';
            } else if ($fieldName == 'bank_address') {
                $fieldValue = '';
            } else if ($fieldName == 'employerCityAndState') {
                $fieldValue = '';
            } else if (substr($fieldName, 0, 13)  == 'signatureDate'){
                $fieldValue = $date;
            } else {
                $fieldValue = isset($data[$fieldName]) ? $data[$fieldName] : '';
            }

            // format phone number
            if($fieldValue && in_array($fieldName, $phoneFields)) {
                $fieldValue = '(' . substr($fieldValue, 0, 3) . ') ' . substr($fieldValue, 3, 3) .'-' . substr($fieldValue, 6, 4);
            }

            if(gettype($location) == "array") {
                $applicationPDF->SetXY($location[0],$location[1]);
                if(isset($location[2])) {
                    $applicationPDF->SetFont('Courier', 'b', $location[2]);
                }

                $applicationPDF->Write(0, $fieldValue);
                $applicationPDF->SetFont('Courier', 'b', 8);
            }
        }

        // now, add the rest of the pages back on
        for ($i = 2; $i <= $totalPages; $i++) {
            $applicationPDF->AddPage('P', 'Letter');
            $page = $applicationPDF->importPage($i);
            $applicationPDF->useTemplate($page);
        }

        // save and output the pdf
        $currentYear = Carbon::now()->year;
        if (!file_exists('../../documents/contracts/temp')) {
            mkdir('../../documents/contracts/temp', 0755, true);
        }

        $outputFilename = "../documents/contracts/temp/" . $data['lastName'] . '-' . $data['firstName'] . '-' . time() . '.pdf';
        $file = File::create(['file_type' => 1, 'app_id' => $appId, 'name' => 'Unsigned Credit Agreement', 'local_path' => $outputFilename]);


        $applicationPDF->output(base_path($outputFilename), 'F');

        return $outputFilename;
    }

}
