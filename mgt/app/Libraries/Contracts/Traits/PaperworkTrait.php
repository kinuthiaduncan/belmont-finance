<?php namespace App\Libraries\Contracts\Traits;
/**
 * Helper Methods for paperwork / contracts
 * Should start using this trait for more shared methods and properties
 */

use App\Services\FileSignatureService;
use Mail;
 
trait PaperworkTrait
{

	// using a given file id (from the filename listings), is it a contract, sales slip, cancellation notice, etc...
	// accepts a string with the number of the file id, and returns a string with the type of file
	public function getDocumentType($fileId)
	{	
		// sales slip or contract
		if ( in_array($fileId, ['271183', '271184_2', '271185', '271186', '271327', '271328', '271329_2', '271330'])) {
			$return = 'contract';
		} else if ( in_array($fileId, ['271482', '271482-water', '271483', '271483-water', '271484', '271484-water', '271485', '271486'])) {
			$return = 'sales-slip';
		} else if ( in_array($fileId, ['280614', '281734', '281735', '281736', '282499'])) {
			$return = 'app';
		} else if ( in_array($fileId, ['254859']) ) {
			$return = 'completion';
		} else if ( $fileId == 'ach' ) {
			$return = 'ach';
		} else if ( in_array($fileId, ['271549', '271550', '271551', '271552', '271553', '271555', '271556']) ) {
			$return = 'cancellation';
		} else {
            $return = false;
        }

		return $return;
	}


 	// get the right notice of cancellation form to use
 	// accepts a state, returns a string value with the file number
    public function retrieveNoticeCancellation($state, $distributor) {
        $filename = [];

        if(in_array($state, ['NV', 'TX', 'WI'])) {
            if( $this->isHomeImprovementProduct($distributor) ) {
                if(in_array($state, ['TX', 'WI'])) {
                    $filename[] = '271552';
                }
            } else {
                $filename[] = '271552';
            }
        } else {
            if($this->isHomeImprovementProduct($distributor)) {
                if($state == 'MD') {
                    $filename[] = '271554';
                } else if( !in_array($state, ['AR', 'CA', 'CO', 'DE', 'ID', 'IL', 'KS', 'LA', 'ME', 'MD', 'MA', 'NE', 'NV', 'NM', 'OH', 'RI', 'SC', 'SD', 'TX', 'WV', 'WI']) ){
                    $filename[] = '271553';
                }
            } else {
                $filename[] = '271553';
            }

            if($state == 'NJ') {
                $filename[] = '271555';
            } else if ( $state == 'ND') {
                $filename[] = '271556';
            } else if ( $state == 'OH') {
                $filename[] = '271549';
            } else if ( $state == 'AR') {
                $filename[] = '271551';
            }
        }

        if($this->isHomeImprovementProduct($distributor)) {
            $filename[] = '271550';
        }
        return $filename;
    }


    // these functions check what type a distributor is
    public function isWaterProduct($distributor){
        return $distributor->product_id == 26 ? true : false;
    }

    public function isInHome($distributor){
        return $distributor->product_id != 29 ? true : false;
    }

    public function isHomeImprovementProduct($distributor){
        return $distributor->product_id == 28 ? true : false;
    }


     // get the file id of the sales slip or contract
    protected function retrieveSalesSlip($state, $app)
    {   
        $distributor = $app->distributor;

        // revolving sales slips
        if($app->credit_type == "Revolving") {
            if($this->isInHome($distributor)) {
                // all products in these two states are on same sales slip
                if(in_array($state, ['MA', 'RI'])) {
                    $filename = '271482';
                } else if (in_array($state, ['MD', 'MI', 'NJ', 'PA']) && $this->isHomeImprovementProduct($distributor))  {
                    $filename = '271484';
                } else {
                    $filename = '271483';
                }

                if($this->isWaterProduct($distributor)) {
                    $filename .= '-water';
                }
            // in-store revolving sales slips
            } else {
                // standard revolving sales slips
                if (in_array($state, ['MI', 'NJ', 'PA']))  {
                    $filename = '271485';
                } else {
                    $filename = '271486';
                }
            }
            // installment / retail sales slips
        } else {
            if($this->isInHome($distributor)) {
                if(in_array($state, ['DE', 'DC', 'MA', 'MD', 'RI'])) {
                    $filename = '271183';
                } else if (in_array($state, ['MD', 'MI', 'NJ', 'PA'])) {
                    $filename = '271185';
                } else if (in_array($state, ['CT', 'FL', 'KY', 'ME', 'MO', 'NJ', 'NY', 'VT'])) {
                    $filename = '271186';
                } else {
                    $filename = '271184_2';
                }
            } else {
                if(in_array($state, ['CT', 'FL', 'KY', 'ME', 'MO', 'NJ', 'NY', 'VT'])) {
                    $filename = '271327';
                } else if (in_array($state, ['DE', 'DC', 'MA', 'MD', 'RI'])) {
                    $filename = '271328';
                } else if (in_array($state, ['MI', 'NJ', 'PA'])) {
                    $filename = '271330';
                } else {
                    $filename = '271329_2';
                }
            }
        }
        return $filename;
    }


      /**
    * Returns correct filename to use for the credit agreement
    **/
    public static function retrieveCreditAgreement($app)
    {
        $distributor = $app->distributor;
        $language = $app->language ? $app->language : 'English';
        $creditType = $app->credit_type;

        if(strtolower($creditType) == 'revolving') {
            if( in_array($distributor->product_id, [26]) ) {
                // return contract 281735 for water product companies
                $filename = '281735';
            } else if( in_array($distributor->product_id, [16]) ) {
                // return contract 282499 for Saladmaster companies
                $filename = '282499';
            } else if( in_array($distributor->product_id, [28]) ) {
                // return contract 281736 for home improvement companies
                $filename = '281736';
            } else {
                // standard revolving credit application for all others
                $filename = '281734';
            }
        } else {
            $filename = '280614';
        }

        // tack on spanish version
        return $filename . ($language == 'Spanish' ? '-spanish' : '');
    }


    /**
     * Takes a file id and translates it to an associated file type / name
     * 
     * @param  int 
     * @param  \App\Models\ContractApplication $app
     * @return [type]
     */
    public function getFileIdByUnsignedFileType($unsignedFileType, \App\Models\ContractApplication $app) {
        if($unsignedFileType == 2) {
            // contract
            $filename = [$this->retrieveSalesSlip($app->salesSlip->sale_state, $app)];
        } else if ($unsignedFileType == 5) {
            // notice of cancellation
            $filename = $this->retrieveNoticeCancellation($app->salesSlip->state, $app->distributor);
        } else if ($unsignedFileType == 13) {
            $filename = ['ach'];
        } else if ($unsignedFileType == 9) {
            $filename = ['254859'];
        } else if ($unsignedFileType == 1) {
            $filename = [$this->retrieveCreditAgreement($app)];
        } else {
            $filename = null;
        }

        return $filename;
    }


    // method to submit any paperwork for signing
    // parameters are the application model, file model of generated pdf, and the request signatures
    public function submitPaperwork($app, $file, $signatures, $isCoApp) 
    {
        // reverse definitions from contractlibrary
        // todo: we don't really need this anymore, could now pass file id directly to signPDF method on contractlibrary
        $fileTypeMap = [
            '1' => 0,
            '2' => 1,
            '5' => 2,
            '7' => 3,
            '9' => 4,
            '11' => 5,
            '13' => 6
        ];

        $docType = $fileTypeMap[$file->file_type];


        // mark if file still needs to be signed by another party
        if(!$isCoApp && $file->getUnsignedSignaturesBySignee('CO')) {
            $isCompleted = 0;
        } elseif ($isCoApp && $file->getUnsignedSignaturesBySignee('PR')) {
            $isCompleted = 0;
        } else {
            // don't mark completed for contract or lcc, since dealer still has to sign them, it will mess up signature placement
            if($file->file_type != 2 || $file->file_type != 9) {
                $isCompleted = 1;
            }
        }

        // a couple document types are legal sized todo: auto detect all page sizes
        if($docType == 1 || $docType == 10) {
            $size = 'Legal';
        } else {
            $size = 'Letter';
        }

        // send for signing to contract library
        $signedFile = \App\Libraries\Contracts\ContractLibrary::signPdf(basename($file->local_path, '.pdf'), $signatures, $app->lastName, $docType, $app, $isCompleted, $size);

        FileSignatureService::recordFileSignature($signedFile, $isCoApp ? 'CO' : 'PR');
        if($isCoApp == 2) {
            FileSignatureService::recordFileSignature($signedFile, 'PR');
        }
        return true;
    }


    // new method with the sequential signing to check and email if all paperwork is signed
    // returns the count of paperwork left
    public function isSigningComplete($appId, $isCoApp = false)
    {

        $filesToCheck =  [1, 2, 5, 9, 13];

        // check to see if any unsigned files remain
        $fileCount = \App\Models\File::where('app_id', $appId)
                ->whereIn('file_type', $filesToCheck)
                ->where(function($q){
                    $q->has('unsignedSignaturesCo')
                        ->orHas('unsignedSignaturesPri');
                })
                ->count();

        return $fileCount ? 0 : 1;
    }


    public function emailStaffPaperwork($app)
    {
        /*$primaryDealerEmail = [\App\Models\Dealer::where('id', '=', $app->dealerId)->first()->email];
        $secondaryDealerEmails = [];

        if($app->dealer) {
            $secondaryDealers = \App\Models\Dealer::where('company_id', '=', $app->dealer->id)->where('admin', '=', 1)->get();

            foreach ($secondaryDealers as $dealer) {
                array_push($secondaryDealerEmails, $dealer->email);
            }
        }

        $dealerEmails = array_merge($primaryDealerEmail, $secondaryDealerEmails);*/
        $staffEmail = 'loandept@belmontfinancellc.com';
        $protocol = (getenv('APP_ENV') == "production") ? 'https://' : 'http://';
        $url = $protocol . 'staff.' . getenv('DOMAIN') . '/apps/search?search=' . $app->fca_id;

        Mail::send('emails.staff.paperwork-submitted', ['app' => $app, 'url' => $url], function($m) use($staffEmail, $app){
            $m->to($staffEmail)->subject('Additional documents have been signed for ' . $app->firstName . ' ' . $app->lastName);
        });
    }


}