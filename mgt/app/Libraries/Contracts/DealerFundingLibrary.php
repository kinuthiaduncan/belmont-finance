<?php namespace App\Libraries\Contracts;

use App\Models\ContractApplication;
use FPDI;
use Carbon\Carbon;
use JWTAuth;
use Illuminate\Http\Request;
use App\Libraries\Contracts\ContractLibrary;
use App\Services\FileSignatureService;
use Mail;

class DealerFundingLibrary {
    private $app;
    private $user;
    private $fileId;

    public function __construct(ContractApplication $app, $user) {
        $this->app = $app;
        $this->user = $user;
    }

    // this will cover both dealer signing LCC and sales slip now...
    public function signContractOrCompletion() {
        if(in_array('9', $this->app->remainingPaperwork()['funding'])) {
            $lcc = new \App\Libraries\Contracts\CompletionLibrary($this->app, $this->user);
            return $lcc->dealerSignCompletionForm();
        } else {
            return $this->signSalesSlip();
        }
    }

    /**
     * View to sign sales slip for this contract
     *
     * @return  array Variables for the sign view
     */
    public function signSalesSlip() {
        // make sure correct access
        $this->authenticateSigning($this->user);

        $this->fileId = $this->retrieveSalesSlip($this->app->salesSlip->sale_state);

        // generate a pdf prior to signing with the data all filled in
        $unsignedPdf = \App\Models\File::whereNull('archived_at')
            ->where('app_id', '=', $this->app->id)
            ->where('file_type', '=', 2)
            ->first();

        $pdfImage = ContractLibrary::generatePdfImage($unsignedPdf->local_path);

        $fileMap = $this->app->credit_type == 'Revolving' ?
            'revolving-sales-slip-' . $this->fileId . '-dealer' :
            'contract-' . $this->fileId . '-dealer';

        // get signature locations
        $signatureLocations = $this->retrieveSignatureLocations($fileMap, $this->app->salesSlip->sale_state);

        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );
        $token = JWTAuth::fromUser($this->user);
        $successView = 'dealer.contract._dealer-submit-success';
        return [
            'app' => $this->app,
            'buyerKey' => 1,
            'includeSuccess' => $successView,
            'pdfImage' => $pdfImage,
            'sessionKey' => 'test',
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/apps/request-funding?token=' . $token
        ];
    }

    /**
     * Post the dealer signatures back from funding
     * 
     * @param  Request $request
     * @return null
     */
    public function submitDealerRequest($request)
    {

        // retrieve original file
        $file = \App\Models\File::where('app_id', '=', $this->app->id)
            ->where('file_type', '=', 2)
            ->first();

        // pass a 14" paper size through just for one file id
        if( $this->app->credit_type == 'Closed-End' ) {
            $size = 'Legal';
        } else {
            $size = null;
        }

        $signedFile = ContractLibrary::signPdf(basename($file->local_path, '.pdf'), $request->signatures, $this->app->lastName, 1, $this->app, 1, $size);
        FileSignatureService::recordFileSignature($signedFile, 'DE');

        $this->app->funding_status = 1;
        $this->app->save();

        $this->emailStaff();
    }

    public function addTerms($state, $user){
        // authenticate user by id
        $this->authenticateSigning($user);
        return $this->writeTerms($state, $user);
    }

    private function writeTerms($state, $user){
        // retrieve signed Sales Slip file reference
        $file = \App\Models\File::where('app_id', '=', $this->app->id)
            ->where('file_type', '=', 2)
            ->firstOrFail();

        // create new writer object
        $contractWriter = new \App\Libraries\Contracts\ContractWriter(base_path($file->local_path));
        // retrieve dealer company
        $dealer = \App\Models\Dealer::find($this->app->dealerId);
        $company = $dealer->company;

        // just go through each file individually
        if($this->fileId == "271552") { // WI, TX, NV
           
        } else if ($this->fileId == "271549") { // OH
            
        }
        else if ($this->fileId == "271551") { // AR
           
        } else if ($this->fileId == "271556") { // ND
            
        } else if ($this->fileId == "271553") { // ND
            
        }

        // generate remaining pages
        for ($i = 2; $i <= $contractWriter->numPages; $i++) {
            $contractWriter->moveToPage($i);
        }

        // save pdf
        $path = '../documents/contracts/temp/' . $this->app->firstName . '-' . $this->app->lastName . '-salesslip-' . time() . '.pdf';
        $contractWriter->finalizePdf($path, $this->app->id, 2, 'Dealer Unsigned' . $this->app->credit_type == 'Revolving' ? 'Sales Slip' : 'Contract');
        return $path;
    }

    private function retrieveSalesSlip($state)
    {   
        $distributor = $this->user->company;

        // revolving sales slips
        if($this->app->credit_type == 'Revolving') {
            if($this->isInHome($distributor)) {
                // all products in these two states are on same sales slip
                if(in_array($state, ['MA', 'RI'])) {
                    $filename = '271482';
                } else if (in_array($state, ['MD', 'MI', 'NJ', 'PA']) && $this->isHomeImprovementProduct($distributor))  {
                    $filename = '271484';
                } else {
                    $filename = '271483';
                }

                if($this->isWaterProduct($distributor)) {
                    $filename .= '-water';
                }
            // in-store revolving sales slips
            } else {
                // standard revolving sales slips
                if (in_array($state, ['MI', 'NJ', 'PA']))  {
                    $filename = '271485';
                } else {
                    $filename = '271486';
                }
            }
            // installment / retail sales slips
        } else {
            if($this->isInHome($distributor)) {
                if(in_array($state, ['DE', 'DC', 'MA', 'MD', 'RI'])) {
                    $filename = '271183';
                } else if (in_array($state, ['MD','MI', 'NJ', 'PA'])) {
                    $filename = '271185';
                } else if (in_array($state, ['CT', 'FL', 'KY', 'ME', 'MO', 'NJ', 'NY', 'VT'])) {
                    $filename = '271186';
                } else {
                    $filename = '271184_2';
                }
            } else {
                if(in_array($state, ['CT', 'FL', 'KY', 'ME', 'MO', 'NJ', 'NY', 'VT'])) {
                    $filename = '271327';
                } else if (in_array($state, ['DE', 'DC', 'MA', 'MD', 'RI'])) {
                    $filename = '271328';
                } else if (in_array($state, ['MI', 'NJ', 'PA'])) {
                    $filename = '271330';
                } else {
                    $filename = '271329_2';
                }
            }
        }

        return $filename;
    }

    private function isWaterProduct($distributor){
        return $distributor->product_id == 26 ? true : false;
    }

    private function isInHome($distributor){
        return $distributor->product_id != 29 ? true : false;
    }

    private function isHomeImprovementProduct($distributor){
        return $distributor->product_id == 28 ? true : false;
    }

  
    private function authenticateSigning($user)
    {
        if( $user->id === $this->app->dealerId){
           // is authenticated
        }else{
            // check for admin user
            $appUser = \App\Models\Dealer::where('id', '=', $this->app->dealerId)->first();
            if($user->company->id === $appUser->company->id && $user->admin) {
                // authenticated
            } else {
                abort(403, 'Authorization error');
            }
        }
    }

    private function retrieveSignatureLocations($pdfFileId)
    {   
        $signatureLoc = json_decode(file_get_contents('../resources/assets/signature/signatureLocations.json'));

        $fileMap = $pdfFileId;

        // some files don't have any general signatures, only state specific
        if(isset($signatureLoc->{$fileMap})) {
            $fieldPositions = $signatureLoc->{$fileMap};
        } else {
            $fieldPositions = [];
        }

        return $fieldPositions;
    }

    protected function emailStaff(){
        // send notice to staff
        $staffEmail = ['funding@belmontfinancellc.com'];

        foreach($staffEmail as $email) {
            Mail::send('emails.staff.funding-requested', ['app' => $this->app], function($m) use($email){
                $m->to($email)->from('noreply@belmontfinancellc.com')->subject('Funding Requested');
            });
        }
    }
}
