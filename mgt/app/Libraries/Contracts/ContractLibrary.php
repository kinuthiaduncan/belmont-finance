<?php
namespace App\Libraries\Contracts;

use App\Http\Requests\ApplicationPostRequest;
use App\Http\Requests\SalesSlipPostRequest;
use App\Models\Contract;
use App\Models\ContractApplication;
use App\Models\Dealer;
use App\Models\File;
use Carbon\Carbon;
use FPDI;
use \Imagick;
use Request;
use Session;
use Mail;
use Excel;
use JWTAuth;
use Log;
use Auth;
use Artisan;

class ContractLibrary {

    /**
    * Add consumer's signatures to a document
    */
    public static function signPdf($filename, $signatures, $lastName, $docType, $app, $completed = 1, $size=null)
    {
		$pdfFileName = $filename;
        $pdfPageSize = $size ? $size : 'Letter';
		$pdf = new FPDI();

		// retrieve original pdf
        if($docType == 0) {
            $fileType = 1;
            $fileDescription = 'Credit Agreement';
        } else if ($docType == 1) {
            $fileType = 2;
            $fileDescription = 'Sales Slip';
        } else if ($docType == 2) {
            $fileType = 5;
            $fileDescription = 'Notice of Cancellation';
        } else if ($docType == 4) {
            $fileType = 9;
            $fileDescription = 'LCC';
        } else if ($docType == 6) {
            $fileType = 13;
            $fileDescription = 'ACH';
        }

        // get the most recent file in case there might be two for some reason?
        $file = File::where('app_id', '=', $app->id)
            ->where('file_type', '=', $fileType)
            ->whereNull('archived_at')
            ->orderBy('created_at', 'desc')
            ->first();

		$totalPages = $pdf->setSourceFile(base_path($file->local_path));
		$pdf->AddPage('P', $pdfPageSize);
        $pageNum = 1;
        $pdfPage = [];

		$pdfPage[] = $pdf->importPage($pageNum);
		$pdf->useTemplate($pdfPage[$pageNum - 1]);

		// add signatures to pdf file
		$signatures = Request::input('signatures');
		foreach ($signatures as $i => $signature) {
			$sigImage = new \Imagick();

            if($signature['sig'] && strlen($signature['sig']) > 0) {
    			$sigImage->readImageBlob(base64_decode(substr($signature['sig'], 22)));
    			$sigImage->resizeImage(400, 80, \Imagick::FILTER_UNDEFINED, 1);
    			if (!file_exists('../../documents/contracts/temp')) {
    				mkdir('../../documents/contracts/temp', 0755, true);
    			}

                $tempImgFilePath = realpath('../../documents/contracts/temp/')  . '/' . Request::input('sessionKey') . $lastName . "-temp$i.png";

    			$sigImage->writeImage($tempImgFilePath);

                $width = $signature['init'] == 1 ? 8 : 40;
                $height = $signature['init'] == 1 ? 6 : 7;

                $prevPageNum = $pageNum;
                $pageHeight = 1;

                if($signature['y'] <= $pageHeight && $pageNum == 1) {
                    $pageNum = 1;
                } if($signature['y'] > $pageHeight && $pageNum == 1) {
                    if(count($pdfPage) < 2) {
                        $signature['y'] -= $pageHeight;
                        $pdf->AddPage('P', $pdfPageSize);
                        $pdfPage[] = $pdf->importPage(2);
                    }
                    $pageNum = 2;
                } else if ($signature['y'] > ($pageHeight * 2)) {
                     if(count($pdfPage) < 3) {
                        $signature['y'] -= ($pageHeight * 2);
                        $pdf->AddPage('P', $pdfPageSize);
                        $pdfPage[] = $pdf->importPage(3);
                    }
                    $pageNum = 3;
                }

                if($prevPageNum != $pageNum) {
                    $pdf->useTemplate($pageNum);
                }



                // sales slip height is 2380px
                // position is percentage decimal sent * factor
                // x position is different, legacy, haven't adjusted yet
                // not sure why factor is different for sales slip, but it is, maybe sales slips etc are legal size
                if($docType == 0 && $app->credit_type == 'Revolving') {
                    $yPosition = ($signature['y'] * 295.5) - ($signature['y'] * 7);
                } else if ($docType == 0 && $app->credit_type == 'Closed-End') {
                    $yPosition = ($signature['y'] * 285) - ($signature['y'] * 6.6);

                } else if ($docType == 4 || $docType == 5) {
                    $yPosition = ($signature['y'] * 358) - ($signature['y'] * 6.6);
                    if($docType == 5) {
                        $yPosition = ($signature['y'] * 283) - ($signature['y'] * 6.6);
                    }
                } else if ($docType == 3){
                    $yPosition = ($signature['y'] * 366) - ($signature['y'] * 6.6);
                } else if ($docType == 6) {
                    $yPosition = ($signature['y'] * 358) - ($signature['y'] * 6.6);
                } else {
                    $yPosition = ($signature['y'] * 366) - ($signature['y'] * 6.6);
                }

                $xPosition = $signature['x'] / 7.5;

    			//$pdf->Image($tempImgFilePath, ($signature['x']/7.5), ($signature['y']/7.7), $width, $height);
                $pdf->Image($tempImgFilePath, $xPosition, $yPosition, $width, $height);
    			unlink($tempImgFilePath);
            }
		}

		// generate remaining pages
		for ($i = count($pdfPage) + 1; $i <= $totalPages; $i++) {
			$pdf->AddPage('P', $pdfPageSize);
			$page = $pdf->importPage($i);
			$pdf->useTemplate($page);
		}

        // generated pdfs are stored in a different folder, but let's move all signed documents to a new folder
        $datePath = Carbon::now()->format('Y/m');
        $newPath = "../documents/contracts/$datePath/";
        $newFilePath = "../documents/contracts/$datePath/" . $pdfFileName . ".pdf";

        unlink(base_path($file->local_path));

        if (!file_exists(base_path($newPath))) {
            mkdir(base_path($newPath), 0755, true);
        }

		$pdf->output(base_path($newFilePath), 'F');

        // record file
        $file->update([
            'local_path' => $newFilePath,
            'name' => $fileDescription
        ]);

        // call console command to flatten document;
        // only on production environment as path errors on local
        // only flattening finished docs right now
        if(getenv('APP_ENV') == 'production' && $completed) {
            \Artisan::call('docs:flatten', [
                'path' => $newFilePath
            ]);
        }

        return $file;
	}

    /**
     * @param  $string Path to the file
     * @return array Image array
     */
    public static function generatePdfImage($link) {
		$currentYear = Carbon::now()->year;
		$unfilteredPdf = new \Imagick();
		$unfilteredPdf->setResolution(170, 170);


        // make sure file exists, 404 if not
        /* if(!base_path($link)) {
            abort(500, 'File no longer exists');
        } */

        $unfilteredPdf->readImage(base_path($link));
		$unfilteredPdf->setImageBackgroundColor('white');
		$imageArray = [];
		foreach ($unfilteredPdf as $i => $page) {
			$pdfImage = new \Imagick();
			$pdfImage->newPseudoImage($unfilteredPdf->getImageWidth(), $unfilteredPdf->getImageHeight(), "canvas:white");
			$pdfImage->compositeImage($unfilteredPdf, \Imagick::COMPOSITE_ATOP, 0, 0);
			$pdfImage->writeImage(base_path($link) . '.png');
			$pdfBlob = new \Imagick();
			$pdfBlob->readImage(base_path("$link.png"));
			unlink(base_path("$link.png"));
			array_push($imageArray, base64_encode($pdfBlob));
		}
		return $imageArray;
	}


    public static function getSignatureLocations($documentType, $coApp = null) {

        // Revolving Contract Application
        if ($documentType == 'revolving application') {
            $coordinates = json_decode(file_get_contents('../resources/assets/signature/signatureLocations.json'));
            $signatureLocations = $coordinates->application;
            $jointSignatureLocations = $coordinates->jointApplication;
            if ($coApp) {
    			$signatureLocations = array_merge($signatureLocations, $jointSignatureLocations);
    		}
        // Closed Contract Application
        } else if ($documentType == 'closed application') {
            $coordinates = json_decode(file_get_contents('../resources/assets/signature/signatureLocations.json'));
            $signatureLocations = $coordinates->closedApplication;
            $jointSignatureLocations = $coordinates->jointClosedApplication;
            if ($coApp) {
    			$signatureLocations = array_merge($signatureLocations, $jointSignatureLocations);
    		}
        } else {
            // use for retail (not happy how this is organized)
            $coordinates = json_decode(file_get_contents('../resources/assets/signature/retail-signatures.json'));
            $signatureLocations = $coordinates->retail;
        }



        // Sales Agreement
        if (substr($documentType,0,14) == 'salesAgreement') {
            // Agreement Type
            $state = substr($documentType,-2);
            $subType = explode(',' , $documentType)[1];
            if ($subType == 'revolving') {
                // Default Agreement - Detect state and override if necessary
                $signatureLocations = $coordinates->revolvingOther;
                if ($state == 'AR') {
                    $signatureLocations = $coordinates->revolvingAR;
                }
                elseif ($state == 'AK' || $state == 'ND') {
                    $signatureLocations = $coordinates->revolvingAK;
                }
                elseif ($state == 'NV' || $state == 'TX' || $state == 'WI') {
                    $signatureLocations = $coordinates->revolvingWI;
                }
                elseif ($state == 'MA') {
                    $signatureLocations = $coordinates->revolvingMA;
                }
                elseif ($state == 'RI') {
                    $signatureLocations = $coordinates->revolvingRI;
                }
                elseif ($state == 'DE' || $state == 'VT') {
                    $signatureLocations = $coordinates->revolvingDE;
                }
                elseif ($state == 'CT' || $state == 'NY' ) {
                    $signatureLocations = $coordinates->revolvingCT;
                    $signatureLocations = ($state == 'NY' ? array_merge($signatureLocations, $coordinates->revolvingNY) : $signatureLocations);
                }
                elseif ($state == 'HI') {
                    $signatureLocations = $coordinates->revolvingHI;
                }
                elseif ($state == 'WA') {
                    $signatureLocations = $coordinates->revolvingWA;
                }
            } else if ($subType == 'closed') {
                if ($state == 'WI') {
                    $signatureLocations = $coordinates->closedWI;
                    $signatureLocations = $coApp ? array_merge($signatureLocations, $coordinates->coClosedWI) : $signatureLocations;
                } else if ($state == 'IL') {
                    $signatureLocations = $coordinates->closedIL;
                    $signatureLocations = $coApp ?  array_merge($signatureLocations, $coordinates->coClosedIL) : $signatureLocations;
                } else if ($state == 'OR') {
                    $signatureLocations = $coordinates->closedOR;
                    $signatureLocations = $coApp ? array_merge($signatureLocations, $coordinates->coClosedOR) : $signatureLocations;
                } else if ($state == 'CA') {
                    $signatureLocations = $coordinates->closedOR;
                    $signatureLocations = $coApp ? array_merge($signatureLocations, $coordinates->coClosedOR) : $signatureLocations;
                }
            } else if ($subType == 'retail') {

            }
        }

        // Sort signatureLocations from top left to bottom right
        usort($signatureLocations, function($a, $b) {
            if ($a[1] == $b[1]) {
                return ($a[0] < $b[0]) ? -1 : 1;
             }
            return ($a[1] < $b[1]) ? -1 : 1;
        });

        return $signatureLocations;
    }

    public function postSubmitSalesSlip(SalesSlipPostRequest $request) {
        $step = $request->input('step');
        $merged = $request->all() + $request->session()->get('sales-slip-data', []);
        $request->session()->put('sales-slip-data', $merged);
        return 'true';
    }

    // application posted
    // includes signatures also, second parameter is if another remote signer needs to sign it or not
    public function postSubmitApplication(ApplicationPostRequest $request, $incomplete = false) {
        $fields = $request->all();

        if( !Session::has('staff.applicationCompanyId') && !Session::has('appId') )
        {
            $user = JWTAuth::parseToken()->authenticate();
            $fields['dealerId'] = $user->id;
            $dealerAccountNum = $user->company->account_number;
            $company = $user->company;
        }
        else if ( Session::has('staff.applicationCompanyId') )
        {
            $fields['dealerId'] = '';
            $company = \App\Models\DealerCompany::findorFail(Session::get('staff.applicationCompanyId'));
            $dealerAccountNum = $company->account_number;
            Session::forget('staff.applicationCompanyId');
        }

        // Merge current requests with previous data
        $step = $request->input('step');

        // use either the legacy sessionkey or the remote token to key store data in session
        $sessionKey = $request->has('sessionKey') ? $request->input('sessionKey') : $request->input('token');
        $merged = $request->session()->get('appFields.' . $sessionKey, []);
        $merged = $fields + $merged;
        $request->session()->put('appFields.' . $sessionKey, $merged);

        // this would be after the signature submission we set step to final
        // prepare for csv compilation
        if ($step == "final") {
            $app = ContractApplication::where('id', '=', $request->session()->get('appId') )->first();

            // send to FCA (disabled for now)
            // $this->sendToFCA($app, $merged);

            // generate signed pdf
            $filename = $merged['lastName'] . '-' . $merged['firstName'] .'-' . time();
            $attachment = ContractLibrary::signPdf($filename, $merged['signatures'], $app->lastName, false, $app);
            Session::forget('appFields.' . $sessionKey);

            // manually mark as pending right away
            if(!$incomplete) {
                $app->status = 0;
                $app->save();

                $this->emailOnApplication($app);
            }

        }

        return isset($applicationId) ? response()->json($applicationId) : response()->json(true);
    }

    // after the signing of the sales slip
    public function postSubmitSalesAgreement($request) {
        $fields = $request->session()->get('sales-slip-data');

        // get application data
        $app = ContractApplication::find($fields['app_id']);

        $app->email = $fields['email'];
        $app->coEmail = isset($fields['coEmail']) ? $fields['coEmail'] : null;
        $app->save();

        // calculate final amount financed
        $salesTax = isset($fields['sales_tax']) ? $fields['sales_tax'] : 0;
        $downPayment = isset($fields['down_payment']) ? $fields['down_payment'] : 0;

        $total = (isset($fields['price_one']) ? $fields['price_one'] : 0) * (isset($fields['qty_one']) ? $fields['qty_one'] : 0);
        $total += (isset($fields['price_two']) ? $fields['price_two'] : 0) * (isset($fields['qty_two']) ? $fields['qty_two'] : 0);
        $total += (isset($fields['price_three']) ? $fields['price_three'] : 0) * (isset($fields['qty_three']) ? $fields['qty_three'] : 0);
        $total -= (isset($fields['trade_in']) ? $fields['trade_in'] : 0);
        $total = $total + (isset($fields['shipping']) ? $fields['shipping'] : 0) + $salesTax - $downPayment;

         $deferredDays = 30;

        if(isset($fields['promos'])) {
            foreach($fields['promos'] as $promoId) {
                $promo = \App\Models\Promo::where('id', '=', $promoId)->first();
                if($promo) {
                    if($promo->days_deferred > $deferredDays) {
                        $deferredDays = $promo->days_deferred;
                    }
                }
            }
        }
        $fields['deferredDays'] = $deferredDays;

        // remove existing sales slip records if they exist
        $existingSalesSlips = \App\Models\SalesSlip::where('app_id', '=', $fields['app_id']);
        foreach($existingSalesSlips as $ss) {
            \App\Models\SalesSlipItems::where('sales_slip_id', '=', $ss->id)->delete();
            $ss->delete();
        }


        // save new  slip data
        $salesSlipRecord = \App\Models\SalesSlip::updateOrCreate(['app_id' => $fields['app_id']],
        [
            'app_id' => $fields['app_id'],
            'promo_id' => isset($fields['promos'][0]) ? $fields['promos'][0] : null,
            'promo_id_2' => isset($fields['promos'][1]) ? $fields['promos'][1] : null,
            'promo_id_3' => isset($fields['promos'][2]) ? $fields['promos'][2] : null,
            'promo_id_4' => isset($fields['promos'][3]) ? $fields['promos'][3] : null,
            'sale_state' => isset($fields['state']) ? $fields['state'] : null,
            'is_addon' => isset($fields['is_addon']) ? $fields['is_addon'] : null,
            'retail_value' => isset($fields['retail_price']) ? $fields['retail_price'] : null,
            'shipping_handling' => isset($fields['shipping']) ? $fields['shipping'] : null,
            'sales_tax' => isset($fields['sales_tax']) ? $fields['sales_tax'] : null,
            'down_payment' => isset($fields['down_payment']) ? $fields['down_payment'] : null,
            'down_payment_method' => isset($fields['down_payment_method']) ? $fields['down_payment_method'] : null,
            'payment_factor' => ContractLibrary::getMinimumPaymentFactor($app, $fields) * 100,
            'amount_financed' => $total,
            'ach_account_number' => isset($fields['ach_account_number']) ? $fields['ach_account_number'] : null,
            'ach_routing_number' => isset($fields['ach_routing_number']) ? $fields['ach_routing_number'] : null,
            'ach_is_savings' => isset($fields['ach_is_savings']) ? $fields['ach_is_savings'] : null,
            'ach_day_of_month' => isset($fields['ach_day_of_month']) ? $fields['ach_day_of_month'] : null,
            'ip_address' => $request->ip()
        ]);

        \App\Models\SalesSlipItems::create([
            'sales_slip_id' => $salesSlipRecord->id,
            'price' => isset($fields['price_one']) ? $fields['price_one'] : null,
            'qty' => isset($fields['qty_one']) ? $fields['qty_one'] : null,
            'sku' => isset($fields['sku_one']) ? $fields['sku_one'] : null,
            'attachments' => isset($fields['attachments']) ? $fields['attachments'] : null,
            'description' => isset($fields['description_one']) ? $fields['description_one'] : null,
        ]);

        if(isset($fields['qty_two']) && $fields['qty_two']) {
            \App\Models\SalesSlipItems::create([
                'sales_slip_id' => $salesSlipRecord->id,
                'price' => isset($fields['price_two']) ? $fields['price_two'] : null,
                'qty' => isset($fields['qty_two']) ? $fields['qty_two'] : null,
                'sku' => isset($fields['sku_two']) ? $fields['sku_two'] : null,
                'description' => isset($fields['description_two']) ? $fields['description_two'] : null,
            ]);
        }

        if(isset($fields['qty_three']) && $fields['qty_three']) {
            \App\Models\SalesSlipItems::create([
                'sales_slip_id' => $salesSlipRecord->id,
                'price' => isset($fields['price_three']) ? $fields['price_three'] : null,
                'qty' => isset($fields['qty_three']) ? $fields['qty_three'] : null,
                'sku' => isset($fields['sku_three']) ? $fields['sku_three'] : null,
                'description' => isset($fields['description_three']) ? $fields['description_three'] : null,
            ]);
        }
        
        return response()->json('success');
    }

    // after the signing of notice of cancellation
    public function submitCancellation($isFinalSignature = 1) {
        // get application data
        $app = ContractApplication::find(Request::input('id'));
        
        $filename = $app->firstName .'-'. $app->lastName . '-' . $app->id . '-c';
        $signedFile = ContractLibrary::signPdf($filename, Request::input('signatures'), $app->lastName, 2, $app, $isFinalSignature);

        // mark sales slip that cancellation has been signed
        if ($isFinalSignature) {
            $app->salesSlip->is_notice_cancellation_completed = 1;
            $app->salesSlip->save();

            $this->emailStaff($app);

            $this->emailConsumer($app);
        }

        return response()->json('success');
    }


    public function emailOnApplication($app)
    {
        // email staff first
        $staffEmail = ['loandept@belmontfinancellc.com'];

        foreach($staffEmail as $email) {

            Mail::send('emails.staff.app-submitted', ['app' => $app, 'dealer' => $app->dealerUser, 'distributor' => $app->dealerUser->company], function($m) use($email){
                $m->to($email)->from('noreply@belmontfinancellc.com')->subject('App submitted');
            });
        }

        // email dealer second
        $primaryDealerEmail = [Dealer::where('id', '=', $app->dealerId)->first()->email];
        $secondaryDealerEmails = [];

        if($app->dealer) {
            $secondaryDealers = Dealer::where('company_id', '=', $app->dealer->id)->where('admin', '=', 1)->get();
            foreach ($secondaryDealers as $dealer) {
                array_push($secondaryDealerEmails, $dealer->email);
            }
        }


        $dealerEmails = array_merge($primaryDealerEmail, $secondaryDealerEmails);

        Mail::send('emails.dealer.app-submitted', ['app' => $app], function($m) use($dealerEmails, $app){
            $m->to($dealerEmails)->subject('Application submitted for ' . $app->firstName . ' ' . $app->secondName);
        });

        // email consumer third
        if($app->email) {
            Mail::send('emails.consumer.app-submitted', ['app' => $app], function($m) use($app){
                $m->to($app->email)->subject('Application submitted to Belmont Finance LLC');
            });
        }
    }

    // send a request for consumer to create their account
    protected function emailConsumer($app)
    {
        // create unique token
        $customerToken = \App\Models\Auth\CustomerToken::create([
            'token' => bin2hex(openssl_random_pseudo_bytes(16)),
            'app_id' => $app->id
        ]);

        $url = (getenv('APP_ENV') === "production" ? 'https://' : 'http://') . 'accounts.' . getenv('DOMAIN') .'/new-account?token=' . $customerToken->token;

        Mail::send('emails.consumer.new-account-request', ['app' => $app, 'url' => $url], function($m) use($app){
            $m->to($app->email, $app->firstName . ' ' . $app->lastName)
                ->subject('Your Completed Documents and Online Website Access');
        });
    }


    // email staff that contract has been signed
    protected function emailStaff($app)
    {
        // send emails to staff
        $staffEmail = \App\Models\Setting::where('name', '=', 'applicationEmails')->first()->value;
        $staffEmail = explode ( ",", $staffEmail );

        Mail::send('emails.staff.contract-signed', ['firstName' => $app->firstName, 'lastName' => $app->lastName, 'id' => $app->fca_id], function($m) use($staffEmail){
            $m->to($staffEmail)->from('noreply@belmontfinancellc.com');
        });
    }


    // We used to generate sales slip on response from watcher handler
    // but now with auto signatures we have to generate it up front through
    public function generateSalesSlip($request, $fields, $id)
    {   
        // first ensure it is an auto decisioned app
        if($request->session()->has('dealer.orderUrl')) {
            Artisan::call('apps:retail', ['appId' => $id]);
        }
    }


    /**
    * If it's a store sent app, post back completion to the store
    */
    protected function updateStore($request, $appId)
    {
        if( $request->session()->has('dealer.orderUrl') && getenv('APP_ENV') == "production" ) {
            $orderUrl = $request->session()->get('dealer.orderUrl');

            $client = new \GuzzleHttp\Client();
            $response = $client->put($request->session()->get('dealer.orderUrl'), [
                'headers' => [
                    'Content-Type' => 'application/json;charset=utf-8'
                ],
                'json' => ['paymentStatus' => 'AWAITING_PAYMENT'],
                'allow_redirects' => false
            ]);

            \App\Models\AppExtras\AppOrders::create(['app_id' => $appId, 'store_type' => 'ECWID', 'order_status' => 'AWAITING_PAYMENT', 'store_order_api_url' => $request->session()->get('dealer.orderUrl')]);
            
            // destroy store session
            Auth::logout();
            $request->session()->flush();
        }
    }

    // return minimum payment factor given the application and posted data from sales slip form
    public static function getMinimumPaymentFactor($app, $fields)
    {
        // get min payment factor either from promo, product
        if($app->credit_type == 'Revolving') {

            // check for custom dealer factor
            $minPmtFactorRecord = \App\Models\Dealers\DealerPmtFactor::where('dist_id', '=', $app->dist_id)
                ->where('min_buy_rate', '<=', $app->buy_rate)
                ->orderBy('pmt_factor', 'ASC')
                ->first();

            // if no payment factor found, revert to product setting
            if(!$minPmtFactorRecord) {
                $minPmtFactor = $app->distributor->product->getPmtFactor($app->buy_rate, $fields['ach_account_number'] ? 1 : 0);
            } else {
                $minPmtFactor = $minPmtFactorRecord->pmt_factor;
            }

            $promos = isset($fields['promos']) ? $fields['promos'] : [];

            $promoPmtFactor = \App\Models\Promo::whereIn('id', array_filter($promos))
                ->whereNotNull('rate_factor')
                ->min('rate_factor');

            return ($promoPmtFactor ? $promoPmtFactor : $minPmtFactor) / 100;
        } else {
            return \App\Libraries\Contracts\ContractLibrary::calcPmtFactor(($fields['apr'] / 100), $fields['term'], $fields['deferredDays']);
        }

    }

    // return object with locations of data
    static function retrieveFieldLocations($pdfFileId)
    {
        // load the correct file and data set plotting the signature locations
        if ($pdfFileId == '281734'  || $pdfFileId == '281734-spanish') {
            $fieldPositions = json_decode(file_get_contents('../resources/assets/signature/fieldLocations.json'))->revolving;
        } else if ($pdfFileId == '281736' ) {
            $fieldPositions = json_decode(file_get_contents('../resources/assets/signature/fieldLocations.json'))->revolving;
        } else if ($pdfFileId == '281735' || $pdfFileId == '281735-spanish') { // water products, 17% revolving
            $fieldPositions = json_decode(file_get_contents('../resources/assets/signature/fieldLocations.json'))->revolving;
        } else if ($pdfFileId == '282499') { // saladmaster
            $fieldPositions = json_decode(file_get_contents('../resources/assets/signature/fieldLocations.json'))->revolving;
        } else if ($pdfFileId == '280614' || $pdfFileId == '280614-spanish'){
            $fieldPositions = json_decode(file_get_contents('../resources/assets/signature/fieldLocations.json'))->closed;
        } else if ($pdfFileId == '271483' || $pdfFileId == '271483-SPANISH'){
            $fieldPositions = json_decode(file_get_contents('../resources/assets/signature/fieldLocations.json'))->salesSlip;
        }

        return $fieldPositions;
    }

     /**
    * Returns correct filename to use for the credit agreement
    **/
    public static function retrieveCreditAgreement($user, $creditType, $language='English')
    {
        $distributor = $user->company;

        if(strtolower($creditType) == 'revolving') {
            if( in_array($distributor->product_id, [26]) ) {
                // return contract 281735 for water product companies
                $filename = '281735';
            } else if( in_array($distributor->product_id, [16]) ) {
                // return contract 282499 for Saladmaster companies
                $filename = '282499';
            } else if( in_array($distributor->product_id, [28]) ) {
                // return contract 281736 for home improvement companies
                $filename = '281736';
            } else {
                // standard revolving credit application for all others
                $filename = '281734';
            }
        } else {
            $filename = '280614';
        }

        // tack on spanish version
        return $filename . ($language == 'Spanish' ? '-spanish' : '');
    }

     /**
    * Returns signature location for all file types
    // coApp is actually a key 1 loads all signatures, 2 loads only coapplicant signatures, 3 loads only primary signatures
    **/
    public static function retrieveSignatureLocations($pdfFileId, $state, $coApp = false, $language = 'english', $spouse = false)
    {   
        $signatureLoc = json_decode(file_get_contents('../resources/assets/signature/signatureLocations.json'));
        // load the correct file and data set plotting the signature locations
        if (in_array($pdfFileId, ['281734', '281734-spanish', '281736', '281735', '281735-spanish', '282499'])) {
            $fileMap = 'application';
        } else if ($pdfFileId == '280614' || $pdfFileId == '280614-spanish'){
            $fileMap = '280614';
        } else if (in_array($pdfFileId, ['271483', '271483-water'])){ // water 
            $fileMap = 'revolving-sales-slip';
        } else if ($pdfFileId == '271482' || $pdfFileId == '271482-water' || $pdfFileId == '271482-SPANISH') {
            $fileMap = 'revolving-sales-slip-271482';
        } else if ($pdfFileId == '271484' || $pdfFileId == '271484-water' || $pdfFileId == '271484-SPANISH') {
            $fileMap = 'revolving-sales-slip-271484';
        } else if ($pdfFileId == '271184_2' || $pdfFileId == '271184_2-SPANISH') {
            $fileMap = 'contract-271184_2';
        } else if ($pdfFileId == '271186') {
            $fileMap = 'contract-271186';
        } else if ($pdfFileId == '271186-SPANISH') {
            $fileMap = 'contract-271186-spanish';
        } else if ($pdfFileId == '271185') {
            $fileMap = 'contract-271185';
        } else if ($pdfFileId == '271183' || $pdfFileId == '271183-SPANISH' ) {
            $fileMap = 'contract-271183';
        } else if ($pdfFileId == '271552') {
            $fileMap = 'cancellation-271552';
        } else if ($pdfFileId == '271549') {
            $fileMap = 'cancellation-271549';
        } else if ($pdfFileId == '271550' || $pdfFileId == '271550-SPANISH') {
            $fileMap = 'cancellation-271550';
        } else if ($pdfFileId == '271551') {
            $fileMap = 'cancellation-271551';
        } else if ($pdfFileId == '271556') {
            $fileMap = 'cancellation-271556';
        } else if ($pdfFileId == '271553') {
            $fileMap = 'cancellation-271553';
        } else if ($pdfFileId == '271555') {
            $fileMap = 'cancellation-271555';
        } else if ($pdfFileId == '254859') {
            $fileMap = 'completion';
        } else if ($pdfFileId == 'ach') {
            $fileMap = 'ach';
        }

        // some files don't have any general signatures, only state specific
        try{
            if(isset($signatureLoc->{$fileMap})) {
                $fieldPositions = $signatureLoc->{$fileMap};
            } else {
                $fieldPositions = [];
            }

            // check for state specific
            if(isset($signatureLoc->{$fileMap . '-' . $state})) {
                $fieldPositions = array_merge($fieldPositions, $signatureLoc->{$fileMap . '-' . $state});
            }

            // add special coapplicant fields 
            if(($coApp) && isset($signatureLoc->{$fileMap . '-co'})) {
                $fieldPositions = array_merge($fieldPositions, $signatureLoc->{$fileMap . '-co'});
            }

            // only get coapplicant signatures
            if($coApp === 2) {
                $tempFieldPositions = [];
                foreach($fieldPositions as $sigLocation) {
                    if(isset($sigLocation[3]) && strtolower(substr($sigLocation[3], 0, 2)) == 'co') {
                        $tempFieldPositions[] = $sigLocation;
                    }
                }
                $fieldPositions = $tempFieldPositions;

                // only get primary applicant signatures
            } else if ($coApp === 3) {
                $tempFieldPositions = [];
                foreach($fieldPositions as $sigLocation) {
                    if(isset($sigLocation[3]) && strtolower(substr($sigLocation[3], 0, 2)) != 'co') {
                        $tempFieldPositions[] = $sigLocation;
                    }
                }
                $fieldPositions = $tempFieldPositions;
            }


            // final check, only load some files that are marked for spouse only
            if(!$spouse) {
                foreach($fieldPositions as $key => $signature)
                {
                    if( isset($fieldPositions[$key][5]) && $fieldPositions[$key][5] == 2) {
                        unset($fieldPositions[$key]);
                    }
                }
            }

        } catch(\Exception $e) {
            abort(501, 'No signature mapping found for given file, or error loading signature positions.');
        }


        usort($fieldPositions, function($a, $b){
            return strcmp($a[3][0], $b[3][0]);
        });

        return $fieldPositions;
    }

    /**
    * Calculate the payment factor for a loan
    * @param float $apr the decimal interest rate
    * @param integer $periods The number of (typically monthly) periods
    * @param integer $deferredDays The number of periods deferral
    * @return float Payment factor
    */
    public static function calcPmtFactor($apr, $periods, $deferredDays){
        $rate = $apr / 12;
        $nper = $periods;
        $pv = (1+$apr*($deferredDays-30)/365)/1;
        $fv = 0;
        $type = 0;

        return ContractLibrary::roundUp(abs((-$fv - $pv * pow(1 + $rate, $nper)) /
        (1 + $rate * $type) /
        ((pow(1 + $rate, $nper) - 1) / $rate)), 5);

    }

    public static function roundUp($number, $precision = 2){
        $fig = pow(10, $precision);
        return (ceil($number * $fig) / $fig);
    }

    // prepare data and send to FCA
    public function sendToFCA($app, $fields) {
            $data = Session::get($sessionKey);

            $dealer = \App\Models\Dealer::findOrFail($merged['dealerId']);
            $dealerAccountNum = $dealer->company->account_number;

            $fields['status'] = 0;

            $licIssueDate = '0000-00-00';
            $licExpDate = isset($fields['licExpDate']) ? $fields['licExpDate'] : '' ;

            $fields['licExpDate'] = date('Y:m:d', strtotime($fields['licExpDate']));

            // put dealer number in
            $fields['distributor_account'] = $dealerAccountNum;

            //format license issue and expiration dates
            $licIssueDate = "01/01/1900";

            $licExpDate = date("m/d/Y", strtotime($licExpDate));
            if( $licExpDate == "01/01/1970"){
                $licExpDate = "";
            }

            // save data
            $sessionKey = $fields['sessionKey'];
            $creditType = $fields["credit_type"];


            // generate signed pdf
            $filename = $merged['lastName'] . '-' . $merged['firstName'] .'-' . time();
            $attachment = ContractLibrary::signPdf($filename, $merged['signatures'], $app->lastName, false, $app);
            Session::forget('appFields.' . $sessionKey);

            $applicationId = $app->id;

            $dealerEmail = isset($user) ? $user->email : '';

            $email = isset($fields['email']) ? $fields['email'] : null;
            $firstName = isset($fields['firstName']) ? $fields['firstName'] : null;
            $lastName = isset($fields['lastName']) ? $fields['lastName'] : null;

            // send notice to staff
            $staffEmail = ['loandept@belmontfinancellc.com'];

            foreach($staffEmail as $email) {
                $contractNumber = ($app->contract ? $app->contract->id : null);

                Mail::send('emails.staff-document-signed', ['firstName' => $firstName, 'lastName' => $lastName, 'contractNumber' => $contractNumber], function($m) use($email){
                    $m->to($email)->from('noreply@belmontfinancellc.com')->subject('App submitted');
                });
            }

            // create a csv file
            Excel::create($applicationId, function($excel) use($fields, $applicationId, $licIssueDate, $licExpDate, $dealerAccountNum, $dealerEmail, $creditType) {
                $excel->sheet('Sheet 1', function($sheet) use($fields, $applicationId, $licIssueDate, $licExpDate, $dealerAccountNum, $dealerEmail, $creditType) {

            $sheet->fromArray(
                [
                    $dealerAccountNum, // dealer number, must match exactly with BFC
                    isset($fields["ssn"]) ? $fields["ssn"] : "",
                    "",
                    isset($fields["lastName"]) ? $fields["lastName"] : "",
                    isset($fields["firstName"]) ? $fields["firstName"] : "",
                    isset($fields["initial"]) ? $fields["initial"] : "",
                    "", // surname
                    isset($fields["address"]) ? $fields["address"] : "",
                    isset($fields["city"]) ? $fields["city"] : "",
                    isset($fields["state"]) ? $fields["state"] : "", // 10
                    isset($fields["zip"]) ? $fields["zip"] : "",
                    isset($fields["monthlyGrossPay"]) ? $fields["monthlyGrossPay"] : "",
                    isset($fields["yrsEmployed"]) ? $fields["yrsEmployed"] : "",
                    isset($fields["yrsAtAddress"]) ? $fields["yrsAtAddress"] : "",
                    "", // credit score, leave blank (15)
                    "",
                    "",
                    isset($fields["refLandlordMonthlyPayment"]) ? $fields["refLandlordMonthlyPayment"] : "",
                    isset($fields["amount_financed"]) ? $fields["amount_financed"] : "",
                    isset($fields["addressTwo"]) ? $fields["addressTwo"] : "", // line (20)
                    date("d", strtotime($fields["birthdate"])),
                    date("m", strtotime($fields["birthdate"])),
                    date("Y", strtotime($fields["birthdate"])),
                    isset($fields["email"]) ? $fields["email"] : "",
                    isset($fields["employer"]) ? $fields["employer"] : "", // (25)
                    isset($fields["employerPhone"]) ? $fields["employerPhone"] : "",
                    isset($fields["homePhone"]) ? $fields["homePhone"] : "",
                    "",
                    isset($fields["coBirthdate"]) ? date("d", strtotime($fields["coBirthdate"])) : "",
                    isset($fields["coBirthdate"]) ? date("m", strtotime($fields["coBirthdate"])) : "",
                    isset($fields["coBirthdate"]) ? date("Y", strtotime($fields["coBirthdate"])) : "", // line 30

                    isset($fields["coEmployer"]) ? $fields["coEmployer"] : "",
                    isset($fields["coEmployerPhone"]) ? $fields["coEmployerPhone"] : "",
                    isset($fields["coFirstName"]) ? $fields["coFirstName"] : "",
                    isset($fields["coLastName"]) ? $fields["coLastName"] : "",
                    isset($fields["coInitial"]) ? $fields["coInitial"] : "",
                    isset($fields["coMonthlyGrossPay"]) ? $fields["coMonthlyGrossPay"] : "",
                    isset($fields["co_ssn"]) ? $fields["co_ssn"] : "",

                    isset($fields["monthsEmployed"]) ? $fields["monthsEmployed"] : "0",
                    isset($fields["prevMonthsEmployed"]) ? $fields["prevMonthsEmployed"] : "", // (40)
                    isset($fields["prevYrsEmployed"]) ? $fields["prevYrsEmployed"] : "0",
                    "", // leave blank closest dealer
                    isset($fields["monthsAtAddress"]) ? $fields["monthsAtAddress"] : "0",
                    isset($fields["residenceType"]) ? $fields["residenceType"] : "",
                    isset($fields["declaredBankruptcy"]) ? $fields["declaredBankruptcy"] : "",
                    isset($fields["prevYrsAtAddress"]) ? $fields["prevYrsAtAddress"] : "",
                    isset($fields["prevMonthsAtAddress"]) ? $fields["prevMonthsAtAddress"] : "",
                    $dealerEmail,
                    isset($fields["cellPhone"]) ? $fields["cellPhone"] : "",
                    isset($fields["coRelation"]) ? $fields["coRelation"] : "", // line (50)
                    isset($fields["driversOrId"]) ? $fields["driversOrId"] : "",
                    isset($fields["coDriversOrId"]) ? $fields["coDriversOrId"] : "",
                    "",
                    isset($fields["loanLength"]) ? substr($fields["loanLength"], 0, 3) : "", // number of terms in months
                    "",
                    "",
                    "",
                    "",
                    isset($fields["jobTitle"]) ? $fields["jobTitle"] : "",
                    isset($fields["coJobTitle"]) ? $fields["coJobTitle"] : "", // line (60)
                    "",
                    "",
                    $creditType,
                    isset($fields["coCellPhone"]) ? $fields["coCellPhone"] : "",
                    isset($fields["state"]) ? $fields["state"] : "", // 65- this is the state contract is written in
                    "",
                    isset($fields["numDependents"]) ? $fields["numDependents"] : "",
                    "",
                    isset($fields["coAddress"]) ? $fields["coAddress"] : "",
                    isset($fields["coAddressTwo"]) ? $fields["coAddressTwo"] : "",
                    isset($fields["coCity"]) ? $fields["coCity"] : "",
                    isset($fields["coState"]) ? $fields["coState"] : "",
                    isset($fields["coZip"]) ? $fields["coZip"] : "",
                    isset($fields["coHomePhone"]) ? $fields["coHomePhone"] : "",
                    (isset($fields["declaredBankruptcy"]) && $fields["declaredBankruptcy"]) ? $fields["declaredBankruptcy"] : "N",
                    (isset($fields["buyerCheckingAccount"]) && $fields["buyerCheckingAccount"]) ? $fields["buyerCheckingAccount"] : "N",
                    "",
                    "",
                    "",
                    "", // line 80
                    "",
                    "",
                    "",
                    "",
                    "",
                    isset($fields["coYrsEmployed"]) ? $fields["coYrsEmployed"] : "", // line 86
                    isset($fields["coMonthsEmployed"]) ? $fields["coMonthsEmployed"] : "",
                    "",
                    $applicationId,
                    "", // 90
                    "",
                    $licIssueDate,
                    "",
                    $licExpDate,
                    "",
                    "",
                    "",
                    "",
                    "0"
                    ]
                );

            });

        })->store('csv', storage_path('exports/unprocessed'));
    }
}
