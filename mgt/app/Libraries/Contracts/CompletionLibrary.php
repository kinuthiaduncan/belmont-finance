<?php namespace App\Libraries\Contracts;

use App\Models\ContractApplication;
use FPDI;
use Carbon\Carbon;
use JWTAuth;
use Illuminate\Http\Request;
use App\Libraries\Contracts\ContractLibrary;
use App\Services\FileSignatureService;

use Mail;

class CompletionLibrary {
    private $app;
    private $user;
    private $fileId;

    public function __construct(ContractApplication $app, $user) {
        $this->app = $app;
        $this->user = $user;
        $this->fileId = '254859';

    }

    // dealer view and sign completion form
    public function dealerSignCompletionForm() {

        // make sure correct access
        $file = \App\Models\File::where('app_id', '=', $this->app->id)
            ->where('file_type', '=', 9)->firstOrFail();

        $this->fileId = '254859';

        $pdfImage = ContractLibrary::generatePdfImage($file->local_path);

        // get signature locations
        $signatureLocations = $this->retrieveSignatureLocations($this->fileId . '-' . 'dealer', $this->app->salesSlip->sale_state);

        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );
        $token = JWTAuth::fromUser($this->user);
        $successView = 'dealer.contract._dealer-submit-success';
        return [
            'app' => $this->app,
            'buyerKey' => 1,
            'includeSuccess' => $successView,
            'pdfImage' => $pdfImage,
            'sessionKey' => 'test',
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/apps/sign-completion?token=' . $token
        ];
    }

    // after dealer signs the agreement to fund
    public function submitSign($request, $dealer = false) {

            $file = \App\Models\File::where('app_id', '=', $this->app->id)
                ->where('file_type', '=', 9)
                ->firstOrFail();

        $signedFile = ContractLibrary::signPdf(basename($file->local_path, '.pdf'), $request->signatures, $this->app->lastName, 4, $this->app, $dealer ? 1 : 0);


        if($dealer) {
            FileSignatureService::recordFileSignature($signedFile, 'DE');

            // send notice to staff
            $staffEmail = ['funding@belmontfinancellc.com'];

            foreach($staffEmail as $email) {
                Mail::send('emails.staff.completion-signed', ['app' => $this->app], function($m) use($email){
                    $m->to($email)->from('noreply@belmontfinancellc.com')->subject('Completion Signed');
                });
            }
        } else {
            FileSignatureService::recordFileSignature($signedFile, 'PR');
        }

        return $signedFile;
    }

    public function addTerms($user, $company){
        // authenticate user by id
        return $this->writeTerms( $user, $company );
    }

    private function writeTerms($user, $company){
        // retrieve signed Sales Slip file reference

        // create new writer object
        $contractWriter = new \App\Libraries\Contracts\ContractWriter(base_path('./resources/assets/completion/' . $this->fileId . '.pdf' ));

        $contractWriter->write($this->app->fca_id, 171, 12.4);
        $contractWriter->write($company->company_name, 76, 46.8);
        $contractWriter->write(Carbon::parse($this->app->salesSlip->created_at)->format('m/d/Y'), 55, 50);
        $contractWriter->write($this->app->address . ', ' . $this->app->city . ',' . $this->app->state .','. $this->app->zip, 68, 63);
        $contractWriter->write(Carbon::now()->format('m/d/Y'), 29, 97);
        $contractWriter->write($user->name, 134, 130);


        // generate remaining pages
        for ($i = 2; $i <= $contractWriter->numPages; $i++) {
            $contractWriter->moveToPage($i);
        }

        // save pdf
        $path = '../documents/contracts/temp/' . $this->app->firstName . '-' . $this->app->lastName . '-comp-' . time() . '.pdf';

        // for some reason i had to use a different file type of 1000...
        return $contractWriter->finalizePdf($path, $this->app->id, 1000, 'Unsigned Notice Completion');
    }

    // separate method to just add one set of info for the consumer after the file is created
    // accepts the path of the existing file and a array of values from the request
    public function addFinalTerms($path, $requestArray){
        // create new writer object
        $contractWriter = new \App\Libraries\Contracts\ContractWriter(base_path($path));

        if(isset($requestArray['installation'])) {
            if($requestArray['installation'] == 'dealer_install') {
                $contractWriter->write('X', 122, 50);
            } else if ( $requestArray['installation'] == 'consumer_install' ) {
                $contractWriter->write('X', 148, 50);
            } else if ( $requestArray['installation'] == 'no_install' ) {
                $contractWriter->write('X', 6, 54);
            }
        }

        // generate remaining pages
        for ($i = 2; $i <= $contractWriter->numPages; $i++) {
            $contractWriter->moveToPage($i);
        }

        // save pdf
        $path = '../documents/contracts/temp/' . $this->app->firstName . '-' . $this->app->lastName . '-comp-' . time() . '.pdf';
        return $contractWriter->finalizePdf($path, $this->app->id, 9, 'Unsigned LCC');
    }

    private function isWaterProduct($distributor){
        return $distributor->product_id == 26 ? true : false;
    }

    private function isInHome($distributor){
        return $distributor->product_id != 29 ? true : false;
    }

    private function isHomeImprovementProduct($distributor){
        return $distributor->product_id == 28 ? true : false;
    }

    private function retrieveSignatureLocations($pdfFileId)
    {   
        $signatureLoc = json_decode(file_get_contents('../resources/assets/signature/signatureLocations.json'));

        $fileMap = 'completion-dealer';

        // some files don't have any general signatures, only state specific
        if(isset($signatureLoc->{$fileMap})) {
            $fieldPositions = $signatureLoc->{$fileMap};
        } else {
            $fieldPositions = [];
        }

        return $fieldPositions;
    }
}
