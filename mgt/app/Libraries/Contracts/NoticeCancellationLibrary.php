<?php namespace App\Libraries\Contracts;

use App\Models\ContractApplication;
use FPDI;
use Carbon\Carbon;
use Illuminate\Http\Request;


class NoticeCancellationLibrary {
    private $app;
    public $fileId;

    // accepts Application model and an array of file string id references
    // only paperwork class right now that appends pdfs together as some states or combinations require multiple
    public function __construct(ContractApplication $app, $files) {
        $this->app = $app;
        $this->files = $files;
    }

    public function addTerms($state, $user){
        // authenticate user by id
        $this->authenticateSigning($user);
        return $this->writeTerms($state);
    }

    public function writeTerms($state){
        // create new writer object
        // retrieve dealer company
        $dealer = \App\Models\Dealer::find($this->app->dealerId);
        $company = $dealer->company;

        $i = 0;
        foreach($this->files as $fileId) {

            // initialize with the first pdf, else add another pdf onto the end
            if($i == 0) {
                $contractWriter = new \App\Libraries\Contracts\ContractWriter(base_path('resources/assets/cancellation/' . $fileId . '.pdf'));
            } else {
                $contractWriter->appendFile(base_path('resources/assets/cancellation/' . $fileId . '.pdf'));
            }

            // just go through each file individually
            if($fileId == "271552") { // WI, TX, NV
                $contractWriter->write($this->app->firstName .' ' . $this->app->lastName, 11, 11);
                $contractWriter->write($this->app->coFirstName .' ' . $this->app->coLastName, 118, 11);

                $contractWriter->writeCompanyAddress($this->app->distributor, 38, 122);

                $contractWriter->write(date('m/d/Y'), 70, 40);

                $contractWriter->write($this->cancellationDate($state), 60, 134.6);

                if($this->app->coLastName) {
                    $contractWriter->writeCompanyAddress($this->app->distributor, 123, 122);
                    $contractWriter->write($this->cancellationDate($state), 156, 134.6);
                    $contractWriter->write(date('m/d/Y'), 170, 40);
                }
            } else if ($fileId == "271549") { // OH
                $contractWriter->write($this->app->firstName .' ' . $this->app->lastName, 11, 10);
                $contractWriter->write($this->app->coFirstName .' ' . $this->app->coLastName, 118, 10);

                $contractWriter->writeCompanyAddress($this->app->distributor, 30, 115);

                $contractWriter->write(date('m/d/Y'), 81, 32);

                $contractWriter->write($this->cancellationDate($state), 60, 127);

                if($this->app->coLastName) {
                    $contractWriter->writeCompanyAddress($this->app->distributor, 123, 115);
                    $contractWriter->write($this->cancellationDate($state), 156, 127);
                    $contractWriter->write(date('m/d/Y'), 181, 32);
                }
            } else if ($fileId == "271550") { // REG-Z
                $contractWriter->write($this->app->firstName .' ' . $this->app->lastName, 11, 18);
                $contractWriter->write($this->app->coFirstName .' ' . $this->app->coLastName, 118, 18);

                // dates by both signature field
                $contractWriter->write(date('m/d/Y'), 70, 31);

                $contractWriter->writeCompanyAddress($this->app->distributor, 30, 175);

                $contractWriter->write(date('m/d/Y'), 71, 72);

                $contractWriter->write($this->cancellationDate($state), 60, 220);

                if($this->app->coLastName) {
                    $contractWriter->writeCompanyAddress($this->app->distributor, 123, 175);

                    // dates by both signature field
                    $contractWriter->write(date('m/d/Y'), 173, 31);

                    $contractWriter->write(date('m/d/Y'), 181, 72);
                    $contractWriter->write($this->cancellationDate($state), 156, 220);
                }
            } else if ($fileId == "271551") { // AR
                $contractWriter->write($this->app->firstName .' ' . $this->app->lastName, 11, 10);
                $contractWriter->write($this->app->coFirstName . ' ' . $this->app->coLastName, 118, 10);

                $contractWriter->write($this->app->distributor->company_name, 30, 115);

                $contractWriter->write(date('m/d/Y'), 81, 40.4);

                $contractWriter->write($this->cancellationDate($state), 60, 131);

                if($this->app->coLastName) {
                    $contractWriter->write($this->app->distributor, 123, 115);
                    $contractWriter->write($this->cancellationDate($state), 156, 131);
                    $contractWriter->write(date('m/d/Y'), 181, 40.4);
                }
            } else if ($fileId == "271555") { // NJ
                $contractWriter->writeCompanyAddress($this->app->distributor, 15, 56);
            } else if ($fileId == "271556") { // ND
                $contractWriter->write($this->app->firstName .' ' . $this->app->lastName, 11, 10);
                $contractWriter->write($this->app->coFirstName .' ' . $this->app->coLastName, 118, 10);

                $contractWriter->writeCompanyAddress($this->app->distributor, 30, 122);

                $contractWriter->write(date('m/d/Y'), 81, 28);
                $contractWriter->write(date('m/d/Y'), 81, 101);

                $contractWriter->write($this->cancellationDate($state), 70, 116.3);

                if($this->app->coLastName) {
                    $contractWriter->writeCompanyAddress($this->app->distributor, 123, 122);
                    $contractWriter->write($this->cancellationDate($state), 170, 115.5);
                    $contractWriter->write(date('m/d/Y'), 181, 28);
                    $contractWriter->write(date('m/d/Y'), 173, 101);
                }
            } else if ($fileId == "271553") { // ND
                $contractWriter->write($this->app->firstName .' ' . $this->app->lastName, 11, 16);
                $contractWriter->write($this->app->coFirstName .' ' . $this->app->coLastName, 118, 16);

                $contractWriter->writeCompanyAddress($this->app->distributor, 20, 118);
                $contractWriter->write(date('m/d/Y'), 82, 35);

                $contractWriter->write($this->cancellationDate($state), 60, 131);

                if($this->app->coLastName) {
                    $contractWriter->writeCompanyAddress($this->app->distributor, 123, 118);
                    $contractWriter->write($this->cancellationDate($state), 156, 131);
                    $contractWriter->write(date('m/d/Y'), 182, 35);
                }
            }

            $i++;
        }

        // save pdf
        $path = '../documents/contracts/temp/' . $this->app->firstName . '-' . $this->app->lastName . '-cancel-' . time() . '.pdf';
        return $contractWriter->finalizePdf($path, $this->app->id, 5, 'Unsigned Notice of Cancellation');
    }

    /**
    * Calculate the start date for a loan and return formatted date
    * @return string formatted date
    */
    private function getPaymentDueDate(){
        $today = Carbon::now();
        return $today->addDays($this->app->defer)->format('m/d/Y');
    }

    /**
    * Calculate the payment factor for a loan
    * @param float $apr the decimal interest rate
    * @param integer $periods The number of (typically monthly) periods
    * @param integer $deferredDays The number of periods deferral
    * @return float Payment factor
    */
    private function calcPmtFactor($apr, $periods, $deferredDays){
        $rate = $apr / 12;
        $nper = $periods;
        $pv = (1+$apr*($deferredDays-30)/365)/1;
        $fv = 0;
        $type = 0;

        return $this->roundUp(abs((-$fv - $pv * pow(1 + $rate, $nper)) /
        (1 + $rate * $type) /
        ((pow(1 + $rate, $nper) - 1) / $rate)), 5);

    }

    private function roundUp($number, $precision = 2){
        $fig = pow(10, $precision);
        return (ceil($number * $fig) / $fig);
    }

    private function authenticateSigning($user)
    {
        if( $user->id === $this->app->dealerId){
           // is authenticated
        }else{
            // check for admin user
            $appUser = \App\Models\Dealer::where('id', '=', $this->app->dealerId)->first();
            if($user->company->id === $appUser->company->id && $user->admin) {
                // authenticated
            } else {
                abort(403, 'Authorization error');
            }
        }
    }

    // calculate cancellation date based on state
    // counts three  
    private function cancellationDate($state) {
        $excludeSaturday = ['DE', 'HI', 'MI', 'MN', 'MO', 'NJ', 'OR', 'VT', 'IA', 'PA', 'WI'];

        $federalHolidays = [
            'New Years Day' => Carbon::parse('January 1'),
            'Martin Luther King Jr Day' => Carbon::parse('third Monday of January'),
            'George Washingtons Birthday' => Carbon::parse('third Monday of February'),
            'Memorial Day' => Carbon::parse('last Monday of May'),
            'Independence Day' => Carbon::parse('July 4'),
            'Labor Day' => Carbon::parse('first Monday of September'),
            'Columbus Day' => Carbon::parse('second Monday of October'),
            'Veterans Day' => Carbon::parse('November 11'),
            'Thanksgiving Day' => Carbon::parse('fourth Thursday of November'),
            'Christmas Day' => Carbon::parse('December 25'),
        ];
        
        $cancelDate = Carbon::parse('today');
        $daysAdded = 0;

        while ($daysAdded < 3) {
            $cancelDate->addDay();

            if ( ($cancelDate->isWeekday() || ($cancelDate->isSaturday() && !in_array($state, $excludeSaturday))) && !in_array($cancelDate, $federalHolidays) ) {
                $daysAdded++;
            }
        }

        return $cancelDate->format('m/d/Y');
    }
}
