<?php namespace App\Libraries\Contracts;

use App\Models\Auth\SignToken;
use Carbon\Carbon;
use Session;
use Mail;
use Request;
use \App\Libraries\Contracts\Traits\PaperworkTrait;

class RemoteSign {
    use PaperworkTrait;

    public $fileId;

    public function __construct($fileId) {
        $this->fileId = $fileId;
    }

    // create and send a new signature request
    // i believe we have phased this out to just LCC now... as it is messy and the sequential signing methods work better
    // $coApp parameter: pass 2 if just coapplicant signature needed, pass 1 for both, 0 for just primary
    public function sendSignatureRequest($email, $dealer, $data,  $coApp = null, $existingFileId = null)
    {
        $signToken = $this->createNewToken($email, $dealer, $data, $coApp, $existingFileId);

        $url = (getenv('APP_ENV') === "production" ? 'https://' : 'http://') . 'dealer.' . getenv('DOMAIN') .'/sign/welcome?token=' . $signToken->token;

        if($this->fileId == '254859') {
            // load specific view for completion, which i think is the only time we use this method for consumer now
            $view = 'emails.consumer.sign-lcc';
        } else {
            $view = 'emails.consumer.sign-request';
        }

        $app = \App\Models\ContractApplication::findOrFail($data['app_id']);

        Mail::send($view,
            [
                'dealer' => $dealer,
                'url' => $url,
                'distributor' => $app->distributor
            ], function($m) use($signToken, $dealer){
            $m->to($signToken->email)->from('noreply@belmontfinancellc.com');
            $m->subject('Documentation completion and signature requested from ' . $dealer->name);
        });

        return true;
    }


    // load the variables to ready the view for signing
    public function prepareDocument($tokenObject)
    {
        $protocol = (getenv('APP_ENV') == "production" ? 'https://' : 'http://' );

        if($tokenObject->exist_file_id) {

            // first get reference to the already created file and the related application
            $file = \App\Models\File::findOrFail($tokenObject->exist_file_id);
            $app = \App\Models\ContractApplication::findOrFail($file->app_id);

            $data = json_decode($tokenObject->form_data);

            if($tokenObject->needs_primary_sign && $tokenObject->needs_co_sign) {
                $whichSignatures = 3;
            } else if ($tokenObject->needs_co_sign) {
                $whichSignatures = 2;
            } else {
                $whichSignatures = $app->coEmail ? true : false;
            }
        }

        // we have to prep all the variables for passing to the signature view
        if( $this->getDocumentType($tokenObject->file_id) == 'completion' ) {
            $completionHelper = new \App\Libraries\Contracts\CompletionLibrary($app, null);

            // add final information to the signed document
            $newFile = $completionHelper->addFinalTerms($file->local_path, Request::all());

            $pdfImage = ContractLibrary::generatePdfImage($newFile->local_path);

            // get signature locations
            $signatureLocations = ContractLibrary::retrieveSignatureLocations($tokenObject->file_id, 'NONE', false);
            $coAppRelationship = 1;
            $app = null;
            $includeSuccess = 'dealer.contract._consumer-completion-success';

        } else if ( $this->getDocumentType($tokenObject->file_id) == 'contract' || $this->getDocumentType($tokenObject->file_id) == 'sales-slip' ) {
            $pdfImage = ContractLibrary::generatePdfImage($file->local_path);

            // get signature locations
            $signatureLocations = ContractLibrary::retrieveSignatureLocations($tokenObject->file_id, isset($data->state) ? $data->state : $app->salesSlip->sale_state, $whichSignatures);
            
            // determine corelationship value, defaults to single buyer = 1
            if($app->coLastName) {
                $coAppRelationship = ($app->coRelation == "spouse") ? 3 : 2;
            } else {
                $coAppRelationship = 1;
            }

            $includeSuccess = 'dealer.contract._remote-sales-slip-continue';

        } else if ( $this->getDocumentType($tokenObject->file_id) == 'ach' ) {
            $pdfImage = ContractLibrary::generatePdfImage($file->local_path);

            // get signature locations
            $achLibrary = new \App\Libraries\Contracts\AchLibrary($app);
            $signatureLocations = $achLibrary->retrieveSignatureLocations($tokenObject->file_id . '-' . 'dealer', $app->salesSlip->sale_state, $whichSignatures);

            $coAppRelationship = 1;
            $includeSuccess = 'dealer.contract._remote-ach';

        } else if ( $this->getDocumentType($tokenObject->file_id) == 'cancellation' ) {
            $pdfImage = ContractLibrary::generatePdfImage($file->local_path);

            // get signature locations, little bit different here as there are multiple documents to look through
            // same as in DealerApplicationController....
            $signatureLocations = [];
            $i = 0;
            $files = $this->retrieveNoticeCancellation($app->salesSlip->sale_state, $app->distributor);
            foreach($files as $fileId) {
                // get signature locations
                $newSignatureLocations = ContractLibrary::retrieveSignatureLocations($fileId, $app->salesSlip->sale_state, $whichSignatures);
                foreach($newSignatureLocations as &$sig) {
                    // add a page worth of y value to each signature
                    if(isset($sig[1])) {
                        $sig[1] = $sig[1] + 100 * $i;
                    }
                }
                $signatureLocations = array_merge($signatureLocations, $newSignatureLocations);
                $i++;
            }

            $coAppRelationship = $app->coLastName ? 3 : 1;
            $includeSuccess = 'dealer.contract._consumer-completion-success';

        } else {
            // it's a credit app

            // first, if it's already been signed by coapplicant
            $data = json_decode($tokenObject->form_data);

            if(isset($data->toSign)) {
                // retrieve existing file
                $file = \App\Models\File::where('id', '=', $tokenObject->exist_file_id)->first();
                $unsignedPdf = $file->local_path;
                $app = \App\Models\ContractApplication::where('id', '=', $data->app_id)->first();
                Session::set('appId', $app->id);
            } else {
                $user = $tokenObject->dealer;

                $app = \App\Models\ContractApplication::where('id', '=', Session::get('appId'))->first();

                Session::put('appFields.' . $tokenObject->token . '.dealerId', $user->id);
                Session::put('appFields.' . $tokenObject->token . '.amount_financed', $app->amount_financed);
                Session::put('appFields.' . $tokenObject->token . '.down_payment_amount', $app->down_payment_amount);

                // generate a pdf prior to signing with the data all filled in
                $data = $app->toArray();
                $data = $data + Session::get('appFields.' . $tokenObject->token);
                $unsignedPdf = ContractLibrary::generatePdf($tokenObject->file_id, $data, $app->id);
            }


            // get an image of the pdf to display for the signee
            $pdfImage = ContractLibrary::generatePdfImage($unsignedPdf);

            // determine corelationship value, defaults to single buyer = 1
            if($app->coLastName) {
                $coAppRelationship = ($app->coRelation == "spouse") ? 2 : 3;
            } else {
                $coAppRelationship = 1;
            }

            if( isset($data->toSign) ) {
                $whichSignatures = 3;
            } else if( Session::has('coAppRemote') ) {
                $whichSignatures = 2;
            } else {
                $whichSignatures = $app->coLastName ? true : false;
            }

            // get signature locations
            $signatureLocations = ContractLibrary::retrieveSignatureLocations($tokenObject->file_id, $app->state, $whichSignatures);

            $includeSuccess = Session::has('coAppRemote') ? 'dealer.contract._credit-remote-coapp' : 'dealer.contract._credit-remote';
        }

        $protocol = (getenv('APP_ENV') == "production") ? 'https://' : 'http://';

        return [
            'app' => $app,
            'buyerKey' => $coAppRelationship,
            'includeSuccess' => $includeSuccess,
            'pdfImage' => $pdfImage,
            'sessionKey' => $tokenObject->token,
            'signatureLocations' => $signatureLocations,
            'submitSignUrl' => $protocol . 'dealer.' . getenv('DOMAIN') . '/sign/submit?token=' . $tokenObject->token
        ];
    }

    // this is slightly different than the dealer application controller's method, but probably should be combined at some point....
    public function saveApp($tokenObject) {
        $data = Session::get('appFields.' . $tokenObject->token);

        // we have a lot of extra miscellaneous data, so only pass defined fields to application
        $fieldsToSave = [
            'firstName','lastName','initial','address','addressTwo','city','state','zip','yrsAtAddress','prevAddress','prevCityStateZip','birthdate','homePhone','cellPhone','ssn','email','numDependents','employer','yrsEmployed','milRank','employerAddress','employerCityStateZip','jobTitle','monthlyGrossPay',
            'employerPhone','employerExt','prevEmployer','prevYrsEmployed','prevEmployerAddress','prevEmployerCityStateZip','prevJobTitle','prevEmployerPhone','prevEmployerExt','coFirstName','coLastName','coInitial','coBirthdate','coAddress','coAddressTwo','coCity','coState','coZip','coEmployer','coYrsEmployed','coMilRank','coEmployerAddress','coEmployerCityStateZip','coJobTitle','coMonthlyGrossPay','coEmployerPhone','coEmployerExt','refLandlordName','refPropertyValue','refAccountNumber','refLandlordPhone','refLandlordAdress',
            'refLandlordCity','refLandlordState','refLandlordZip','refLandlordMonthlyPayment','refLandlordPresBalance','credrefOneName','credrefOneAccount','credrefOnePhone','credrefOneStreetAdress','credrefOneCity','credrefOneState','credrefOneZip','credrefOneMonthlyPayment','credrefOneMonthlyPresBalance','credrefTwoName','credrefTwoAccount','credrefTwoPhone','credrefTwoStreetAdress','credrefTwoCity','credrefTwoState','credrefTwoZip','credrefTwoMonthlyPayment','credrefTwoMonthlyPresBalance','buyerCheckingAccountNum','buyerCheckingBank','buyerCheckingBankCity','buyerSavingsAccountNum','buyerSavingsBank','buyerSavingsBankCity','yearofBankruptcy','cityStateOfBankruptcy','personalRefOneName','personalRefOnePhone','personalRefOneRelationship','personalRefTwoName','personalRefTwoPhone',
            'personalRefTwoRelationship','initials','buyerSign','buyerDate','coSign','coDate','monthsAtAddress','prevAddressTwo','prevYrsAtAddress','prevMonthsAtAddress','selfEmployed','monthsEmployed','prevMonthsEmployed','coRelation','coSelfEmployed','coMonthsEmployed','residenceStatus','driversOrId','residenceType','buyerCheckingAccount','buyerSavingsAccount','declaredBankruptcy','coHomePhone','coCellPhone','co_ssn','coDriversOrId','coEmail','refLandlordAddress','dealer_req_by','lastStatus','status','revolving','buyerCreditCards','buyerCreditReferences','fca_id','distributor_account',
            'licExpDate','licIssueDate','revolving_signature','revolving_signature_cobuyer','buyerAutoLoan','pdf_path','sales_agreement','description','addon','sales_agreement_signed','sales_tax_amount','shipping_amount','sale_description','agreementType','apr','doc_fees','loanLength','defer','tradein','product_model','product_serial','product_attachments',
            'identification_type', 'identification_state', 'co_identification_type', 'co_identification_state', 'co_licExpDate', 'mailing_address', 'mailing_address_two', 'mailing_city', 'mailing_zip', 'co_mailing_address', 'co_mailing_address_two', 'co_mailing_city', 'co_mailing_zip', 'other_income_source', 'other_income_amount', 'co_other_income_source', 'co_other_income_amount', 'coRefLandlordMonthlyPayment'
        ];

        $finalAppData = ['is_online' => 1];

        foreach($fieldsToSave as $field) {
            if(isset($data[$field]) && $data[$field]) {
                $finalAppData[$field] = $data[$field];
            }
        }

        $app = \App\Models\ContractApplication::where('id', '=', Session::get('appId'))
                ->first();

        if($app->down_payment_amount == null) {
            $app->down_payment_amount = Session::get('appFields.' . $tokenObject->token . '.down_payment_amount');
        }

        return $app->update($finalAppData);
    }


    // sends a request from the dealer to the consumer for signing any remaining unsigned documents
    public static function sendSequentialRequest($email, $coEmail, $dealer, $data)
    {
         $signToken = SignToken::create([
            'dealer_id' => $dealer->id,
            'email' => $email,
            'token' => bin2hex(openssl_random_pseudo_bytes(16)),
            'form_data' => json_encode($data)
        ]);

        $app = \App\Models\ContractApplication::findOrFail($data['app_id']);

        // fetch the documents that will need to be signed
        $existFiles = \App\Models\File::where('app_id', $app->id)
            ->has('unsignedSignaturesPri')
            ->orderBy('file_type', 'ASC')
            ->get();


        $url = (getenv('APP_ENV') === "production" ? 'https://' : 'http://') . 'dealer.' . getenv('DOMAIN') .'/sign/welcome?token=' . $signToken->token;

        Mail::send('emails.consumer.sign-documentation',
            [
                'dealer' => $dealer,
                'name' => $app->firstName . ' ' . $app->lastName,
                'distributor' => $app->distributor,
                'files' => $existFiles,
                'url' => $url
            ], function($m) use($signToken, $app){
                $m->to($signToken->email)->from('noreply@belmontfinancellc.com');
                $m->subject('Documentation completion and signature requested from ' . $app->distributor->company_name);
        });

        if($coEmail) {
            $data['coApp'] = 1;

            // fetch the documents that will need to be signed by the coapplicant. Is probably always the same but could be different down the road
            $existFiles = \App\Models\File::where('app_id', $app->id)
            ->has('unsignedSignaturesCo')
            ->orderBy('file_type', 'ASC')
            ->get();

            $signTokenTwo = SignToken::create([
                'dealer_id' => $dealer->id,
                'email' => $coEmail,
                'token' => bin2hex(openssl_random_pseudo_bytes(16)), // for some reason token was always the same as primary so switching up the bytes
                'form_data' => json_encode($data)
            ]);

            $url = (getenv('APP_ENV') === "production" ? 'https://' : 'http://') . 'dealer.' . getenv('DOMAIN') .'/sign/welcome?token=' . $signTokenTwo->token;

            Mail::send('emails.consumer.sign-documentation', [
                'dealer' => $dealer,
                'name' => $app->coFirstName . ' ' . $app->coLastName,
                'distributor' => $app->distributor,
                'files' => $existFiles,
                'url' => $url
            ], function($m) use($signTokenTwo, $app){
                $m->to($signTokenTwo->email)->from('noreply@belmontfinancellc.com');
                $m->subject('Documentation completion and signature requested from ' . $app->distributor->company_name);
            });
        }

        return true;
    }


    // pass 1 for both coapp and primary signatures needed, pass 2 for just coapplicant signature needed
    public function createNewToken($email, $dealer, $data, $coApp = null, $existingFileId = null)
    {

        // verify uniqueness and delete old record if needed
        $existingToken = SignToken::where('email', '=', $email)
            ->where('file_id', '=', $this->fileId)->first();

        if($existingToken) {
            $existingToken->delete();
        }

        $signToken = SignToken::create([
            'dealer_id' => $dealer->id,
            'email' => $email,
            'token' => bin2hex(openssl_random_pseudo_bytes(16)),
            'form_data' => json_encode($data),
            'file_id' => $this->fileId,
            'needs_primary_sign' => $coApp == 2 ? 0 : 1,
            'needs_co_sign' => $coApp ? 1 : null,
            'exist_file_id' => $existingFileId
        ]);

        return $signToken;
    }
}
