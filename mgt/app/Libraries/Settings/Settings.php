<?php
namespace App\Libraries\Settings;
use Illuminate\Support\Facades\DB;

// service to handle all setting retrieval, storage
class Settings{

    public function __construct()
    {
    }

    /**
     * returns specified distributor setting
     *
     * @var string
     */
    public static function getDistributorSetting($distributorRecord, $settingName, $filter = null){
        if($filter)
            $results = DB::table('dist_settings')->where('name', $settingName)->select('value', 'filter_one')->where('company_id',$distributorRecord->id)->where('filter_one', $filter)->get();
        else
            $results = DB::table('dist_settings')->where('name', $settingName)->select('value', 'filter_one')->where('company_id',$distributorRecord->id)->get();
        return $results;
    }

    /**
     * returns specified distributor setting
     *
     * @var string
     */
    public static function getDistributorApr($distributor){
        $states = collect(DB::table('settings_state')->select('state', 'max_apr')->get())->keyBy('state');

        $results = Settings::getDistributorSetting($distributor, 'apr');

        $states->each(function($state) use($results){
            $state->aprs = array_filter($results, function($apr) use($state){
                return $apr->filter_one == $state->state;
            });
            $state->aprs[] = (object) [
                'value' => $state->max_apr,
            ];
        });
        return $states;
    }


}