<?php
namespace App\Libraries;

use Log;

class Mailchimp{

	public function __construct()
	{
		$this->client = new \GuzzleHttp\Client([
			'base_uri' => getenv('MAILCHIMP_API_URL')
		]);

		$this->verify = getenv('PRODUCTION') == "false" ? false : true;
		$this->credentials = ['developer', getenv('MAILCHIMP_API_KEY')];
	}

    public function addToList($email, $listID, array $mergeFields)
    {
    	$data = [
    		'email_address' => $email,
    		'status' => 'subscribed',
    		'merge_fields' => $mergeFields
    	];

    	try {
    		$response = $this->client->post("lists/$listID/members", ['json' => $data, 'verify' => $this->verify, 'auth' => $this->credentials]);
    	}catch (\GuzzleHttp\Exception\ClientException $e) {
		    Log::info($e->getResponse()->getBody()->getContents());
		}
    }

    public function updateSubscriber($email, $listID, array $mergeFields)
    {
        $data = [
            'email_address' => $email,
            'status' => 'subscribed',
            'merge_fields' => $mergeFields
        ];

        $memberId =  md5($email);

        try {
            $response = $this->client->put("lists/$listID/members/$memberId", ['json' => $data, 'verify' => $this->verify, 'auth' => $this->credentials]);
        }catch (\GuzzleHttp\Exception\ClientException $e) {
            dd($e->getResponse()->getBody()->getContents());
        }
    }


}
?>