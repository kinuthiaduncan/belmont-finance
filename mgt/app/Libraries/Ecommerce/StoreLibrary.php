<?php namespace App\Libraries\Ecommerce;

use Guzzle\Guzzle;

class StoreLibrary{

    protected $store;

    /**
     * Create a new instance of the storelibrary class
     * @param \App\Models\AppExtras\AppOrders $appOrder app orders store model
     * @return void
     */
    public function __construct(\App\Models\AppExtras\AppOrders $appOrder)
    {
        $this->store = $appOrder;
    }

    public function updateOrder(){
        $client = new \GuzzleHttp\Client();
        
        $response = $client->put($this->store->store_order_api_url, [
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8'
            ],
            'json' => ['paymentStatus' => 'PAID'],
            'allow_redirects' => false
        ]);

        $this->store->order_status = "PAID";
        $this->store->save();

    }
}