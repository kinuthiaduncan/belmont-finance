<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Request;
use Mail;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException',
		'Illuminate\Validation\ValidationException',
        'Illuminate\Http\Exception\HttpResponseException',
        'Illuminate\Session\TokenMismatchException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{	

		/*if ($this->shouldReport($e)) {

			$email = getenv('DEV_EMAIL');
			$url = Request::url();

			Mail::send('emails.dev-log', ['exception' => $e, 'url' => $url], function ($m) use ($email) {
	            $m->to(explode(',', $email), 'Dev')->subject('Belmont Exception Log');
	        });

		}*/
        return parent::report($e);


	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		$subdomain = explode(".", $request->url());
        $subdomain = explode("/", array_shift($subdomain));
		$subdomain = array_pop($subdomain);

		if($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException)
		{
			return response()->view('errors.404', [], 404);
		}
		elseif ($e instanceof \Illuminate\Session\TokenMismatchException)
		{
            //redirect to form an example of how I handle mine
            return response()->view('errors.token', [], 498);
        }
        elseif ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
		{
            return response()->view('errors.token', [], 498);
        }
		elseif ($e instanceof ModelNotFoundException)
		{
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }
        elseif ($e instanceof \Illuminate\Validation\ValidationException){
        	if($request->ajax()) {
        		$msgBag = $e->validator->errors()->getMessages();
           		return response()->json($msgBag, 422);
	        } else {
	            return back()->withInput()->withErrors($e->validator->errors());
	        }
        }elseif ($e instanceof \Illuminate\Http\Exception\HttpResponseException){
                return parent::render($request, $e);
        }
        elseif (getenv('APP_ENV') == 'production')
        {
        	if (app()->bound('sentry') && $this->shouldReport($e)) {
		        app('sentry')->captureException($e);
		    }
	        return response()->view('errors.500-' . $subdomain, ['error' => $e->getMessage()], 500);
	    }
	    else 
	    {
	    	return parent::render($request, $e);
	    }

	}

}
