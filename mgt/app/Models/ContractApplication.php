<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Crypt;
use DB;

class ContractApplication extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contractapp';

	protected $primaryKey = "id";

	protected $appends = ['age'];
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	public function distributor()
	{
		if($this->dist_id) {
			return $this->hasOne('App\Models\DealerCompany', 'id', 'dist_id');
		} else {
			// legacy relationship for apps that did not have the direct relation setup. Should all be the other going forward now
			return $this->hasOne('App\Models\DealerCompany', 'account_number', 'distributor_account');
		}
	}

	public function dealerUser()
	{
		return $this->hasOne('App\Models\Dealer', 'id', 'dealerId');
	}

	public function contract()
	{
		return $this->hasOne('App\Models\Contract', 'applicationId', 'fca_id');
	}

	public function flags()
	{
		return $this->hasMany('App\Models\AppExtras\AppFlag', 'app_id');
	}



	// ***** collection of file relation methods
	public function files(){
		return $this->hasMany('App\Models\File', 'app_id', 'id')->whereNull('archived_at');
	}

	public function allFiles(){
		return $this->hasMany('App\Models\File', 'app_id', 'id');
	}

	public function consumerSignedFiles(){
		return $this->files()
			->has('signatures')
			->doesntHave('unsignedSignaturesPri')
			->doesntHave('unsignedSignaturesCo');			
	}

	public function completelySignedFiles(){
		return $this->files()->doesntHave('unsignedSignatures');
	}

	public function hasUnsignedFile($fileType) {
		return $this->files()
			->where('file_type', '=', $fileType)
			->has('unsignedSignatures');
	}

	public function hasFileType($fileType)
	{
		$fileTypes = is_array($fileType) ? $fileType : [$fileType];
		return $this->files()->whereIn('file_type', $fileTypes)->count();
	}

	public function salesSlipFile(){
		return $this->files()
			->where('file_type', '=', 2)
			->doesntHave('unsignedSignaturesPri')
			->doesntHave('unsignedSignaturesCo');
	}

	public function salesSlip(){
		return $this->hasOne('App\Models\SalesSlip', 'app_id', 'id');
	}

	// returns whether or not the paperwork is complete in an array form, depending on distributor requirements
	// basic array = required paperwork for first stage, funding array = required paperwork for second stage, other = maybe optional paperwork?
	public function remainingPaperwork(){
		// different criteria for hi and water
		if( in_array($this->distributor->product_id, [26, 28]) && $this->credit_type == 'Revolving') {
			$required = [1, 2, 5]; // credit, contract, and cancellation
			$requiredForFunding  = [1, 2, 9]; // credit,  dealer signed contract, and dealer signs LCC
			$other = [13, 9]; // ach, consumer signed lcc
		} else {
			$required = [1, 2, 5]; // credit, contract, and cancellation
			$requiredForFunding  = [1, 2]; // credit, contract, and dealer signs the contract
			$other = [13]; // ach
		}

		$consumerFiles = $this->consumerSignedFiles()->groupBy('file_type')->lists('file_type')->toArray();
		$fundingFiles = $this->completelySignedFiles()->groupBy('file_type')->lists('file_type')->toArray();

		// show the option with the fewest requirements?

		return [
			'basic' => array_diff($required, $consumerFiles),
			'funding' => array_diff($requiredForFunding, $fundingFiles),
			'other' => array_diff($other, $consumerFiles)
		];
	}





	public function eligiblePromos($dealer, $company){
		$promoIds = $dealer->promos($company);

		$loanType = $this->credit_type == 'Revolving' ? 'REVOLVE' : 'SIMPLE';

		$promos = \App\Models\Promo::where(function($query) {
				$query->whereNull('min_buy_rate')
					->orWhere('min_buy_rate', '<=', $this->buy_rate);
			})
			->whereIn('loan_type', [$loanType, 'BOTH'])
			->whereIn('id', $promoIds->toArray())
			->orderBy('ordinal')
			->orderBy('name')
			->get();

		return $promos;
	}

	public function getAgeAttribute()
    {
    	$since = Carbon::now()->timestamp - Carbon::parse($this->created_at)->timestamp;
	    $chunks = array(
	        array(60 * 60 * 24 * 365 , 'year'),
	        array(60 * 60 * 24 * 30 , 'month'),
	        array(60 * 60 * 24 * 7, 'week'),
	        array(60 * 60 * 24 , 'day'),
	        array(60 * 60 , 'hour'),
	        array(60 , 'minute'),
	        array(1 , 'second')
	    );

	    for ($i = 0, $j = count($chunks); $i < $j; $i++) {
	        $seconds = $chunks[$i][0];
	        $name = $chunks[$i][1];
	        if (($count = floor($since / $seconds)) != 0) {
	            break;
	        }
	    }

	    $print = ($count == 1) ? '1 '.$name : "$count {$name}s";
	    return $print;
    }

    public function getAgeDays()
    {
    	return Carbon::now()->diffInDays(Carbon::parse($this->created_at));
    }

	/**
	* Define relationship for store orders
	*/
	public function appOrders(){
		return $this->hasOne('App\Models\AppExtras\AppOrders', 'app_id', 'id');
	}


	/*** App Filtering Methods ***/

	// relationship for app filter - pass the filter group id
	public function appFilter() {
		return $this->hasOne('App\Models\AppExtras\AppFilter', 'app_id', 'id');
	}

	public function getFilter($groupId) {
		return $this->appFilter ? $this->appFilter()->where('filter_group', '=', $groupId)->first() : null;
	}



	/*** App Scopes ***/

	public function scopeOfCompleted($query, $completed = true){
		if($completed){
			return $query()->salesSlip->where('is_completed', '=', 1);
		} else {
			return $query()->salesSlip->whereNull('is_completed');
		}
	}

	// returns applications that belong to either the dealer or the company if admin user
	public function scopeOfUser($query, $user, $distributor){
		// include scope of company here
		if(!$user->hasRole('admin')){
			$query->where('dealerId', '=', $user->id);
		}

		$query->where(function($q) use($distributor){
			// newer association for online apps
			$q->where('dist_id', '=', $distributor->id)
				->orWhere('distributor_account', '=', $distributor->account_number);
		});
	}

	// filters application by filters that staff set
	public function scopeOfFilter($query, $filterGroupId, $filterName){
		$joinTableName = 'app_filters_' . $filterGroupId;
		// include scope of company here
		if( $filterName ) {
			return $query->leftJoin('app_filters as ' . $joinTableName, 'contractapp.id', '=', $joinTableName . '.app_id')
				->where($joinTableName . '.filter_group', '=', $filterGroupId)
				->where($joinTableName . '.filter_name', '=', $filterName)
				->addSelect(DB::raw($joinTableName . '.filter_name as filter_name_' . $filterGroupId));
		} else {
			$query->leftJoin('app_filters as ' . $joinTableName, 'contractapp.id', '=', $joinTableName . '.app_id')
				->addSelect(DB::raw($joinTableName . '.filter_name as filter_name_' . $filterGroupId));
		}
	}


	public function getFileLogs(){
		$logPath = storage_path() . '/' . getenv('APP_LOG_DIR');
		$filePath =  $logPath . date('Y-m-d', strtotime($this->created_at)) . '/';
		$files = glob($filePath . $this->id . "*.xml");
		foreach($files as &$file){
			$file = str_replace('/', '|', date('Y-m-d', strtotime($this->created_at)) . '/' . basename($file));
		};
		sort($files);
		return $files;
	}

	// methods to set and retrieve SSN values
	public function setSsnAttribute($value)
    {
        $this->attributes['ssn'] =  Crypt::encrypt($value);
    }

    public function setCoSsnAttribute($value)
    {
        $this->attributes['co_ssn'] =  Crypt::encrypt($value);
    }

    public function getSsnAttribute($value)
    {
         return $value ? Crypt::decrypt($value) : '';
    }

    public function getCoSsnAttribute($value)
    {
        return $value ? Crypt::decrypt($value) : '';
    }

    public function setBirthdateAttribute($value)
    {
    	$this->attributes['birthdate'] = Carbon::parse($value)->format('Y-m-d h:i:s');
    }

    public function setCoBirthdateAttribute($value)
    {
    	$this->attributes['coBirthdate'] = Carbon::parse($value)->format('Y-m-d h:i:s');
    }

    public function setLicExpDateAttribute($value)
    {
    	$this->attributes['licExpDate'] = Carbon::parse($value)->format('Y-m-d h:i:s');
    }

    public function setCoLicExpDateAttribute($value)
    {
    	$this->attributes['co_licExpDate'] = Carbon::parse($value)->format('Y-m-d h:i:s');
    }



    // unfinished completion form, meaning the dealer hasn't signed it yet
    public function unCompletionForm()
    {
    	return $this->hasOne('App\Models\File', 'app_id', 'id')
    		->where('file_type', '=', 9);
    }

    // finished completion form with both signatures
    public function completionForm()
    {
    	return $this->hasOne('App\Models\File', 'app_id', 'id')
    		->where('file_type', '=', 11);
    }
}
