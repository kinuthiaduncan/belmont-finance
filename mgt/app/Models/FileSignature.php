<?php

namespace App\Models;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class FileSignature extends Model
{
    protected $table = 'files_signatures';

    protected $primaryKey = "id";

    protected $fillable = ['file_id', 'signed_at', 'ip_address','signee'];

    public function file(){
        return $this->belongsTo(File::class,'file_id');
    }
}
