<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_tags';

	public function getCount(){
		// 
	}

	public function taggedContracts(){
		return $this->hasMany('App\Models\Relations\TagRelation', 'tag_id', 'id');
	}
}
