<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\DealerCompany;

class Contract extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contracts';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];

	// one to one distributer users
	public function distributorUser(){
		return $this->hasOne('App\Models\Dealer', 'id', 'dist_user_id');
	}

	// one to one dealerCompany users
	public function dealer(){
		return $this->hasOne('App\Models\DealerCompany', 'dist_id', 'distributorId')->withTrashed();
	}

	public function customer(){
		return $this->hasOne('App\Models\Customer', 'contract_id', 'id');
	}

	public function app(){
		return $this->hasOne('App\Models\ContractApplication', 'fca_id', 'applicationId');
	}

	public function rating(){
		return $this->hasOne('App\Models\Rating', 'id', 'ratingId');
	}

	public function getPromotional(){
		return $this->hasOne('App\Models\Promotional', 'id', 'sameAsCashId');
	}

	public function getFewLogs(){
		return $this->hasMany('App\Models\Log', 'table_id', 'id')->where('table_name','=', 'contracts')->orderBy('created_at', 'desc')->limit(5);
	}

	public function getLogs(){
		return $this->hasMany('App\Models\Log', 'table_id', 'id')->where('table_name','=', 'contracts')->orderBy('created_at', 'desc');
	}

	public function getComments(){
		return $this->hasMany('App\Models\Comment', 'contract_id', 'id')->orderBy('created_at', 'desc');
	}

	public function getDistributorComments(){
		return $this->hasMany('App\Models\Comment', 'contract_id', 'id')->where('show_dist', true)->orderBy('created_at', 'desc');
	}

	public function mostRecentComment(){
		return $this->hasOne('App\Models\Comment', 'contract_id', 'id')->where('show_dist', true)->orderBy('created_at', 'desc');
	}

	public function hasRecurring(){
		$recurringAch = \App\Models\AchPaymentRecurring::where('contract_id', $this->id)->first();
		$recurringCard = \App\Models\PaymentRecurring::where('contract_id', $this->id)->first();
		if($recurringAch)
		{
			return $recurringAch;
		}
		else if ($recurringCard) {
			return $recurringCard;
		}
		else
		{
			return false;
		}
	}

	public function scopeOfStatus($query, $status){
		if($status == "approved"){
			return $query->where('statusId', '=', '1');
		}
		elseif($status == "cancelled"){
			return $query->where('statusId', '=', '2');
		}
		elseif($status == "pending"){
			return $query->where('statusId', '=', '0');
		}
		else{
			return $query;
		}
	}

	public function scopeOfTag($query, $arrayTagIds){
		if($arrayTagIds){
			return $query->join('dist_tag_relations', function($join) use($arrayTagIds){
				$join->on('contracts.id', '=', 'dist_tag_relations.contract_id')->whereIn('dist_tag_relations.tag_id', $arrayTagIds);
			});
		}
		else
		{
			return $query;
		}

	}

	public function scopeOfDist($query, $accountNum){
		if($accountNum){
			$distributor = DealerCompany::withTrashed()->where('account_number', '=', $accountNum)->first();
			if($distributor){
				return $query->where('distributorId', '=', $distributor->dist_id );
			} else {
				return $query->where('distributorId', '=', DealerCompany::withTrashed()->find($accountNum)->dist_id );
			}
		}
		else{
			return $query;
		}
	}

	public function scopeOfUser($query, $user){
		if(!$user->hasRole('admin')){
			return $query->where('dist_user_id', '=', $user->id);
		}
		else{
			return $query;
		}
	}

	public function scopeOfProduct($query, $productId){
		if($productId){
			return $query->whereIn('distributorId', DealerCompany::withTrashed()->where('product_id', '=', $productId)->lists('dist_id') );
		}
		else{
			return $query;
		}
	}

	//date scopes - pass one of the following strings
	public function scopeOfDateRange($query, $range)
    {
    	if($range && gettype($range) == "string"){
    		$tonight = date('Y-m-d H:i:s', strtotime("24:59:59"));

    		if($range == "today"){
    			return $query->whereBetween('applicationDate', [date('Y-m-d H:i:s', strtotime("00:00:00")), $tonight]);
    		}
    		elseif($range == "yesterday"){
    			return $query->whereBetween('applicationDate', [date('Y-m-d 00:00:00', strtotime("yesterday")), date('Y-m-d 23:59:59', strtotime("yesterday"))]);
    		}
    		elseif($range == "week"){
    			return $query->whereBetween('applicationDate', [date('Y-m-d 00:00:00', strtotime("this week")), $tonight]);
    		}
    		elseif($range == "last+week"){
    			return $query->whereBetween('applicationDate', [date('Y-m-d 00:00:00', strtotime("last sunday", strtotime("last week"))), date('Y-m-d 23:59:59', strtotime("last saturday"))]);
    		}
    		elseif($range == "month"){
    			return $query->whereBetween('applicationDate', [date('Y-m-01 00:00:00', strtotime("this month")), $tonight]);
    		}
    		elseif($range == "last+month"){
    			return $query->whereBetween('applicationDate', [date('Y-m-01 00:00:00', strtotime("last month")), date("Y-m-t", strtotime("last month"))]);
    		}
    		elseif($range == "quarter"){

    			$current_month = date('m');
				$current_year = date('Y');
				if($current_month>=1 && $current_month<=3)
				{
				$start_date = strtotime('1-January-'.$current_year);  // timestamp or 1-Januray 12:00:00 AM
				$end_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
				}
				else  if($current_month>=4 && $current_month<=6)
				{
				$start_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM
				$end_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
				}
				else  if($current_month>=7 && $current_month<=9)
				{
				$start_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM
				$end_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
				}
				else  if($current_month>=10 && $current_month<=12)
				{
				$start_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM
				$end_date = strtotime('1-January-'.($current_year+1));  // timestamp or 1-January Next year 12:00:00 AM means end of 31 December this year
				}

    			return $query->whereBetween('applicationDate', [date('Y-m-d 00:00:00', $start_date), date("Y-m-d", $end_date)]);
    		}
    		elseif($range == "last+quarter"){

    			$current_month = date('m');
				$current_year = date('Y');

				if($current_month>=1 && $current_month<=3)
				{
					$start_date = strtotime('1-October-'.($current_year-1));  // timestamp or 1-October Last Year 12:00:00 AM
					$end_date = strtotime('31-December-'.($current_year-1));  // // timestamp or 1-January  12:00:00 AM means end of 31 December Last year
				}
				else if($current_month>=4 && $current_month<=6)
				{
					$start_date = strtotime('1-January-'.$current_year);  // timestamp or 1-Januray 12:00:00 AM
					$end_date = strtotime('31-March-'.$current_year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
				}
				else  if($current_month>=7 && $current_month<=9)
				{
					$start_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM
					$end_date = strtotime('30-June-'.$current_year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
				}
				else  if($current_month>=10 && $current_month<=12)
				{
					$start_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM
					$end_date = strtotime('30-September-'.$current_year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
				}

    			return $query->whereBetween('applicationDate', [date('Y-m-d 00:00:00', $start_date), date("Y-m-d", $end_date)]);
    		}
    		elseif($range == "year"){
    			return $query->whereBetween('applicationDate', [date('Y-01-01 00:00:00'), $tonight]);
    		}
    		else{
    			return $query;
    		}
    	}elseif($range && gettype($range) == "array"){
    		return $query->whereBetween('applicationDate', [date('Y-m-d 00:00:00', strtotime($range['start'])), date("Y-m-d", strtotime($range['end']))]);
    	}
    	else
    	{
    		return $query;
    	}
    }
}
