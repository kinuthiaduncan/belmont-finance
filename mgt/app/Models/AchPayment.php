<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AchPayment extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'payments_ach';
	protected $guarded = ['account_number'];
	protected $fillable = array('bank_name', 'bank_address', 'routing_number', 'is_savings_account', 'electronic_signature', 'amount', 'payment_date');

	public function customer(){
		return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
	}
}
