<?php namespace App\Models\Relations;

use Illuminate\Database\Eloquent\Model;

class DealerDenyPromoRelation extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_user_denied_promos';
	protected $guarded = [];
}
