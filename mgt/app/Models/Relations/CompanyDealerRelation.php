<?php namespace App\Models\Relations;

use Illuminate\Database\Eloquent\Model;

class CompanyDealerRelation extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_dealer_relations';
	protected $guarded = [];
}