<?php namespace App\Models\Relations;

use Illuminate\Database\Eloquent\Model;

class TagRelation extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public $timestamps = false;
	protected $table = 'dist_tag_relations';
}
