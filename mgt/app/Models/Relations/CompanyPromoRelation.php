<?php namespace App\Models\Relations;

use Illuminate\Database\Eloquent\Model;

class CompanyPromoRelation extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_promos';
	protected $guarded = [];
}
