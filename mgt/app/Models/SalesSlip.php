<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Crypt;

class SalesSlip extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sales-slips';

	protected $guarded = ['id'];



    public function items()
    {
        return $this->hasOne('App\Models\SalesSlipItems');
    }

    

	public function scopeIsCompleted($query, $isCompleted = true)
    {
    	if($isCompleted) {
    		return $query->where('is_completed_signed', '=', 1);
    	} else {
    		return $query->whereNull('is_completed_signed');
    	}
    }

    // mutators to auto encrypt or decrypt the account number
    public function setAchAccountNumberAttribute($value)
    {
    	$this->attributes['ach_account_number'] =  Crypt::encrypt($value);
    }

    public function getAchAccountNumberAttribute($value)
    {
    	return Crypt::decrypt($value);
    }
}