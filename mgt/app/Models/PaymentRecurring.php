<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentRecurring extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'payments_recurring';
	protected $guarded = ['cc', 'ccv'];
  protected $fillable = array('cc', 'ccv', 'routing_number', 'card_name', 'amount', 'withdrawal_day', 'start_date');

	public function customer(){
		return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
	}

	public function contract(){
		return $this->hasOne('App\Models\Contract', 'id', 'contract_id');
	}

	public function application(){
		return $this->hasOne('App\Models\ContractApplication', 'id', 'application_id');
	}

}
