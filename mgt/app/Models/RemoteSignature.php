<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemoteSignature extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'signature_tokens';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	public function dealer(){
		return $this->hasOne('App\Models\Dealer', 'id', 'dealer_id');
	}

}
