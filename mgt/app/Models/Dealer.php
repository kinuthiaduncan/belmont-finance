<?php namespace App\Models;

// model for each dealer/distributor user who logs in

use Illuminate\Auth\Authenticatable;
use Auth;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Dealer extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'username', 'email', 'password', 'admin', 'company_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function hasRole($role, $company = null){
		$company = $company ? $company : $this->company;
		if( isset($this->companies()->where('distributor_id', $company->id)->first()->pivot->{$role}) )
		{
			return $this->companies()->where('distributor_id', $company->id)->first()->pivot->{$role};
		}
	}

	// return the allowable promo ids by company for this dealer
	public function promos($company){
		$deniedPromoIds = \App\Models\Relations\DealerDenyPromoRelation::where('user_id', '=', $this->id)
			->where('distributor_id', '=', $company->id)
			->pluck('promo_id')->toArray();

		$promos = $this->company->promos();
		$filteredPromos = $promos->filter(function ($value, $key) use ($deniedPromoIds){
		    return ! in_array($value->id, $deniedPromoIds);
		});

		return $filteredPromos->pluck('id');
	}


	// return the user status of this dealer, by company
	public function getUserStatus($company){
		return $company->users()->find($this->id)->pivot->user_status;
	}

	public function companies(){
		return $this->belongsToMany('App\Models\DealerCompany', 'dist_dealer_relations', 'dealer_id', 'distributor_id')
			->withPivot('admin', 'user_status', 'parent_user_id');
	}

	// accessor that allows to simulate legacy one to one dealer to distributor relationship
	// could potentially fail for a call like app->dealerUser->company if the dealer has multiple companies assigned
	// all apps should be directly tie to distributors going forward
	public function getCompanyAttribute()
    {
    	// first try to access by session where we will store the user's selected company
    	// but this would only be if the user is logged in
    	if(Session::has('selectedDistributorId')) {
    		return \App\Models\DealerCompany::where('id', '=', Session::get('selectedDistributorId'))->first();

    	} else {
    		// go by first record of dealer company for legacy... could be a mixup in the future
    		$distributorRelation = $this->companies()->first();
    		return $distributorRelation;
    	}
    }

	public function getContracts(){
		return $this->hasMany('App\Models\Contract', 'dist_user_id', 'id');
	}

	public function subUsers(){
		// add extra check to be sure htey are in the same company
		return $this->hasMany('App\Models\Dealer', 'parent_user_id', 'id')->where('company_id', $this->company_id);
	}

	public function getReserveCredit(){
		return $this->getContracts()->sum('current_reserve_amount');
	}
}
