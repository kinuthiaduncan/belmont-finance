<?php namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class CustomerToken extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customer_tokens';
	protected $guarded = [];

	public function app(){
		return $this->hasOne('App\Models\ContractApplication', 'id', 'app_id');
	}
}
