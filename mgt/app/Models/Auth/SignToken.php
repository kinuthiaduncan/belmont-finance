<?php namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class SignToken extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'signature_tokens';
	protected $guarded = [];

	public function dealer(){
		return $this->hasOne('App\Models\Dealer', 'id', 'dealer_id');
	}
}
