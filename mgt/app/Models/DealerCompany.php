<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use DB;

class DealerCompany extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_companies';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['company_name', 'account_number', 'dist_id', 'owner', 'product_id', 'dealer_status'];


	// relationship to states that dealer is allowed to work in
    public function dealerStates()
    {
        return $this->hasMany('App\Models\Dealers\DealerState', 'dist_id', 'id');
    }

	// one to many distributer users
	public function users(){
		return $this->belongsToMany('App\Models\Dealer', 'dist_dealer_relations', 'distributor_id', 'dealer_id')
		->withPivot('admin', 'parent_user_id', 'user_status');
	}

	// return only top level users
	public function primaryUsers(){
		return $this->users()
			->wherePivot('parent_user_id', '=', null);
	}

	public function product(){
		return $this->hasOne('App\Models\DealerProduct', 'id', 'product_id');
	}

	public function contracts(){
		return $this->hasMany('App\Models\Contract', 'distributorId', 'dist_id');
	}

	public function isActive(){
		if( $this->users()->wherePivot('admin', '=', '1')->where('password', '!=', "")->first() ){
			return true;
		}
	}

	// disable all paperwork temporarily
	public function getEnableNewOnlineAttribute()
	{
		if( $this->id != 469 ) {
			return false;
		} else {
			return $this->getOriginal('enable_new_online');
		}
	}

	// returns the promos that are available for this distributor to use
	public function promos(){
		if($this->allow_revolving == 1) {
			$simpleOrRevolving = 'REVOLVE';
		} else if ($this->allow_revolving == 0) {
			$simpleOrRevolving = 'SIMPLE';
		} else {
			$simpleOrRevolving = 'BOTH';
		}

		$dealerPromos = \App\Models\Relations\CompanyPromoRelation::where('dist_id', '=', $this->id)->pluck('promo_id');

		if(!$dealerPromos) {
			return \App\Models\Promo::where('program_id', '=', $this->program_id)->get();
		} else {
			return \App\Models\Promo::whereIn('id', $dealerPromos)->get();
		}
	}

	public function recentContracts(){
		return $this->contracts()->whereBetween('applicationDate', [date('Y-m-d H:i:s', strtotime('-30 days')), date('Y-m-d H:i:s', strtotime("24:59:59"))]);
	}

	public function getReserveTotal(){
		return $this->contracts()->sum('current_reserve_amount');
	}

	public function getPayable(){
		return $this->contracts()->where('distributorPayable', '<>', '0')->sum('distributorPayable');
	}

	public function getPending(){
		return $this->contracts()->where('statusId','=','0')->count();
	}

	public function getSettings(){
		return DB::table('dist_settings')->where('company_id', $this->id)->get();
	}

	public function getSetting($settingName){
		return DB::table('dist_settings')->where('company_id', $this->id)->where('name', $settingName)->value('value');
	}

	public function setSetting($settingName, $value){
		if($this->getSetting($settingName) === null)
		{
			DB::table('dist_settings')->insert(['company_id' => $this->id, 'name' => $settingName, 'value' => $value]);
		}
		else{
			DB::table('dist_settings')->where('company_id', $this->id)->where('name', $settingName)->update(['value' => $value]);
		}
	}

	public function getTags($contractId = null){
		if($contractId){
			return $this->hasMany('App\Models\Tag', 'company_id', 'id')->leftJoin('dist_tag_relations', function ($join) use($contractId)
			{
            	$join->on('dist_tags.id', '=', 'dist_tag_relations.tag_id')
            		->where('dist_tag_relations.contract_id', '=', $contractId);

        	})->select('dist_tags.id', 'dist_tags.name', 'dist_tag_relations.tag_id')
        	->orderBy('dist_tags.name')
        	->get();

		}else{
			return $this->hasMany('App\Models\Tag', 'company_id', 'id');
		}

	}

	public function scopeOfStatus($query, $filter){
		if($filter == "active"){
			return $query->join('dist_users', function($join){
				$join->on('dist_companies.id', '=', 'dist_users.company_id');
			})->select('dist_companies.*')->groupBy('dist_companies.id');
		}elseif($filter == "inactive"){
			return $query->leftJoin('dist_users', function($join){
				$join->on('dist_companies.id', '=', 'dist_users.company_id');
			})->whereNull('dist_users.company_id')->select('dist_companies.*')->groupBy('dist_companies.id');
		}
		else
		{
			return $query;
		}
	}

	public function scopeOfDeleted($query, $user){
		if($user->hasRole('admin')){
			return $query->withTrashed();
		}else{
			return $query->whereNull('dist_companies.deleted_at');
		}
	}

	public function scopeOfSortBy($query, $filter){
		if($filter == "payable"){
			return $query->join('contracts', 'dist_companies.dist_id', '=', 'contracts.distributorId')
				->select('dist_companies.*', DB::raw('SUM(distributorPayable) as d_pay'))
				->groupBy('dist_companies.id')
				->orderBy('d_pay', 'desc');
		}elseif($filter == "pending"){
			return $query->join('contracts', function($join){
					$join->on('dist_companies.dist_id', '=', 'contracts.distributorId')
					->where('contracts.statusId','=','0');
				})->select('dist_companies.*', DB::raw('count(distinct contracts.id) as countPending'))
				->groupBy('dist_companies.id')
				->orderBy('countPending', 'desc');
		}
		elseif($filter == "recApproved"){
			return $query->leftJoin('contracts', function($join){
					$join->on('dist_companies.dist_id', '=', 'contracts.distributorId')
					->where('applicationDate', '>=', date('Y-m-d H:i:s', strtotime('-180 days')))
					->where('applicationDate', '<=', date('Y-m-d H:i:s', strtotime("24:59:59")));
				})->select('dist_companies.*', DB::raw('count(distinct contracts.id) as countTotal'))
				->groupBy('dist_companies.id')
				->orderBy('countTotal', 'desc');
		}
		else{
			return $query->orderBy('company_name', 'desc');
		}
	}
}
