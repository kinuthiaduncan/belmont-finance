<?php namespace App\Models\AppExtras;

use Illuminate\Database\Eloquent\Model;
	
// This model sets up different filters for the contractapplication model
// Allows for different groups. Define the allowable filter names by the app id of zero

class AppFlag extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contractapp_flags';

	protected $guarded = [];

	public function app(){
		return $this->belongsTo('App\Models\ContractApplication', 'app_id');
	}
}