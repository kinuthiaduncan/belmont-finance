<?php namespace App\Models\AppExtras;

use Illuminate\Database\Eloquent\Model;
	
// This model sets up different filters for the contractapplication model
// Allows for different groups. Define the allowable filter names by the app id of zero

class AppFilter extends Model {

	const GROUP_NAME = [
		1 => 'Pending Type',
		2 => 'Status'
	];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'app_filters';

	protected $guarded = [];

	// return the list of selectable filter options
	// pass the filter group id to choose which group
	public static function getOptions()
	{
		return AppFilter::where('app_id', '=', 0)
			->get()->groupBy('filter_group');
	}
}