<?php namespace App\Models\AppExtras;

use Illuminate\Database\Eloquent\Model;

class AppOrders extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;
    protected $table = 'app_store_orders';
    protected $fillable = ['app_id', 'order_status', 'store_type', 'store_order_id', 'store_order_api_url'];
}
