<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';
	protected $primaryKey = 'name';
	protected $fillable = array('name', 'value');
	public $incrementing = false;

	public function upsert($name, $value){
		$this->updateOrCreate(['name' => $name], ['value' => $value]);
	}
}
