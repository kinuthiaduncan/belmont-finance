<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesSlipItems extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sales-slip-items';
	protected $guarded = ['id'];
}