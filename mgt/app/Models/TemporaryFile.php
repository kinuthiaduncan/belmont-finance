<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporaryFile extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'temporary_files';

	protected $guarded = [];
}