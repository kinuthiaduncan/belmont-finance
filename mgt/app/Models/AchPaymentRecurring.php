<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AchPaymentRecurring extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'payments_ach_recurring';
	protected $guarded = ['account_number'];
	protected $fillable = array('bank_name', 'bank_address', 'routing_number', 'electronic_signature', 'amount', 'withdrawal_day', 'start_date');

	public function customer(){
		return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
	}

	public function contract(){
		return $this->hasOne('App\Models\Contract', 'id', 'contract_id');
	}

	public function application(){
		return $this->hasOne('App\Models\ContractApplication', 'id', 'application_id');
	}
}