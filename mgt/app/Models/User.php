<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'admin', 'loan', 'loan_manager', 'csr', 'csr_manager', 'distributor', 'distributor_manager'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function hasRole($role){
		if( isset( $this->{$role} ) )
		{
			return $this->{$role};
		}
	}

	// company relation
	public function company(){
		return $this->hasOne('App\Models\DealerCompany', 'id', 'company_id');
	}

}
