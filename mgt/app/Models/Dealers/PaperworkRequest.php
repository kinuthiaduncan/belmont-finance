<?php namespace App\Models\Dealers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaperworkRequest extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dealer_paperwork_request';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function getItemsRequestedWords() {
        $itemsRequested = ($this->installment_paperwork ? explode(',', $this->installment_paperwork) : [])
         + ($this->revolving_paperwork ? explode(',', $this->revolving_paperwork) : []);

        if( count($itemsRequested > 1) ) {
            $itemsRequested[count($itemsRequested) - 1] = 'and ' . $itemsRequested[count($itemsRequested) - 1];
        }

        return count($itemsRequested) > 2 ? implode(', ', $itemsRequested) : implode(' ', $itemsRequested);
    }

    public function getItemsRequested() {
        $itemsRequested = ($this->installment_paperwork ? explode(',', $this->installment_paperwork) : [])
         + ($this->revolving_paperwork ? explode(',', $this->revolving_paperwork) : []);
         
        return $itemsRequested;
    }

    public function dealer() {
        return $this->hasOne('App\Models\Dealer', 'id', 'dealer_id');
    }

    public function company(){
        return $this->hasOne('App\Models\DealerCompany', 'id', 'company_id');
    }

}
