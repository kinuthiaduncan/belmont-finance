<?php namespace App\Models\Dealers;

use Illuminate\Database\Eloquent\Model;

class DealerState extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_states';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];

}
