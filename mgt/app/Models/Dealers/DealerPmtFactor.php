<?php namespace App\Models\Dealers;

use Illuminate\Database\Eloquent\Model;

class DealerPmtFactor extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_pmt_factors';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
}
