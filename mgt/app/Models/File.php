<?php namespace App\Models;

use App\Models\FileSignature;
use Illuminate\Database\Eloquent\Model;

class File extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'files';

	protected $fillable = ['file_type', 'name', 'local_path', 'app_id'];

	/**
	 * Boot method to add events for creation and deletion
	 * 
	 * @return null
	 */
	public static function boot() {
        parent::boot();
        static::created(function($file) {
            \Event::fire('file.created', $file);

        });

        static::deleting(function($file) {
            \Event::fire('file.deleting', $file);
        });
    }

	public function app(){
		return $this->hasOne('App\Models\ContractApplication', 'id', 'app_id');
	}


	/**
	 * Gets all file signatures, singned or unsigned of this file
	 * 	
	 * @return FileSignature
	 */
	public function signatures()
	{
	    return $this->hasMany(FileSignature::class);
    }

    /**
     * Gets all file signatures, singned or unsigned of this file
     *  
     * @return FileSignature
     */
    public function signedSignatures()
    {
        return $this->signatures()->whereNotNull('signed_at');
    }

    /**
     * Gets all signatures that haven't been signed
     * 
     * @return FileSignature
     */
    public function unsignedSignatures()
    {
    	return $this->signatures()->whereNull('signed_at');
    }

    /**
     * Gets all signatures that haven't been signed
     * 
     * @return FileSignature
     */
    public function unsignedSignaturesPri()
    {
    	return $this->signatures()->whereNull('signed_at')->where('signee', '=', 'PR');
    }

     /**
     * Gets all signatures that haven't been signed
     * 
     * @return FileSignature
     */
    public function unsignedSignaturesCo()
    {
    	return $this->signatures()->whereNull('signed_at')->where('signee', '=', 'CO');
    }

    /**
     * Gets all signatures that haven't been signed by Key
     *
     * @param  string $key
     * @return FileSignature
     */
    public function getUnsignedSignaturesBySignee($key)
    {
    	return $this->signatures()->whereNull('signed_at')->where('signee', '=', $key)->get();
    }


}