<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'logs';

	function getUser(){
		if($this->staff_changed){
			return $this->hasOne('App\Models\User', 'id', 'staff_user');
		}else{
			return $this->hasOne('App\Models\Dealer', 'id', 'dealer_user');
		}
	}

	function getFriendlyField(){
		$field = $this->column_name;
		$fields = [
			'statusId' => 'Status',
			'distributorPayable' => 'Distributor Payable',
			'reserveFee' => 'Reserve',
			'discount' => 'Discount'
		];
		return array_key_exists($field, $fields) ? $fields[$field] : $field;
	}

	function getFriendlyValue($old = false){
		$field = $this->column_name;

		$logField = $old ? "old_value" : "new_value";

		$fields = [
			'contracts' => [
				'statusId' => [
					'0' => 'Pending',
					'1' => 'Approved',
					'2' => 'Cancelled'
				]
			]
		];

		$fieldList = $fields[$this->table_name];



		return array_key_exists($field, $fieldList) ? $fieldList[$field][$this->{$logField}] : $this->{$logField};
	}
}