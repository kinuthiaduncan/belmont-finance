<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealerProduct extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dist_products';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];

	public function pmtFactors()
    {
        return $this->hasMany('App\Models\Dealers\DealerProductPmtFactor', 'product_id', 'id')
        	->orderBy('pmt_factor', 'asc');
    }

    public function getPmtFactor($buyRate, $hasAch = 0) {
    	$achValues = $hasAch ? [0, 1] : [0];

		$minPaymentFactor = $this->pmtFactors()
			->where('min_buy_rate', '<=', $buyRate)
			->whereIn('ach_required', $achValues)
			->orderBy('pmt_factor', 'ASC')->first();

		if(!$minPaymentFactor) {
			abort(500, 'Dealer product not set up properly');
		}

		return $minPaymentFactor->pmt_factor;
	}

}
