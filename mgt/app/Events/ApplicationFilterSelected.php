<?php

namespace App\Events;

use App\Events\Event;
use App\Models\ContractApplication;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class ApplicationFilterSelected extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Models\AppExtras\AppFilter $filter, ContractApplication $app)
    {
        $this->filter = $filter;
        $this->app = $app;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
