<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\ContractApplication;

class ApplicationApproved extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ContractApplication $app)
    {
        //
        $this->app = $app;

        $contract = \App\Models\Contract::where('applicationId', $this->app->fca_id)->first();
        $this->contractId = $contract ? $contract->id : null;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
