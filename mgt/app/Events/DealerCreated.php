<?php

namespace App\Events;

use App\Events\Event;
use App\Models\Dealer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class DealerCreated extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Dealer $dealer, \App\Models\DealerCompany $distributor)
    {
        $this->dealer = $dealer;
        $this->distributor = $distributor;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
