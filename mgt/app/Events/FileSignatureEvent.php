<?php

namespace App\Events;

use App\Events\Event;
use App\Models\ContractApplication;
use App\Models\FileSignature;
use App\Models\File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Carbon\Carbon;

class FileSignatureEvent extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    public function fileCreated(File $file)
    {
        $non_sig_file_types = [50, 51];
        $dealer_file_types = [2, 9];
        $consumerFileTypes = [1, 2, 5, 9, 13];

        $applicant_types = ['PR','CO'];
        $app = ContractApplication::findOrFail($file->app_id);

        // add signatures to all file signature types
        if(!in_array($file->file_type, $non_sig_file_types)){
            if ($app->coLastName && $file->file_type != 9) {
                foreach ($applicant_types as &$value) {
                    FileSignature::create([
                        'file_id' => $file->id,
                        'signee' => $value
                    ]);
                }
            } else {
                FileSignature::create([
                    'file_id' => $file->id,
                    'signee' => 'PR'
                ]);
            }

            // create dealer signature entry
            if(in_array($file->file_type, $dealer_file_types)){
                FileSignature::create([
                    'file_id' => $file->id,
                    'signee' => 'DE'
                ]);
            }
        }

        // archive non-uploads and any document that has some signatures on it
        if($file->file_type != 50 && $file->signatures()->whereNotNull('signed_at')) {
            $matchingFiles = File::where('app_id', '=', $file->app_id)
                ->where('file_type', '=', $file->file_type)
                ->where('id', '!=', $file->id)
                ->update(['archived_at' => Carbon::now()->format('Y-m-d h:i:s')]);
        }
    }


    /** 
     * @param  File
     * @return null
     */
    public function fileDeleting(File $file)
    {   
        // remove local file
        if( file_exists( base_path($file->local_path) ) ){
            unlink( base_path($file->local_path) );
        }

        $file->signatures()->delete();
    }
}
