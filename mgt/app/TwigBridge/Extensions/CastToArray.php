<?php namespace App\TwigBridge\Extensions;

use Twig_Extension;
use Twig_SimpleFilter;

/**
 * This file is part of Twig.
 *
 * (c) 2009 Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author Ricard Clau <ricard.clau@gmail.com>
 * @package Twig
 * @subpackage Twig-extensions
 */
class CastToArray extends \Twig_Extension
{
    /**
     * Returns a list of filters.
     *
     * @return array
     */
   public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('cast_array', array($this, 'castToArray')),
        );
    }

    /**
     * Name of this extension
     *
     * @return string
     */
    public function getName()
    {
        return 'cast_array';
    }

    /**
     * CAsts an object as an array
     *
     * @param stClassObj
     * @return array
     */
    public function castToArray($stdClassObject)
    {
        foreach ($stdClassObject->getAttributes() as $key => $value) {
            $response[$key] = $value;
        }
        return $response;
    }

}