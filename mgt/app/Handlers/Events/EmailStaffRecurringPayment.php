<?php

namespace App\Handlers\Events;

use App\Events\ApplicationApproved;
use App\Models\AchPaymentRecurring;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Crypt;
use Mail;
use Log;

class EmailStaffRecurringPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RecurringAchPaymentCreated  $event
     * @return void
     */
    public function handle(ApplicationApproved $event)
    {

        $app = $event->app;

        $payment = AchPaymentRecurring::where('application_id', '=', $app->id)->first();

        if($payment)
        {
            /* $payment->belmont_account_number = $event->belmont_account_number;

            $payment->contract_id = $event->contractId; */

            $payment->save();

            $staffEmail = \App\Models\Setting::where('name', '=', 'paymentEmails')->first()->value;

            $staffEmail = explode ( ",", $staffEmail );
            $emails = [];
            foreach($staffEmail as $email)
            {
                $emails[$email] = $email;
            }

            $protocol = getenv('PRODUCTION') == "FALSE" ? 'http://' : 'https://';
            
            $url =  $protocol . 'staff.' . getenv('DOMAIN') . '/payments/recurring/' . $payment->id;
            
            Mail::send('emails.staff-recurring-payment', ['url' => $url], function ($m) use($emails, $app){
                $first = $app->firstName;
                $last = $app->lastName;
                $m->to($emails)->subject("ACH ready - $first $last");
            });
        }
    }
}
