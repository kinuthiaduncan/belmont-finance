<?php

namespace App\Handlers\Events;

use App\Services\AppService;

use App\Events\AppWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FlagApplication
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AppWasCreated  $event
     * @return void
     */
    public function handle(AppWasCreated $event)
    {
        // call our flag application handler
        $appService = new AppService($event->app->id);
        $appService->checkForFlags();
    }
}