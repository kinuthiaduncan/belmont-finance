<?php

namespace App\Handlers\Events;

use \App\Events\DealerCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\Libraries\Mailchimp;
use Log;

class AddDealerToMailchimp
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(DealerCreated $event)
    {
        $chimp = new Mailchimp();
        $mergeFields = [
            'FNAME' => explode(' ', trim($event->dealer->name))[0],
            'LNAME' => isset(explode(' ', trim($event->dealer->name))[1]) ? explode(' ', trim($event->dealer->name))[1] : '',
            'ADMIN' => $event->dealer->hasRole('admin') ? 1 : 0,
            
            'INDUSTRY' =>  $event->distributor->product ? $event->distributor->product->name : ''
        ];

        $chimp->addToList($event->dealer->email, getenv('MAILCHIMP_DEALER_LIST'), $mergeFields);
    }
}
