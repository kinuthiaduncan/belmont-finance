<?php

namespace App\Handlers\Events;

use App\Events\ApplicationFilterSelected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class ApplyFilterAction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationFilterSelected  $event
     * @return void
     */
    public function handle(ApplicationFilterSelected $event)
    {
        //
        if($event->filter->filter_name == 'Cancelled') {
            $event->app->status = 2;
            $event->app->save();
        }
    }
}
