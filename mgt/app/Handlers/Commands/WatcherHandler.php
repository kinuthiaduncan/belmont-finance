<?php namespace App\Handlers\Commands;

use Artisan;
use \App\Models\Contract;
use \App\Models\Dealer;
use \App\Models\DealerCompany;
use Mail;
use \App\Models\Setting;
use \App\Events\ApplicationApproved;

class WatcherHandler
{
    public function __construct($appResult, $application)
    {

        echo 'APPID: ' . $application->id .  PHP_EOL;
        $this->appResult = $appResult;
        $this->application = $application;

        try{
            $this->user = Dealer::find($this->application->dealerId);
        }
        catch (\Exception $e) {
            echo 'Exception line ' . $e->getLine() . ' (still running): ',  $e->getMessage(), "\n";
        }

        // emails for staff
        //$this->staffEmail = Setting::find('applicationEmails')->value;
        $this->staffEmail = 'loandept@belmontfinancellc.com';

        // initiate emails to send to dealers - if no email, make sure staff gets email instead
        if($this->user){
            $this->dealerEmails = $this->user->company->users()->where('dist_dealer_relations.admin', '=', '1')->get()->lists('name', 'email')->all();
            $this->dealerEmails[$this->user->email] = $this->user->name;
            if(!$this->dealerEmails) {
                $this->dealerEmails = $this->staffEmail;
                $this->staffEmail = [];
            }
        }else{
            $this->dealerEmails = $this->staffEmail;
            $this->staffEmail = [];
        }
    }

    // this is for a new application or a status change of an application
    public function appStatusChange(){
        $storedAdminEmails = Setting::find('applicationEmails')->value;
        $storedAdminEmails = explode ( ",", $storedAdminEmails );
        $emails = [];
        foreach($storedAdminEmails as $email)
        {
            $emails[$email] = $email;
        }

        // if application number is unique // try to find existing contract
        if( isset($this->appResult['ApplicationNumber']) )
        {
            $existingContract = Contract::where('applicationId', '=', $this->appResult['ApplicationNumber'])->first();
            if($existingContract === null){
                echo 'Contract not found by app number ' . PHP_EOL;
            }
            else{
                echo 'Contract ID: ' . $existingContract->id . PHP_EOL;
            }
        } else{
            echo 'appresult appnumber not set ' . PHP_EOL;
            $existingContract = null;
        }

        if(is_null($existingContract) && isset($this->appResult['AccountNumber']) && $this->appResult['AccountNumber'])
        {
            $existingContract = Contract::where('accountNumber', '=', $this->appResult['AccountNumber'])->orWhere('accountNumber', '=', (int)$this->appResult['AccountNumber'])->first();
             if($existingContract === null){
                echo 'Contract not found by account number' . PHP_EOL;
            }
        }

        // prepare messages for email and contract
        if( isset($this->appResult['ApplicationStatus']) )
        {
            $message = $this->application->firstName . ' ' . $this->application->lastName . ' issued ' . $this->appResult['ApplicationStatus'] . " status\n";
        }
        else
        {
            $message = $this->application->firstName . ' ' . $this->application->lastName . ' application received.';
        }



        // get stipulations
        $stipulations = [];
        if( isset($this->appResult['ApprovalConditions']) ){
            foreach($this->appResult['ApprovalConditions'] as $condition)
            {
                foreach($condition as $item) {
                    if(isset($item['TextData'])) {
                        $stipulations[] = $item['TextData'];
                    }
                }               
            }
        }

        if( is_null($existingContract) )
        {
             echo 'Creating new contract ' . PHP_EOL;
        // distributor company id if filled in by belmont staff
          if(!$this->user && isset($this->appResult['OriginalApplication']) && $this->appResult['OriginalApplication']['DealerNumber'])
          {
            $dealer = DealerCompany::where('account_number', $this->appResult['OriginalApplication']['DealerNumber'])->first();
            if($dealer)
            {
                $distId = $dealer->dist_id;
            }
            else
            {
                $distId = getenv('BELMONT_DEALER_ID');

                // log belmont that there was no dealer for this one
                Mail::send('emails.staff-no-dealer', ['name' => $this->appResult['FirstName'] . ' ' . $this->appResult['LastName'] , 'dealerNumber' => $this->appResult['OriginalApplication']['DealerNumber']], function ($m){
                    $m->to('loandept@belmontfinancellc.com')->subject('Dealer not found');
                });

            }
          }
        elseif($this->user)
        {
            $distId = $this->user->company->dist_id;
        }
        else{
            $dealer = DealerCompany::where('account_number', $this->application->distributor_account)->first();
            if($dealer) {
                $distId = $dealer->dist_id;
            } else {
                $distId = getenv('BELMONT_DEALER_ID');
            }
            
        }

          $contract = new Contract;
          $contract->customerName = isset($this->appResult['FirstName']) && isset($this->appResult['LastName']) ? $this->appResult['FirstName'] . ' ' . $this->appResult['LastName'] : 'N/A';
          $contract->applicationId = isset($this->appResult['ApplicationNumber']) ? (string) $this->appResult['ApplicationNumber'] : '';
          $contract->dist_user_id = $this->application->dealerId;
          $contract->distributorId = $distId;
          $contract->customerName = $this->application->firstName . ' ' . $this->application->lastName;
          $contract->applicationDate = date("Y-m-d H:i:s");
          $contract->statusId = 1;
          $contract->amountFinanced = isset($this->appResult['AmountRequested']) ? $this->appResult['AmountRequested']: $contract->amountFinanced;
        }else{
            $contract = $existingContract;
        }

        $contract->accountNumber = isset($this->appResult['AccountNumber']) ? $this->appResult['AccountNumber'] : $contract->accountNumber;

        // set email view to default
        $emailView = 'emails.appupdate';

        if(isset($this->appResult['ApplicationStatus'])){
            if(strtolower($this->appResult['ApplicationStatus']) == "approved" )
            {
                echo 'contract approved' . PHP_EOL;
                $contract->statusId = 1;
                $contract->statusText = $message;
                $emailView = 'emails.dealer.app-approved';

                // reduce the ssn
                $this->application->ssn = substr($this->application->ssn, -4);
                $this->application->co_ssn = substr($this->application->co_ssn, -4);
                $this->application->status = 1;
            }
            elseif( strtolower($this->appResult['ApplicationStatus']) == "pending" )
            {
                echo 'contract pending' . PHP_EOL;
                $contract->statusId = 0;
                $this->application->status = 0;
                $contract->statusText = $message;
            }
            elseif( strtolower($this->appResult['ApplicationStatus']) == "declined" )
            {
                echo 'contract denied' . PHP_EOL;
                $contract->statusId = 3;
                $this->application->status = 3;
                $emailView = 'emails.dealer.app-denied';

                // reduce the ssn
                $this->application->ssn = substr($this->application->ssn, -4);
                $this->application->co_ssn = substr($this->application->co_ssn, -4);

                $contract->statusText = $message;
                $contract->delete();
            }
            else
            {
                echo 'contract cancel' . PHP_EOL;
                $contract->statusId = 2;
                // reduce the ssn
                $this->application->ssn = substr($this->application->ssn, -4);
                $this->application->co_ssn = substr($this->application->co_ssn, -4);
                $this->application->status = 2;
            }
        }

        $this->application->buy_rate = isset($this->appResult['FundingRatio']) ? $this->appResult['FundingRatio'] : $this->application->buy_rate;
        $contract->buy_rate = isset($this->appResult['FundingRatio']) ? $this->appResult['FundingRatio'] : $contract->buy_rate;
        $contract->amountFinanced = isset($this->appResult['AmountRequested']) ? $this->appResult['AmountRequested']: $contract->amountFinanced;
        $contract->dist_user_id = $this->application->dealerId;

        if($this->application->status != 3)
        {
            $contract->save();
        }


        // make sure FCA's application id is assigned to application on our end
        $this->application->fca_id = isset( $this->appResult['ApplicationNumber'] ) ? $this->appResult['ApplicationNumber'] : '';

        // email both distrib and belmont user(s) application result
        $appStatus = isset($this->appResult['ApplicationStatus']) ? $this->appResult['ApplicationStatus'] : '';
        $this->application->lastStatus = $message . '; ' . $appStatus;
        $this->application->save();

        $appStatus = strtolower($appStatus) == "declined" ? 'Denied' : $appStatus;
        if($this->dealerEmails){
            Mail::send($emailView, ['user' => $this->user, 'app' => $this->application, 'stipulations' => $stipulations], function ($m) use($appStatus){
                $appNumber = isset( $this->appResult['ApplicationNumber'] ) ? $this->appResult['ApplicationNumber'] : '';
                $m->to($this->dealerEmails)
                    ->bcc($this->staffEmail)
                    ->subject('Application ' . $appStatus . ' for ' . $this->application->firstName . ' ' . $this->application->lastName);
            });
        }
    }


    // when status of contract disposition changes, eg receive updates on remittance
    public function appDisposition(){

        // check for contract
        $appNumber = $this->appResult['ApplicationNumber'] ? $this->appResult['ApplicationNumber'] : '';
        $contract = Contract::where('applicationId', '=', $this->appResult['ApplicationNumber'])->orWhere('applicationId', '=', ltrim($this->appResult['ApplicationNumber'], '0'))->first();

        // check for contract by account number
        if($contract === null && isset($this->appResult['DispositionStatus']) && isset($this->appResult['DispositionStatus']['RemittanceDetail']) && isset($this->appResult['DispositionStatus']['RemittanceDetail']['AccountNumber']) )
        {
            $accountNum = $this->appResult['DispositionStatus']['RemittanceDetail']['AccountNumber'];
            $deLetter = in_array(strtolower(substr($accountNum, 0, 1)), ['s', 'x', 'r']) ? substr($accountNum, 1) : $accountNum;
            echo 'Finding contract ' . $accountNum . PHP_EOL;
            $contract = Contract::where('accountNumber', '=', $accountNum)->orWhere('accountNumber', '=', $deLetter)->first();
        }

        // look for funding status, problems, stipulations, etc
        if($contract){
            $contract->buy_rate = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['BuyRatePercent']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['BuyRatePercent'] : $contract->buy_rate;

            if(isset($this->appResult['DispositionStatus']['VerificationProblems']['Problem'])){
                if( gettype($this->appResult['DispositionStatus']['VerificationProblems']['Problem']) == "array" && count($this->appResult['DispositionStatus']['VerificationProblems']['Problem']) > 1){
                    $problems = $this->appResult['DispositionStatus']['VerificationProblems']['Problem'];
                    $problemArray = [];
                    foreach($problems as $problem){
                        $problemArray[] = ucfirst(strtolower(trim($problem['Description'])));
                    }
                    $contract->disposition_status = 'PENDING - ' . implode(', ', $problemArray);

                }
                else{
                    $contract->disposition_status = 'PENDING - ' . ucfirst(strtolower(trim( $this->appResult['DispositionStatus']['VerificationProblems']['Problem']['Description'] )));
                }

            }
            else{
                if(isset($this->appResult['DispositionStatus']['DispositionDescription'])){
                    $contract->disposition_status = $this->appResult['DispositionStatus']['DispositionDescription'];
                }else{
                    $contract->disposition_status = "";
                }

                // mark application when funded
                if( isset($this->appResult['DispositionStatus']['DispositionCode']) && 
                    $this->appResult['DispositionStatus']['DispositionCode'] == 'FUND'
                ){
                    $this->application->funding_status = 2;
                    $this->application->save();
                    $this->emailDealerFunding();
                }

                // mark application remittance paid
                if( isset($this->appResult['DispositionStatus']['DispositionCode']) && 
                    $this->appResult['DispositionStatus']['DispositionCode'] == 'PAID'
                ){
                    $this->application->funding_status = 3;
                    $this->application->save();
                }
            }

            $contract->accountNumber = isset($this->appResult['DispositionStatus']['RemittanceDetail']['AccountNumber']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['AccountNumber'] : $contract->accountNumber;
            $contract->disposition_date = isset($this->appResult['DispositionStatus']['DispositionChangeDate']) ? date( 'Y-m-d', strtotime( $this->appResult['DispositionStatus']['DispositionChangeDate'] ) ) : null;
            $contract->promo_rate = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['PromotionRatePercent']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['PromotionRatePercent'] : $contract->promo_rate;
            $contract->promo_code = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['PromotionCode']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['PromotionCode'] : $contract->promo_code;
            $contract->discount = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['BuyRateDiscount']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['BuyRateDiscount'] : $contract->discount_other;
            $contract->discount_promo = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['PromotionDiscount']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['PromotionDiscount'] : $contract->discount_promo;
            $contract->fl_doc_fee = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['FLDocFee']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['FLDocFee'] : $contract->fl_doc_fee;
            $contract->wire_charge = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['WireCharges']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['WireCharges'] : $contract->wire_charge;
            $contract->loan_charge = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['LoanCharges']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['LoanCharges'] : $contract->loan_charge;
            $contract->transfer_amount = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['Transfer']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['Transfer'] : $contract->transfer_amount;
            $contract->acquisitionFee = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['AcquisitionFee']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['AcquisitionFee'] : $contract->acquisitionFee;
            $contract->reserveFee = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['ReserveAmount']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['FeeDetails']['ReserveAmount'] : $contract->reserveFee;
            $contract->distributorPayable = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['NetAmountDue']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['NetAmountDue'] : $contract->distributorPayable;
            $contract->current_reserve_amount = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['CurrentReserveAmount']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['CurrentReserveAmount'] : $contract->current_reserve_amount;
            $contract->amountFinanced = isset($this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['ContractAmount']) ? $this->appResult['DispositionStatus']['RemittanceDetail']['ContractDetails']['ContractAmount'] : $contract->amountFinanced;
            if(!$contract->applicationId){
                $contract->applicationId = $this->appResult['ApplicationNumber'];
            }
            $contract->save();
        }else{
            echo 'No contract found for disposition update';
        }
    }


    public function appInvalid($message){
        Mail::send('emails.appupdate', ['user' => $this->user, 'body' => (string)$message], function ($m){
            $name = $this->user ? $this->user : 'NO USER';
            $m->to($this->dealerEmails)->bcc($this->staffEmail)->subject('# ' . $this->appResult['ApplicationNumber'] .' (' . $this->appResult['FirstName'] . ' ' . $this->appResult['LastName'] . ') Error Occurred');
        });
    }

    public function emailDealerFunding(){
        Mail::send('emails.dealer.app-funded', ['app' => $this->application], function ($m){
            $m->to($this->dealerEmails)->subject('# ' . $this->appResult['ApplicationNumber'] .' (' . $this->appResult['FirstName'] . ' ' . $this->appResult['LastName'] . ') Funded');
        });
    }
}
