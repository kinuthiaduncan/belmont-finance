<?php namespace App\Services;

use \App\Models\ContractApplication;
use Mail;
	
// try to use this class for all Sales Slip changes in the futrure changes
class SalesSlipService{

	public $app;

	// load our application and authenticate it if necessary
	public function __construct($appId, $dealer = null)
	{
		$this->app = ContractApplication::findOrFail($appId);
		if($dealer) {
			// authenticate dealer
			$this->authDealer($dealer);
		}
	}

	// authenticate that the dealer is correct, including admin dealers
	// returns multiple times if true but aborts if false, requires Dealer model as a parameter
	private function authDealer(\App\Models\Dealer $dealer) {
		if($this->app->dealerId) {
			if($dealer->id == $this->app->dealerId) {
				return true;
			} else {
				// get dealer model from app
				$appDealer = \App\Models\Dealer::findOrFail($this->app->dealerId);
				if( $dealer->admin && $dealer->company->id == $appDealer->company->id ) {
					return true;
				}
			}
		}

		// try to authenticate by company id
		if( $dealer->admin && $dealer->company->account_number == $this->app->distributor_account ) {
			return true;
		}

		// all auth attempts have failed, abort
		abort(403, 'Invalid access.');
	}
}