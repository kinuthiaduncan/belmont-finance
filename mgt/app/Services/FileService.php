<?php namespace App\Services;

use \App\Models\File;
use \Illuminate\Http\Request;
use \App\Models\TemporaryFile;
use FPDI;
use \Carbon\Carbon;
use Mail;
use Log;
	
// use this class for all file methods
class FileService{

    private $fileSignatureService;

    public function __construct()
    {
    }

    // creates a temporary file and returns the temporary id
	public static function storeTemporary(Request $request, $dealer)
	{	
		// error message if no file sent
		if(!$request->hasFile('file')) {
		    return response()->json(['No file sent'], 400);
		}

		$file = $request->file('file');

		// always make sure path exists...
		$path = getenv('TEMP_STORAGE') . 'uploads/';
		if (!file_exists($path)) {
		    mkdir($path, 0755, true);
		}

		$fileName = time() . '-' . $file->getClientOriginalName();

		$file->move( $path, $fileName );

		// create temp file record
		$tempFile = \App\Models\TemporaryFile::create([
			'dealer_id' => $dealer->id,
			'path' => $path . $fileName
		]);

  		return $tempFile;
	}


	// take temporary files and assign them to an application or generic dealer upload file
	public function assignTemporary($tempIds, $app, $description)
	{
		$pdfFilePath = $this->condenseToPdf($tempIds, $app);

		$file = File::create([
			'app_id' => $app->id,
			'local_path' => $pdfFilePath,
			'file_type' =>  50,
			'name' => $description
		]);

		$protocol = getenv('APP_ENV') == "production" ? 'https://' : 'http://';

        $url = $protocol . 'staff.' . getenv('DOMAIN') . '/files/' . $file->id . '/review';

		Mail::send('emails.staff.file-uploaded', ['app' => $app, 'description' => $description, 'url' => $url], function ($m) use ($app) {
       	    $m->to('loandept@belmontfinancellc.com')->subject('File uploaded - #' . $app->fca_id);
       	});
	}


	// return an array of base64 thumbnails from a given pdf file
	// accepts a File model object
	public static function getThumbnails($file)
	{
		// get number of pages in pdf file
		$imCount = new \Imagick();
		$imCount->pingImage(base_path($file->local_path));
	
		$thumbnails = [];		

		// loop over pages in pdf
		for($i = 0; $i < $imCount->getNumberImages(); $i++) {
			$im = new \Imagick(base_path($file->local_path) . '[' . $i . ']');
		    $im->setImageFormat("jpg");
		    $thumbnails[] = 'data:image/jpeg;base64,' . base64_encode($im->getImageBlob());
		}

		return $thumbnails;
	}

	// 
	public function arrangeFiles($request, $file)
	{
		// organize the pages array for easier parsing later
		$pageTypes = [];
		foreach($request->pages as $key => $type) {
			$pageTypes[$type][] = $key + 1;
		}

		$fileNames = [
			'1' => 'Credit Agreement',
			'2' => 'Contract / Sales Slip',
			'5' => 'Notice of Cancellation',
			'13' => 'ACH',
			'11' => 'LCC',
			'51' => 'Other'
		];

	
		foreach($pageTypes as $pageType => $pages) {

			// create a new pdf reference for a new file type
			$pdf = new FPDI();
    		$pdf->SetAutoPageBreak(false, 0);
    		$count = $pdf->setSourceFile(base_path($file->local_path));

			foreach($pages as $page) {
				// basically, just add the page into the current pdf
				Log::info('import page ' . $page . ' of ' . $count);
				$templateId = $pdf->importPage($page);
		        $size = $pdf->getTemplateSize($templateId);
		        if ($size['w'] > $size['h']) {
		            $pdf->AddPage('L', array($size['w'], $size['h']));
		        } else {
		            $pdf->AddPage('P', array($size['w'], $size['h']));
		        }
		        $pdf->useTemplate($templateId);
			}

			$fileName = $file->app_id . '-arr-' . $pageType .'-' . time() .  '.pdf';
			$path = '../documents/contracts/' . Carbon::now()->format('Y/m') . '/';

			if (!file_exists(base_path($path))) {
			    mkdir(base_path($path), 0755, true);
			}

			$pdf->output(base_path($path . $fileName), 'F');

			File::create([
				'app_id' => $file->app_id,
				'local_path' => $path . $fileName,
				'file_type' =>  $pageType,
				'name' => $fileNames[$pageType]
			]);
		}

		// finally, remove old file
		$file->delete();
	}

	private function condenseToPdf($tempIds, $app)
	{
		$images = [
			'png',
            'jpe',
            'jpeg',
            'jpg',
            'gif',
            'bmp'
		];

		// loop through ids, find the temporary file, add them to single pdf
		$pdf = new FPDI();
        $pdf->SetAutoPageBreak(false, 0);

		foreach($tempIds as $tempId) {
			$tempFile = TemporaryFile::find($tempId);

			if($tempFile) {
				$mimeType = $this->get_mime_type($tempFile->path);
					
				// if it's an image, add image to pdf
				if(in_array($mimeType, $images)) {

					// add image to new pdf page
            		$pdf->AddPage('P', 'Legal');
            		$pdf->Image($tempFile->path, 0, 0, 300);
				} else if ($mimeType == 'pdf') {

					// concatenate pdf
					// issue with newer pdfs, fallback convert with ghostscript
					try {
					    $pageCount = $pdf->setSourceFile($tempFile->path);
					}
					catch (\Exception $e) {
					    //convert
					    shell_exec("gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$tempFile->path $tempFile->path");

					    // try again
					    $pageCount = $pdf->setSourceFile($tempFile->path);
					}
					

		            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
						                // import a page
				        $templateId = $pdf->importPage($pageNo);
				        // get the size of the imported page
				        $size = $pdf->getTemplateSize($templateId);
		                
		                if ($size['w'] > $size['h']) {
				            $pdf->AddPage('L', array($size['w'], $size['h']));
				        } else {
				            $pdf->AddPage('P', array($size['w'], $size['h']));
				        }

		                $pdf->useTemplate($templateId);
		            }
		        }
			}

			// remove old file and destroy temp record
			unlink($tempFile->path);
			$tempFile->delete();
		}

		$fileName = $app->id . '-' . $app->lastName . '-' . time() .  '-upload.pdf';
		$path = '../documents/uploaded-contracts/' . Carbon::now()->format('Y/m') . '/';

		if (!file_exists(base_path($path))) {
		    mkdir(base_path($path), 0755, true);
		}

		$outputPath = $path . $fileName;

		$pdf->output(base_path($outputPath), 'F');
		return $outputPath;
	}

	public function get_mime_type($path) {
		$pathArray = explode('.', $path);
		return array_values(array_slice($pathArray, -1))[0];
	}
}