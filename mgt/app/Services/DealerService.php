<?php namespace App\Services;

use \App\Models\Dealer;
	
// try to use this class for all dealer user changes
class DealerService{


	// load our application and authenticate it if necessary
	public function __construct()
	{
	}


	// remove dealer user in a consistent fashion
	// this does not really delete the dealer but removes access from the distributor association
	public function delete(Dealer $dealer, \App\Models\DealerCompany $company, $replaceUser = null)
	{
		// reassign associated contracts
		/*$contracts = Contract::whereDistUserId($id)
			->where('distributorId', '=', $this->user->company->id)
			->update(["dist_user_id" => $this->user->id]);*/


		// search for the best user to replace any existing relationships
		if(!$replaceUser) {
			$replaceUser = $company->users()
				->wherePivot('admin', '=', 1)
				->orderBy('created_at', 'ASC')
				->first();
		}

		// promote DTs (subusers) up a level
		$dealer->companies()
			->newPivotStatement()
			->where('distributor_id', '=', $company->id)
			->where('parent_user_id', '=', $dealer->id)
			->update(['parent_user_id' => $replaceUser->id]);

		// remove dealer's promo settings
		\App\Models\Relations\DealerDenyPromoRelation::where('distributor_id', '=', $company->id)
				->where('user_id', '=', $dealer->id)
				->delete();

		// remove access from company
		$dealer->companies()->detach($company->id);

		// reassign unfinished applications
		$unfinishedApps = \App\Models\ContractApplication::where('dealerId', '=', $dealer->id)
			->whereIn('status', ['0', '1'])
			->where('dist_id', $company->id)
			->where(function ($query) {
		        $query->where('funding_status', '=', 1)
		            ->orWhereNull('funding_status');
		    });

		// update with the first admin from the company
		$unfinishedApps->update(['dealerId' => $replaceUser->id]);


		// $user->delete(); don't actually delete user account anymore?
	}
}