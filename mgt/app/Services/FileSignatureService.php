<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 8/10/18
 * Time: 4:56 AM
 */

namespace App\Services;

use App\Models\ContractApplication;
use App\Models\File;
use App\Models\FileSignature;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FileSignatureService
{
    /**
     * Record file signature
     * @param Request $request
     * @param $file
     * @param $key A two letter key that designates the signing party, either PR, or CO, (Or DE)
     */
    public static function recordFileSignature($file, $key = 'PR'){
        $empty_signatures = FileSignature::where('file_id', $file->id)
            ->where('ip_address', null)
            ->where('signee', '=', $key)
            ->get();

        if( $empty_signatures) {
            foreach ($empty_signatures as &$empty_signature) {
                $empty_signature->update([
                    'signed_at' => Carbon::now(),
                    'ip_address' => request()->ip()
                ]);
            }
        }
    }

    /**
     * Get all signatures made to a file
     * @param $file
     * @return mixed
     */
    public function getFileSignature($file){
        $signatures = FileSignature::where('file_id', $file->id)->get();
        return $signatures;
    }

    /**
     * Get all files and their signatures info
     * @return mixed
     */
    public function getAllFilesSignatures(){
        $files = File::where('file_type','!=',50)
            ->with('signature')
            ->get();
        return $files;
    }
}