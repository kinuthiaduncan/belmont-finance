<?php namespace App\Services;

use \App\Models\ContractApplication;
use Mail;
use DB;
	
// try to use this class for all application changes
class AppService{

	public $app;

	// load our application and authenticate it if necessary
	public function __construct($appId, $dealer = null)
	{
		$this->app = ContractApplication::findOrFail($appId);
		if($dealer) {
			// authenticate dealer
			$this->authDealer($dealer);
		}
	}



	// cancels an application, emailing staff
	// expects ContractApplication parameter
	// returns boolean 1;
	public function cancel() {
		$this->app->status = 2;
		$this->app->save();

		// also set the contract to cancel
		$this->app->contract->statusId = 2;
		$this->app->contract->save();

		// send notice to staff
        $staffEmail = ['loandept@belmontfinancellc.com', 'funding@belmontfinancellc.com'];
        Mail::send('emails.staff.app-cancelled', ['app' => $this->app], function($m) use($staffEmail){
            $m->to($staffEmail)
            	->from('noreply@belmontfinancellc.com')
            	->subject('Cancellation - ' . $this->app->firstName . ' ' . $this->app->lastName);
        });

        return true;
	}


	// removes an application's sales slip and any files so a dealer can recreate it
	// returns boolean 1;
	public function resetSalesSlip() {

		// if funding status has started or is finished, we don't want them to do this...
		if($this->app->funding_status == 1 || $this->app->funding_status == 2) {
			abort(403, 'Action not allowed');
		}

		// these are all file types (signed and unsigned) for sales slips, ach, notice of cancellation, completion
		$fileTypes = [2,4,5,6,7,8,9,10,11,12,13,14];

		$files = \App\Models\File::where('app_id', '=', $this->app->id)
		 	->whereIn('file_type', $fileTypes)
		 	->get();

		 // delete any files associated post sales slip stage
        foreach($files as $file) {
            if( file_exists ( base_path($file->local_path) ) ) {
                unlink(base_path($file->local_path));
            }
            
            $file->delete();
        }

        // delete the sales slip record
        $this->deleteSalesSlip();

        return true;
	}


	// remove an application's sales slip record from the database
	// this does not remove any stored files, just the db record
	// returns boolean for complete
	public function deleteSalesSlip()
	{
		// first, remove any items of the sales slip
		$salesSlipItems = \App\Models\SalesSlipItems::where('sales_slip_id', '=', $this->app->salesSlip->id);

		foreach($salesSlipItems as $item) {
			$item->delete();
		}

		// then, delete the sales slip
		$this->app->salesSlip->delete();

		return true;
	}


	// look through an application and flag it for any errors
	public function checkForFlags()
	{
		$this->flagDuplicateEmails();
	}

	// flag any duplicate emails
	private function flagDuplicateEmails()
	{
		// if key already exists, delete old one
		\App\Models\AppExtras\AppFlag::where('app_id', '=', $this->app->id)
			->where('flag_key', '=', 'DUPL_EMAIL')->delete();

		$appIds = DB::select('select id from contractapp where ((email <> "" and (email = :email or email = :coEmail)) or (coEmail <> "" and (coEmail = :email_two or coEmail = :coEmail_two))) and id != :id',
				['email' => $this->app->email, 'email_two' => $this->app->email,  'coEmail' => $this->app->coEmail, 'coEmail_two' => $this->app->coEmail, 'id' => $this->app->id]
			);

		if($appIds) {
			// create flag and store
			// use an {?app:4124124?} syntax to parse with js in template for linking
			array_walk($appIds, function(&$id) {
				$id = '{?app:' . $id->id .  '?}';
			});
			$this->createFlag('DUPL_EMAIL', $appIds);
		}

		return true;
	}

	private function createFlag($flagKey, $data = false)
	{
		// keep flag keys 10 char or less

		if($flagKey == 'DUPL_EMAIL') {
			$message = 'Applicant or coapplicant email found on other applications: ' . implode(', ', $data);
		}

		$flag = \App\Models\AppExtras\AppFlag::create([
			'app_id' => $this->app->id,
			'flag_key' => $flagKey,
			'note' => $message
		]);

		return $flag;
	}


	// authenticate that the dealer is correct, including admin dealers
	// returns multiple times if true but aborts if false, requires Dealer model as a parameter
	private function authDealer(\App\Models\Dealer $dealer) {
		if($this->app->dealerId) {
			if( $dealer->id === $this->app->dealerId){
	           // is authenticated
	        }else{
	            // check for admin user
	            if($dealer->hasRole('admin', $this->app->distributor) && $dealer->companies()->where('dist_companies.id', '=', $this->app->distributor->id)->exists()) {
	                // authenticated
	            } else {
	                abort(403, 'Authorization error');
	            }
	        }
		}

		// try to authenticate by company id
		if( $dealer->admin && $dealer->company->account_number == $this->app->distributor_account ) {
			return true;
		}

		// all auth attempts have failed, abort
		abort(403, 'Invalid access.');
	}
}