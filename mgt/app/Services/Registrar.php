<?php namespace App\Services;

//this registar handles both registrations for dealer and staff users

use App\Models\User;
use App\Models\Dealer;
use App\Models\DealerCompany;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Auth;
use Log;
use Session;
use Hash;
use App\Events\DealerCreated;

use Request;

class Registrar implements RegistrarContract {

	public function __construct(){
		$url = Request::url();
		$subdomain = explode("/", array_shift((explode(".", $url ))) );
		$subdomain = array_pop($subdomain);
		$this->context = $subdomain;

		if($subdomain === "dealer"){
			if($pseudoId = Session::get('admin-pseudo')){
				$this->user = \App\Models\Dealer::findOrFail($pseudoId);
			}else{
				$this->user = Auth::guard('dealer')->user();
			}
		}
		elseif($subdomain === "accounts"){
			$this->user = Auth::guard('consumer')->user();
		}
		elseif($subdomain === "staff"){
			$this->user = Auth::guard('web')->user();
		}
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		if($this->context == "staff")
		{

			//creating new distributor account
			if(isset($data['_distrib']) && $data['_distrib'])
			{
				// a distributor reset whole company account
				if(isset($data['_reset']) && $data['_reset']){
					$validate = Validator::make($data, [
						'email' => 'required|email|max:255|unique:dist_users',
					]);
				}
				else{
					// create a new distributor
					$validate = Validator::make($data, [
						'company_name' => 'required|max:255',
						'owner_name' => 'max:255',
						'admin' => 'boolean',
						'account_number' => 'required|max:100|unique:dist_companies',
						'email' => 'email|max:255|unique:dist_users',
					]);
				}
			}

			//creating staff account
			else{
				$messages = [
				     'role.required' => 'You must specify at least one user role'
				 ];

				$validate = Validator::make($data, [
					'name' => 'required|max:255',
					'email' => 'required|email|max:255|unique:users',
					'password' => 'confirmed|min:6',
					'role' => 'required|min:1',
				], $messages);
			}
		}
		else{
			// if it's a distributor creating a new user
			$validate = Validator::make($data, [
				'password' => 'confirmed|min:6',
				'email' => 'required|email|max:255|unique:dist_users',
			]);
		}

		return $validate;
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		if($this->context == "staff")
		{
			//creating new distributor admin account
			if(isset($data['_distrib']) && $data['_distrib'])
			{

				// if this is a new company or existing company
				if(!isset($data['_company_id'])){
					$companyData = [
						'company_name' => $data['company_name'],
						'account_number' => $data['account_number'],
						'owner' => $data['owner_name'],
						'product_id' => $data['product_id'],
						'dealer_status' => 'Approved'
					];

					$company = new DealerCompany($companyData);
					$company->save();
				}else{
					$company = DealerCompany::findOrFail($data['_company_id']);
				}

				// allow to create distributor company without email address
				if(isset($data['email']) && $data['email'])
				{

					$userData = [
						'email' => $data['email'],
						'admin' => 1,
						'username' => NULL,
					];

					try{
						$dealer = Dealer::create($userData);
						$company->users()->save($dealer);
					}
					catch(\Illuminate\Database\QueryException $e){
						if(!isset($data['_company_id'])){
							$company->delete();
						}
						abort(403, $e);
					}

					// send email for password resets
					Password::dealer()->sendResetLink(['email' => $dealer->email], function($message) {
					    $message->subject('Password reminder');
					});

					\Event::fire(new DealerCreated($dealer));

				}

				return $company;
			}
			else{
				// create normal staff account
				$userRoles = [
					'admin' =>  false,
					'loan' =>  false,
					'loan_manager' => false,
					'csr' => false,
					'csr_manager' => false,
					'distributor' => false,
					'distributor_manager' =>  false
				];

				//fill in user roles
				if(isset($data['role'])){
					if(is_array($data['role']))
					{
						foreach($data['role'] as $value)
						{
							if(array_key_exists($value, $userRoles))
							{
								$userRoles[$value] = true;
							}
						}
					}
					else{
						if(array_key_exists($data['role'], $userRoles))
						{
							$userRoles[$data['role']] = true;
						}
					}
				}

				$userData = array_merge($userRoles,
					[
						'name' => $data['name'],
						'email' => $data['email'],
						'password' => bcrypt($data['password']),
					]
				);

				$user = User::create($userData);

				if(!$data['password'] || $data['password'] == ""){
					// send email for password resets
					Password::user()->sendResetLink(['email' => $user->email], function($message) {
					    $message->subject('Password reminder');
					});
				}

				return $user;
			}
		}
		else{
			// if it's a distributor creating user roles

			$user = $this->user;
			$companyId = $user->company->id;

			// if not admin or if checked for subuser
			$isSubuser = ( isset($data['subuser']) && $data['subuser'] ) || !$user->hasRole('admin') ? true : false;


			if($isSubuser){
				$parentId = $user->id;
			}else{
				$parentId = null;
			}

			if(!isset($data['password'])){
				$userData = [
					'email' => $data['email'],
					'username' => NULL
				];
			}else{
				$userData = [
					'email' => $data['email'],
					'password' => Hash::make($data['password']),
					'username' => NULL,
				];
			}

			$dealer = Dealer::create($userData);
			$dealer->admin = isset($data['admin']) ? $data['admin'] : 0;
			$dealer->company_id = $companyId;
			$dealer->parent_user_id = $parentId;
			$dealer->user_status = "Approved";
			$dealer->name = $data['name'];
			$dealer->save();

			// send email for password resets
			if(!isset($data['password'])){
				Password::dealer()->sendResetLink(['email' => $dealer->email], function($message) {
				    $message->subject('Belmont Finance - Set password');
				});
			}

			// add to mailchimp
			\Event::fire(new \App\Events\DealerCreated($dealer));

			return $dealer;
		}



	}

}
