<?php namespace App\Services;

use \App\Models\ContractApplication;
use FPDI;
use \App\Libraries\Contracts\Traits\PaperworkTrait;


// try to use this class for all application changes
class PaperworkService{
	use PaperworkTrait; 

	public $app;
	public $options;

	// load our application and authenticate it if necessary
	public function __construct(ContractApplication $app, $dealer)
	{
		$this->app = $app;

		if($dealer) {
			// authenticate dealer
			$this->authDealer($dealer);
			$this->dealer = $dealer;
		}
	}



	/**
	 * Look over the distributor's settings to ensure they are set up properly
	 * @return string Return a message if 
	 */
	public function failedSetup()
	{
		$failedSetup = false;

		// check for buy rate
		if(is_null($this->app->buy_rate)) {
			$failedSetup = 'This application does not have all the approval information necessary to continue.';
		}

		// check for payment factor if app is revolving
		if($this->app->credit_type == 'Revolving') {
			if($this->dealer->company->product->pmtFactors->isEmpty()) {
				$failedSetup = 'Your distributor is not set up properly for revolving loans.';
			}
		}

		return $failedSetup;
	}


	// complete and generate all pdfs
	public function generateAll($request)
	{
		$session = $request->session();

		$this->options = $session->get('paperwork_options');

		$this->removeUnsigned($this->options);

		// could check for other document types also, but so far just lcc
		if(isset($this->options['docs'])) {
			$this->generateCompletion($session);
		} else {
			$this->generateCredit($session);

			$this->generateContract($session);

			$this->generateACH($session);

			$this->generateCancellation($session);

			$this->generateCompletion($session);
		}

			
	}


	// generate just the sales slip or contract
	public function generateCredit($session)
	{
        // Check which type of agreement is being signed
        $fileId = \App\Libraries\Contracts\ContractLibrary::retrieveCreditAgreement($this->dealer, $session->get('credit_type'));

        // get our installment contract library
        $creditLibrary = new \App\Libraries\Contracts\CreditLibrary($this->app, $fileId);

        // generate a pdf prior to signing with the data all filled in
        $data = $this->app->toArray();

        foreach($data as &$attribute) {
        	if(!$attribute) {
        		$attribute = '';
        	}
        }
		$unsignedPdf = $creditLibrary->addTerms($data);

		return $unsignedPdf;
	}


	// generate just the sales slip or contract
	public function generateContract($session)
	{
        // Check which type of agreement is being signed
        $fileId = $this->retrieveSalesSlip($session->get('sales-slip-data.state'), $this->app);     

        // get our installment contract library
        $salesSlip = new \App\Libraries\Contracts\InstallmentContractLibrary($this->app, $fileId);

        // generate a pdf prior to signing with the data all filled in
		$unsignedPdf = $salesSlip->addTerms($session->get('sales-slip-data'), $this->dealer);

		return $unsignedPdf;
	}

	// generate just the ACH
	public function generateACH($session)
	{
		if($this->app->salesSlip->ach_routing_number) {
	        // get our installment contract library
	        $achLibrary = new \App\Libraries\Contracts\AchLibrary($this->app);

	        // generate a pdf prior to signing with the data all filled in
			$unsignedPdf =  $achLibrary->writeTerms();

			return $unsignedPdf;
		}
	}

	public function generateCancellation($session)
	{
        $files = $this->retrieveNoticeCancellation($session->get('sales-slip-data.state'), $this->app->dealerUser->company);

        $cancellation = new \App\Libraries\Contracts\NoticeCancellationLibrary($this->app, $files);

        // generate a pdf prior to signing with the data all filled in
        $unsignedPdf = $cancellation->addTerms($session->get('sales-slip-data.state'), $this->app->dealerUser);

        return $unsignedPdf;
	}


	public function generateCompletion($session)
	{
		// only for HI and Revolving
		if(($this->dealer->company->product_id == 26 || $this->dealer->company->product_id == 28) && $this->options['completion']) {
			$completionLibrary = new \App\Libraries\Contracts\CompletionLibrary($this->app, $this->app->distributor);
			$firstFile = $completionLibrary->addTerms($this->dealer, $this->dealer->company);
			$secondFile = $completionLibrary->addFinalTerms($firstFile->local_path, $session->get('sales-slip-data'));
			
			return $secondFile;

		} else {
			return false;
		}

	}


	/**
	 * Removes any files that have an unsigned signature
	 * 
	 * @return boolean
	 */
	public function removeUnsigned()
	{
		if(isset($this->options['docs'])) {
			\App\Models\File::where('app_id', $this->app->id)
				->where('file_type', '=', 9)
	        	->has('unsignedSignatures')
	        	->delete();
		} else {
			\App\Models\File::where('app_id', $this->app->id)
	        	->has('unsignedSignatures')
	        	->delete();
		}

        return true;
	}


	// takes existing documents of this app and combines them for printing
	public function combinePdfs()
	{
		$files = \App\Models\File::where('app_id', '=', $this->app->id)
			->whereIn('file_type', [1, 2, 5, 9, 13])
			->where(function($q){
				$q->has('unsignedSignaturesPri')
					->orHas('unsignedSignaturesCo');
			})
			->get();

		// loop through files and combine them
		$pdf = new FPDI();
        $pdf->SetAutoPageBreak(false, 0);

		foreach($files as $file) {
			$pageCount = $pdf->setSourceFile(base_path($file->local_path));

            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
				                // import a page
		        $templateId = $pdf->importPage($pageNo);
		        // get the size of the imported page
		        $size = $pdf->getTemplateSize($templateId);
                
                if ($size['w'] > $size['h']) {
		            $pdf->AddPage('L', array($size['w'], $size['h']));
		        } else {
		            $pdf->AddPage('P', array($size['w'], $size['h']));
		        }

                $pdf->useTemplate($templateId);
            }
        }

        $outputPath = sys_get_temp_dir() . '/combined-' . $this->app->id . '.pdf';

		$pdf->output($outputPath, 'F');
		return $outputPath;
	}


	// authenticate that the dealer is correct, including admin dealers
	// returns multiple times if true but aborts if false, requires Dealer model as a parameter
	private function authDealer(\App\Models\Dealer $dealer) {
		if($this->app->dealerId) {
			if($dealer->id == $this->app->dealerId) {
				return true;
			} else {
				// get dealer model from app
				$appDealer = \App\Models\Dealer::findOrFail($this->app->dealerId);
				if( $dealer->admin && $dealer->company->id == $appDealer->company->id ) {
					return true;
				}
			}
		}

		// try to authenticate by company id
		if( $dealer->admin && $dealer->company->account_number == $this->app->distributor_account ) {
			return true;
		}

		// all auth attempts have failed, abort
		abort(403, 'Invalid access.');
	}
}