<?php

/**
 * This file is part of the TwigBridge package.
 *
 * @copyright Robert Crowe <hello@vivalacrowe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App;

use Twig_Extension;

/**
 * Access Laravels input class in your Twig templates.
 */
class UrlDecodeFilter extends Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('url_decode', array($this, 'urlDecodeFilter')),
        );
    }

    public function urlDecodeFilter($text)
    {
        return urldecode($text);
    }

    public function getName()
    {
        return 'TwigBridge_Extension_Url_Decode';
    }
}
