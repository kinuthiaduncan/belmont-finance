<?php namespace App\Console\Commands\Documents;

use Illuminate\Console\Command;
use FPDI;

class FlattenPdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'docs:flatten {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flattens a layered pdf to a single layer image pdf for document security';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');

        $im = new \Imagick();

        // local environemnet doesn't recognize path for some reason
        if(getenv('APP_ENV') == 'local') {
            $path = $path;
        } else {
            $path = base_path($path);
        }

        $this->generatePdfImage($path);
    }

    private function generatePdfImage($path) {
        $unfilteredPdf = new \Imagick();
        $unfilteredPdf->setResolution(200, 200);

        $unfilteredPdf->readImage($path);
        $unfilteredPdf->setImageBackgroundColor('white');
        $imageArray = [];

        $pdf = new FPDI();
        $pdf->SetAutoPageBreak(false, 0);

        foreach ($unfilteredPdf as $i => $page) {
            $pdfImage = new \Imagick();
            $pdfImage->newPseudoImage($unfilteredPdf->getImageWidth(), $unfilteredPdf->getImageHeight(), "canvas:white");
            $pdfImage->compositeImage($unfilteredPdf, \Imagick::COMPOSITE_ATOP, 0, 0);
            $pdfImage->setImageFormat('PNG8');
            $pdfImage->setOption('png:color-type', 6);
            $pdfImage->setOption('png:bit-depth', 8);
            $imgPath = $path . $i . '.png';
            $pdfImage->writeImage($imgPath);

            // add image to new pdf page
            $pdf->AddPage('P', 'Legal');
            $pdf->Image($imgPath, 0, 0, 212);
            unlink($imgPath);
            $pdfImage->destroy();
        }

        $pdf->output($path, 'F');
    }
}