<?php

namespace App\Console\Commands\Imports;

use Illuminate\Console\Command;
use App\Models\Dealers\DealerState;
use Excel;
use DB;

class ImportDealerStates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:dealerstates {filepath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import dealer states from csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = microtime(true);

        $handle = fopen($this->argument('filepath'), "r");
        $handleWrite = fopen($this->argument('filepath'). '2', 'w') or die('fopen failed');
        $isFirstLine = true;

        $failedEntries = [];

        // get array of dealer account numbers to dealer ids in memory
        $dealerIdArray = \App\Models\DealerCompany::select('id', 'account_number')
            ->get()->pluck('id', 'account_number');

        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if(!$isFirstLine) {
                    $lineData = explode(',',$line);
                    $accountNumber = str_replace('"', '', $lineData[0]);

                    // make sure we have a dealer record for them, if not add to log array
                    if(isset($dealerIdArray[$accountNumber])) {
                        if($lineData[2][0] == 'T' && $lineData[3][0] == 'T') {
                            fputs($handleWrite, '"' . $dealerIdArray[$accountNumber] . '","' . $lineData[1] . '","' . '1' . "\"\n");
                        }
                    } else {
                        $failedEntries[] = $accountNumber;
                    }
                }
                if($isFirstLine === true) {
                     $isFirstLine = false;
                }
            }
            fclose($handle);
            fclose($handleWrite);

            // run queries and remove files
            DB::connection()->getpdo()->exec('TRUNCATE table dist_states;');
            DB::connection()->getpdo()->exec("LOAD DATA LOCAL INFILE '" . $this->argument('filepath'). '2' . "' INTO TABLE dist_states 
            FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n' (dist_id,state,licensed);");

            unlink($this->argument('filepath'));
            unlink($this->argument('filepath'). '2');


        } else {
            // error opening the file.
        }

        $time_elapsed_secs = microtime(true) - $start;

        echo $time_elapsed_secs;
    }
}
