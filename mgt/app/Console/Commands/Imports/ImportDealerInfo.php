<?php

namespace App\Console\Commands\Imports;

use Illuminate\Console\Command;
use App\Models\Dealers\DealerState;
use Excel;
use DB;

class ImportDealerInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:dealerinfo {filepath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import dealer info from csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = microtime(true);

        $handle = fopen($this->argument('filepath'), "r");
        $handleWrite = fopen($this->argument('filepath'). '2', 'w') or die('fopen failed');
        $isFirstLine = true;

        $failedEntries = [];

        // get array of dealer account numbers to dealer ids in memory
        $dealerIdArray = \App\Models\DealerCompany::select('id', 'account_number')
            ->get()->pluck('id', 'account_number');

        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if(!$isFirstLine) {
                    $lineData = explode(',',$line);
                    $accountNumber = str_replace('"', '', $lineData[0]);

                    // make sure we have a dealer record for them, if not add to log array
                    if(isset($dealerIdArray[$accountNumber])) {
                        $company = \App\Models\DealerCompany::find($dealerIdArray[$accountNumber]);
                        $company->program_id = $lineData[7];
                        $company->address = $lineData[13];
                        $company->address_two = $lineData[14];
                        $company->city = $lineData[15];
                        $company->state = $lineData[16];
                        $company->zip = str_replace('"', '', $lineData[17]);
                        $company->phone = $lineData[18];
                        $company->contact_email = $lineData[20];
                        $company->save();
                    } else {
                        $failedEntries[] = $accountNumber;
                    }
                }
                if($isFirstLine === true) {
                     $isFirstLine = false;
                }
            }
            fclose($handle);
            fclose($handleWrite);

            unlink($this->argument('filepath'));
            unlink($this->argument('filepath'). '2');


        } else {
            // error opening the file.
        }

        $time_elapsed_secs = microtime(true) - $start;

        echo $time_elapsed_secs;
    }
}
