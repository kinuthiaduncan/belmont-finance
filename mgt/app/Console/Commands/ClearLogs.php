<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears application logs older than 30 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $logPath = storage_path() . '/' . getenv('APP_LOG_DIR');
        $files = scandir($logPath);
        $dirs = [];
        foreach($files as $file){
            if( is_dir($logPath . $file) && ($file != '.' && $file != '..') ){
                if( strtotime($file) < strtotime('-30 days') ) {

                    // delete directory and all contents
                    $this->rrmdir($logPath . $file);
                 }
            }
        }
    }

    private function rrmdir($dir) { 
       if (is_dir($dir)) { 
         $objects = scandir($dir); 
         foreach ($objects as $object) { 
           if ($object != "." && $object != "..") { 
             if (is_dir($dir."/".$object))
               rrmdir($dir."/".$object);
             else
               unlink($dir."/".$object); 
           } 
         }
         rmdir($dir); 
       } 
     }
}
