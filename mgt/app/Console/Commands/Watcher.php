<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Nathanmac\Utilities\Parser\Parser;
use Zipper;
use \App\Models\Contract;
use \App\Models\ContractApplication;
use \App\Models\Dealer;
use \App\Handlers\Commands\WatcherHandler;
use Crypt;
use Mail;

class Watcher extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'watcher';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';


	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->monitorPath = getenv('MONITOR_APP_DIR');
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{


		// up maximum execution time to infinite
		set_time_limit ( 0 );

		$files = new \Illuminate\Filesystem\Filesystem();

		$tracker = new \JasonLewis\ResourceWatcher\Tracker();

		$watcher = new \JasonLewis\ResourceWatcher\Watcher($tracker, $files);

		// create listener
		echo 'setting listener on ' . $this->monitorPath . PHP_EOL;
		$listener = $watcher->watch($this->monitorPath);

		echo 'setting listener on ' . storage_path(getenv('MONITOR_UNPROCESSED_DIR')) . PHP_EOL;
		$csvListener = $watcher->watch(storage_path(getenv('MONITOR_UNPROCESSED_DIR')));

		echo 'setting listener on ' . storage_path(getenv('MONITOR_ZIP_DIR')) . PHP_EOL;
		$zipListener = $watcher->watch(storage_path(getenv('MONITOR_ZIP_DIR')));

		$listener->onAnything(function ($event, $resource, $path){
		    switch ($event->getCode()) {
		        case \JasonLewis\ResourceWatcher\Event::RESOURCE_DELETED:
		            echo "{$path} deleted".PHP_EOL;
		            break;
		        case \JasonLewis\ResourceWatcher\Event::RESOURCE_MODIFIED:
		            echo "{$path} modified".PHP_EOL;
		            break;
		        case \JasonLewis\ResourceWatcher\Event::RESOURCE_CREATED:
		            echo "{$path} created".PHP_EOL;

		            // call api request
		            $this->processContract($path);
		            break;
		    }
		});

		$csvListener->create(function($resource, $path){
			$zipper = new \Chumper\Zipper\Zipper;
			$ext = pathinfo($path, PATHINFO_EXTENSION);

			if($ext === "csv"){

				try{
					// get filename without extension
					$baseName = basename($path, ".csv");

					// check file for ANSI only character set, no blank required fields
					$this->fileChecks($path);

					// first rename file
					rename( dirname( $path ) . '/' . $baseName . '.csv' , dirname( $path ) . '/' . $baseName . '.oap');

					$zipper->zip( storage_path(getenv('MONITOR_ZIP_DIR')) . $baseName . '.zip')->add( dirname( $path ) . '/' . $baseName . '.oap' );

					echo 'creating zip';
				}
				catch (\Exception $e) {
				    echo 'Exception (still running): ',  $e->getMessage(), "\n";

				    Mail::send('emails.dev.watcher-log', ['e' => $e->getMessage()], function ($m){
	                    $m->to([getenv('DEV_EMAIL'), 'jpankow@belmontfinancellc.com'])->subject('Watcher error');
	                });
				}

			}
		});


		$zipListener->create(function($resource, $path){
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			if($ext === "zip"){

				// get filename without extension
				$baseName = basename($path, ".zip");
				try{
					//unlink old csv file
					unlink(storage_path(getenv('MONITOR_UNPROCESSED_DIR')) . '/' .  $baseName . '.oap');

					// move completed zip files
					echo $this->monitorPath . $baseName . '.zip';
					rename( dirname( $path ) . '/' . $baseName . '.zip' , $this->monitorPath . $baseName . '.zip');
					
					// fix problem with user permissions, since for some reason this is running under root cron
					if(getenv('SCRIPT_USER')) {
						chown($this->monitorPath . $baseName . '.zip', getenv('SCRIPT_USER'));
					}
				}
				catch (\Exception $e) {
				    echo 'Exception (still running): ',  $e->getMessage(), "\n";
				}
			}
		});

		/*
		|--------------------------------------------------------------------------
		| Start Watching
		|--------------------------------------------------------------------------
		|
		| Now that all the listeners are bound we can start watching. By default
		| the watcher will poll for changes every second. You can adjust this by
		| passing in an optional first parameter. The interval is given in
		| microseconds. 1,000,000 microseconds is 1 second.
		|
		| By default the watch will continue until such time that it's aborted from
		| the terminal. To set a timeout pass in the number of microseconds before
		| the watch will abort as the second parameter.
		|
		*/

		$watcher->start();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [

		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [

		];
	}

	protected function processContract($filepath){
	   if( is_file($filepath) ){
	      $path_parts = pathinfo($filepath);
	      $baseName = basename($filepath, ".xml");

	      if( $path_parts['extension'] == "xml" ){

	        $parser = new Parser();

	        try{
	          	$appResult = $parser->xml(file_get_contents($filepath, true));
	        }
	        catch (\Exception $e) {
				echo '(' . $e->getLine() . ') Parsing Exception (still running): ',  $e->getMessage(), "\n";
		    }

		    // return original application
		    try{
				  // copy files for logging purposes
			      // disable this for security - ssn numbers stored. only log consumer applicants.
		    	if( isset( $appResult['LastName'] ) && strtolower($appResult['LastName']) == 'consumer') {
		    		$newFilepath = storage_path() . '/' . getenv('APP_LOG_DIR') . date('Y-m-d');
					if (!file_exists($newFilepath)) {
						mkdir($newFilepath, 0755, true);
					}

					$newFilepath = $newFilepath . '/' . $baseName . '.xml';
					copy($filepath, $newFilepath);
		    	}

		    	$submitNumber = isset( $appResult['SubmitNumber'] ) ? $appResult['SubmitNumber'] : false;

		    	$application = null;

		    	// first we load app by our submit number
		    	if($submitNumber) {
	            	$application = \App\Models\ContractApplication::find($submitNumber);
	            }

	            // if no results, try by fca application number ( for fca submitted apps )
	            if($application === null) {
	            	$appNumber = isset( $appResult['ApplicationNumber'] ) ? $appResult['ApplicationNumber'] : false;
	            	$application = \App\Models\ContractApplication::whereFcaId($appNumber)->first();
	            }

	            // if still no results, search ONLINE submitted apps by name, and ensure SSN matches
	            // once we do get a match, it should match by application id next time
	            if($application === null && isset($appResult['OriginalApplication']['ApplicantSSN']) && strlen($appResult['OriginalApplication']['ApplicantSSN']) >= 4) {

	            	$lastName = isset( $appResult['OriginalApplication']['ApplicantLastName'] ) ? $appResult['OriginalApplication']['ApplicantLastName'] : '';
	            	$firstName = isset( $appResult['OriginalApplication']['ApplicantFirstName'] ) ? $appResult['OriginalApplication']['ApplicantFirstName'] : '';
	            	$ssn = isset( $appResult['OriginalApplication']['ApplicantSSN'] ) ? $appResult['OriginalApplication']['ApplicantSSN'] : '';

	            	$appQueryBuilder = \App\Models\ContractApplication::where('is_online', '=', '1')
	            		->where('lastName', '=', $lastName)
	            		->where('firstName', '=', $firstName)
	            		->where('status', '=', 0);

	            	// add additional query info if there is a coapp
					if( isset( $appResult['OriginalApplication']['CoApplicantLastName'] ) ) {
						$coAppLastName = $appResult['OriginalApplication']['CoApplicantLastName'];
						$coAppFirstName = $appResult['OriginalApplication']['CoApplicantFirstName'];

						$appQueryBuilder->where('coLastName', '=', $coAppLastName)
							->where('coFirstName', '=', $coAppFirstName);
					}

					$matchingApps = $appQueryBuilder->get();
					foreach($matchingApps as $app) {
						if(str_replace('-', '', $app->ssn) == $ssn || substr($ssn, -4) == $app->ssn) {
							// confirm the application match
							$application = $app;
							echo "matched app by names\n";
							break;
						}
					}
	            }

	            // finally, create a new application if none exists
	            // dispositions may run into no application early on...
	            if($application === null) {
	            	$application = new \App\Models\ContractApplication();

	            	$application = $this->updateApplication($appResult, $application);
	            	echo 'created application - ';
	            }
	        }
	        catch (\Exception $e) {
	            echo '(' . $e->getLine() . ') Error getting application (still running): ',  $e->getMessage(), "\n";
	        }

	        if(isset($application) && $application){
		    	$handler = new WatcherHandler($appResult, $application);

			    // ready to process data
		          try{
		          	switch ($appResult['ResponseType']) {
					    case '1000':
					        $handler->appStatusChange();
					        break;
					    case '1005':
					        $this->updateApplication($appResult, $application);
							$this->updateContract($application);
					        break;
					    case '1010':
					        $handler->appInvalid('Duplicate Application submittal - application cancelled as a result');
					        break;
					    case '1020':
					        $handler->appInvalid('Invalid Dealer Setup - Please Contact Belmont Finance LLC to make sure you are configured properly.');
					        break;
					    case '1021':
					        $handler->appInvalid('Invalid state of residence.');
					        break;
					    case '1030':
					        $handler->appInvalid('Unable to pull credit at this time - an update should be sent later.');
					        break;
					    case '1040':
					        $handler->appInvalid('System Error. Please try again later');
					        break;
					    case '1050':
					        $handler->appInvalid('Application Cancelled');
					        break;
					    case '1060':
					        $handler->appInvalid('Application Deleted');
					        break;
					    case '1070':
					        $handler->appDisposition();
					        break;
					    }
					}

					catch (\Exception $e) {
					    	echo '(' . $e->getLine() . ', ' . $e->getFile() . ') Exception (still running): ',  $e->getMessage(), "\n";
					}
				}
				else
				{
					echo 'application could not be found or created';
				}
			unlink($filepath);
	      }
	    }
	}

	private function updateContract($application) {
		if ($application && $application->fca_id) {
			$contract = Contract::where('applicationId', '=', $application->fca_id)->first();
			if ($contract) {
				$contract->amountFinanced = ($application->amount_financed ?: null);

				if ($application->firstName && $application->lastName) {
					$contract->customerName = $application->firstName . ' ' . $application->lastName;
				}
				$contract->save();
			}
		}
	}

	private function updateApplication($appResult, $application){

		$mapping = [
			'firstName' => 'ApplicantFirstName',
			'lastName' => 'ApplicantLastName',
			'initial' => 'ApplicantMiddleInitial',
			'address' => 'ApplicantAddress1',
			'addressTwo' => 'ApplicantAddress2',
			'city' => 'ApplicantCity',
			'state' => 'ApplicantState',
			'zip' => 'ApplicantZip',
			'dealer_req_by' => ['value' => 'PersonRequesting', 'length' => 64],
			'yrsAtAddress' => 'ApplicantYearsInCurrentResidence',
			'prevAddress' => '',
			'prevCityStateZip' => '',
			'birthdate' => '',
			'homePhone' => 'ApplicantHomePhone',
			'cellPhone' => 'ApplicantCellPhone',
			'ssn' => 'ApplicantSSN',
			'email' => 'ApplicantEmail',
			'numDependents' => 'ApplicantNumberOfDependents',
			'employer' => 'ApplicantEmployer',
			'yrsEmployed' => 'ApplicantYearsOnCurrentJob',
			'milRank' => '',
			'employerAddress' => '',
			'employerCityStateZip' => '',
			'jobTitle' => 'ApplicantPosition',
			'monthlyGrossPay' => 'ApplicantMonthlyIncome',
			'employerPhone' => 'ApplicantEmployerPhone',
			'employerExt' => '',
			'prevEmployer' => '',
			'prevYrsEmployed' => '',
			'prevEmployerAddress' => '',
			'prevEmployerCityStateZip' => '',
			'prevJobTitle' => '',
			'prevEmployerPhone' => '',
			'prevEmployerExt' => '',
			'coFirstName' => 'CoApplicantFirstName',
			'coLastName' => 'CoApplicantLastName',
			'coInitial' => 'CoApplicantMiddleInitial',
			'coBirthdate' => '',
			'coAddress' => 'CoApplicantAddress1',
			'coAddressTwo' => 'CoApplicantAddress2',
			'coCity' => 'CoApplicantCity',
			'coState' => 'CoApplicantState',
			'coZip' => 'CoApplicantZip',
			'coEmployer' => 'CoApplicantEmployer',
			'coYrsEmployed' => 'CoApplicantYearsOnCurrentJob',
			'coMilRank' => '',
			'coEmployerAddress' => '',
			'coEmployerCityStateZip' => '',
			'coJobTitle' => 'CoApplicantPosition',
			'coMonthlyGrossPay' => 'CoApplicantMonthlyIncome',
			'coEmployerPhone' => 'CoApplicantEmployerPhone',
			'coEmployerExt' => '',
			'refLandlordName' => '',
			'refPropertyValue' => '',
			'refAccountNumber' => '',
			'refLandlordPhone' => '',
			'refLandlordAdress' => '',
			'refLandlordCity' => '',
			'refLandlordState' => '',
			'refLandlordZip' => '',
			'refLandlordMonthlyPayment' => '',
			'refLandlordPresBalance' => '',
			'credrefOneName' => '',
			'credrefOneAccount' => '',
			'credrefOnePhone' => '',
			'credrefOneStreetAdress' => '',
			'credrefOneCity' => '',
			'credrefOneState' => '',
			'credrefOneZip' => '',
			'credrefOneMonthlyPayment' => '',
			'credrefOneMonthlyPresBalance' => '',
			'credrefTwoName' => '',
			'credrefTwoAccount' => '',
			'credrefTwoPhone' => '',
			'credrefTwoStreetAdress' => '',
			'credrefTwoCity' => '',
			'credrefTwoState' => '',
			'credrefTwoZip' => '',
			'credrefTwoMonthlyPayment' => '',
			'credrefTwoMonthlyPresBalance' => '',
			'buyerCheckingAccountNum' => '',
			'buyerCheckingBank' => '',
			'buyerCheckingBankCity' => '',
			'buyerSavingsAccountNum' => '',
			'buyerSavingsBank' => '',
			'buyerSavingsBankCity' => '',
			'yearofBankruptcy' => '',
			'cityStateOfBankruptcy' => '',
			'personalRefOneName' => '',
			'personalRefOnePhone' => '',
			'personalRefOneRelationship' => '',
			'personalRefTwoName' => '',
			'personalRefTwoPhone' => '',
			'personalRefTwoRelationship' => '',
			'initials' => '',
			'buyerSign' => '',
			'buyerDate' => '',
			'coSign' => '',
			'coDate' => '',
			'monthsAtAddress' => '',
			'prevAddressTwo' => '',
			'prevYrsAtAddress' => '',
			'prevMonthsAtAddress' => '',
			'selfEmployed' => '',
			'monthsEmployed' => '',
			'prevMonthsEmployed' => '',
			'coRelation' => 'CoApplicantRelationship',
			'coSelfEmployed' => '',
			'coMonthsEmployed' => '',
			'residenceStatus' => '',
			'driversOrId' => '',
			'residenceType' => 'ApplicantResidenceType',
			'buyerCheckingAccount' => 'ApplicantHasOpenChecking',
			'buyerSavingsAccount' => '',
			'declaredBankruptcy' => 'ApplicantFiledForBankruptcy',
			'coHomePhone' => 'CoApplicantHomePhone',
			'coCellPhone' => 'CoApplicantCellPhone',
			'co_ssn' => 'CoApplicantSSN',
			'coDriversOrId' => '',
			'coEmail' => '',
			'refLandlordAddress' => '',
			'dealerId' => '',
			'lastStatus' => '',
			'status' => '',
			'amount_financed' => 'AmountRequested',
			'revolving' => '',
			'credit' => '',
			'buyerCreditCards' => 'ApplicantHasCreditCards',
			'buyerCreditReferences' => 'ApplicantHasCreditReference',
			'distributor_account' => '',
			'fca_id' => 'ApplicationNumber'
		];

		if( isset($appResult['OriginalApplication']) )
		{
			foreach ($mapping as $key => $value)
			{

				// check for birthdate fields
				if($key == 'birthdate' && isset($appResult['OriginalApplication']['ApplicantBirthDay']))
				{
					$application->birthdate = $appResult['OriginalApplication']['ApplicantBirthYear'] . '-' . $appResult['OriginalApplication']['ApplicantBirthMonth'] . '-' . $appResult['OriginalApplication']['ApplicantBirthDay'];
				}
				elseif($key == 'coBirthdate' && isset($appResult['OriginalApplication']['CoApplicantBirthDay']))
				{
					$application->coBirthdate = $appResult['OriginalApplication']['CoApplicantBirthYear'] . '-' . $appResult['OriginalApplication']['CoApplicantBirthMonth'] . '-' . $appResult['OriginalApplication']['CoApplicantBirthDay'];
				}
				elseif($key == 'ssn' && isset($appResult['OriginalApplication']['ApplicantSSN']))
				{
					// encrypt last 4 of ssn
					$application->ssn = substr($appResult['OriginalApplication']['ApplicantSSN'], -4);
				}
				elseif($key == 'co_ssn' && isset($appResult['OriginalApplication']['CoApplicantSSN']))
				{
					// encrypt last 4 of ssn
					$application->co_ssn = substr($appResult['OriginalApplication']['CoApplicantSSN'], -4);
				}
				elseif($key == 'distributor_account' && isset($appResult['DealerNumber']))
				{
					$application->distributor_account = $appResult['DealerNumber'];
				}
				elseif($value)
				{
					// optional read length value
					if( gettype( $value ) == "array" ) {

						$retValue = $appResult['OriginalApplication'][$value['value']];

						// if there's a length defined, truncate results
						if(isset($value['length'])) {
							echo 'cutting off length' . $value['length'];
							$retValue = substr($retValue, 0, $value['length']);
						}

						$application->{$key} = $retValue;
					} else {
						if( isset($appResult['OriginalApplication'][$value]) ) {
							// just translate the data straight in
							$application->{$key} = $appResult['OriginalApplication'][$value];
						}
					}
				}
			}
			$application->save();

		}
		else{
			// create mostly empty application from disposition
			if(isset($appResult['ResponseType']) && ($appResult['ResponseType'] == "1070" || $appResult['ResponseType'] == "1000"))
			{
				$application = new \App\Models\ContractApplication();
				$application->fca_id = isset($appResult['ApplicationNumber']) ? $appResult['ApplicationNumber'] : null;
				$application->distributor_account = isset($appResult['DealerNumber']) ? $appResult['DealerNumber'] : null;
				$application->firstName = isset($appResult['FirstName']) ? $appResult['FirstName'] : '';
				$application->lastName = isset($appResult['LastName']) ? $appResult['LastName'] : '';
			}
			else{
				return false;
			}
		}


		// fetch distributor by dealer number
		if( isset($appResult['DealerNumber']) && $appResult['DealerNumber'] ) {
			$distributor = \App\Models\DealerCompany::where('account_number', '=', $appResult['DealerNumber'])->first();

			$application->dist_id = $distributor->id;

			if(!$distributor) {

				// log to staff if we don't have a match
				Mail::send('emails.staff.error-no-distributor', ['appnumber' => $appResult['ApplicationNumber']], function ($m){
		            $m->to('loandept@belmontfinancellc.com')->subject('# ' . $appResult['ApplicationNumber'] .' (' . $appResult['FirstName'] . ' ' . $appResult['LastName'] . ') Error Occurred');
		        });

			}
		}

		$application->save();
		return $application;
	}

	private function fileChecks($path) {
		// check for only ascii character set
		if( mb_check_encoding(file_get_contents($path), 'ASCII') === false ) {
			throw new \Exception($path .' - invalid character set');
		}

		// required field positions
		$contents = file_get_contents($path);

		// array of the indexes of required fields by BFC
		$requiredFields = [
			1,2,4,5,8,9,10,11,12,13,14,
			19,21,22,23,43,44,51,65,67
		];

		$fields = explode(',', $contents);

		// check number of field array items
		if(count($fields) != 99) {
			throw new \Exception($path .' - Invalid number of fields');
		}
		foreach($requiredFields as $reqField) {
			$reqFieldIndex = $reqField - 1;

			if(strlen(str_replace('"', '', $fields[$reqFieldIndex])) == 0) {
				throw new \Exception($path .' - required field empty (' . $reqField . ')');
			}
		}
	}

}
