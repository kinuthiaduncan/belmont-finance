<?php namespace App\Console\Commands;

// hide deprecation warnings
error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);

use Illuminate\Console\Command;
use Laravel\LegacyEncrypter\McryptEncrypter;
use Crypt;

class UpdateLegacy extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'update-legacy';

	protected $encrypter; 

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'update some feature of app from legacy';


	public function __construct()
    {
        parent::__construct();

        $this->encrypter = new McryptEncrypter('i8h88xDvyrmLSEfLz2V0icXUgcLoOr73');


    }

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		\App\Models\AchPaymentRecurring::chunk('50', function($payments){
			foreach($payments as $pay) {
				$oldValue = $this->encrypter->decrypt($pay->account_number);
				$pay->account_number = Crypt::encrypt($oldValue);
				$pay->save();
			}
		});
		

		\App\Models\AchPayment::chunk('100', function($payments){
			foreach($payments as $pay) {
				$oldValue = $this->encrypter->decrypt($pay->account_number);
				$pay->account_number = Crypt::encrypt($oldValue);
				$pay->save();
			}
		});
		
	}

}
