<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearConsumer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apps:clearconsumer {firstName?} {lastName?} {identifier?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears Jonathan consumer applications and associated data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if($this->argument('firstName')){
            if(!$this->argument('lastName') || !$this->argument('identifier')) {
                die('also need last name and identifier to run this command');
            }

            $consumers = \App\Models\ContractApplication::where('firstName', '=', $this->argument('firstName'))
                ->where('lastName', '=', $this->argument('lastName'))
                ->where(function($query) {
                    $query->where('id', '=', $this->argument('identifier'))
                        ->orWhere('fca_id', '=', $this->argument('identifier'));
                })
                ->where('is_online', '=', '1')
                ->get();
        } else {
        //
            $consumers = \App\Models\ContractApplication::where('lastName', '=', 'Consumer')
                ->where('is_online', '=', '1')
                ->get();
        }

        foreach($consumers as $consumer) {
            if($consumer->contract) {
                if( $consumer->contract->customer ) {
                    $consumer->contract->customer->delete();
                }

                $consumer->contract->delete();
            }

            $files = \App\Models\File::where('app_id', '=', $consumer->id)->get();
            foreach($files as $file) {
                if( file_exists ( base_path($file->local_path) ) ) {
                    unlink(base_path($file->local_path));
                }
                
                $file->delete();
            }

            $consumer->delete();
        }
       
    }
}
