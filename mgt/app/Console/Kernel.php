<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\Watcher',
		'App\Console\Commands\ClearLogs',
		'App\Console\Commands\ClearConsumer',
		'App\Console\Commands\Imports\ImportDealerStates',
		'App\Console\Commands\Imports\ImportDealerInfo',
		'App\Console\Commands\Documents\FlattenPdf',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('logs:clear')->daily();
	}

}
